# About
 * The idea is to have S.T.A.L.K.E.R. like anomaly meter
 * Under hood is ESP32, which technically not only allow do sensor processing,
   but also it can detect BT/WiFi devices, which can act as "anomaly" which
   can be detected.
 * Project fairly working, but still not final version (as everything related
   to software)

# HW
## Version 00
 * **NOT tested**
 * Missing protective diode
 * Not recommended to build

## Version 01
 * **Tested**
 * Add protective diode
 * Add some extra graphics to silk layer

## Version 02
 * **NOT tested**
 * Under development, (maybe) not final version
 * Not recommended to build, but you might want to try :)


# FW
 * The "anomaly detector" is working. Basically it do passive scanning and
   search for specific MAC. When found, start beeping and showing signal
   strength graph
 * Service UI (via 2nd UART) allow to setup device "on the fly". This allow
   to add MAC of "anomaly".
 * Compass mode works, although it is required to do calibration in first place
 * Flashlight mode by using RGB LED
 * Acceleration mode. Basically useless for users, but just looks cool when
   moving

# Documentation for developer
 * 1) [Assembly instructions](doc/assembly.md)
 * 2) [Calibration](doc/calibration.md)
 * 3) [UI service](doc/ui_service.md)
 * 4) [MAC settings](doc/mac_list.md)

# Documentation for user
 * 1) [Getting started](doc/getting_started.md)
