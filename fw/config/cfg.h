/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration file for Anomaly meter
 *
 * Mainly here is HW configuration
 */
#ifndef __CFG_H__
#define __CFG_H__
// ===============================| Includes |================================
#include <driver/adc.h>
#include <esp_log.h>
// ================================| Defines |================================
// ==================================| I/O |==================================
/**
 * @brief I2C GPIO mapping
 *
 * @{
 */
///@brief I2C data
#define IO_I2C_SDA (21)
///@brief I2C clock
#define IO_I2C_SCL (22)

/**
 * @}
 */

/**
 * @brief LED mapping
 *
 * @{
 *
 * @brief Output for red LED
 */
#define IO_LED_R (27)

/**
 * @brief Output for green LED
 */
#define IO_LED_G (33)

/**
 * @brief Output for blue LED
 */
#define IO_LED_B (32)

/**
 * @}
 */

/**
 * @brief LCD pin mapping
 *
 * It is advised to use native SPI pins, so clock can be eventually higher
 *
 * @{
 */
#define IO_LCD_SPI_HOST (VSPI_HOST)
#define IO_LCD_DC (4)
#define IO_LCD_BCK_LGHT (2)
/**
 * @}
 */

/**
 * @brief Keyboard related stuff
 *
 * @{
 */
#define IO_KBRD_ADC1_CHANNEL (ADC_CHANNEL_6)

// Recommended scan time for keyboard operations for high level functions
#define KBRD_RECOMMENDED_SCAN_TIME_MS (100)
/**
 * @}
 */

/**
 * @brief Battery related stuff
 *
 * @{
 */
#define IO_BATT_ADC1_EXT_PWR (ADC_CHANNEL_0)
#define IO_BATT_ADC1_BATT (ADC_CHANNEL_3)
/**
 * @}
 */

/**
 * @brief Bit resolution for ADC1
 *
 * @note One ADC is used for multiple functionalities. 10 bits is easy to debug
 *       since raw value is close to mV value.
 */
#define ADC1_BIT_RESOLUTION (ADC_WIDTH_BIT_10)

/**
 * @brief Define which DAC will be used for buzzer
 *
 * DAC channel 1 is connected to pin 25, channel 2 to pin 26
 */
#define IO_BUZZER_DAC (DAC_CHANNEL_2)

/**
 * @brief Define which DAC will be used for gauge voltmeter
 *
 * DAC channel 1 is connected to pin 25, channel 2 to pin 26
 */
#define IO_GAUGE_VMETER (DAC_CHANNEL_1)
// ===============================| Advanced |================================
/**
 * @brief I2C clock speed in bits per second
 *
 * Standard speed is 100 kbps and it is highly recommended to stick with this
 * value. But, destiny is in your hands
 */
#define I2C_SCL_SPEED (100000)

/**
 * @brief Number of iterations when calibrating gyroscope
 *
 * It is pointless to collect tons of data, since at the end it will drift
 * away
 */
#define GYRO_CALIBRATION_ITERATIONS (1000)

/**
 * @brief When enabled, it shows stack usage
 */
#define SHOW_STACK_USAGE (0)
// =================================| RTOS |==================================
#define SENSORS_TASK_PRIORITY (tskIDLE_PRIORITY + 10)
#define PWR_LED_FADE_IN_TASK_PRIORITY (tskIDLE_PRIORITY + 2)
#define PWR_LED_NVS_TASK_PRIORITY (tskIDLE_PRIORITY + 1)
// ===============================| Power LED |===============================
/**
 * @brief PWM channel for red LED
 */
#define LED_R_PWM_CH (LEDC_CHANNEL_0)

/**
 * @brief PWM channel for green LED
 */
#define LED_G_PWM_CH (LEDC_CHANNEL_1)

/**
 * @brief PWM channel for blue LED
 */
#define LED_B_PWM_CH (LEDC_CHANNEL_2)

/**
 * @brief Timer for generating PWM for power LED
 */
#define LED_RGB_TIMER (LEDC_TIMER_0)
// ============================| Default values |=============================
/**
 * @brief Define function which returns ms (32 bit value) as time reference
 */
#define GET_CLK_MS_32BIT() esp_log_timestamp()
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
#endif  // __CFG_H__
