/**
 * @file
 * @author Martin Stejskal
 * @brief Main user function - mainly for creating tasks
 */
#include <esp_spi_flash.h>
#include <esp_system.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <sdkconfig.h>
#include <stdio.h>
//#include <driver/adc.h>
#include <esp_log.h>
#include <nvs_flash.h>

#include "cfg.h"
#include "ui.h"

// Only ESP32 is supported
#if !CONFIG_IDF_TARGET_ESP32
#error "Unsupported MCU"
#endif

/**
 * @brief Tag for logger
 */
static const char *MAIN_TAG = "main";

#if SHOW_STACK_USAGE
static const char *STACK_TAG = "Stack usage";
/**
 * @brief Task which periodically print stack usage
 * @param pvParameters Parameters - not used actually
 */
void showStackUsage(void *pvParameters) {
  UBaseType_t uNumOfTasks;
  TaskStatus_t *pxTaskStatusArray;
  uint32_t u32TotalRunTime;

  for (;;) {
    vTaskDelay(5000 / portTICK_PERIOD_MS);
    ESP_LOGI(STACK_TAG, "=================================");

    // Take a snapshot of the number of tasks in case it changes while this
    // function is executing.
    uNumOfTasks = uxTaskGetNumberOfTasks();
    // Allocate a TaskStatus_t structure for each task.  An array could be
    // allocated statically at compile time.
    pxTaskStatusArray = pvPortMalloc(uNumOfTasks * sizeof(TaskStatus_t));

    // Pointer should not be empty, but just in case check it
    if (pxTaskStatusArray) {
      uxTaskGetSystemState(pxTaskStatusArray, uNumOfTasks, &u32TotalRunTime);
      for (int iTskCnt = 0; iTskCnt < uNumOfTasks; iTskCnt++) {
        ESP_LOGI(STACK_TAG, "%s | Free Bytes: %d",
                 pxTaskStatusArray[iTskCnt].pcTaskName,
                 pxTaskStatusArray[iTskCnt].usStackHighWaterMark);
      }
    }

    // The array is no longer needed, free the memory it consumes.
    vPortFree(pxTaskStatusArray);
    ESP_LOGI(STACK_TAG, "=================================");
  }
}
#endif

void app_main(void) {
  // We need to run program from external flash -> initialize it
  esp_err_t ret = nvs_flash_init();
  if (ret == ESP_ERR_NVS_NO_FREE_PAGES ||
      ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    ESP_ERROR_CHECK(nvs_flash_erase());
    ret = nvs_flash_init();
  }
  ESP_ERROR_CHECK(ret);

  // esp_err_t eErrCode;

  ESP_LOGI(MAIN_TAG, "Creating tasks...\n");
  xTaskCreate(uiTaskRTOS, "ui", 4 * 1024, NULL, tskIDLE_PRIORITY, NULL);

#if SHOW_STACK_USAGE
  xTaskCreate(showStackUsage, "task usage", 2 * 1024, NULL, 4, NULL);
#endif

  ESP_LOGI(MAIN_TAG, "All tasks created\n");
}
