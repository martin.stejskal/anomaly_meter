# Notes
## Problem with debugging when enabling `O0` optimization
 * Can not use optimization `O0` for debug, because then simply bootloader
   get nuts and even changing partitions not help and getting:

```
rst:0x3 (SW_RESET),boot:0x13 (SPI_FAST_FLASH_BOOT)
configsip: 0, SPIWP:0xee
clk_drv:0x00,q_drv:0x00,d_drv:0x00,cs0_drv:0x00,hd_drv:0x00,wp_drv:0x00
mode:DIO, clock div:2
load:0x3fff0018,len:44
ho 0 tail 12 room 4
load:0x3fff0044,len:10284
load:0x40078000,len:21676
load:0x40080400,len:6832
entry 0x400806e4
E (94) bootloader_flash: bootloader_flash_read dest 0x3ffe3d75 not 4-byte aligned
E (95) boot: failed to load bootloader header!
ets Jun  8 2016 00:22:57
```

 * Partition table for reference:
```
# Espressif ESP32 Partition Table
# Name,   Type, SubType, Offset,  Size, Flags
nvs,      data, nvs,           ,  0x60000,
phy_init, data, phy,           ,  0x20000,
factory,  app,  factory,       , 2M,
```
