/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for gauge meter
 *
 * Expecting gauge meter with working range 0 ~ 3 V
 */
#ifndef __GAUGE_VMETER_H__
#define __GAUGE_VMETER_H__
// ===============================| Includes |================================
// Standard
#include <stdint.h>

// ESP specific
#include <esp_err.h>
// ================================| Defines |================================
/**
 * @brief Initial delay in ms when testing gauge
 */
#define GAUGE_INITIAL_DELAY_MS (700)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
/**
 * @brief Initialize DAC for gauge meter as task
 *
 * @note After initialization task close itself. Only task purpose it
 *       parallelism.
 * @return ESP_OK if no error
 */
void gaugeMeterTask(void *pvParameters);

/**
 * @brief Show RSSI value on gauge meter
 *
 * Function will do all necessary recalculations, so basically whole range will
 * be used.
 *
 * @param i8rssi RSSI value as signed integer
 * @return ESP_OK if no error
 */
esp_err_t gaugeMeterSetRssi(int8_t i8rssi);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Set DAC value to zero
 * @return ESP_OK if no error
 */
esp_err_t gaugeMeterOff(void);
// ==========================| Internal functions |===========================
#endif  // __GAUGE_VMETER_H__
