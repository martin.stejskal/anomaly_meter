/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for gauge meter
 *
 * Expecting gauge meter with working range 0 ~ 3 V
 */
// ===============================| Includes |================================
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ESP specific
#include <driver/dac.h>

// project specific modules
#include "err_handlers.h"
#include "gauge_vmeter.h"
#include "ui_modes_common.h"

// Project specific settings
#include "cfg.h"
// ================================| Defines |================================
/**
 * @brief Define maximum value for given analog gauge meter
 *
 * The 255 corresponds to 3.3 V -> need to cut at +3 V. We do not need
 * to shrink it, because in real life 255 means WIFI_MAX_RSSI, which
 * require to be next to the radio source -> it is OK to cut.
 * x = (255*3)/3.3 = 232
 */
#define DAC_MAX (232)

/**
 * @brief Number of steps for initial gauge test
 *
 * More steps makes better effect, but it may more "annoy" CPU. Value 30 is
 * enough for real use cases.
 */
#define NUM_OF_STEPS (30)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum { REQ_INTERNAL, REQ_EXTERNAL } teRequestType;
// ===========================| Global variables |============================
/**
 * @brief Keep track about aviability
 */
static uint8_t mu8ready = 0;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static esp_err_t _setDac(uint8_t u8value, const teRequestType eType);
// =========================| High level functions |==========================
void gaugeMeterTask(void *pvParameters) {
  // Reset ready flag
  mu8ready = false;

  esp_err_t eErrCode = ESP_FAIL;

  eErrCode = dac_output_enable(IO_GAUGE_VMETER);

  // Small test - go from minimum to maximum and back
  // Initial DAC value
  uint8_t u8dacValue = 0;

  eErrCode = _setDac(u8dacValue, REQ_INTERNAL);

  // Up
  for (uint8_t u8step = 0; u8step < NUM_OF_STEPS; u8step++) {
    u8dacValue += DAC_MAX / NUM_OF_STEPS;

    eErrCode |= _setDac(u8dacValue, REQ_INTERNAL);
    vTaskDelay(GAUGE_INITIAL_DELAY_MS / portTICK_PERIOD_MS / NUM_OF_STEPS);
  }

  // Down
  for (uint8_t u8step = 0; u8step < NUM_OF_STEPS; u8step++) {
    u8dacValue -= DAC_MAX / NUM_OF_STEPS;

    eErrCode |= _setDac(u8dacValue, REQ_INTERNAL);
    vTaskDelay(GAUGE_INITIAL_DELAY_MS / portTICK_PERIOD_MS / NUM_OF_STEPS);
  }

  // Set gauge to initial position
  eErrCode |= _setDac(u8dacValue, REQ_INTERNAL);

  // If no error, set ready flag
  if (!eErrCode) {
    mu8ready = 1;
  }

  // Terminate this task - not needed anymore
  vTaskDelete(NULL);
}

inline esp_err_t gaugeMeterSetRssi(int8_t i8rssi) {
  // Convert real RSSI to range 0~255
  return _setDac(ui_detector_wifi_rssi_to_u8(i8rssi), REQ_EXTERNAL);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
inline esp_err_t gaugeMeterOff(void) { return (_setDac(0, REQ_EXTERNAL)); }
// ==========================| Internal functions |===========================
static esp_err_t _setDac(uint8_t u8value, const teRequestType eType) {
  // For internal requests it is allow to bypass check below
  if (eType != REQ_INTERNAL) {
    // If not ready, return error
    if (!mu8ready) {
      return ESP_ERR_INVALID_STATE;
    }
  }

  if (u8value > DAC_MAX) {
    u8value = DAC_MAX;
  }
  return (dac_output_voltage(IO_GAUGE_VMETER, u8value));
}
