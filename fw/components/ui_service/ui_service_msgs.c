/**
 * @file
 * @author Martin Stejskal
 *
 * @brief Kind of language file
 *
 * Contains all main messages. It does NOT contain error messages
 */
// ===============================| Includes |================================
// ================================| Defines |================================

/**
 * @brief Set of commands which are used
 *
 * @{
 */
#define CMD_GET_MAC_LIST "get mac list"
#define CMD_SET_MAC "set mac "
#define CMD_DEL_MAC "del mac "
#define CMD_DEL_ALL_MAC "del mac all"
#define CMD_SET_PASSWD "passwd "
#define CMD_WIFI_SCAN "wifi scan"
#define CMD_WIFI_SCAN_ALL "wifi scan all"
#define CMD_GET_WIFI_CH "get wifi ch"
#define CMD_SET_WIFI_CH "set wifi ch "
#define CMD_GET_WIFI_SCAN_TYPE "get wifi scan type"
#define CMD_SET_WIFI_SCAN_TYPE "set wifi scan type "
#define CMD_GET_WIFI_SCAN_TIME "get wifi scan time"
#define CMD_SET_WIFI_SCAN_TIME "set wifi scan time "
#define CMD_CALIBRATE_KBRD "calibrate kbrd"
#define CMD_DEL_KBRD_CALIB "del kbrd calib"
#define CMD_CAL_BATT_ADC "calibrate bat adc"
#define CMD_CAL_BATT_GET_RAW "bat adc get calib raw"
#define CMD_CAL_BATT_SET_RAW "bat adc set calib raw "

#define CMD_GET_BAT_OFFSET "bat get static offset"
#define CMD_SET_BAT_OFFSET "bat set static offset "

#define CMD_CLR_BATT_STAT "clr bat stat"
#define CMD_DEL_COMPASS_CALIB "del compass calib"
#define CMD_GET_INIT_BEEP_MS "get beep period"
#define CMD_SET_INIT_BEEP_MS "set beep period "
#define CMD_GET_DEVICE_ID "get dev id"
#define CMD_DEFAULT "default"
#define CMD_REBOOT "reboot"
#define CMD_EXIT "exit"
/**
 * @}
 */

#define SET_PWR_SUPPLY(voltage) ("Set power supply to " voltage "V\n")
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================

// ===========================| Global variables |============================

static const char *pacHelp =
    "\n\n=== DAZ ====\n==<| Help |>==\n"
    " Here is list of available commands:\n"
    "   " CMD_GET_MAC_LIST
    " - Return list of registered WiFi MAC\n"

    "   " CMD_SET_MAC
    "11:22:33:44:55:66 [description] - register new WiFi MAC to list.\n"
    "                                  Optionally description can be add\n"

    "   " CMD_DEL_MAC
    "11:22:33:44:55:66 - remove WiFi MAC from list\n"

    "   " CMD_DEL_ALL_MAC
    " - Remove all registered MAC at once\n"

    "   " CMD_SET_PASSWD
    "newPassw@Rd - set new password\n"

    "   " CMD_WIFI_SCAN
    " - Do WiFi scan with current settings and show results\n"

    "   " CMD_WIFI_SCAN_ALL
    " - One shot scan on all channels\n"

    "   " CMD_GET_WIFI_CH
    " - Get current channel setting. 0 means all channels\n"

    "   " CMD_SET_WIFI_CH
    "1 - Set current channel to 1. 0 means all channels\n"

    "   " CMD_GET_WIFI_SCAN_TYPE
    " - Get current scan type (active/passive)\n"

    "   " CMD_SET_WIFI_SCAN_TYPE
    "passive - set passive scan method. Another option is: active\n"

    "   " CMD_GET_WIFI_SCAN_TIME
    " - Get scan time per channel in ms.\n"

    "   " CMD_SET_WIFI_SCAN_TIME
    "220  - Set scan time per channel. Value is in ms, range 0 ~ 1500\n"

    "   " CMD_CALIBRATE_KBRD
    " - Calibrate keyboard - all buttons\n"

    "   " CMD_DEL_KBRD_CALIB
    " - Remove keyboard calibration data. Factory values will be used\n"

    "   " CMD_CAL_BATT_ADC
    " - Calibrate ADC used for sensing battery voltage\n"

    "   " CMD_CAL_BATT_GET_RAW
    " - Get battery calibration data in raw format\n"

    "   " CMD_CAL_BATT_SET_RAW
    "100,223,342,453,567,674,734,892 - set battery calibration data directly\n"

    "   " CMD_GET_BAT_OFFSET
    " - Get current static battery offset in mV. This offset is applied to\n"
    "           final displayed value\n"

    "   " CMD_SET_BAT_OFFSET
    "60 - Set new static battery offset in mV. This offset is applied to\n"
    "           final displayed value\n"

    "   " CMD_CLR_BATT_STAT
    " - Clear all battery statistics\n"

    "   " CMD_DEL_COMPASS_CALIB
    " - Delete compass calibration data. Factory accuracy will be achieved\n"

    "   " CMD_GET_INIT_BEEP_MS
    " - current period of initial beep in ms\n"

    "   " CMD_SET_INIT_BEEP_MS
    "500 - set initial beep to 500 ms\n"

    "   " CMD_GET_DEVICE_ID
    " - Get device ID\n"

    "   " CMD_DEFAULT
    " - restore default values (factory settings)\n"

    "   " CMD_REBOOT
    " - reboot device immediately\n"

    "   " CMD_EXIT " - logout\n";

static const char *pacLogin = "\n=== DAZ ====\n== Login ==";
static const char *pacOK = "[OK]";
static const char *pacCancel = "[Cancel]";
static const char *pacFail = "[FAIL]";
static const char *pacInvalidPassword = "Incorrect password\n";
static const char *pacUnknownCmd = "Unknown command :/\n";
static const char *pacMacAlreadyInList = "MAC is already registered\n";
static const char *pacNoMacRegistered = "<No MAC registered yet>\n";
static const char *pacCanNotParseMac = "Can not parse MAC\n";
static const char *pacMacDescTooLong = "MAC description is too long\n";
static const char *pacCanNotWriteMac =
    "Can not write MAC."
    " All slots are full.\nPlease delete some unused MAC\n";
static const char *pacMacNotInList = "MAC not registered\n";
static const char *pacPwdTooLong = "Password too long\n";
static const char *pacPwdTooShort = "Password too short\n";
static const char *pacNvsWriteFail = "Can not save setting\n";
static const char *pacNvsReadFail =
    "Can not read from memory.\n"
    "This should not happen :(\n";
static const char *pacWiFiScanFail = "WiFi scan failed\n";
static const char *pacCanNotParseCh = "Can not parse channel number\n";
static const char *pacChOutOfRange = "Out of range. Expect 0 ~ 14\n";
static const char *pacScanTypeInvalid =
    "Invalid scan type."
    " Expecting \"active\" or \"passive\"\n";
static const char *pacCanNotParseTime =
    "Can not parse time value. "
    "Expecting value 0 ~ 1500\n";
static const char *pacInvalidTimeValue = "Invalid value. Expecting 0 ~ 1500\n";
static const char *pac_invalid_offset_mv_value =
    "Invalid value. Expecting range -127 ~ 127\n";
static const char *pacPressEnterWhenReady = "Press enter when ready";
static const char *pacCalibrating = "Calibrating...\n";
static const char *pacCalibDone = "Calibration done\n";
static const char *pacRlseAllBtns = "Release all buttons\n";
static const char *pacPrssEnter = "Press enter button on device\n";
static const char *pacPrssDown = "Press down button on device\n";
static const char *pacPrssUp = "Press up button on device\n";
static const char *pacPrssRight = "Press right button on device\n";
static const char *pacPrssLeft = "Press left button on device\n";
static const char *pacRestoreFactorySettings =
    "Restoring factory settings...\n";
static const char *pacCanNotParseValue = "Can not parse given number\n";
static const char *pacBeepPeriodOutOfRange = "Expected value 0 ~ 65535\n";
static const char *pacDoneReboot = "Done. Rebooting....\n\n\n";
static const char *pacLogOff = "Bye bye. See you next time ;)\n";
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
