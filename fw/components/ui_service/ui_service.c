/**
 * @file
 * @author Martin Stejskal
 * @brief UI service module
 *
 * Allow to configure device through service UART
 */
// ===============================| Includes |================================
#include <assert.h>
#include <string.h>

// Standard ESP32 libraries
#include <driver/uart.h>
#include <esp_err.h>
#include <esp_log.h>
#include <esp_system.h>

#include "ui_mode_compass.h"
#include "ui_modes_common.h"
#include "ui_service.h"
#include "ui_service_msgs.c"

// Work with non-volatile-storage
#include "nvs_settings.h"

// Work with MAC structure is easier thi that one
#include "mac.h"

// Access WiFi scan
#include "wifi.h"

// Calibration routines for sensors
#include "sensors.h"

// Keyboard calibration routines
#include "keyboard.h"

// Battery calibration routines
#include "battery.h"

// Initial buzzer beep period
#include "buzzer.h"

// Utilities
#include "str_int_conv.h"
// ================================| Defines |================================
#define _uiPrintStr(str) \
  uart_write_bytes(UI_SERVICE_UART_INTERFACE, str, strlen(str))

#define _uiPrintRaw(data, len) \
  uart_write_bytes(UI_SERVICE_UART_INTERFACE, data, len)

#define _LOADING_PERIOD_MS (200)

/**
 * @brief Shorten form for comparing "get" commands
 */
#define _CMD_GET(cmd_str) (strcmp(cmd_str, pacUserInputBuff) == 0)

/**
 * @brief Shorted form for comparing "set" commands
 */
#define _CMD_SET(cmd_str) \
  (memcmp(cmd_str, pacUserInputBuff, sizeof(cmd_str) - 1) == 0)

/**
 * @brief Returns address where parameter is stored
 */
#define _GET_PARAM_PTR(cmd_str) (&(pacUserInputBuff[sizeof(cmd_str) - 1]))

/**
 * @brief Wait for "enter" button. If pressed escape, call "return"
 */
#define _WAIT_FOR_ENTER_RET_WHEN_ESC()                 \
  if (_waitForEnterEsc(pacUserInputBuff) == KEY_ESC) { \
    _uiPrintStr(pacCancel);                            \
    return;                                            \
  }

#define _RET_IF_ERR_WRITE_FAIL(eErrCode) \
  if (eErrCode) {                        \
    _uiPrintStr(pacFail);                \
    return;                              \
  }

/**
 * @brief Identificator for NVS
 */
#define _PWD_ID "UiSrvcPwd"
#define _MAC_ID "Mac"
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  KEY_NONE = 0,  //!< KEY_NONE
  KEY_ENTER,     //!< KEY_ENTER
  KEY_ESC,       //!< KEY_ESC
  KEY_MAX,       //!< KEY_MAX
} teSpecialKey;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "UI service";
static uint8_t mu8uartDriverInstalled = 0;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| Process CMD functions |=========================
static void _cmdGetMacList(char *pacUserInputBuff,
                           teUiServiceState *peStateActual);
static void _cmdSetMac(char *pacUserInputBuff, teUiServiceState *peStateActual);
static void _cmdDelAllMac(char *pacUserInputBuff,
                          teUiServiceState *peStateActual);
static void _cmdDelMac(char *pacUserInputBuff, teUiServiceState *peStateActual);

static void _cmdWiFiScan(char *pacUserInputBuff,
                         teUiServiceState *peStateActual, bool bScanAll);

static void _cmdWiFiGetCh(char *pacUserInputBuff,
                          teUiServiceState *peStateActual);
static void _cmdWiFiSetCh(char *pacUserInputBuff,
                          teUiServiceState *peStateActual);

static void _cmdWiFiGetType(char *pacUserInputBuff,
                            teUiServiceState *peStateActual);
static void _cmdWiFiSetType(char *pacUserInputBuff,
                            teUiServiceState *peStateActual);

static void _cmdWiFiGetScanTime(char *pacUserInputBuff,
                                teUiServiceState *peStateActual);
static void _cmdWiFiSetScanTime(char *pacUserInputBuff,
                                teUiServiceState *peStateActual);

static void _cmdSetPwd(char *pacUserInputBuff, teUiServiceState *peStateActual);

static void _cmdCalibrateKbrd(char *pacUserInputBuff,
                              teUiServiceState *peStateActual);

static void _cmdDelKbrdCalib(char *pacUserInputBuff,
                             teUiServiceState *peStateActual);

static void _cmdCalibrateBatAdc(char *pacUserInputBuff,
                                teUiServiceState *peStateActual);

static void _cmdBatAdcGetRawCalibData(char *pacUserInputBuff,
                                      teUiServiceState *peStateActual);
static void _cmdBatAdcSetRawCalibData(char *pacUserInputBuff,
                                      teUiServiceState *peStateActual);

static void _cmd_get_bat_offset_mv(char *pacUserInputBuff,
                                   teUiServiceState *pe_state_actual);
static void _cmd_set_bat_offset_mv(char *pacUserInputBuff,
                                   teUiServiceState *pe_state_actual);

static void _cmdClrBattStat(char *pacUserInputBuff,
                            teUiServiceState *peStateActual);

static void _cmdDelCompassCalib(char *pacUserInputBuff,
                                teUiServiceState *peStateActual);

static void _cmdGetInitBeepMs(char *pacUserInputBuff,
                              teUiServiceState *peStateActual);
static void _cmdSetInitBeepMs(char *pacUserInputBuff,
                              teUiServiceState *peStateActual);

static void _cmd_get_device_id(char *pacUserInputBuff,
                               teUiServiceState *peStateActual);

static void _cmdDefaultSettings(char *pacUserInputBuff,
                                teUiServiceState *peStateActual);
// =======================| Other internal functions |========================

static esp_err_t _initUART(void);
static teSpecialKey _waitForUser(char *pacUserInputBuff,
                                 uint16_t *pu16userInputBuffWriteIdx,
                                 const char *pacInitialString);

static teSpecialKey _waitForEnterEsc(char *pacUserInputBuff);

static esp_err_t _appendBuffer(const char *pacSrc, const uint16_t u16SrcSize,
                               char *pacDst, uint16_t *pu16dstWriteIdx,
                               const uint16_t u16dstBuffSize);
static bool _findSpecialKey(const teSpecialKey eKey, const char *pacBuff,
                            const uint16_t u16maxIdx, uint16_t *pu16keyIdx);
static teNvsSttngsErr _delAllSettings(void);
static esp_err_t _printMac(uint8_t au8mac[static MAC_SIZE_BYTES]);
static esp_err_t _printHelp(void);
// =========================| High level functions |==========================
/**
 * @brief UI task for RTOS
 *
 * When RTOS system is used, this task suppose to run
 *
 * @param pvParameters Pointer to parameters
 */
void uiServiceTaskRTOS(void *pvParameters) {
  // User input buffer for complete user input
  static char acUserInputBuff[UI_SERVICE_RX_BUFFER_SIZE];
  // Write index for user input buffer
  static uint16_t u16userInputBuffWriteIdx;

  static teUiServiceState eStateActual = UI_SRVCE_WAIT_FOR_LOGIN;

  // Keep track about previous state. Simulate jump from "CMD" (exit command)
  static teUiServiceState eStatePrev = UI_SRVCE_PROCESS_CMD;

  ESP_ERROR_CHECK(_initUART());

  for (;;) {
    switch (eStateActual) {
      case UI_SRVCE_WAIT_FOR_LOGIN:
        uiServiceWaitForLogin(&eStatePrev, &eStateActual, acUserInputBuff,
                              &u16userInputBuffWriteIdx);
        break;
      case UI_SRVCE_WAIT_FOR_CMD:
        uiServiceWaitForCmd(&eStatePrev, &eStateActual, acUserInputBuff,
                            &u16userInputBuffWriteIdx);
        break;
      case UI_SRVCE_PROCESS_CMD:
        uiServiceProcessCmd(&eStatePrev, &eStateActual, acUserInputBuff,
                            &u16userInputBuffWriteIdx);
        break;
      default:
        ESP_LOGE(TAG, "Unknown state %d -> fallback to login", eStateActual);
        eStateActual = UI_SRVCE_WAIT_FOR_LOGIN;
    }
    vTaskDelay(UI_SERVICE_TASK_PERIOD_MS / portTICK_PERIOD_MS);
  }

  ESP_LOGE(TAG, "UI service task terminated");
}
// ========================| Middle level functions |=========================
void uiServiceWaitForLogin(teUiServiceState *peStatePrev,
                           teUiServiceState *peStateActual,
                           char *pacUserInputBuff,
                           uint16_t *pu16userInputBuffWriteIdx) {
  // Pointers should not be empty
  assert(peStatePrev);
  assert(peStateActual);
  assert(pacUserInputBuff);

  assert(*peStateActual == UI_SRVCE_WAIT_FOR_LOGIN);

  teSpecialKey eKey;

  if (*peStatePrev != *peStateActual) {
    // Show login
    _uiPrintStr(pacLogin);
  }
  *peStatePrev = *peStateActual;

  eKey =
      _waitForUser(pacUserInputBuff, pu16userInputBuffWriteIdx, "\npassword: ");
  if (eKey == KEY_ENTER) {
    char acPwdInNvm[UI_SERVICE_NVS_PASSWORD_MAX_LEN];
    // Compare with password

    if (nvsSettingsGet(_PWD_ID, acPwdInNvm, UI_SERVICE_DEFAULT_PASSWORD,
                       UI_SERVICE_NVS_PASSWORD_MAX_LEN)) {
      // Error when loading password from memory. This is core
      // functionality -> try restart chip. Maybe this is because of
      // some voltage drop
      char acErrMsg[] = "Can not load password from NVM";
      _uiPrintStr(acErrMsg);
      ESP_LOGE(TAG, "%s", acErrMsg);
      assert(0);
      esp_restart();
    }

    // Password is loaded, compare it
    if (strcmp(acPwdInNvm, pacUserInputBuff) == 0) {
      // Login successful - ready to receive commands
      _uiPrintStr(pacOK);

      // Send request to UI to switch to "Service mode" too (disable
      // all tasks)
      requestUiEnableServiceMode();
      vTaskDelay(_LOADING_PERIOD_MS / portTICK_PERIOD_MS);

      // Wait until processing is done
      while (!isUiInServiceMode()) {
        vTaskDelay(_LOADING_PERIOD_MS / portTICK_PERIOD_MS);
        _uiPrintStr(".");
      }
      _uiPrintStr("\n");

      *peStateActual = UI_SRVCE_WAIT_FOR_CMD;
    } else if (strcmp(UI_SERVICE_RESTORE_FACTORY_SETTINGS, pacUserInputBuff) ==
               0) {
      // Restore factory settings
      _uiPrintStr(pacRestoreFactorySettings);

      // No mercy. Do not care about error code
      ESP_LOGI(TAG, "restore factory settings error code: %d",
               _delAllSettings());

      _uiPrintStr(pacDoneReboot);
      // Finish him!
      esp_restart();
    } else {
      // Wrong password
      _uiPrintStr(pacInvalidPassword);
      _uiPrintStr(pacLogin);
    }
  } else if (eKey == KEY_ESC) {
    // Show login again
    _uiPrintStr(pacLogin);
  } else {
    // do nothing, keep collecting data
  }
}

void uiServiceWaitForCmd(teUiServiceState *peStatePrev,
                         teUiServiceState *peStateActual,
                         char *pacUserInputBuff,
                         uint16_t *pu16userInputBuffWriteIdx) {
  // Pointers should not be empty
  assert(peStatePrev);
  assert(peStateActual);
  assert(pacUserInputBuff);
  assert(pu16userInputBuffWriteIdx);

  assert(*peStateActual == UI_SRVCE_WAIT_FOR_CMD);

  teSpecialKey eKey;

  if ((*peStatePrev != *peStateActual) &&
      (*peStatePrev == UI_SRVCE_WAIT_FOR_LOGIN)) {
    _printHelp();
  }
  *peStatePrev = *peStateActual;

  eKey = _waitForUser(pacUserInputBuff, pu16userInputBuffWriteIdx, "\n$ ");
  if (eKey == KEY_ENTER) {
    // Process data
    *peStateActual = UI_SRVCE_PROCESS_CMD;
  } else {
    // do nothing. Function "_waitForUser" will print "$" sign again
  }
}

void uiServiceProcessCmd(teUiServiceState *peStatePrev,
                         teUiServiceState *peStateActual,
                         char *pacUserInputBuff,
                         uint16_t *pu16userInputBuffWriteIdx) {
  // Pointers should not be empty
  assert(peStatePrev);
  assert(peStateActual);
  assert(pacUserInputBuff);
  assert(pu16userInputBuffWriteIdx);

  assert(*peStateActual == UI_SRVCE_PROCESS_CMD);

  ESP_LOGI(TAG, "Processing command: %s", pacUserInputBuff);

  *peStatePrev = *peStateActual;

  // Wait for another command - default behavior
  *peStateActual = UI_SRVCE_WAIT_FOR_CMD;

  if (_CMD_GET(CMD_GET_MAC_LIST)) {
    // Get list of MAC
    _cmdGetMacList(pacUserInputBuff, peStateActual);

  } else if (_CMD_SET(CMD_SET_MAC)) {
    // Set new MAC
    _cmdSetMac(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_DEL_ALL_MAC)) {
    // This command have to be before CMD_DEL_MAC, since command are quite
    // similar and we need to check for match this first to avoid processing
    // it as "bad MAC"
    _cmdDelAllMac(pacUserInputBuff, peStateActual);

  } else if (_CMD_SET(CMD_DEL_MAC)) {
    _cmdDelMac(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_WIFI_SCAN)) {
    // Do not scan all channels (false)
    _cmdWiFiScan(pacUserInputBuff, peStateActual, false);

  } else if (_CMD_GET(CMD_WIFI_SCAN_ALL)) {
    // Scan all channels
    _cmdWiFiScan(pacUserInputBuff, peStateActual, true);

  } else if (_CMD_GET(CMD_GET_WIFI_CH)) {
    _cmdWiFiGetCh(pacUserInputBuff, peStateActual);

  } else if (_CMD_SET(CMD_SET_WIFI_CH)) {
    _cmdWiFiSetCh(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_GET_WIFI_SCAN_TYPE)) {
    _cmdWiFiGetType(pacUserInputBuff, peStateActual);

  } else if (_CMD_SET(CMD_SET_WIFI_SCAN_TYPE)) {
    _cmdWiFiSetType(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_GET_WIFI_SCAN_TIME)) {
    _cmdWiFiGetScanTime(pacUserInputBuff, peStateActual);

  } else if (_CMD_SET(CMD_SET_WIFI_SCAN_TIME)) {
    _cmdWiFiSetScanTime(pacUserInputBuff, peStateActual);

  } else if (_CMD_SET(CMD_SET_PASSWD)) {
    _cmdSetPwd(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_CALIBRATE_KBRD)) {
    _cmdCalibrateKbrd(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_DEL_KBRD_CALIB)) {
    _cmdDelKbrdCalib(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_CAL_BATT_ADC)) {
    _cmdCalibrateBatAdc(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_CAL_BATT_GET_RAW)) {
    _cmdBatAdcGetRawCalibData(pacUserInputBuff, peStateActual);

  } else if (_CMD_SET(CMD_CAL_BATT_SET_RAW)) {
    _cmdBatAdcSetRawCalibData(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_GET_BAT_OFFSET)) {
    _cmd_get_bat_offset_mv(pacUserInputBuff, peStateActual);

  } else if (_CMD_SET(CMD_SET_BAT_OFFSET)) {
    _cmd_set_bat_offset_mv(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_CLR_BATT_STAT)) {
    _cmdClrBattStat(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_DEL_COMPASS_CALIB)) {
    _cmdDelCompassCalib(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_GET_INIT_BEEP_MS)) {
    _cmdGetInitBeepMs(pacUserInputBuff, peStateActual);

  } else if (_CMD_SET(CMD_SET_INIT_BEEP_MS)) {
    _cmdSetInitBeepMs(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_GET_DEVICE_ID)) {
    _cmd_get_device_id(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_DEFAULT)) {
    _cmdDefaultSettings(pacUserInputBuff, peStateActual);

  } else if (_CMD_GET(CMD_REBOOT)) {
    // Easy one
    esp_restart();

  } else if (_CMD_GET(CMD_EXIT)) {
    _uiPrintStr(pacLogOff);
    // Send request to UI that now it is safe to go back to normal mode
    requestUiDisableServiceMode();
    vTaskDelay(_LOADING_PERIOD_MS / portTICK_PERIOD_MS);

    while (isUiInServiceMode()) {
      vTaskDelay(_LOADING_PERIOD_MS / portTICK_PERIOD_MS);
      _uiPrintStr(".");
    }

    *peStateActual = UI_SRVCE_WAIT_FOR_LOGIN;
  } else {
    _uiPrintStr(pacUnknownCmd);
    _printHelp();
  }
}
// ==========================| Low level functions |==========================
esp_err_t uiServiceGetMacList(tsUiServiceMacList *psMacList) {
  // Create dummy default values. This time default values are quite
  // deterministic, so no need to pass it from user space
  tsUiServiceMacList sDefaultMacList;

  memset(sDefaultMacList.au8macAddr, 0xFF, sizeof(sDefaultMacList.au8macAddr));
  memset(sDefaultMacList.acMacDescription, 0,
         sizeof(sDefaultMacList.acMacDescription));

  return nvsSettingsGet(_MAC_ID, psMacList, &sDefaultMacList,
                        sizeof(tsUiServiceMacList));
}
// ==========================| Internal functions |===========================
//================================| Commands |=================================
static void _cmdGetMacList(char *pacUserInputBuff,
                           teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  tsUiServiceMacList sMacList;
  bool bListEmpty = true;

  // No need to process user input more. Command is correct
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // Try to read data from NVS. This should not fail, but if so, handle it
  if (uiServiceGetMacList(&sMacList)) {
    _uiPrintStr(pacNvsReadFail);
    _uiPrintStr(pacFail);
    return;
  }

  for (uint8_t u8idx = 0; u8idx < UI_SERVICE_MAX_MAC_STORED; u8idx++) {
    // Print only non-empty MAC
    if (!macIsEmpty(sMacList.au8macAddr[u8idx])) {
      bListEmpty = false;
      _printMac(sMacList.au8macAddr[u8idx]);

      // Print also description
      _uiPrintStr(" ");
      _uiPrintStr(sMacList.acMacDescription[u8idx]);
      _uiPrintStr("\n");
    }
  }

  if (bListEmpty) {
    _uiPrintStr(pacNoMacRegistered);
  }
}
static void _cmdSetMac(char *pacUserInputBuff,
                       teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  tsUiServiceMacList sMacList;

  // Try to read data from NVS. This should not fail, but if so, handle it
  if (uiServiceGetMacList(&sMacList)) {
    _uiPrintStr(pacNvsReadFail);
    return;
  }

  // Check if MAC is valid. Find start of that string
  char *pacMac = _GET_PARAM_PTR(CMD_SET_MAC);
  // After MAC there should be space and optional description.
  // Expected format: xx:xx:xx:xx:xx:xx -> 17 chars
  char *pacDescription = pacMac + (MAC_SIZE_BYTES * 2 + 5);

  uint8_t au8newMac[MAC_SIZE_BYTES];

  // Will be used later. Need to check if free slot is available
  bool bFoundFreeSlot = false;
  uint8_t u8slotIdx;

  esp_err_t eErrCode = macStrToBin(pacMac, au8newMac);
  if (eErrCode) {
    _uiPrintStr(pacCanNotParseMac);
    _uiPrintStr(pacFail);
    return;
  }

  // Check if MAC is not already in list
  for (u8slotIdx = 0; u8slotIdx < UI_SERVICE_MAX_MAC_STORED; u8slotIdx++) {
    if (memcmp(au8newMac, sMacList.au8macAddr[u8slotIdx], sizeof(au8newMac)) ==
        0) {
      // Already in list
      _uiPrintStr(pacMacAlreadyInList);
      _uiPrintStr(pacFail);
      return;
    }
  }

  // Otherwise it is not in list yet. Find free slot
  for (u8slotIdx = 0; u8slotIdx < UI_SERVICE_MAX_MAC_STORED; u8slotIdx++) {
    if (macIsEmpty(sMacList.au8macAddr[u8slotIdx])) {
      bFoundFreeSlot = true;
      break;
    }
  }

  if (bFoundFreeSlot) {
    // Write given MAC there
    memcpy(sMacList.au8macAddr[u8slotIdx], au8newMac, sizeof(au8newMac));

    // If there is space after MAC, let's store it as description
    if (*pacDescription == ' ') {
      // There is something. Start processing data after description
      pacDescription += 1;

      if (strlen(pacDescription) > UI_SERVICE_MAC_DESCRIPTION_MAX_LENGTH) {
        _uiPrintStr(pacMacDescTooLong);
        _uiPrintStr(pacFail);
        return;
      }

      // Else length is fine -> copy description including NULL
      memcpy(sMacList.acMacDescription[u8slotIdx], pacDescription,
             strlen(pacDescription) + 1);
    }

    if (nvsSettingsSet(_MAC_ID, &sMacList, sizeof(sMacList))) {
      _uiPrintStr(pacCanNotWriteMac);
      _uiPrintStr(pacFail);
    } else {
      _uiPrintStr(pacOK);
    }
  } else {
    _uiPrintStr(pacCanNotWriteMac);
    _uiPrintStr(pacFail);
  }
}

static void _cmdDelAllMac(char *pacUserInputBuff,
                          teUiServiceState *peStateActual) {
  if (nvsSettingsDel(_MAC_ID)) {
    _uiPrintStr(pacFail);
  } else {
    _uiPrintStr(pacOK);
  }
}

static void _cmdDelMac(char *pacUserInputBuff,
                       teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  tsUiServiceMacList sMacList;

  // Try to read data from NVS. This should not fail, but if so, handle it
  if (uiServiceGetMacList(&sMacList)) {
    _uiPrintStr(pacNvsReadFail);
    _uiPrintStr(pacFail);
    return;
  }

  // Check if MAC is valid. Find start of that string
  char *pacMac = &(pacUserInputBuff[sizeof(CMD_DEL_MAC) - 1]);
  uint8_t au8deleteMac[MAC_SIZE_BYTES];

  esp_err_t eErrCode = macStrToBin(pacMac, au8deleteMac);
  if (eErrCode) {
    _uiPrintStr(pacCanNotParseMac);
    _uiPrintStr(pacFail);
    return;
  }

  // Check if MAC is not already in list
  for (uint8_t u8slotIdx = 0; u8slotIdx < UI_SERVICE_MAX_MAC_STORED;
       u8slotIdx++) {
    if (memcmp(au8deleteMac, sMacList.au8macAddr[u8slotIdx],
               sizeof(au8deleteMac)) == 0) {
      // In list -> erase (FF:FF:FF:FF:FF:FF means empty MAC)
      memset(sMacList.au8macAddr[u8slotIdx], 0xFF,
             sizeof(sMacList.au8macAddr[u8slotIdx]));

      // Delete description (set NULL character)
      sMacList.acMacDescription[u8slotIdx][0] = '\0';

      // Write it back
      if (nvsSettingsSet(_MAC_ID, &sMacList, sizeof(sMacList))) {
        _uiPrintStr(pacCanNotWriteMac);
        _uiPrintStr(pacFail);
      } else {
        _uiPrintStr(pacOK);
      }
      return;
    }
  }
  // Otherwise MAC not found -> tell user
  _uiPrintStr(pacMacNotInList);
  _uiPrintStr(pacFail);
}

static void _cmdWiFiScan(char *pacUserInputBuff,
                         teUiServiceState *peStateActual, bool bScanAll) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  uint16_t u16apCount = 0;
  wifi_ap_record_t asApInfo[WIFI_SCAN_LIST_SIZE];

  // Print OK
  _uiPrintStr(pacOK);

  if (wifiInit()) {
    _uiPrintStr(pacWiFiScanFail);
    _uiPrintStr(pacFail);
    return;
  }

  // Load configuration
  tsWiFiScanCfg sWiFiCfg;

  if (bScanAll) {
    // Scan for all channels - use temporary values

    // Use active scan type - more certain answer
    sWiFiCfg.eScan_type = WIFI_SCAN_TYPE_ACTIVE;

    // Zero means "all channels"
    sWiFiCfg.u8channel = 0;

    // Use default ESP value (they know well)
    sWiFiCfg.u32maxScanMs = 0;
  } else {
    // Load from NVS
    if (wifiGetScanCfgNvs(&sWiFiCfg)) {
      _uiPrintStr(pacNvsReadFail);
      _uiPrintStr(pacFail);
      return;
    }
  }

  // Show WiFi settings. Basically 3 parameters - limit it to 40 characters
  // per line -> should be fine
  char acWiFiSettings[3 * 40];
  char acScanType[8] = "active";

  switch (sWiFiCfg.eScan_type) {
    case WIFI_SCAN_TYPE_ACTIVE:
      // Nothing to do - already set to "active"
      break;
    case WIFI_SCAN_TYPE_PASSIVE:
      strcpy(acScanType, "passive");
      break;
  }

  sprintf(acWiFiSettings,
          "\nScan type: %s\n"
          "Channel: %u\n"
          "Scan time per channel: %u ms\n\n",
          acScanType, sWiFiCfg.u8channel, sWiFiCfg.u32maxScanMs);
  _uiPrintStr(acWiFiSettings);

  if (wifiScan(&u16apCount, asApInfo, &sWiFiCfg)) {
    _uiPrintStr(pacWiFiScanFail);
    _uiPrintStr(pacFail);
    return;
  }

  if (wifiDeInit()) {
    ESP_LOGW(TAG, "WiFi deinitialization failed");
  }

  // Expect to print 4 lines. Let's assume that line length will not exceed
  // 80 characters -> 80*4
  char acTmp[80 * 4];

  for (uint16_t u16apIdx = 0;
       (u16apIdx < WIFI_SCAN_LIST_SIZE) && (u16apIdx < u16apCount);
       u16apIdx++) {
    sprintf(acTmp, "\nSSID: %s\nBSSID: %s\nRSSI: %d\nChannel: %d\n",
            asApInfo[u16apIdx].ssid, macBinToStr(asApInfo[u16apIdx].bssid),
            asApInfo[u16apIdx].rssi, asApInfo[u16apIdx].primary);
    _uiPrintStr(acTmp);
  }
}

static void _cmdWiFiGetCh(char *pacUserInputBuff,
                          teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  tsWiFiScanCfg sWiFiCfg;
  if (wifiGetScanCfgNvs(&sWiFiCfg)) {
    _uiPrintStr(pacNvsReadFail);
    _uiPrintStr(pacFail);
    return;
  }

  // In this phase everything should be good
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // ESP support only 2.4 GHz -> 14 channels -> 2 digits. But Compiler would
  // complain, because technically it is possible that value might be 3
  // digits long -> 3x characters
  char acChStr[] = "???";
  sprintf(acChStr, "%u", sWiFiCfg.u8channel);

  _uiPrintStr(acChStr);
}

static void _cmdWiFiSetCh(char *pacUserInputBuff,
                          teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  // Get pointer to value
  char *pacCh = _GET_PARAM_PTR(CMD_SET_WIFI_CH);

  // Temporary storage
  uint32_t u32ch;

  esp_err_t eErrCode = strToUint32(pacCh, &u32ch);

  // WiFi setting structure
  tsWiFiScanCfg sWiFiCfg;

  // If something went wrong, write error
  if (eErrCode) {
    _uiPrintStr(pacCanNotParseCh);
    _uiPrintStr(pacFail);
    return;
  }

  if (u32ch > 14) {
    _uiPrintStr(pacChOutOfRange);
    _uiPrintStr(pacFail);
    return;
  }

  // Value seems to be OK, write it it NVS -> need to read out current values
  if (wifiGetScanCfgNvs(&sWiFiCfg)) {
    _uiPrintStr(pacNvsReadFail);
    _uiPrintStr(pacFail);
    return;
  }

  // Change channel
  sWiFiCfg.u8channel = (uint8_t)u32ch;

  if (wifiSetScanCfgNvs(&sWiFiCfg)) {
    _uiPrintStr(pacNvsWriteFail);
    _uiPrintStr(pacFail);
    return;
  }

  // Everything is cool
  _uiPrintStr(pacOK);
}

static void _cmdWiFiGetType(char *pacUserInputBuff,
                            teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  tsWiFiScanCfg sWiFiCfg;
  if (wifiGetScanCfgNvs(&sWiFiCfg)) {
    _uiPrintStr(pacNvsReadFail);
    _uiPrintStr(pacFail);
    return;
  }

  // In this phase everything should be good
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  switch (sWiFiCfg.eScan_type) {
    case WIFI_SCAN_TYPE_ACTIVE:
      _uiPrintStr("active");
      break;
    case WIFI_SCAN_TYPE_PASSIVE:
      _uiPrintStr("passive");
      break;
  }
}
static void _cmdWiFiSetType(char *pacUserInputBuff,
                            teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  tsWiFiScanCfg sWiFiCfg;
  if (wifiGetScanCfgNvs(&sWiFiCfg)) {
    _uiPrintStr(pacNvsReadFail);
    _uiPrintStr(pacFail);
    return;
  }

  // Get pointer to parameter itself
  char *pacType = _GET_PARAM_PTR(CMD_SET_WIFI_SCAN_TYPE);

  if (strcmp(pacType, "active") == 0) {
    sWiFiCfg.eScan_type = WIFI_SCAN_TYPE_ACTIVE;
  } else if (strcmp(pacType, "passive") == 0) {
    sWiFiCfg.eScan_type = WIFI_SCAN_TYPE_PASSIVE;
  } else {
    // Unknown parameter
    _uiPrintStr(pacScanTypeInvalid);
    _uiPrintStr(pacFail);
    return;
  }

  // Save settings
  if (wifiSetScanCfgNvs(&sWiFiCfg)) {
    _uiPrintStr(pacNvsWriteFail);
    _uiPrintStr(pacFail);
    return;
  }

  // Everything is cool
  _uiPrintStr(pacOK);
}

static void _cmdWiFiGetScanTime(char *pacUserInputBuff,
                                teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  tsWiFiScanCfg sWiFiCfg;
  if (wifiGetScanCfgNvs(&sWiFiCfg)) {
    _uiPrintStr(pacNvsReadFail);
    _uiPrintStr(pacFail);
    return;
  }

  // In this phase everything should be good
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // The 32 bit value can have max 10 digits, plus some units. Eh, 20 Bytes
  char acTime[20];

  sprintf(acTime, "%u ms", sWiFiCfg.u32maxScanMs);
  _uiPrintStr(acTime);
}
static void _cmdWiFiSetScanTime(char *pacUserInputBuff,
                                teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  tsWiFiScanCfg sWiFiCfg;
  if (wifiGetScanCfgNvs(&sWiFiCfg)) {
    _uiPrintStr(pacNvsReadFail);
    _uiPrintStr(pacFail);
    return;
  }

  // Get pointer to parameter itself
  char *pacTime = _GET_PARAM_PTR(CMD_SET_WIFI_SCAN_TIME);
  uint32_t u32newTimeMs;

  // If conversion fails
  if (strToUint32(pacTime, &u32newTimeMs)) {
    _uiPrintStr(pacCanNotParseTime);
    _uiPrintStr(pacFail);
    return;
  }

  // Check range
  if (u32newTimeMs > 1500) {
    _uiPrintStr(pacInvalidTimeValue);
    _uiPrintStr(pacFail);
    return;
  }

  // So far so good, store values
  sWiFiCfg.u32maxScanMs = u32newTimeMs;

  if (wifiSetScanCfgNvs(&sWiFiCfg)) {
    _uiPrintStr(pacNvsWriteFail);
    _uiPrintStr(pacFail);
    return;
  }

  // Everything is cool
  _uiPrintStr(pacOK);
}

static void _cmdSetPwd(char *pacUserInputBuff,
                       teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  // Password starts after command
  char *pacPwd = _GET_PARAM_PTR(CMD_SET_PASSWD);
  size_t iPwdLen = strlen(pacPwd);

  // New password
  if (iPwdLen > UI_SERVICE_NVS_PASSWORD_MAX_LEN) {
    _uiPrintStr(pacPwdTooLong);
    _uiPrintStr(pacFail);
  } else if (iPwdLen < 4) {
    _uiPrintStr(pacPwdTooShort);
    _uiPrintStr(pacFail);
  } else {
    // Try to save it
    if (nvsSettingsSet(_PWD_ID, pacPwd, UI_SERVICE_NVS_PASSWORD_MAX_LEN)) {
      _uiPrintStr(pacNvsWriteFail);
    } else {
      _uiPrintStr(pacOK);
    }
  }
}

static void _cmdCalibrateKbrd(char *pacUserInputBuff,
                              teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  // ===========================| No button pressed |=======================
  _uiPrintStr(pacRlseAllBtns);
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(kbrdCalibrateButton(KBRD_BTN_NONE));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| Enter |=============================
  _uiPrintStr(pacPrssEnter);
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(kbrdCalibrateButton(KBRD_BTN_ENTER));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| Down |==============================
  _uiPrintStr(pacRlseAllBtns);
  _uiPrintStr(pacPrssDown);
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(kbrdCalibrateButton(KBRD_BTN_DOWN));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // ==================================| Up |===============================
  _uiPrintStr(pacRlseAllBtns);
  _uiPrintStr(pacPrssUp);
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(kbrdCalibrateButton(KBRD_BTN_UP));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| Right |=============================
  _uiPrintStr(pacRlseAllBtns);
  _uiPrintStr(pacPrssRight);
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(kbrdCalibrateButton(KBRD_BTN_RIGHT));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| Left |==============================
  _uiPrintStr(pacRlseAllBtns);
  _uiPrintStr(pacPrssLeft);
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(kbrdCalibrateButton(KBRD_BTN_LEFT));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  _uiPrintStr(pacCalibDone);
}

static void _cmdDelKbrdCalib(char *pacUserInputBuff,
                             teUiServiceState *peStateActual) {
  if (kbrdDeleteCalibration()) {
    _uiPrintStr(pacFail);
  } else {
    _uiPrintStr(pacOK);
  }
}

static void _cmdCalibrateBatAdc(char *pacUserInputBuff,
                                teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);
  // =================================| 3.6 V |===============================
  _uiPrintStr(SET_PWR_SUPPLY("3.6"));
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(batteryCalibrateAdc(BATTERY_CALIB_3_6V));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| 3.7 V |===============================
  _uiPrintStr(SET_PWR_SUPPLY("3.7"));
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(batteryCalibrateAdc(BATTERY_CALIB_3_7V));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| 3.8 V |===============================
  _uiPrintStr(SET_PWR_SUPPLY("3.8"));
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(batteryCalibrateAdc(BATTERY_CALIB_3_8V));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| 3.9 V |===============================
  _uiPrintStr(SET_PWR_SUPPLY("3.9"));
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(batteryCalibrateAdc(BATTERY_CALIB_3_9V));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| 4.0 V |===============================
  _uiPrintStr(SET_PWR_SUPPLY("4.0"));
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(batteryCalibrateAdc(BATTERY_CALIB_4_0V));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| 4.1 V |===============================
  _uiPrintStr(SET_PWR_SUPPLY("4.1"));
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(batteryCalibrateAdc(BATTERY_CALIB_4_1V));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| 4.2 V |===============================
  _uiPrintStr(SET_PWR_SUPPLY("4.2"));
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(batteryCalibrateAdc(BATTERY_CALIB_4_2V));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // =================================| 4.3 V |===============================
  _uiPrintStr(SET_PWR_SUPPLY("4.3"));
  _WAIT_FOR_ENTER_RET_WHEN_ESC();

  _uiPrintStr(pacCalibrating);

  _RET_IF_ERR_WRITE_FAIL(batteryCalibrateAdc(BATTERY_CALIB_4_3V));
  _uiPrintStr(pacOK);
  _uiPrintStr("\n");
}

static void _cmdBatAdcGetRawCalibData(char *pacUserInputBuff,
                                      teUiServiceState *peStateActual) {
  uint16_t au16calibData[BATTERY_CALIB_MAX];

  batteryGetCalibrationRaw(au16calibData);

  _uiPrintStr(pacOK);
  _uiPrintStr("\n");

  // Every value is 16 bit long -> 5 characters, plus comma, plus NULL -> 7.
  // Better to allocate bit more - we have plenty of RAM here
  char acOutput[10];

  for (uint8_t u8idx = 0; u8idx < BATTERY_CALIB_MAX; u8idx++) {
    sprintf(acOutput, "%d", au16calibData[u8idx]);
    _uiPrintStr(acOutput);

    // If this is not last item, add comma
    if (u8idx != (BATTERY_CALIB_MAX - 1)) {
      _uiPrintStr(",");
    } else {
      // Last item -> extra new line
      _uiPrintStr("\n");
    }
  }
}

static void _cmdBatAdcSetRawCalibData(char *pacUserInputBuff,
                                      teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  uint16_t au16calibData[BATTERY_CALIB_MAX];

  // Temporary storage
  uint32_t u32tmp;

  // Keep error codes
  esp_err_t eErrCode = ESP_FAIL;

  // Pointer to values
  char *pacData = _GET_PARAM_PTR(CMD_CAL_BATT_SET_RAW);

  for (uint8_t u8idx = 0; u8idx < BATTERY_CALIB_MAX; u8idx++) {
    eErrCode = strToUint32(pacData, &u32tmp);

    // If there is problem with conversion
    if (eErrCode) {
      break;
    }

    // If value does not fit into 16 bit value
    if (u32tmp > 0xFFFF) {
      eErrCode = ESP_ERR_INVALID_ARG;
      break;
    }

    // Else conversion was successful -> store value
    au16calibData[u8idx] = (uint16_t)u32tmp;

    // Move pointer to next value (if not last iteration) -> find another comma
    if (u8idx < (BATTERY_CALIB_MAX - 1)) {
      while (*pacData != ',') {
        pacData++;

        // Check if character is null
        if (*pacData == 0) {
          // Error. Expected comma and then number
          eErrCode = ESP_ERR_INVALID_ARG;
          break;
        }
      }

      // If searching for next comma failed -> quit also this cycle
      if (eErrCode) {
        break;
      } else {
        // Searching succeed -> move by 1 to get "after" comma
        pacData++;
      }
    }
  }

  // If there is registered any problem, fail
  if (eErrCode) {
    _uiPrintStr(pacFail);
  } else {
    // So far no problem. Try to write data to NVS
    if (batteryOverwriteCalibrationRaw(au16calibData)) {
      _uiPrintStr(pacFail);
    } else {
      _uiPrintStr(pacOK);
    }
  }
}

static void _cmd_get_bat_offset_mv(char *pacUserInputBuff,
                                   teUiServiceState *pe_state_actual) {
  int8_t i8_batt_offset_mv = battery_get_static_offset_mv();

  // sign + 3 characters + " mV" + new line + null -> 10 should be enough
  char ac_output[10];

  sprintf(ac_output, "%d mv\n", i8_batt_offset_mv);
  _uiPrintStr(ac_output);
}

static void _cmd_set_bat_offset_mv(char *pacUserInputBuff,
                                   teUiServiceState *pe_state_actual) {
  // Temporary storage
  uint32_t u32_tmp;

  // Keep error codes
  esp_err_t e_err_code = ESP_FAIL;

  // Pointer to values
  char *pacData = _GET_PARAM_PTR(CMD_CAL_BATT_SET_RAW);

  bool b_is_negative = false;

  if (*pacData == '-') {
    b_is_negative = true;
    // Move pointer to next symbol - hopefully number
    pacData++;
  }

  e_err_code = strToUint32(pacData, &u32_tmp);

  if (e_err_code) {
    _uiPrintStr(pacCanNotParseValue);
    _uiPrintStr(pacFail);
    return;
  }

  if (u32_tmp > 127) {
    _uiPrintStr(pac_invalid_offset_mv_value);
    _uiPrintStr(pacFail);
    return;
  }

  int8_t i8_bat_offset_mv = u32_tmp;

  if (b_is_negative) {
    i8_bat_offset_mv *= -1;
  }

  battery_set_static_offset_mv(i8_bat_offset_mv);
}

static void _cmdClrBattStat(char *pacUserInputBuff,
                            teUiServiceState *peStateActual) {
  if (batteryDeleteStatisticData()) {
    // Some fail :(
    _uiPrintStr(pacFail);
  } else {
    // Everything OK
    _uiPrintStr(pacOK);
  }

  // Anyway it is necessary to reload values in battery module
  batteryForceReloadValuesFromNvs();
}

static void _cmdDelCompassCalib(char *pacUserInputBuff,
                                teUiServiceState *peStateActual) {
  if (sensMagClearCalibration()) {
    // Some fail :(
    _uiPrintStr(pacFail);
  } else {
    // Everything OK
    _uiPrintStr(pacOK);
  }
}

static void _cmdGetInitBeepMs(char *pacUserInputBuff,
                              teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  uint16_t u16periodMs = buzzerGetInitialBeepPeriod();

  // Print value. Value is 16 bit long -> 65535 as reference for generating
  // array
  char acPeriodMs[] = "65535 ms";

  sprintf(acPeriodMs, "%u ms", u16periodMs);

  _uiPrintStr(acPeriodMs);
}
static void _cmdSetInitBeepMs(char *pacUserInputBuff,
                              teUiServiceState *peStateActual) {
  assert(pacUserInputBuff);
  assert(peStateActual);

  // Pointer to value
  char *pacPeriodMs = _GET_PARAM_PTR(CMD_SET_INIT_BEEP_MS);

  // Temporary storage
  uint32_t u32value;

  esp_err_t eErrCode = strToUint32(pacPeriodMs, &u32value);

  if (eErrCode) {
    _uiPrintStr(pacCanNotParseValue);
    _uiPrintStr(pacFail);
    return;
  }

  // Check value range. Target value is 16 bit long
  if (u32value > 0xFFFF) {
    _uiPrintStr(pacBeepPeriodOutOfRange);
    _uiPrintStr(pacFail);
    return;
  }

  // Value is fine, set it
  buzzerSetInitialBeepPeriod((uint16_t)u32value);

  // Else everything is cool
  _uiPrintStr(pacOK);
}

static void _cmd_get_device_id(char *pacUserInputBuff,
                               teUiServiceState *peStateActual) {
  // Load chip info. But this one does not contain unique information, like MAC
  esp_chip_info_t s_chip_info;
  esp_chip_info(&s_chip_info);

  // MAC should be unique per ESP device
  uint8_t au8_mac[6];
  esp_read_mac(au8_mac, ESP_MAC_WIFI_STA);

  char ac_output_text[80];
  sprintf(ac_output_text,
          "%s_%02X:%02X:%02X:%02X:%02X:%02X (%d cores, rev %d)\n",
          CONFIG_IDF_TARGET, au8_mac[0], au8_mac[1], au8_mac[2], au8_mac[3],
          au8_mac[4], au8_mac[5], s_chip_info.cores, s_chip_info.revision);
  _uiPrintStr(ac_output_text);
}

static void _cmdDefaultSettings(char *pacUserInputBuff,
                                teUiServiceState *peStateActual) {
  if (_delAllSettings()) {
    _uiPrintStr(pacFail);
  } else {
    _uiPrintStr(pacOK);
  }
}
//========================| Other internal functions |========================
/**
 * @brief Initialize UART interface through which user will interact
 * @return Zero if everything is fine, otherwise there is error
 */
static esp_err_t _initUART(void) {
  esp_err_t eErrCode;
  // =================================| UART |==============================
  const uart_config_t uart_config = {.baud_rate = UI_SERVICE_UART_BAUDRATE,
                                     .data_bits = UART_DATA_8_BITS,
                                     .parity = UART_PARITY_DISABLE,
                                     .stop_bits = UART_STOP_BITS_1,
                                     .flow_ctrl = UART_HW_FLOWCTRL_DISABLE};
  // Try to remove driver if already installed
  if (mu8uartDriverInstalled) {
    uart_driver_delete(UI_SERVICE_UART_INTERFACE);
  }

  eErrCode = uart_param_config(UI_SERVICE_UART_INTERFACE, &uart_config);
  if (eErrCode) {
    ESP_LOGE(TAG, "UART configuration failed");
    return ESP_FAIL;
  }

  eErrCode = uart_set_pin(UI_SERVICE_UART_INTERFACE, IO_UI_SERVICE_TXD_PIN,
                          IO_UI_SERVICE_RXD_PIN, UART_PIN_NO_CHANGE,
                          UART_PIN_NO_CHANGE);
  if (eErrCode) {
    ESP_LOGE(TAG, "UART pin set fail");
    return ESP_FAIL;
  }

  eErrCode = uart_driver_install(UI_SERVICE_UART_INTERFACE,
                                 UI_SERVICE_RX_BUFFER_SIZE, 0, 0, NULL, 0);
  if (eErrCode) {
    ESP_LOGE(TAG, "UART driver install failed: %d", eErrCode);
    return ESP_FAIL;
  } else {
    mu8uartDriverInstalled = 1;
  }
  return ESP_OK;
}

/**
 * @brief Wait until user finish some action by pressing special key
 * @param pacUserInputBuff Input buffer
 * @param piUserInputBuffWriteIdx Write index for that buffer
 * @param pacInitialString Print extra string. Useful for questions
 * @return Return which special key was used to terminate current input
 */
static teSpecialKey _waitForUser(char *pacUserInputBuff,
                                 uint16_t *pu16userInputBuffWriteIdx,
                                 const char *pacInitialString) {
  // Reset logic?
  static bool bReset = true;

  // Tell where special key was found
  uint16_t u16keyIdx = -1;

  // If previous state was processing command, clear buffers
  if (bReset) {
    *pu16userInputBuffWriteIdx = 0;
    _uiPrintStr(pacInitialString);
    bReset = false;
  }

  // Temporary buffer
  char acRxBytes[UI_SERVICE_RX_BUFFER_SIZE] = {0};

  int iRxBytesCnt =
      uart_read_bytes(UI_SERVICE_UART_INTERFACE, (uint8_t *)acRxBytes,
                      sizeof(acRxBytes), UI_SERVICE_RX_USER_INPUT_TIMEOUT_MS);
  // If nothing to process, exit preliminary
  if (iRxBytesCnt == 0) {
    return KEY_NONE;
  }
  ///@todo Solve "problem" with deleting "$ " when pushing backspace
  // Print input back to user to have some feedback
  _uiPrintRaw(acRxBytes, iRxBytesCnt);

  ESP_LOGV(TAG, "RX %d user chars (Widx: %d)", iRxBytesCnt,
           *pu16userInputBuffWriteIdx);

  // Put data to user input buffer
  assert(iRxBytesCnt > 0);
  assert(iRxBytesCnt <= 0xFFFF);
  esp_err_t eErrCode =
      _appendBuffer(acRxBytes, (uint16_t)iRxBytesCnt, pacUserInputBuff,
                    pu16userInputBuffWriteIdx, UI_SERVICE_RX_BUFFER_SIZE);
  if (eErrCode) {
    // Something went wrong -> re-initialize
    ESP_LOGE(TAG, "Error when adding user input to input buffer");

    bReset = true;
    return KEY_NONE;
  }

  for (int i = 0; i < *pu16userInputBuffWriteIdx; i++) {
    ESP_LOGV(TAG, " > %c (%d)", pacUserInputBuff[i], (int)pacUserInputBuff[i]);
  }

  // Search for keys
  assert(KEY_NONE == 0);
  for (teSpecialKey eKey = (teSpecialKey)(KEY_NONE + 1); eKey < KEY_MAX;
       eKey++) {
    if (_findSpecialKey(eKey, pacUserInputBuff, *pu16userInputBuffWriteIdx,
                        &u16keyIdx)) {
      // Write new line, so new written text will not be overwritten
      _uiPrintStr("\n");

      // Write null to position of found character, so input will be
      // treated as string
      pacUserInputBuff[u16keyIdx] = 0x00;

      // Reset statemachine of this function
      bReset = true;
      return eKey;
    }
  }

  // Return whatever was set - should be KEY_NONE
  return KEY_NONE;
}

static teSpecialKey _waitForEnterEsc(char *pacUserInputBuff) {
  // Write index for buffer - start from 0
  uint16_t u16writeIdx = 0;
  teSpecialKey eSpecKey = KEY_NONE;

  while ((eSpecKey != KEY_ENTER) && (eSpecKey != KEY_ESC)) {
    if (eSpecKey == KEY_ESC) {
      break;
    }
    // Reset write index
    u16writeIdx = 0;

    // Release task for a while
    vTaskDelay(UI_SERVICE_TASK_PERIOD_MS / portTICK_PERIOD_MS);

    eSpecKey =
        _waitForUser(pacUserInputBuff, &u16writeIdx, pacPressEnterWhenReady);
  }

  return eSpecKey;
}

/**
 * @brief Append data from source buffer to destination buffer
 *
 * @param pacSrc Pointer to source buffer
 * @param iSrcSize Number of Bytes which will be copied to destination buffer
 * @param pacDst Pointer to destination buffer
 * @param piDstWriteIdx Actual write index for destination buffer. From this
 *                      index will be written data from source buffer.
 * @param iDstBuffSize Total size of destination buffer. This will help to
 *                     detect potential buffer overflow.
 * @return Non zero value if there is problem.
 */
static esp_err_t _appendBuffer(const char *pacSrc, const uint16_t u16SrcSize,
                               char *pacDst, uint16_t *pu16dstWriteIdx,
                               const uint16_t u16dstBuffSize) {
  // If nothing to process
  if (u16SrcSize == 0) {
    return ESP_OK;
  }
  ESP_LOGV(TAG, "_appendBuffer: srcSize: %i ; Widx: %d ; DstBsize: %i",
           u16SrcSize, *pu16dstWriteIdx, u16dstBuffSize);

  for (uint16_t u16procByteCnt = 0; u16procByteCnt < u16SrcSize;
       u16procByteCnt++) {
    /* Check if there is "backspace" character. If yes, do not move forward
     * but decrease write pointer, so previous character will be deleted
     */
    if (pacSrc[u16procByteCnt] == '\b') {
      // It is pointless to write "backspace" character - skip it
      if (*pu16dstWriteIdx >= 1) {
        (*pu16dstWriteIdx)--;
        /* Print to console space (overwrite old character) and print
         * backspace
         */
        _uiPrintStr(" \b");
      } else {
        // Do not decrease Write index - keep it as it is
      }

    }  ///@todo Handle other non-character symbols
    else {
      // Copy character, increase write index
      pacDst[*pu16dstWriteIdx] = pacSrc[u16procByteCnt];

      // Increase write index & check value
      (*pu16dstWriteIdx)++;
      if (*pu16dstWriteIdx >= u16dstBuffSize) {
        ESP_LOGE(TAG, "Not enough memory in input buffer");
        return ESP_ERR_NO_MEM;
      }
    }
  }
  return ESP_OK;
}

/**
 * @brief Find new line or carriage return in buffer
 * @param pacBuff Pointer to buffer with characters
 * @param u16maxIdx Write index - up to this index will be searching.
 * @param pu16keyIdx Index where key was found
 * @return True if new line found, false otherwise
 */
static bool _findSpecialKey(const teSpecialKey eKey, const char *pacBuff,
                            const uint16_t u16maxIdx, uint16_t *pu16keyIdx) {
  for (uint16_t u16idx = 0; u16idx < u16maxIdx; u16idx++) {
    // According to the key, process
    switch (eKey) {
      case KEY_NONE:
        // This should not happen. This is invalid parameter
        ESP_LOGE(TAG, "Invalid key KEY_NONE - can not be used");
        assert(0);
        return KEY_NONE;

      case KEY_ENTER:
        if ((pacBuff[u16idx] == '\n') || (pacBuff[u16idx] == '\r')) {
          *pu16keyIdx = u16idx;
          return true;
        }
        break;
      case KEY_ESC:
        // ASCI ESC - 0x1B
        if (pacBuff[u16idx] == 0x1B) {
          *pu16keyIdx = u16idx;
          return true;
        }
        break;
      case KEY_MAX:
        // This should not happen. This is invalid parameter
        ESP_LOGE(TAG, "Invalid key KEY_MAX - can not be used");
        assert(0);
        return KEY_NONE;
    }
  }
  return false;
}

/**
 * @brief Delete all settings stored in NVS
 *
 * Basically recover "factory settings"
 *
 * @return Error code from NVS functions
 */
static teNvsSttngsErr _delAllSettings(void) {
  // Try to erase whatever is possible
  teNvsSttngsErr eErrCode = kbrdDeleteCalibration();
  eErrCode |= nvsSettingsDel(_MAC_ID);
  eErrCode |= nvsSettingsDel(_PWD_ID);
  eErrCode |= wifiDelScanCfgNvs();
  eErrCode |= batteryDeleteStatisticData();
  eErrCode |= kbrdResetBacklightAutoOff();
  eErrCode |= sensMagClearCalibration();
  eErrCode |= uiModeCompassResetLedBright();

  return eErrCode;
}

static esp_err_t _printMac(uint8_t au8mac[static MAC_SIZE_BYTES]) {
  esp_err_t eErrCode = ESP_FAIL;
  eErrCode = _uiPrintStr(macBinToStr(au8mac));

  return eErrCode;
}

/**
 * @brief Prints help for user
 * @return Error code - zero if no error, non-zero otherwise.
 */
static esp_err_t _printHelp(void) { return (_uiPrintStr(pacHelp)); }
