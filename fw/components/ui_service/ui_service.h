/**
 * @file
 * @author Martin Stejskal
 * @brief UI service module
 *
 * Allow to configure device through service UART
 */
#ifndef __UI_SERVICE_H__
#define __UI_SERVICE_H__
// ===============================| Includes |================================
#include <driver/gpio.h>
#include <driver/uart.h>
#include <esp_err.h>

#include "mac.h"

// When compiler support it, check if user file exists and if yes, include it
#if defined __has_include
#if __has_include("cfg.h")
#include "cfg.h"
#endif
#endif
// ================================| Defines |================================
// ============================| Default values |=============================
#ifndef UI_SERVICE_UART_BAUDRATE
#define UI_SERVICE_UART_BAUDRATE (115200)
#endif  // UI_SERVICE_UART_BAUDRATE

#ifndef UI_SERVICE_UART_INTERFACE
#define UI_SERVICE_UART_INTERFACE (UART_NUM_1)
#endif  // UI_SERVICE_UART_INTERFACE

#ifndef IO_UI_SERVICE_TXD_PIN
#define IO_UI_SERVICE_TXD_PIN (GPIO_NUM_17)
#endif  // IO_UI_SERVICE_TXD_PIN

#ifndef IO_UI_SERVICE_RXD_PIN
#define IO_UI_SERVICE_RXD_PIN (GPIO_NUM_16)
#endif  // IO_UI_SERVICE_RXD_PIN

#ifndef UI_SERVICE_RX_BUFFER_SIZE
/**
 * @brief Buffer size in Bytes
 *
 * Usually commands should be really short, so something like 256 should be
 * more than enough.
 *
 * @note For some reason, size under 128 is not allowed. Just be warned
 */
#define UI_SERVICE_RX_BUFFER_SIZE (512)
#endif  // UI_SERVICE_RX_BUFFER_SIZE

#ifndef UI_SERVICE_RX_USER_INPUT_TIMEOUT_MS
/**
 * @brief Define period in ms within UART RX function stop scanning
 *
 * For best user experience (fast reply from MCU) keep this value set to 1
 */
#define UI_SERVICE_RX_USER_INPUT_TIMEOUT_MS (1)
#endif  // UI_SERVICE_RX_USER_INPUT_TIMEOUT_MS

#ifndef UI_SERVICE_TASK_PERIOD_MS
/**
 * @brief Some reasonable period within UI task is called in ms
 *
 * Recommended value is 50
 */
#define UI_SERVICE_TASK_PERIOD_MS (35)
#endif  // UI_SERVICE_TASK_PERIOD_MS

#ifndef UI_SERVICE_DEFAULT_PASSWORD
/**
 * @brief Default password to login into system
 *
 * Password can be changed later on.
 */
#define UI_SERVICE_DEFAULT_PASSWORD "daz2020"
#endif  // UI_SERVICE_DEFAULT_PASSWORD

#ifndef UI_SERVICE_RESTORE_FACTORY_SETTINGS
#define UI_SERVICE_RESTORE_FACTORY_SETTINGS "factory reset   "
#endif  // UI_SERVICE_RESTORE_FACTORY_SETTINGS

#ifndef UI_SERVICE_NVS_PASSWORD_MAX_LEN
/**
 * @brief Maximum length for password
 */
#define UI_SERVICE_NVS_PASSWORD_MAX_LEN (50)
#endif  // UI_SERVICE_NVS_PASSWORD_MAX_LEN

#ifndef UI_SERVICE_MAX_MAC_STORED
/**
 * @brief Maximum number of registered MAC in NVS
 */
#define UI_SERVICE_MAX_MAC_STORED (30)
#endif  // UI_SERVICE_MAX_MAC_STORED

#ifndef UI_SERVICE_MAC_DESCRIPTION_MAX_LENGTH
/**
 * @brief Maximum description length for every registered MAC
 */
#define UI_SERVICE_MAC_DESCRIPTION_MAX_LENGTH (15)
#endif  // UI_SERVICE_MAC_DESCRIPTION_MAX_LENGTH
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  UI_SRVCE_WAIT_FOR_LOGIN,  //!< UI_SRVCE_WAIT_FOR_LOGIN
  UI_SRVCE_WAIT_FOR_CMD,    //!< UI_SRVCE_WAIT_FOR_CMD
  UI_SRVCE_PROCESS_CMD,     //!< UI_SRVCE_PROCESS_CMD
} teUiServiceState;

typedef struct {
  uint8_t au8macAddr[UI_SERVICE_MAX_MAC_STORED][MAC_SIZE_BYTES];
  char acMacDescription[UI_SERVICE_MAX_MAC_STORED]
                       [UI_SERVICE_MAC_DESCRIPTION_MAX_LENGTH];
} tsUiServiceMacList;
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
void uiServiceTaskRTOS(void *pvParameters);
// ========================| Middle level functions |=========================
void uiServiceWaitForLogin(teUiServiceState *peStatePrev,
                           teUiServiceState *peStateActual,
                           char *pacUserInputBuff,
                           uint16_t *pu16userInputBuffWriteIdx);

void uiServiceWaitForCmd(teUiServiceState *peStatePrev,
                         teUiServiceState *peStateActual,
                         char *pacUserInputBuff,
                         uint16_t *pu16userInputBuffWriteIdx);

void uiServiceProcessCmd(teUiServiceState *peStatePrev,
                         teUiServiceState *peStateActual,
                         char *pacUserInputBuff,
                         uint16_t *pu16userInputBuffWriteIdx);
// ==========================| Low level functions |==========================
esp_err_t uiServiceGetMacList(tsUiServiceMacList *psMacList);
// ==========================| Internal functions |===========================
#endif  // __UI_SERVICE_H__
