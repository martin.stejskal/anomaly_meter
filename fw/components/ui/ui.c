/**
 * @file
 * @author Martin Stejskal
 * @brief User interface
 */
// ===============================| Includes |================================
#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ESP specific
#include <esp_err.h>
#include <esp_log.h>

// Local modules
#include "battery.h"
#include "buzzer.h"
#include "err_handlers.h"
#include "gauge_vmeter.h"
#include "keyboard.h"
#include "led_rgb.h"
#include "sensors.h"
#include "ui.h"
#include "ui_display.h"
#include "ui_mode_acc_graph.h"
#include "ui_mode_anomaly_detector.h"
#include "ui_mode_batt.h"
#include "ui_mode_compass.h"
#include "ui_mode_flashlight.h"
#include "ui_modes_common.h"
#include "ui_service.h"
#include "ui_settings.h"
// ================================| Defines |================================
#define UI_TASK_PERIOD_MS (50)
#define INIT_MODE (UI_MODE_ANOMALY_DETECTOR)

#define DEFAULT_MODE_PRIORITY (tskIDLE_PRIORITY + 3)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  UI_STATE_SEL_MODE,
  UI_RUN_NEW_MODE,
  UI_WAIT_FOR_EXIT_MODE
} teUiMainState;

// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char* TAG = "UI";
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static void _selectNewMode(teUiMainState* peMainState, teUiModes* peCurrentMode,
                           teUiModes* peExitMode,
                           tsUiModeCfg asModesCfg[static UI_MODE_MAX]);
static void _runNewMode(teUiModes eMode);

static void _processRequest(teUiReq eReq, teUiMainState* peMainState,
                            teUiModes* peCurrentMode);

static void _serviceMode(void);

static void _incMode(teUiModes* peMode,
                     tsUiModeCfg asModesCfg[static UI_MODE_MAX]);
static void _decMode(teUiModes* peMode,
                     tsUiModeCfg asModesCfg[static UI_MODE_MAX]);
// =========================| High level functions |==========================
void uiTaskRTOS(void* pvParameters) {
  // In loop here will be executed some mode task and we will wait until
  // task will terminate itself (user input). When do so, state machine
  // jump back to "select mode" and allow user to change mode. Then it will
  // wait again for exit new mode.
  static teUiMainState eMainState = UI_RUN_NEW_MODE;

  // Start with mode below. Register mode when exit -> can be used as
  // fallback when selecting new mode.
  static teUiModes eCurrentMode = INIT_MODE, eExitMode = INIT_MODE;

  // Keep every mode setting safe and sound
  static tsUiModeCfg asModesCfg[UI_MODE_MAX];

  // Mode "none" is there only for debug purposes in order to reduce number
  // of messages
  // eCurrentMode = UI_MODE_NONE;

  ESP_LOGD(TAG, "Starting UI...");
  // Setup ADC1 - common for multiple modules
  // Setup ADC. Resolution 10 bits is enough and also it is kind of close
  // to real value in mV -> quite easy to debug
  if (adc1_config_width(ADC1_BIT_RESOLUTION)) {
    return;
  }

  // Enable modes that "are not depended on anything"
  asModesCfg[UI_MODE_SETTINGS].bEnabled = 1;
  asModesCfg[UI_MODE_FLASHLIGHT].bEnabled = 1;

  // Keyboard service
  xTaskCreate(kbrdTaskRTOS, "kbrd", 3 * 1024, NULL, tskIDLE_PRIORITY, NULL);

  // Service UI
  xTaskCreate(uiServiceTaskRTOS, "uiSrv", 4 * 1024, NULL, tskIDLE_PRIORITY,
              NULL);

  // Battery manager service
  xTaskCreate(batteryTaskRTOS, "batt", 3 * 1024, NULL, tskIDLE_PRIORITY, NULL);
  asModesCfg[UI_MODE_BATT].bEnabled = 1;

  // Buzzer
  xTaskCreate(buzzerTaskRTOS, "Buzzer", 3 * 1024, NULL, tskIDLE_PRIORITY + 10,
              NULL);

  // RGB LED
  xTaskCreate(ledRGBtask, "rgb", 3 * 1024, NULL, tskIDLE_PRIORITY, NULL);

  // Gauge
  xTaskCreate(gaugeMeterTask, "gauge", 3 * 1024, NULL, tskIDLE_PRIORITY, NULL);

  ESP_ERROR_CHECK(uiDispInit());

  if (sensorsInit()) {
    // Sensors not detected -> disable appropriate modes
    asModesCfg[UI_MODE_COMPASS].bEnabled = 0;
    asModesCfg[UI_MODE_ACC_GRAPH].bEnabled = 0;
  } else {
    asModesCfg[UI_MODE_COMPASS].bEnabled = 1;
    asModesCfg[UI_MODE_ACC_GRAPH].bEnabled = 1;
  }

  // Everything for anomaly meter is ready. Enable it!
  asModesCfg[UI_MODE_ANOMALY_DETECTOR].bEnabled = 1;

  while (1) {
    switch (eMainState) {
      case UI_STATE_SEL_MODE:
        _selectNewMode(&eMainState, &eCurrentMode, &eExitMode, asModesCfg);
        break;
      case UI_RUN_NEW_MODE:
        // Execute new task
        _runNewMode(eCurrentMode);
        // And change state to "wait"
        eMainState = UI_WAIT_FOR_EXIT_MODE;
        break;
      case UI_WAIT_FOR_EXIT_MODE:
        // Once exited from current mode, switch to "select mode"
        if (uiGetIsModeExited()) {
          ESP_LOGI(TAG, "Mode %s was terminated", uiGetModeName(eCurrentMode));
          // Clear "exit" flag - when run new mode, it will be ready
          uiClearModeExitFlag();

          // Check current mode. It is possible that we're going from
          // some test mode -> need to check current mode
          if (eCurrentMode <= UI_MODE_SERVICE) {
            eCurrentMode = UI_MODE_SERVICE + 1;
          }

          // Store it for later use
          eExitMode = eCurrentMode;

          eMainState = UI_STATE_SEL_MODE;
        }
        break;
      default:
        // Should not happen
        ESP_LOGE(TAG, "Unknown state: %d", eMainState);
        // When in debug, halt
        assert(0);
        // Otherwise try to start again
        eMainState = UI_STATE_SEL_MODE;
    }
    // Check if there is any pending request
    _processRequest(uiGetReq(), &eMainState, &eCurrentMode);

    // Check mode change once per some time (not critical)
    vTaskDelay(UI_TASK_PERIOD_MS / portTICK_PERIOD_MS);
  }

  ESP_LOGE(TAG, "UI task terminated");
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
static void _selectNewMode(teUiMainState* peMainState, teUiModes* peCurrentMode,
                           teUiModes* peExitMode,
                           tsUiModeCfg asModesCfg[static UI_MODE_MAX]) {
  // Register pressed buttons
  tsKbrdBtns sButtons = {0};

  // Show menu
  if (uiDispShowMode(*peCurrentMode, asModesCfg)) {
    // Display failed -> not much we can do -> keep what is set
    assert(0);
    return;
  }

  // Read from keyboard
  kbrPressedReleased(&sButtons);
  if (kbrdAnyButtonPressed(&sButtons)) {
    kbrdShowPressedButtons(&sButtons);

    if (sButtons.bEnter || sButtons.bRight) {
      // Current mode is valid -> exit this function
      // New mode selected -> execute it
      *peMainState = UI_RUN_NEW_MODE;
      return;
    } else if (sButtons.bLeft) {
      // Kind of "escape" -> back to initial mode
      *peCurrentMode = *peExitMode;
      *peMainState = UI_RUN_NEW_MODE;
      return;
    } else if (sButtons.bUp) {
      _decMode(peCurrentMode, asModesCfg);
    } else if (sButtons.bDown) {
      _incMode(peCurrentMode, asModesCfg);
    }
  } else {
    // No key pressed -> return
  }
}

static void _runNewMode(teUiModes eMode) {
  ESP_LOGD(TAG, "Executing mode %s", uiGetModeName(eMode));

  switch (eMode) {
    case UI_MODE_NONE:
      // Dummy - do nothing - reduce logging, allow debug
      break;
    case UI_MODE_SERVICE:
      // Call function which do something once
      _serviceMode();
      break;
    case UI_MODE_ANOMALY_DETECTOR:
      xTaskCreate(uiModeAnomalyDetectorTaskRTOS, "UI_det", 16 * 1024, NULL,
                  DEFAULT_MODE_PRIORITY, NULL);
      break;
    case UI_MODE_ACC_GRAPH:
      xTaskCreate(uiModeAccTaskRTOS, "UI_acc", 4 * 1024, NULL,
                  DEFAULT_MODE_PRIORITY, NULL);
      break;
    case UI_MODE_BATT:
      xTaskCreate(uiModeBattTaskRTOS, "UI_batt", 4 * 1024, NULL,
                  DEFAULT_MODE_PRIORITY, NULL);
      break;
    case UI_MODE_FLASHLIGHT:
      xTaskCreate(uiModeFlashlightRTOS, "UI_flshlght", 4 * 1024, NULL,
                  DEFAULT_MODE_PRIORITY, NULL);
      break;
    case UI_MODE_COMPASS:
      xTaskCreate(uiModeCompassTaskRTOS, "UI_cmps", 4 * 1024, NULL,
                  DEFAULT_MODE_PRIORITY, NULL);
      break;
    case UI_MODE_SETTINGS:
      xTaskCreate(uiModeSettingsRTOS, "UI_sttngs", 4 * 1024, NULL,
                  DEFAULT_MODE_PRIORITY, NULL);
      break;
    default:
      ESP_LOGE(TAG, "Unknown mode: %d", eMode);
      // If in debug mode, stop there
      assert(0);
      // Else try to do something - run first mode
      _runNewMode((teUiModes)0);
  }
}

static void _processRequest(teUiReq eReq, teUiMainState* peMainState,
                            teUiModes* peCurrentMode) {
  switch (eReq) {
    case UI_REQ_NONE:
      // Nothing to do -> exit right away
      return;
    case UI_REQ_EN_SERVICE_MODE:
      // Enable service mode
      // If in "select mode", just need to change mode. Otherwise it required
      // to stop running mode task properly

      if (*peMainState == UI_WAIT_FOR_EXIT_MODE) {
        // First stop running task
        uiForceCloseTaskRequest(true);

        // Wait for task to finish
        vTaskDelay(UI_TASK_PERIOD_MS / portTICK_PERIOD_MS);
        while (!uiGetIsModeExited()) {
          // Give task few more cycles
          vTaskDelay(UI_TASK_PERIOD_MS / portTICK_PERIOD_MS);
          ESP_LOGD(TAG, "Waiting for mode to exit");
        }
        // Clear flag
        uiClearModeExitFlag();

        // Abort "force close" flag, since task successfully stopped
        uiForceCloseTaskRequest(false);
      }

      // OK, technically we're in service mode
      *peMainState = UI_RUN_NEW_MODE;
      *peCurrentMode = UI_MODE_SERVICE;

      uiSwithedToServiceMode();
      break;
    case UI_REQ_DIS_SERVICE_MODE:
      // Simply "terminate" service mode. Statemachine do rest
      taskSetModeExit();
      uiExitedServiceMode();
      break;
  }
}

static void _serviceMode(void) {
  // Not critical, so do not care about error code
  pcd8544clearFb();

  pcd8544printfFb(0, 2, "Service mode");
  pcd8544writeFbToLCD();
}

static void _incMode(teUiModes* peMode,
                     tsUiModeCfg asModesCfg[static UI_MODE_MAX]) {
  do {
    *peMode += 1;

    if (*peMode >= UI_MODE_MAX) {
      // Start from first
      *peMode = (teUiModes)0;
    }
  } while (!asModesCfg[*peMode].bEnabled);
}
static void _decMode(teUiModes* peMode,
                     tsUiModeCfg asModesCfg[static UI_MODE_MAX]) {
  do {
    *peMode -= 1;

    if (*peMode == UI_MODE_NONE) {
      // Start from end. Well, from last valid item
      *peMode = UI_MODE_MAX - 1;
    }
  } while (!asModesCfg[*peMode].bEnabled);
}
