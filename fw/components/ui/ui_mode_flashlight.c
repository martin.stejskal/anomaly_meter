/**
 * @file
 * @author Martin Stejskal
 * @brief Flashlight mode
 */
// ===============================| Includes |================================
#include "ui_mode_flashlight.h"

#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ESP specific
#include <esp_err.h>
#include <esp_log.h>

#include "cfg.h"
#include "keyboard.h"
#include "led_rgb.h"
#include "ui_display.h"
#include "ui_keyboard.h"
#include "ui_modes_common.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "Flshlght mode";
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static esp_err_t _incBrightness(void *pArgs);
static esp_err_t _decBrightness(void *pArgs);
// =========================| High level functions |==========================
void uiModeFlashlightRTOS(void *pvParameters) {
  ESP_LOGI(TAG, "Executing flashlight mode...");
  teLedRGBlevel eLedBrightness = LED_LVL_7;

  // Automatic backlight off feature setting - backup
  bool bAutoOffBcklghtFeatBckp;

  // Current backlight status - backup
  bool bBacklightStatusBckp;

  // Turn on LED - max power
  ledSetAll((uint8_t)eLedBrightness);

  // Try to display - not critical. If fails, just print error
  if (uiDispFlashlight(eLedBrightness)) {
    ESP_LOGE(TAG, "Writing to LCD failed");
  }

  // Get status of "auto off backlight" feature
  bAutoOffBcklghtFeatBckp = kbrdGetBacklightAutoOff();

  // Get current backlight status
  bBacklightStatusBckp = uiDispGetBacklightStatus();

  // If auto off feature is on, disable it, do not save
  if (bAutoOffBcklghtFeatBckp) {
    kbrdSetBacklightAutoOff(false, false);
  }

  // Always enable backlight, but not save setting
  uiDispSetBacklight(true, false);

  tsKbrdMenuParams sMenu = {
      .pfUp = (void *)&_incBrightness,
      .pfDown = (void *)&_decBrightness,
  };

  if (uiKbrdMenu(&sMenu)) {
    // Someting failed, but flashlight is not super important
    ESP_LOGE(TAG, "Flashlight menu failed!");
  }

  ledTurnOffAll();

  // Restore backlight settings
  if (bAutoOffBcklghtFeatBckp) {
    // re-enable feature, do not save setting - do not save setting
    kbrdSetBacklightAutoOff(bAutoOffBcklghtFeatBckp, false);
  } else {
    // Auto off was disabled - restore state (do not save)
    uiDispSetBacklight(bBacklightStatusBckp, false);
  }

  taskSetModeExit();
  // Terminate itself
  vTaskDelete(NULL);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
static esp_err_t _incBrightness(void *pArgs) {
  // Increase brightness
  ledIncLvlAll();

  // Load current LED level and pass it to display
  return uiDispFlashlight(ledGetLvlAll());
}

static esp_err_t _decBrightness(void *pArgs) {
  // Decrease brightness
  ledDecLvlAll();

  // Load current LED level and pass it to display
  return uiDispFlashlight(ledGetLvlAll());
}
