/**
 * @file
 * @author Martin Stejskal
 * @brief Battery mode
 */
// ===============================| Includes |================================
#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ESP specific
#include <esp_log.h>

#include "battery.h"
#include "cfg.h"
#include "keyboard.h"
#include "ui_display.h"
#include "ui_mode_batt.h"
#include "ui_modes_common.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "BATT mode";
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
void uiModeBattTaskRTOS(void *pvParameters) {
  ESP_LOGI(TAG, "Executing Battery mode...");
  // Register pressed buttons
  tsKbrdBtns sButtons = {0};

  uint8_t u8runTask = 1;

  // Current battery status
  tsBattStatus sBattStatus;

  // Default zoom. Using integer as fractional value - everything is multiplied
  // by 1000, so eventually also 3 fractional decimal digits are hold
  static uint32_t u32zoom = 1000;  // 1000 -> 1.000 x

  while (u8runTask) {
    // Get battery status
    batteryGetStatus(&sBattStatus);

    // Display data
    uiDispBattInfo(&sBattStatus, u32zoom);

    kbrPressedReleased(&sButtons);
    if (kbrdAnyButtonPressed(&sButtons)) {
      kbrdShowPressedButtons(&sButtons);

      // Get out from this mode
      if (sButtons.bLeft) {
        u8runTask = 0;
        break;
      }

      if (sButtons.bUp) {
        u32zoom = (u32zoom * UI_MODE_BATT_MUL_ZOOM_1000X) / 1000;

        if (u32zoom > UI_MODE_BATT_MAX_ZOOM_1000X) {
          u32zoom = UI_MODE_BATT_MAX_ZOOM_1000X;
        }

        ESP_LOGI(TAG, "Zoom: %d (1000x)", u32zoom);
      }

      if (sButtons.bDown) {
        u32zoom = (u32zoom * 1000) / UI_MODE_BATT_MUL_ZOOM_1000X;

        if (u32zoom < UI_MODE_BATT_MIN_ZOOM_1000X) {
          u32zoom = UI_MODE_BATT_MIN_ZOOM_1000X;
        }

        ESP_LOGI(TAG, "Zoom: %d (1000x)", u32zoom);
      }
    }

    // Check if UI request exit this task
    if (taskGetForceCloseFlag()) {
      break;
    }
    // Keep scanning keyboard within some intervals. Take a nap
    vTaskDelay(KBRD_RECOMMENDED_SCAN_TIME_MS / portTICK_PERIOD_MS);
  }

  taskSetModeExit();
  // Terminate itself
  vTaskDelete(NULL);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
