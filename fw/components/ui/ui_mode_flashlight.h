/**
 * @file
 * @author Martin Stejskal
 * @brief Flashlight mode
 */
#ifndef __UI_FLASHLIGHT_H__
#define __UI_FLASHLIGHT_H__
// ===============================| Includes |================================
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
void uiModeFlashlightRTOS(void *pvParameters);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
#endif  // __UI_FLASHLIGHT_H__
