/**
 * @file
 * @author Martin Stejskal
 * @brief Anomaly detector mode
 */
// ===============================| Includes |================================
#include "ui_mode_anomaly_detector.h"

#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

// ESP specific
#include <esp_log.h>

#include "battery.h"
#include "buzzer.h"
#include "cfg.h"
#include "gauge_vmeter.h"
#include "keyboard.h"
#include "led_rgb.h"
#include "mac.h"
#include "nvs_settings.h"
#include "ui_display.h"
#include "ui_modes_common.h"
#include "ui_service.h"

// ================================| Defines |================================
/**
 * @brief Define brightness of LED which indicate scanning
 *
 * Range level 0 ~ 9
 */
#define _WIFI_SCAN_LED_BRIGHNESS (LED_LVL_3)

/**
 * @brief Default warning beep duration in MS
 */
#define _UI_ANOMALY_DETECTOR_DEFAULT_WARNING_BEEP_DURATION_MS (500)

/**
 * @brief Default pause between warning beeps as ratio
 *
 * This is basically warning beep duration divided by this number. You do the
 * math.
 */
#define _UI_ANOMALY_DETECTOR_DEFAULT_WARNING_BEEP_PAUSE_DURATION_DIV (5)

/**
 * @brief Define time when anomaly should be "forget" and warning status reset
 *
 * To avoid super often beeping, device will only beep if warning level
 * increase. This warning level is reset when anomaly is not detected for
 * time specified here. Value is in ms.
 */
#define _UI_ANOMALY_DETECTOR_ANOMALY_FORGET_TIME_MS (7000)

/**
 * @brief ID for NVS when dealing with LED
 */
#define _NVS_DETECTOR_LED_ID "DetLed"
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "Anm det";
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
/**
 * @brief Prepare display data, set buzzer mode, gauge and LED
 * @param[in, out] asApInfo List of found APs
 * @param[in] u16apCount Number of found APs
 * @param[in] au8macAnomalyList List of anomalies as BSSID
 * @param[out] psUiDispDetector Structure used for displaying anomalies
 */
static void _prepareDisplayDataSetBuzzerGaugeLeds(
    wifi_ap_record_t asApInfo[static WIFI_SCAN_LIST_SIZE],
    const uint16_t u16apCount, tsUiServiceMacList *psMacList,
    tsUiDispDetector *psUiDispDetector);

/**
 * @brief Search for anomaly. If found, tell index in result structure
 * @param asApInfo List of found AP
 * @param pu16apCount Number of found AP
 * @param psMacList List of anomalies as MAC and description
 * @param pu16anomalyIdx If anomaly found, output index of anomaly MAC in
 *        asApInfo
 * @return True if anomaly found, false otherwise
 */
static bool _isAnomalyFound(
    const wifi_ap_record_t asApInfo[static WIFI_SCAN_LIST_SIZE],
    const uint16_t u16apCount, tsUiServiceMacList *psMacList,
    uint16_t *pu16anomalyIdxAp, char **ppacAnomalyDescription);

static void _updateWarningLevel(
    teUiAnomalyDetectorWarningThresholds *peDetectorWarning,
    const int8_t i8rssi);

static void _warnUserIfNeeded(
    teUiAnomalyDetectorWarningThresholds *peDetectorWarning);

static void _setLedColorAccordingToWarningLevel(
    teUiAnomalyDetectorWarningThresholds eDetectorWarning,
    teLedRGBlevel eLedLevel);

/**
 * @brief Warning beep plus setting RGB LED
 * @param u8count Number of beeps
 */
static void _beepWarning(const uint8_t u8count);

/**
 * @brief Prepare task for termination
 */
static void _exitModeTermTask(void);

// =========================| High level functions |==========================
void uiModeAnomalyDetectorTaskRTOS(void *pvParameters) {
  ESP_LOGI(TAG, "Executing detector mode...");
  esp_err_t eErrCode = ESP_FAIL;
  // Register pressed buttons
  tsKbrdBtns sButtons = {0};

  // Want to run task
  uint8_t u8runTask = 1;

  // WiFi things
  uint16_t u16apCount = 0;
  wifi_ap_record_t asApInfo[WIFI_SCAN_LIST_SIZE];

  // Store anomaly MAC list
  tsUiServiceMacList sMacList;

  // Current battery status
  tsBattStatus sBattStatus;

  // Here are stored RSSI data. Variable is static because it is more user
  // friendly to have data stored even when jump to menu
  static uint8_t au8bufferRssi[UI_DISP_X_RES];

  // Data for display
  static tsUiDispDetector sUiDispDetector = {
      .eWarningLvl = UI_ANOMALY_DETECTOR_NO_WARNING,
      .sRssiRingBuff = {.pvBuffer = au8bufferRssi,
                        .u16bufferSizeBytes = sizeof(au8bufferRssi),
                        .u8itemSizeBytes = sizeof(au8bufferRssi[0]),
                        .u16readIdx = 0,
                        .u16writeIdx = 0}};

  // WiFi configuration
  tsWiFiScanCfg sWiFiCfg;

  // Load LED brightness
  teLedRGBlevel eLedLevel;

  const teLedRGBlevel eLedDefaultPwm = LED_LVL_5;

  // Loading should not fail, otherwise it signalize something fatal. As
  // default use something decent (2nd parameter)
  ESP_ERROR_CHECK(nvsSettingsGet(_NVS_DETECTOR_LED_ID, (uint8_t *)&eLedLevel,
                                 (uint8_t *)&eLedDefaultPwm, sizeof(uint8_t)));

  // Load list of "anomaly" MAC, WiFi settings
  if (uiServiceGetMacList(&sMacList) || wifiGetScanCfgNvs(&sWiFiCfg)) {
    _exitModeTermTask();
    return;
  }

  ESP_ERROR_CHECK(wifiInit());

  while (u8runTask) {
    // Signal scanning. Also change color based on previous processing
    _setLedColorAccordingToWarningLevel(sUiDispDetector.eWarningLvl, eLedLevel);

    eErrCode = wifiScan(&u16apCount, asApInfo, &sWiFiCfg);

    // Get battery status
    batteryGetStatus(&sBattStatus);

    // Turn off LED, do processing data
    ledTurnOffAll();

    if (eErrCode) {
      char acErrCodeStr[50];
      ESP_LOGE(TAG, "WiFi scan failed: %s",
               esp_err_to_name_r(eErrCode, acErrCodeStr, sizeof(acErrCodeStr)));
      // Try to run scan again
      continue;
    }

    _prepareDisplayDataSetBuzzerGaugeLeds(asApInfo, u16apCount, &sMacList,
                                          &sUiDispDetector);

    eErrCode = uiDispDetector(&sUiDispDetector, &sBattStatus);
    if (eErrCode) {
      // This should not happen
      ESP_LOGE(TAG, "Writing to display failed: %d", eErrCode);
      u8runTask = 0;
      break;
    }

    // Do annoying beep sound if needed
    _warnUserIfNeeded(&sUiDispDetector.eWarningLvl);

    kbrPressedReleased(&sButtons);
    if (kbrdAnyButtonPressed(&sButtons)) {
      kbrdShowPressedButtons(&sButtons);

      // Get out from this mode
      if (sButtons.bLeft) {
        u8runTask = 0;
        break;
      }

      // Increase LED brightness
      if (sButtons.bUp) {
        eLedLevel = ledIncPwmLvl(eLedLevel);
        // And save setting to NVS
        ESP_ERROR_CHECK(nvsSettingsSet(_NVS_DETECTOR_LED_ID,
                                       (uint8_t *)&eLedLevel, sizeof(uint8_t)));
      }

      // Decrease LED brightness
      if (sButtons.bDown) {
        eLedLevel = ledDecPwmLvl(eLedLevel);
        // And save setting to NVS
        ESP_ERROR_CHECK(nvsSettingsSet(_NVS_DETECTOR_LED_ID,
                                       (uint8_t *)&eLedLevel, sizeof(uint8_t)));
      }
    }

    // Check if UI requested exit
    if (taskGetForceCloseFlag()) {
      break;
    }
    // Keep scanning keyboard within some intervals. Take a nap
    vTaskDelay(KBRD_RECOMMENDED_SCAN_TIME_MS / portTICK_PERIOD_MS);
  }

  // Shutdown WiFi feature
  ESP_ERROR_CHECK(wifiDeInit());

  _exitModeTermTask();
}

// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
static void _prepareDisplayDataSetBuzzerGaugeLeds(
    wifi_ap_record_t asApInfo[static WIFI_SCAN_LIST_SIZE],
    const uint16_t u16apCount, tsUiServiceMacList *psMacList,
    tsUiDispDetector *psUiDispDetector) {
  // Pointers should not be empty
  assert(asApInfo);
  assert(psMacList);
  assert(psUiDispDetector);

  uint16_t u16anomalyFoundIdx;
  int8_t i8latestRssi;

  // Pointer to pointer. Need actual pointer to keep "pointer" value somewhere
  char *pacAnomalyDescription;
  char **ppacAnomalyDescription = &pacAnomalyDescription;

  assert(ppacAnomalyDescription);

  // Check if any anomaly was found in first place.
  if (!_isAnomalyFound(asApInfo, u16apCount, psMacList, &u16anomalyFoundIdx,
                       ppacAnomalyDescription)) {
    // Anomaly not found -> clean SSID of first AP (just to re-use
    // memory) and set pointer. Noting more is required now.
    psUiDispDetector->bDetected = false;
    asApInfo[0].ssid[0] = 0;
    psUiDispDetector->pacSSIDorDescription = (char *)asApInfo[0].ssid;

    // Push "empty" value to RSSI variable. Put super low RSSI there.
    // Ideally lowest possible value
    i8latestRssi = WIFI_MIN_RSSI;
  } else {
    // Anomaly detected - store pointer to SSID
    psUiDispDetector->bDetected = true;
    psUiDispDetector->pacSSIDorDescription =
        (char *)asApInfo[u16anomalyFoundIdx].ssid;

    // If SSId is empty, use description
    if (strlen(psUiDispDetector->pacSSIDorDescription) == 0) {
      psUiDispDetector->pacSSIDorDescription = *ppacAnomalyDescription;
    }

    i8latestRssi = asApInfo[u16anomalyFoundIdx].rssi;
  }

  ringBuffPush(&psUiDispDetector->sRssiRingBuff, &i8latestRssi);

  // Get latest value from ring buffer and display it
  if (gaugeMeterSetRssi(i8latestRssi)) {
    ESP_LOGE(TAG, "Settings gauge vmeter failed");
  }
  // Keep beeping via buzzer
  buzzerModeGeigerWiFiRssi(i8latestRssi);

  // Reset read index in ring buffer (used for display)
  ringBuffResetReadIdx(&psUiDispDetector->sRssiRingBuff);

  // Evaluate warning level
  _updateWarningLevel(&psUiDispDetector->eWarningLvl, i8latestRssi);
}

static bool _isAnomalyFound(
    const wifi_ap_record_t asApInfo[static WIFI_SCAN_LIST_SIZE],
    const uint16_t u16apCount, tsUiServiceMacList *psMacList,
    uint16_t *pu16anomalyIdxAp, char **ppacAnomalyDescription) {
  assert(pu16anomalyIdxAp);
  assert(pu16anomalyIdxAp);
  assert(ppacAnomalyDescription);

  // Go through all found results from top to bottom
  int8_t i8lastRssi = 0;

  for (uint16_t u16foundApIdx = 0; u16foundApIdx < u16apCount;
       u16foundApIdx++) {
    // Some paranoia check
    if (asApInfo[u16foundApIdx].rssi > i8lastRssi) {
      ESP_LOGE(TAG, "APs are not sorted by signal strength!");
      assert(0);
    }
    i8lastRssi = asApInfo[u16foundApIdx].rssi;

    for (uint16_t u16anomalyIdx = 0; u16anomalyIdx < UI_SERVICE_MAX_MAC_STORED;
         u16anomalyIdx++) {
      if (areMacSame(asApInfo[u16foundApIdx].bssid,
                     psMacList->au8macAddr[u16anomalyIdx])) {
        // Store index
        *pu16anomalyIdxAp = u16foundApIdx;

        *ppacAnomalyDescription =
            &psMacList->acMacDescription[u16anomalyIdx][0];

        // Make sure that pointer is really set
        assert(*ppacAnomalyDescription);

        return true;
      }
    }
  }

  return false;
}

static void _updateWarningLevel(
    teUiAnomalyDetectorWarningThresholds *peDetectorWarning,
    const int8_t i8rssi) {
  // Pointer should not be empty
  assert(peDetectorWarning);
  // RSSI can not be zero or higher
  assert(i8rssi < 0);

  // Keep track when anomaly was detected last time
  static uint32_t u32anomalyDetectedMs = 0;

  // Check level by level. If found
  if (i8rssi >= UI_ANOMALY_DETECTOR_DEAD) {
    if (*peDetectorWarning != UI_ANOMALY_DETECTOR_DEAD) {
      *peDetectorWarning = UI_ANOMALY_DETECTOR_DEAD;

      // Treat this one bit special way
      ESP_LOGI(TAG, "Anomaly critically close");
    } else {
      // Already beeped
    }

  } else if (i8rssi >= UI_ANOMALY_DETECTOR_3RD_WARNING) {
    if (*peDetectorWarning != UI_ANOMALY_DETECTOR_3RD_WARNING) {
      *peDetectorWarning = UI_ANOMALY_DETECTOR_3RD_WARNING;

      ESP_LOGI(TAG, "Anomaly - 3rd warning");
    } else {
      // Already beeped
    }
  } else if (i8rssi >= UI_ANOMALY_DETECTOR_2ND_WARNING) {
    if (*peDetectorWarning != UI_ANOMALY_DETECTOR_2ND_WARNING) {
      *peDetectorWarning = UI_ANOMALY_DETECTOR_2ND_WARNING;

      ESP_LOGI(TAG, "Anomaly - 2nd warning");
    } else {
      // Already beeped
    }
  } else if (i8rssi >= UI_ANOMALY_DETECTOR_1ST_WARNING) {
    if (*peDetectorWarning != UI_ANOMALY_DETECTOR_1ST_WARNING) {
      *peDetectorWarning = UI_ANOMALY_DETECTOR_1ST_WARNING;

      ESP_LOGI(TAG, "Anomaly - 1st warning");
    } else {
      // Already beeped
    }
  } else {
    // No warning level
  }

  // Check if anomaly not detected for some time. If so, reset warnings
  if (i8rssi <= WIFI_MIN_RSSI) {
    if ((GET_CLK_MS_32BIT() - u32anomalyDetectedMs) >
        _UI_ANOMALY_DETECTOR_ANOMALY_FORGET_TIME_MS) {
      // Anomaly lost or not found -> reset detector warning threshold
      *peDetectorWarning = UI_ANOMALY_DETECTOR_NO_WARNING;
    }

  } else {
    // Write timestamp of found anomaly
    u32anomalyDetectedMs = GET_CLK_MS_32BIT();
  }
}

static void _warnUserIfNeeded(
    teUiAnomalyDetectorWarningThresholds *peDetectorWarning) {
  // Pointer should not be empty
  assert(peDetectorWarning);

  static teUiAnomalyDetectorWarningThresholds ePreviousWarning =
      UI_ANOMALY_DETECTOR_NO_WARNING;

  // If there is warning level change
  if (ePreviousWarning < *peDetectorWarning) {
    ePreviousWarning = *peDetectorWarning;

    switch (*peDetectorWarning) {
      case UI_ANOMALY_DETECTOR_NO_WARNING:
        // Nothing to do actually
        break;
      case UI_ANOMALY_DETECTOR_1ST_WARNING:
        _beepWarning(1);
        break;
      case UI_ANOMALY_DETECTOR_2ND_WARNING:
        _beepWarning(2);
        break;
      case UI_ANOMALY_DETECTOR_3RD_WARNING:
        _beepWarning(3);
        break;
      case UI_ANOMALY_DETECTOR_DEAD:
        // Long beep - you're dead man
        buzzerModeContinuous(true);
        // Set also LED
        ledTurnOffAll();
        ledRGBsetR(LED_LVL_MAX);

        // Super long beep - 4x times base one
        vTaskDelay(4 * _UI_ANOMALY_DETECTOR_DEFAULT_WARNING_BEEP_DURATION_MS /
                   portTICK_PERIOD_MS);

        buzzerModeContinuous(false);
        ledTurnOffAll();
        break;
      default:
        // This should not happen, but if so...
        ESP_LOGE(TAG, "Unexpected detector warning value: %d",
                 *peDetectorWarning);
    }
  } else if (*peDetectorWarning == UI_ANOMALY_DETECTOR_NO_WARNING) {
    // No danger anymore -> store value
    ePreviousWarning = UI_ANOMALY_DETECTOR_NO_WARNING;
  } else {
    // No warning change, but still in danger zone
  }
}

static void _setLedColorAccordingToWarningLevel(
    teUiAnomalyDetectorWarningThresholds eDetectorWarning,
    teLedRGBlevel eLedLevel) {
  if (eDetectorWarning <= UI_ANOMALY_DETECTOR_NO_WARNING) {
    // Safe distance
    ledRGBsetG(eLedLevel);
  } else if (eDetectorWarning <= UI_ANOMALY_DETECTOR_2ND_WARNING) {
    // Bit dangerous
    ledRGBsetG(eLedLevel);
    ledRGBsetR(eLedLevel);
  } else {
    // Direct danger
    ledRGBsetR(eLedLevel);
  }
}

static void _beepWarning(const uint8_t u8count) {
  // Set LED to Orange
  ledTurnOffAll();
  ledRGBsetR(LED_LVL_MAX);
  ledRGBsetG(LED_LVL_MAX);

  for (uint8_t u8beepCounter = 0; u8beepCounter < u8count; u8beepCounter++) {
    buzzerModeContinuous(true);

    vTaskDelay(_UI_ANOMALY_DETECTOR_DEFAULT_WARNING_BEEP_DURATION_MS /
               portTICK_PERIOD_MS);

    buzzerModeContinuous(false);

    // If this is last beep, do not add extra delay
    if (u8beepCounter != (u8count - 1)) {
      vTaskDelay(_UI_ANOMALY_DETECTOR_DEFAULT_WARNING_BEEP_DURATION_MS /
                 _UI_ANOMALY_DETECTOR_DEFAULT_WARNING_BEEP_PAUSE_DURATION_DIV /
                 portTICK_PERIOD_MS);
    }
  }
  ledTurnOffAll();
}

static void _exitModeTermTask(void) {
  // Off buzzer
  buzzerOff();

  // Set voltage on gauge back to zero
  gaugeMeterOff();

  // Off all lights
  ledTurnOffAll();

  taskSetModeExit();
  // Terminate itself
  vTaskDelete(NULL);
}
