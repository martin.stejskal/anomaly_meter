/**
 * @file
 * @author Martin Stejskal
 * @brief Compass mode
 */
#ifndef __UI_COMPASS_H__
#define __UI_COMPASS_H__
// ===============================| Includes |================================
#include <esp_err.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
void uiModeCompassTaskRTOS(void *pvParameters);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
esp_err_t uiModeCompassResetLedBright(void);
#endif  // __UI_COMPASS_H__
