/**
 * @file
 * @author Martin Stejskal
 * @brief Acceleration mode with graph
 */
#ifndef __UI_MODE_ACC_GRAPH_H__
#define __UI_MODE_ACC_GRAPH_H__
// ===============================| Includes |================================
#include <esp_err.h>
#include <stdbool.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
void uiModeAccTaskRTOS(void *pvParameters);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
#endif  // __UI_MODE_ACC_GRAPH_H__
