/**
 * @file
 * @author Martin Stejskal
 * @brief Anomaly detector mode
 */
#ifndef __UI_ANOMALY_DETECTOR_H__
#define __UI_ANOMALY_DETECTOR_H__
// ===============================| Includes |================================
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
void uiModeAnomalyDetectorTaskRTOS(void *pvParameters);
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
#endif  // __UI_ANOMALY_DETECTOR_H__
