/**
 * @file
 * @author Martin Stejskal
 * @brief Display functions for display
 */
#ifndef __UI_DISPLAY_H__
#define __UI_DISPLAY_H__
// ===============================| Includes |================================
#include <esp_err.h>
#include <esp_wifi_types.h>
#include <inttypes.h>
#include <stdbool.h>

// Due to mapping functions
#include "battery.h"
#include "led_rgb.h"
#include "pcd8544.h"
#include "ring_buff.h"
#include "ui_modes_common.h"
// Vector things
#include "sensors_common.h"
// ================================| Defines |================================
/**
 * @brief Map some definitions from LCD driver directly
 *
 * @{
 */
#define UI_DISP_X_RES PCD8544_X_RES
#define UI_DISP_Y_RES PCD8544_Y_RES
#define UI_DISP_Y_RES_ROW PCD8544_Y_RES_ROW
#define UI_DISP_LINES PCD8544_LINES
#define UI_DISP_FONT_WIDTH_PX PCD8544_FONT_WIDTH_PX
#define UI_DISP_FONT_HEIGHT_PX PCD8544_FONT_HEIGHT_PX
/**
 * @}
 */

/**
 * @brief Map some functionality from LCD driver directly
 *
 * @{
 */
#define uiDispIsHwInitialized() pcd8544isInitialized()

#define uiDispGetBacklightStatus() pcd8544getBacklightStatus()
#define uiDispSetBacklight(bOn, bSave) pcd8544setBacklight(bOn, bSave)

// Some low level stuff
#define uiDispClearFb() pcd8544clearFb()
#define uiDispWriteFbToDisplay() pcd8544writeFbToLCD()

#define uiDispPuts(u8xPos, u8row, pacText) pcd8544putsFb(pacText, u8xPos, u8row)

#define uiDispPrintFb(u8xPos, u8row, format, ...) \
  pcd8544printfFb(u8xPos, u8row, __VA_ARGS__)

#define uiDispSetbytesInFb(u8xPos, u8row, pu8bytes, u16numOfBytes) \
  pcd8544setBytesInFb(pu8bytes, u16numOfBytes, u8xPos, u8row, WRITE_SET)
/**
 * @}
 */
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef struct {
  // Pointer to string which contain SSID
  char *pacSSIDorDescription;

  // This is just ring buffer "settings"
  tsRingBuffer sRssiRingBuff;

  // Current warning level
  teUiAnomalyDetectorWarningThresholds eWarningLvl;

  // Anomaly detected
  bool bDetected;
} tsUiDispDetector;
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Initialize display
 * @return ESP_OK if no error detected
 */
esp_err_t uiDispInit(void);

/**
 * @brief Deinitialize display
 * @return ESP_OK if no error detected
 */
esp_err_t uiDispDeInit(void);

/**
 * @brief Show current mode
 * @param eMode Displayed mode
 * @param asModesCfg Configurations for every mode
 * @return ESP_OK if no error
 */
esp_err_t uiDispShowMode(teUiModes eMode,
                         tsUiModeCfg asModesCfg[static UI_MODE_MAX]);

esp_err_t uiDispDetector(tsUiDispDetector *psDetectorData,
                         tsBattStatus *psBattStatus);

/**
 * @brief Show battery status
 * @param[in, out] psStatus Battery status structure
 * @param u32zoom Zoom value multiplied by 1000. So value 1500 means zoom 1.5x
 * @return ESP_OK if no error
 */
esp_err_t uiDispBattInfo(tsBattStatus *psStatus, uint32_t u32zoom);

/**
 * @brief Show set brightness in flashlight mode
 * @param eLedBrightness Set brightness
 * @return ESP_OK if no error
 */
esp_err_t uiDispFlashlight(teLedRGBlevel eLedBrightness);

esp_err_t uiDispCompass(tsVector32 sSumMagDataVector,
                        const uint16_t u16numOfMagSamples,
                        tsVector16 sAccVector,
                        const teLedRGBlevel eLedMaxLevel);

/**
 * @brief Show acceleration data on display
 * @param psRingBuff Pointer to source ring buffer
 * @param psVectDivFactor Pointer to vector division factor
 * @return ESP_OK if no error
 */
esp_err_t uiDispVectGraphRow(tsRingBuffer *psRingBuff,
                             tsVector16 *psVectDivFactor);
// ========================| Middle level functions |=========================
esp_err_t uiDispDrawMenu(const char *pacHead, const char **pacOptions,
                         uint8_t u8currentOptIdx, uint8_t u8maxOptIdx);
// ==========================| Low level functions |==========================
esp_err_t uiDispDrawCircle(const uint8_t u8centerX, const uint8_t u8centerY,
                           const uint8_t u8size);

#endif  // __UI_DISPLAY_H__
