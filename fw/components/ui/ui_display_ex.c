/**
 * @file
 * @author Martin Stejskal
 * @brief Extensions to UI display module
 */
// ===============================| Includes |================================
#include "ui_display_ex.h"

#include "pcd8544.h"

// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
esp_err_t uiDispExBargraph(const uint8_t u8xPos, const uint8_t u8yPos,
                           const int8_t i8height, const uint8_t u8width) {
  if (u8width == 0) {
    return ESP_ERR_INVALID_ARG;
  }

  // Need to work with it as signed
  assert(u8yPos <= 127);

  esp_err_t eErrCode = ESP_OK;

  for (uint8_t u8widthDrawer = 0; u8widthDrawer < u8width; u8widthDrawer++) {
    // The Y coordinates are logically reversed on display. Higher number
    // means "lower pixel", but user expectation is opposite -> reason why
    // deducting height
    eErrCode |= pcd8544drawLineInFb(u8xPos + u8widthDrawer, u8yPos,
                                    u8xPos + u8widthDrawer,
                                    (int8_t)u8yPos - i8height, WRITE_SET);
  }

  return eErrCode;
}

esp_err_t uiDispEx8bitValTo8bitGraphInfill(uint8_t u8value, uint8_t *pu8graph) {
  assert(pu8graph);

  // Initial value of "graph"
  uint8_t u8graph = 0x80;

  // 256 / 8 = 32 (max value / bit resolution) -> step is 32
  // Threshold need to be 16 bit long to avoid overflow
  // (8 bit -> 32*8 = 256 -> 0x01), which would lead to infinite loop
  for (uint16_t u16threshold = 32; u16threshold < u8value;
       u16threshold = u16threshold + 32) {
    // If under threshold, shift right (LSB is actually at top of display
    // line) and add "infill"
    u8graph = u8graph >> 1 | 0x80;
  }

  // Set value
  *pu8graph = u8graph;

  return ESP_OK;
}

esp_err_t uiDispEx8bitValTo24bitGraphInfill(uint8_t u8value,
                                            uint8_t *pu8graphTop,
                                            uint8_t *pu8graphMid,
                                            uint8_t *pu8graphBottom) {
  assert(pu8graphTop);
  assert(pu8graphMid);
  assert(pu8graphBottom);

  // Temporary variable for easy preprocessing. Initial value means bottom
  // line for graph itself
  uint32_t u32graph = 0x00;

  // 256 / 24 = 10.667 -> at about 11.
  // Threshold have to be 16 bit long to avoid overflow
  for (uint16_t u16threshold = 11; u16threshold < u8value;
       u16threshold = u16threshold + 11) {
    // Add another pixel to "bottom" and shift
    u32graph = u32graph << 1 | 0x01;
  }

  // Now pixels have to be distributed to the 8 bit parts. Note that due to
  // nature of display, bit order have to be swapped
  *pu8graphTop = pcd8544swapBitsInByte((uint8_t)((u32graph >> 16) & 0xFF));
  *pu8graphMid = pcd8544swapBitsInByte((uint8_t)((u32graph >> 8) & 0xFF));
  *pu8graphBottom = pcd8544swapBitsInByte((uint8_t)(u32graph & 0xFF));

  return ESP_OK;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
esp_err_t uiDispExDrawCursorRight4px(uint8_t u8xPos, uint8_t u8row) {
  esp_err_t eErrCode;

  eErrCode = pcd8544setByteInFb(0xFF, u8xPos, u8row, WRITE_SET);
  eErrCode |= pcd8544setByteInFb(0x7E, u8xPos + 1, u8row, WRITE_SET);
  eErrCode |= pcd8544setByteInFb(0x3C, u8xPos + 2, u8row, WRITE_SET);
  eErrCode |= pcd8544setByteInFb(0x18, u8xPos + 3, u8row, WRITE_SET);

  return eErrCode;
}

esp_err_t uiDispExDrawBattery(uint8_t u8_row, uint8_t u8_x_pos,
                              uint8_t u8_percentage) {
  // This bitmap interprets empty battery icon
  uint8_t au8_bitmap[] = {0x7E, 0x43, 0x43, 0x43, 0x7E};

  // According to battery percentage, "fill" in battery image
  if (u8_percentage >= UI_DISP_EX_BATT_THRESHOLD_4) {
    au8_bitmap[1] = au8_bitmap[2] = au8_bitmap[3] = 0x7F;

  } else if (u8_percentage >= UI_DISP_EX_BATT_THRESHOLD_3) {
    au8_bitmap[1] = au8_bitmap[2] = au8_bitmap[3] = 0x7B;

  } else if (u8_percentage >= UI_DISP_EX_BATT_THRESHOLD_2) {
    au8_bitmap[1] = au8_bitmap[2] = au8_bitmap[3] = 0x73;

  } else if (u8_percentage >= UI_DISP_EX_BATT_THRESHOLD_1) {
    au8_bitmap[1] = au8_bitmap[2] = au8_bitmap[3] = 0x63;

  } else {
    // Keep battery image as it is -> empty battery
  }

  return (pcd8544setBytesInFb(au8_bitmap, sizeof(au8_bitmap), u8_x_pos, u8_row,
                              WRITE_SET));
}

esp_err_t uiDispExDrawPowerPlug(uint8_t u8row, uint8_t u8xPos) {
  const uint8_t au8bitmap[] = {0x1C, 0x27, 0x64, 0x27, 0x1C};

  return (pcd8544setBytesInFb(au8bitmap, sizeof(au8bitmap), u8xPos, u8row,
                              WRITE_SET));
}

esp_err_t uiDispExDrawDegreeSymbol(uint8_t u8xPos, uint8_t u8row) {
  esp_err_t eErrCode;
  eErrCode = pcd8544setByteInFb(0x02, u8xPos, u8row, WRITE_SET);
  eErrCode |= pcd8544setByteInFb(0x05, u8xPos + 1, u8row, WRITE_SET);
  eErrCode |= pcd8544setByteInFb(0x02, u8xPos + 2, u8row, WRITE_SET);

  return eErrCode;
}
// ==========================| Internal functions |===========================
