#ifndef __BITMAPS_H__
#define __BITMAPS_H__

const static uint8_t gu8logo[] = {
    0x00, 0x00, 0x80, 0x80, 0x80, 0x00, 0x00, 0x0e, 0x3e, 0x4e, 0x80, 0x00,
    0x00, 0x00, 0xc0, 0xc0, 0xc0, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x40, 0x20, 0x90, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50,
    0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x50, 0x48,
    0xe8, 0xe8, 0xe8, 0x08, 0x08, 0x08, 0x10, 0xe0, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x80, 0x40, 0x20, 0x10, 0x10, 0x10, 0x10, 0xb8, 0xb8, 0xb8, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0xf8, 0x04, 0x03, 0x03, 0x03, 0x00, 0x80, 0xc0, 0xe0, 0x70, 0x30, 0x31,
    0x32, 0x34, 0x35, 0x35, 0x35, 0x34, 0x34, 0x34, 0x34, 0x34, 0x34, 0x34,
    0x34, 0x34, 0x34, 0x32, 0x31, 0x30, 0x30, 0x37, 0x3f, 0x37, 0x30, 0x30,
    0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30,
    0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x3f, 0x30, 0x30, 0x30, 0x30,
    0x3f, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x33, 0x33, 0x33, 0x31,
    0x31, 0x31, 0x31, 0x31, 0x31, 0xf1, 0xf1, 0x01, 0x02, 0x04, 0xf8, 0x00,
    0xff, 0x00, 0x00, 0x00, 0x00, 0xff, 0xff, 0x01, 0x00, 0x80, 0xe0, 0xfe,
    0xff, 0x1f, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07, 0x07,
    0xff, 0xff, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0xfc, 0xfe, 0xff, 0xc7,
    0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7,
    0xff, 0xfe, 0xfc, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x83, 0xc7, 0xc7,
    0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7, 0xc7,
    0xff, 0xff, 0xff, 0x00, 0x00, 0xff, 0xff, 0x00, 0x00, 0x00, 0xff, 0x00,
    0x01, 0x02, 0x04, 0x08, 0x08, 0xff, 0xff, 0x00, 0x7e, 0x3f, 0x1f, 0x0f,
    0x0e, 0x0e, 0x0e, 0x0e, 0x0e, 0x0e, 0x0e, 0x0e, 0x0e, 0x0e, 0x0e, 0x0e,
    0x1f, 0x3f, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x7f, 0x3f, 0x1f, 0x01,
    0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01,
    0x1f, 0x3f, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0x60, 0x71, 0x71,
    0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71, 0x71,
    0x7f, 0x7f, 0x7f, 0x00, 0x80, 0xff, 0xff, 0x00, 0x00, 0x80, 0x7f, 0x00,
    0x00, 0x70, 0x70, 0x70, 0x20, 0x27, 0x27, 0x26, 0x26, 0x26, 0x26, 0x26,
    0x26, 0x26, 0x26, 0x26, 0x46, 0x86, 0x06, 0x06, 0xe6, 0xe6, 0xe6, 0x46,
    0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46, 0x46,
    0x46, 0x46, 0x26, 0x26, 0x26, 0x26, 0x26, 0x26, 0x26, 0xc6, 0x06, 0x06,
    0x06, 0x3e, 0x46, 0x86, 0x06, 0x06, 0x06, 0xc6, 0xe6, 0xd6, 0x16, 0x16,
    0x16, 0x16, 0x16, 0x16, 0x16, 0x16, 0x16, 0x1e, 0x06, 0xfe, 0x06, 0x06,
    0x06, 0x06, 0x06, 0xe7, 0x13, 0x09, 0x04, 0x02, 0x01, 0x1c, 0xfc, 0x1c,
    0xbf, 0xf1, 0x55, 0xf1, 0x9f, 0x6a, 0xad, 0xd9, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x02, 0x02, 0x02, 0x02,
    0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02, 0x02,
    0x02, 0x02, 0x02, 0x02, 0x07, 0x07, 0x07, 0x00, 0x07, 0x07, 0x07, 0x00,
    0x07, 0x1f, 0x27, 0x40, 0xb9, 0xbe, 0xb8, 0x81, 0x81, 0x81, 0x80, 0x80,
    0x80, 0x80, 0x80, 0x80, 0x87, 0x9f, 0xa7, 0xa0, 0xa7, 0xa7, 0xa7, 0xa0,
    0xa0, 0x90, 0x88, 0x87, 0x80, 0x80, 0x80, 0x40, 0x20, 0x10, 0x0f, 0x00,
};

const static uint8_t gu8magCalibrating[] = {
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xc0, 0x60, 0x20, 0x10, 0x10,
    0x18, 0x08, 0x08, 0x08, 0x08, 0x04, 0x04, 0x04, 0x04, 0x08, 0x08, 0x08,
    0x08, 0x08, 0x10, 0x10, 0x20, 0x60, 0xc0, 0x80, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x10, 0x30, 0x70, 0xf8, 0x76, 0x31, 0x10, 0x00, 0x00, 0x00, 0x00,
    0xf8, 0x04, 0xf4, 0x94, 0x94, 0x94, 0xd4, 0xb4, 0xb4, 0x94, 0xf4, 0x04,
    0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x10, 0x31, 0x7e, 0xf8, 0x70, 0x30,
    0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0x80, 0x80, 0x40, 0x40, 0x40,
    0x40, 0x40, 0x40, 0x40, 0xf8, 0xf0, 0xe0, 0x40, 0x00, 0x00, 0x00, 0x00,
    0xff, 0x02, 0x02, 0x7a, 0x7a, 0x7a, 0x7a, 0x7a, 0x7a, 0x7a, 0x02, 0x02,
    0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x40, 0xe0, 0xf0, 0xf8, 0x40, 0x40,
    0x40, 0x40, 0x40, 0x40, 0x40, 0x80, 0x80, 0x80, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x04, 0x0b, 0x11, 0x20, 0x60, 0x40, 0xc0, 0x80, 0x80,
    0x00, 0x00, 0x00, 0x00, 0x03, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x3f, 0x40, 0x48, 0x54, 0x48, 0x40, 0x40, 0x48, 0x5c, 0x48, 0x40, 0x40,
    0x3f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x00, 0x00,
    0x80, 0x80, 0xc0, 0x40, 0x40, 0x20, 0x30, 0x10, 0x0d, 0x06, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x01, 0x01, 0x01, 0x02, 0x02, 0x02, 0x02, 0x04, 0x04, 0x04, 0x04, 0x04,
    0x04, 0x04, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08, 0x04, 0x04, 0x04,
    0x04, 0x04, 0x04, 0x04, 0x02, 0x02, 0x02, 0x02, 0x02, 0x01, 0x01, 0x01,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};

#endif
