/**
 * @file
 * @author Martin Stejskal
 * @brief Settings mode for user
 */
#ifndef __UI_SETTINGS_H__
#define __UI_SETTINGS_H__
// ===============================| Includes |================================
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
void uiModeSettingsRTOS(void *pvParameters);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
#endif  // __UI_SETTINGS_H__
