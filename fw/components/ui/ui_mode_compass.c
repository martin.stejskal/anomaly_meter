/**
 * @file
 * @author Martin Stejskal
 * @brief Compass mode
 */
// ===============================| Includes |================================
#include "ui_mode_compass.h"

#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

// ESP specific
#include <esp_log.h>

// Local modules
#include "cfg.h"
#include "keyboard.h"
#include "led_rgb.h"
#include "nvs_settings.h"
#include "sensors.h"
#include "ui_display.h"
#include "ui_modes_common.h"
// ================================| Defines |================================
/**
 * @brief Number of samples from magnetometer kept in buffer
 */
#define _NUM_OF_SAMPLES (50)

/**
 * @brief ID for NVS when loading LED brightness
 */
#define _NVS_COMPASS_LED_ID "CompLedPwm"
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "Compass";
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
void uiModeCompassTaskRTOS(void *pvParameters) {
  ESP_LOGI(TAG, "Executing compass mode...");

  tsVector16 sMagDataRaw;

  // Register pressed buttons
  tsKbrdBtns sButtons = {0};

  // Buffer + ring buffer structure ("configuration")
  tsVector16 asBufferSamples[_NUM_OF_SAMPLES];
  tsRingBuffer sRingBuffSamples = {
      .pvBuffer = asBufferSamples,
      .u16bufferSizeBytes = sizeof(asBufferSamples),
      .u8itemSizeBytes = sizeof(tsVector16)};

  // Temporary vector for sum from values in ring buffer
  tsVector32 sTmpVect32;

  // Vector for keeping acceleration data
  tsVector16 sAccRaw;

  uint32_t u32actualTimeMs;
  uint32_t u32scanKbrdShowDataMs = 0;

  // Load LED brightness
  teLedRGBlevel eLedLevel;

  const teLedRGBlevel eLedDefaultPwm = LED_LVL_5;

  // Loading should not fail, otherwise it signalize something fatal. As
  // default use something decent (2nd parameter)
  ESP_ERROR_CHECK(nvsSettingsGet(_NVS_COMPASS_LED_ID, (uint8_t *)&eLedLevel,
                                 (uint8_t *)&eLedDefaultPwm, sizeof(uint8_t)));

  // Clean ring buffer
  ringBuffClean(&sRingBuffSamples);

  while (1) {
    u32actualTimeMs = GET_CLK_MS_32BIT();

    // Load data and display them
    ESP_ERROR_CHECK(sensMagGetInt(&sMagDataRaw));

    // Store magnetic vector in ring buffer
    ringBuffPush(&sRingBuffSamples, &sMagDataRaw);

    if (u32actualTimeMs >= u32scanKbrdShowDataMs) {
      u32scanKbrdShowDataMs = u32actualTimeMs + KBRD_RECOMMENDED_SCAN_TIME_MS;

      // Load acceleration data
      ESP_ERROR_CHECK(sensAccGetInt(&sAccRaw));

      // To get average, typically is required sum all values and then divide
      // them by number of records. However in this case we just need to know
      // vector direction -> do not care about size.
      memset(&sTmpVect32, 0, sizeof(sTmpVect32));
      for (uint8_t u8cnt = 0; u8cnt < _NUM_OF_SAMPLES; u8cnt++) {
        sTmpVect32.i32x += (int32_t)asBufferSamples[u8cnt].i16x;
        sTmpVect32.i32y += (int32_t)asBufferSamples[u8cnt].i16y;
        sTmpVect32.i32z += (int32_t)asBufferSamples[u8cnt].i16z;
      }

      ESP_ERROR_CHECK(
          uiDispCompass(sTmpVect32, _NUM_OF_SAMPLES, sAccRaw, eLedLevel));

      // Keyboard test
      kbrPressedReleased(&sButtons);
      // Get out from this mode
      if (sButtons.bLeft) {
        break;
      }

      // Increase LED brightness
      if (sButtons.bUp) {
        eLedLevel = ledIncPwmLvl(eLedLevel);
        // And save setting to NVS
        ESP_ERROR_CHECK(nvsSettingsSet(_NVS_COMPASS_LED_ID,
                                       (uint8_t *)&eLedLevel, sizeof(uint8_t)));
      }

      // Decrease LED brightness
      if (sButtons.bDown) {
        eLedLevel = ledDecPwmLvl(eLedLevel);
        // And save setting to NVS
        ESP_ERROR_CHECK(nvsSettingsSet(_NVS_COMPASS_LED_ID,
                                       (uint8_t *)&eLedLevel, sizeof(uint8_t)));
      }

      // Check if UI request exit this task
      if (taskGetForceCloseFlag()) {
        break;
      }
    }

    // Keep scanning keyboard within some intervals. Take a nap
    vTaskDelay(2);
  }

  // Turn off all LED
  ledTurnOffAll();

  taskSetModeExit();
  // Terminate itself
  vTaskDelete(NULL);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
esp_err_t uiModeCompassResetLedBright(void) {
  return nvsSettingsDel(_NVS_COMPASS_LED_ID);
}
// ==========================| Internal functions |===========================
