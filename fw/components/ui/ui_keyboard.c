/**
 * @file
 * @author Martin Stejskal
 * @brief High level keyboard functionality for UI
 */
// ===============================| Includes |================================
#include "ui_keyboard.h"

#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <stdint.h>

#include "cfg.h"
#include "err_handlers.h"
#include "keyboard.h"
#include "ui_display.h"
#include "ui_display_ex.h"
#include "ui_modes_common.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static esp_err_t _displayOkCancel(teKbrdOkCancel eSelectedOption,
                                  const char *pacMsg);
// =========================| High level functions |==========================
esp_err_t uiKbrdMenu(tsKbrdMenuParams *psParams) {
  assert(psParams);

  // Register pressed buttons
  tsKbrdBtns sButtons = {0};

  esp_err_t eErrCode = ESP_OK;

  // Callback - before main loop. Typically things for display. If not empty,
  // call it
  if (psParams->pfPreLoop) {
    RET_WHEN_ERR(psParams->pfPreLoop(psParams->pvPreLoopParam));
  }

  while (1) {
    kbrPressedReleased(&sButtons);
    if (kbrdAnyButtonPressed(&sButtons)) {
      kbrdShowPressedButtons(&sButtons);

      // Get out from this mode
      if (sButtons.bLeft) {
        break;
      }

      if (sButtons.bRight || sButtons.bEnter) {
        // Selected option. If pointer is not empty, call function
        if (psParams->pfEnter) {
          RET_WHEN_ERR(psParams->pfEnter(psParams->pvEnterParam));
        }
        if (psParams->bExitAfterEnterCallback) {
          break;
        }
      }

      if (sButtons.bUp) {
        if (psParams->pfUp) {
          RET_WHEN_ERR(psParams->pfUp(psParams->pvUpParam));
        }
      }

      if (sButtons.bDown) {
        if (psParams->pfDown) {
          RET_WHEN_ERR(psParams->pfDown(psParams->pvDownParam));
        }
      }

      // Callback - if set
      if (psParams->pfAnyBtn) {
        RET_WHEN_ERR(psParams->pfAnyBtn(psParams->pvAnyBtnParam));
      }
    }

    // Check if UI request exit this task
    if (taskGetForceCloseFlag()) {
      break;
    }
    // Keep scanning keyboard within some intervals. Take a nap
    vTaskDelay(KBRD_RECOMMENDED_SCAN_TIME_MS / portTICK_PERIOD_MS);
  }

  // If there should be any "post loop" callback
  if (psParams->pfPostLoop) {
    RET_WHEN_ERR(psParams->pfPostLoop(psParams->pvPostLoopParam));
  }

  return eErrCode;
}

esp_err_t uiKbrdOkCancel(teKbrdOkCancel *peSelectedOption, const char *pacMsg) {
  assert(peSelectedOption && pacMsg);
  assert((*peSelectedOption == KBRD_OK) || (*peSelectedOption == KBRD_CANCEL));

  // Register pressed buttons
  tsKbrdBtns sButtons = {0};

  esp_err_t eErrCode = ESP_OK;

  // Print message to display
  RET_WHEN_ERR(_displayOkCancel(*peSelectedOption, pacMsg));

  while (1) {
    kbrPressedReleased(&sButtons);
    if (kbrdAnyButtonPressed(&sButtons)) {
      kbrdShowPressedButtons(&sButtons);

      // Get out from this mode
      if (sButtons.bLeft) {
        *peSelectedOption = KBRD_CANCEL;
        break;
      }

      if (sButtons.bRight || sButtons.bEnter) {
        // Just use options which is set
        break;
      }

      // There are only 2 options -> simply always invert current one.
      // Does not matter if button "up"or "down" was pressed
      if (sButtons.bUp || sButtons.bDown) {
        switch (*peSelectedOption) {
          case KBRD_OK:
            *peSelectedOption = KBRD_CANCEL;
            break;
          case KBRD_CANCEL:
            *peSelectedOption = KBRD_OK;
            break;
        }

        // Either way, render change
        RET_WHEN_ERR(_displayOkCancel(*peSelectedOption, pacMsg));
      }
    }

    // Check if UI request exit this task
    if (taskGetForceCloseFlag()) {
      break;
    }
    // Keep scanning keyboard within some intervals. Take a nap
    vTaskDelay(KBRD_RECOMMENDED_SCAN_TIME_MS / portTICK_PERIOD_MS);
  }

  return eErrCode;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
static esp_err_t _displayOkCancel(teKbrdOkCancel eSelectedOption,
                                  const char *pacMsg) {
  esp_err_t eErrCode = ESP_FAIL;
  // Set some crap value - if there is bug, easier to find root
  uint8_t u8arrowXpos = UI_DISP_X_RES / 2;

  RET_WHEN_ERR(uiDispClearFb());
  // Write message from beginning of display
  RET_WHEN_ERR(uiDispPuts(0, 0, pacMsg));

  // Render "OK" and "Cancel"
  RET_WHEN_ERR(
      uiDispPuts(UI_DISP_FONT_WIDTH_PX * 1, UI_DISP_LINES - 1, "[OK]"));
  RET_WHEN_ERR(
      uiDispPuts(UI_DISP_FONT_WIDTH_PX * 8, UI_DISP_LINES - 1, "[ X ]"));

  // Draw arrow according to the selected option
  switch (eSelectedOption) {
    case KBRD_OK:
      u8arrowXpos = 0;
      break;
    case KBRD_CANCEL:
      u8arrowXpos = UI_DISP_FONT_WIDTH_PX * 8 - 4;
      break;
  }

  RET_WHEN_ERR(uiDispExDrawCursorRight4px(u8arrowXpos, UI_DISP_LINES - 1));

  RET_WHEN_ERR(uiDispWriteFbToDisplay());

  return eErrCode;
}
