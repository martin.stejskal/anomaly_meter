/**
 * @file
 * @author Martin Stejskal
 * @brief Extensions to UI display module
 */
#ifndef __UI_DISPLAY_EX_H__
#define __UI_DISPLAY_EX_H__
// ===============================| Includes |================================
// Standard
#include <inttypes.h>

// ESP specific
#include <esp_err.h>
// ================================| Defines |================================
/**
 * @brief Battery thresholds in percentage
 *
 * When displaying battery icon, these thresholds will decide how "full"
 * rendered battery will be.
 *
 * @{
 */
#define UI_DISP_EX_BATT_THRESHOLD_1 (20)
#define UI_DISP_EX_BATT_THRESHOLD_2 (40)
#define UI_DISP_EX_BATT_THRESHOLD_3 (60)
#define UI_DISP_EX_BATT_THRESHOLD_4 (80)
/**
 * @}
 */
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===============================| Functions |===============================

// =========================| High level functions |==========================
esp_err_t uiDispExBargraph(const uint8_t u8xPos, const uint8_t u8yPos,
                           const int8_t i8height, const uint8_t u8width);
esp_err_t uiDispEx8bitValTo8bitGraphInfill(uint8_t u8value, uint8_t *pu8graph);
esp_err_t uiDispEx8bitValTo24bitGraphInfill(uint8_t u8value,
                                            uint8_t *pu8graphTop,
                                            uint8_t *pu8graphMid,
                                            uint8_t *pu8graphBottom);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
esp_err_t uiDispExDrawCursorRight4px(uint8_t u8xPos, uint8_t u8row);
esp_err_t uiDispExDrawBattery(uint8_t u8_row, uint8_t u8_x_pos,
                              uint8_t u8_percentage);
esp_err_t uiDispExDrawPowerPlug(uint8_t u8row, uint8_t u8xPos);
esp_err_t uiDispExDrawDegreeSymbol(uint8_t u8xPos, uint8_t u8row);

#endif  // __UI_DISPLAY_EX_H__
