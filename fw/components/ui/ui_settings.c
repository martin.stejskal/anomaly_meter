/**
 * @file
 * @author Martin Stejskal
 * @brief Settings mode for user
 */
// ===============================| Includes |================================
#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <stdint.h>

// ESP specific
#include <esp_log.h>

#include "bitmaps.h"
#include "cfg.h"
#include "err_handlers.h"
#include "keyboard.h"  // Low level keyboard stuff
#include "nvs_settings.h"
#include "sensors.h"
#include "ui_display.h"
#include "ui_keyboard.h"
#include "ui_modes_common.h"
#include "ui_settings.h"
// ================================| Defines |================================
#define GET_NUM_OF_OPTS(pacOpts) (sizeof(pacOpts) / sizeof(pacOpts[0]))

#define GET_MAX_OPT_IDX(pacOpts) ((GET_NUM_OF_OPTS(pacOpts)) - 1)

// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  TOP_MENU_DISPLIGHT_LIGHT,
  TOP_MENU_COMPASS_CALIBRATION,
  TOP_MENU_BATTERY,
  TOP_MENU_MAX
} teTopMenuOrder;

typedef enum {
  SUB_MENU_DISPLAY_LIGHT_ON,
  SUB_MENU_DISPLAY_LIGHT_OFF,
  SUB_MENU_DISPLAY_LIGHT_AUTO,
  SUB_MENU_DISPLAY_LIGHT_MAX
} teSubMenuDisplayLight;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "Setting mode";

static const char *mpacTopMenu[] = {"Display light", "Compass cal.", "Battery"};
static const char *mpacSubMenuBacklight[] = {"On", "Off", "Auto"};
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static esp_err_t _incTopMenu(teTopMenuOrder *peSelectedItem);
static esp_err_t _decTopMenu(teTopMenuOrder *peSelectedItem);
static esp_err_t _showTopMenu(teTopMenuOrder *peSelectedItem);
static esp_err_t _selectNewSubMenu(teTopMenuOrder *peSelectedItem);

static esp_err_t _taskSubMenuBacklight(void);
static esp_err_t _incSubMenuBacklight(teSubMenuDisplayLight *peSelectedItem);
static esp_err_t _decSubMenuBacklight(teSubMenuDisplayLight *peSelectedItem);
static esp_err_t _enterSubMenuBacklight(teSubMenuDisplayLight *peSelectedItem);
static esp_err_t _showSubMenuBacklight(teSubMenuDisplayLight *peSelectedItem);

static esp_err_t _taskSubMenuCompassCalibration(void);

static esp_err_t _taskSubMenuBattery(void);

static uint8_t _incMenuOpt(uint8_t u8currentValue, uint8_t u8maxValueEnum);
static uint8_t _decMenuOpt(uint8_t u8currentValue, uint8_t u8maxValueEnum);
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
void uiModeSettingsRTOS(void *pvParameters) {
  ESP_LOGI(TAG, "Executing setting mode...");

  // Top menu setting - select very first item
  teTopMenuOrder eTopMenuItem = 0;

  tsKbrdMenuParams sTopMenu = {
      .pfPreLoop = (void *)&_showTopMenu,
      .pvPreLoopParam = &eTopMenuItem,

      .pfUp = (void *)&_decTopMenu,
      .pvUpParam = &eTopMenuItem,

      .pfDown = (void *)&_incTopMenu,
      .pvDownParam = &eTopMenuItem,

      .pfEnter = (void *)&_selectNewSubMenu,
      .pvEnterParam = &eTopMenuItem,
      // Stay in menu
      .bExitAfterEnterCallback = false,
  };

  // Should not happen error. But if so, report it
  ESP_ERROR_CHECK(uiKbrdMenu(&sTopMenu));

  taskSetModeExit();
  // Terminate itself
  vTaskDelete(NULL);
}
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
static esp_err_t _incTopMenu(teTopMenuOrder *peSelectedItem) {
  assert(peSelectedItem);

  *peSelectedItem = _incMenuOpt(*peSelectedItem, TOP_MENU_MAX);

  return (_showTopMenu(peSelectedItem));
}

static esp_err_t _decTopMenu(teTopMenuOrder *peSelectedItem) {
  assert(peSelectedItem);

  *peSelectedItem = _decMenuOpt(*peSelectedItem, TOP_MENU_MAX);

  return (_showTopMenu(peSelectedItem));
}

inline static esp_err_t _showTopMenu(teTopMenuOrder *peSelectedItem) {
  assert(peSelectedItem);

  return (uiDispDrawMenu("Settings", mpacTopMenu, *peSelectedItem,
                         GET_MAX_OPT_IDX(mpacTopMenu)));
}

static esp_err_t _selectNewSubMenu(teTopMenuOrder *peSelectedItem) {
  assert(*peSelectedItem <= (GET_MAX_OPT_IDX(mpacSubMenuBacklight)));

  esp_err_t eErrCode = ESP_FAIL;

  // Index number match mode in mpacTopMenu. Order should be respected here
  switch (*peSelectedItem) {
    case TOP_MENU_DISPLIGHT_LIGHT:
      eErrCode = _taskSubMenuBacklight();
      break;
    case TOP_MENU_COMPASS_CALIBRATION:
      eErrCode = _taskSubMenuCompassCalibration();
      break;
    case TOP_MENU_BATTERY:
      eErrCode = _taskSubMenuBattery();
      break;
    case TOP_MENU_MAX:
      // Should not happen - print error and rise assert
      ESP_LOGE(TAG, "Can not switch to MAX menu: %u", *peSelectedItem);
      assert(0);
  }

  RET_WHEN_ERR(eErrCode);

  // Refresh data on display
  return _showTopMenu(peSelectedItem);
}

static esp_err_t _taskSubMenuBacklight(void) {
  // Set it to something wrong, so checking if value was set will be easy
  teSubMenuDisplayLight eSelectedItem = SUB_MENU_DISPLAY_LIGHT_MAX;

  // Based on current settings set selected item
  if (kbrdGetBacklightAutoOff()) {
    // Auto off function is enabled
    eSelectedItem = SUB_MENU_DISPLAY_LIGHT_AUTO;
  } else {
    // Check if display is on or off
    if (uiDispGetBacklightStatus()) {
      // Backlight is on
      eSelectedItem = SUB_MENU_DISPLAY_LIGHT_ON;
    } else {
      // Backlight is off
      eSelectedItem = SUB_MENU_DISPLAY_LIGHT_OFF;
    }
  }

  tsKbrdMenuParams sSubMenu = {.pfPreLoop = (void *)&_showSubMenuBacklight,
                               .pvPreLoopParam = &eSelectedItem,

                               .pfUp = (void *)&_decSubMenuBacklight,
                               .pvUpParam = &eSelectedItem,

                               .pfDown = (void *)&_incSubMenuBacklight,
                               .pvDownParam = &eSelectedItem,

                               .pfEnter = (void *)&_enterSubMenuBacklight,
                               .pvEnterParam = &eSelectedItem,
                               .bExitAfterEnterCallback = true};

  return uiKbrdMenu(&sSubMenu);
}

static esp_err_t _incSubMenuBacklight(teSubMenuDisplayLight *peSelectedItem) {
  assert(peSelectedItem);

  *peSelectedItem = _incMenuOpt(*peSelectedItem, SUB_MENU_DISPLAY_LIGHT_MAX);
  return (_showSubMenuBacklight(peSelectedItem));
}

static esp_err_t _decSubMenuBacklight(teSubMenuDisplayLight *peSelectedItem) {
  assert(peSelectedItem);

  *peSelectedItem = _decMenuOpt(*peSelectedItem, SUB_MENU_DISPLAY_LIGHT_MAX);
  return (_showSubMenuBacklight(peSelectedItem));
}

static esp_err_t _enterSubMenuBacklight(teSubMenuDisplayLight *peSelectedItem) {
  assert(peSelectedItem);

  esp_err_t eErrCode = ESP_FAIL;

  switch (*peSelectedItem) {
    case SUB_MENU_DISPLAY_LIGHT_ON:
      // Enable backlight and save
      eErrCode = uiDispSetBacklight(true, true);
      // Turn off auto backlight, save setting
      eErrCode |= kbrdSetBacklightAutoOff(false, true);
      break;
    case SUB_MENU_DISPLAY_LIGHT_OFF:
      // Disable backlight and save setting
      eErrCode = uiDispSetBacklight(false, true);
      // Turn off auto backlight, save setting
      eErrCode |= kbrdSetBacklightAutoOff(false, true);
      break;
    case SUB_MENU_DISPLAY_LIGHT_AUTO:
      // Enable auto off and save setting
      eErrCode = kbrdSetBacklightAutoOff(true, true);
      break;
    case SUB_MENU_DISPLAY_LIGHT_MAX:
      ESP_LOGE(TAG, "Invalid option for sub menu display backlight: %u",
               *peSelectedItem);
      // Error code is already set
  }

  return eErrCode;
}

inline static esp_err_t _showSubMenuBacklight(
    teSubMenuDisplayLight *peSelectedItem) {
  return (uiDispDrawMenu(" Display light", mpacSubMenuBacklight,
                         *peSelectedItem,
                         GET_MAX_OPT_IDX(mpacSubMenuBacklight)));
}

static esp_err_t _taskSubMenuCompassCalibration(void) {
  esp_err_t eErrCode = ESP_FAIL;

  // Select default option and wait for user
  teKbrdOkCancel eOkCancel = KBRD_CANCEL;

  RET_WHEN_ERR(uiKbrdOkCancel(&eOkCancel,
                              "! Warning !\n"
                              "Current data\n"
                              "will be\n"
                              "overwritten"));

  // User do not want to make calibration. Fine
  if (eOkCancel == KBRD_CANCEL) {
    return eErrCode;
  } else {
    // Let's calibrate it!
    RET_WHEN_ERR(uiDispClearFb());
    RET_WHEN_ERR(uiDispPuts(0, 0, "Calibrating..."));
    // Start drawing bitmap from row index 1 (2nd)
    RET_WHEN_ERR(
        uiDispSetbytesInFb(0, 1, gu8magCalibrating, sizeof(gu8magCalibrating)));
    RET_WHEN_ERR(uiDispWriteFbToDisplay());

    return (sensMagCalibrate(3500));
  }
}

static esp_err_t _taskSubMenuBattery(void) {
  esp_err_t eErrCode = ESP_FAIL;

  // Select default option and wait for user
  teKbrdOkCancel eOkCancel = KBRD_CANCEL;

  RET_WHEN_ERR(uiKbrdOkCancel(&eOkCancel,
                              "! Warning !\n"
                              "Delete\n"
                              "battery\n"
                              "measurement?\n"));

  // User do not want to make delete battery statistic data. Fine
  if (eOkCancel == KBRD_CANCEL) {
    return eErrCode;
  } else {
    // Let's remove it
    eErrCode = batteryDeleteStatisticData();

    // Need to reload values in battery module
    batteryForceReloadValuesFromNvs();

    return eErrCode;
  }
}

static uint8_t _incMenuOpt(uint8_t u8currentValue, uint8_t u8maxValueEnum) {
  u8currentValue++;
  if (u8currentValue >= u8maxValueEnum) {
    // It would overflow -> start from zero
    return 0;
  } else {
    return u8currentValue;
  }
}

static uint8_t _decMenuOpt(uint8_t u8currentValue, uint8_t u8maxValueEnum) {
  if (u8currentValue == 0) {
    // Would underflow - load last one (MAX is in enumeration actually
    // very last item, but not valid for menus -> -1
    return u8maxValueEnum - 1;
  } else {
    return --u8currentValue;
  }
}
