/**
 * @file
 * @author Martin Stejskal
 * @brief Kind of IPC for UI and separated modes
 */
#ifndef __UI_MODES_COMMON_H__
#define __UI_MODES_COMMON_H__
// ===============================| Includes |================================
#include <stdbool.h>

#include "wifi.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  // Modes for development/service guy
  UI_MODE_NONE,
  UI_MODE_SERVICE,

  // User modes
  UI_MODE_ANOMALY_DETECTOR,
  UI_MODE_COMPASS,
  UI_MODE_BATT,
  UI_MODE_FLASHLIGHT,
  UI_MODE_SETTINGS,
  UI_MODE_ACC_GRAPH,

  // Signalize last item in enumeration
  UI_MODE_MAX,
} teUiModes;

typedef struct {
  // Enable/disable mode for user
  bool bEnabled;
} tsUiModeCfg;

typedef enum {
  UI_REQ_NONE,
  UI_REQ_EN_SERVICE_MODE,
  UI_REQ_DIS_SERVICE_MODE
} teUiReq;

/**
 * @brief Define levels for different warnings
 */
typedef enum {
  //!< UI_ANOMALY_DETECTOR_NO_WARNING No warning required
  UI_ANOMALY_DETECTOR_NO_WARNING = WIFI_MIN_RSSI,

  //!< UI_ANOMALY_DETECTOR_1ST_WARNING First warning
  UI_ANOMALY_DETECTOR_1ST_WARNING = -93,

  //!< UI_ANOMALY_DETECTOR_2ND_WARNING Second warning
  UI_ANOMALY_DETECTOR_2ND_WARNING = -87,

  //!< UI_ANOMALY_DETECTOR_3RD_WARNING Third warning
  UI_ANOMALY_DETECTOR_3RD_WARNING = -77,

  //!< UI_ANOMALY_DETECTOR_DEAD You're dead
  UI_ANOMALY_DETECTOR_DEAD = -60
} teUiAnomalyDetectorWarningThresholds;
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Current mode should call this function when terminate itself
 */
void taskSetModeExit(void);

/**
 * @brief Tells if current mode is still running or already exited
 * @return Zero when still running, otherwise non-zero value
 */
bool uiGetIsModeExited(void);

/**
 * @brief Before UI main loop select new mode, it should call this function
 *
 * This function will clear "exit" flag
 */
void uiClearModeExitFlag(void);

/**
 * @brief UI send request to running task to exit
 *
 * Due to various reasons, UI (top layer) can decide to shutdown current UI
 * mode task. In order to make it "nice", it will send this request first, so
 * running task can close itself properly
 *
 * @param bForceExit True enable request, false clear flag
 */
void uiForceCloseTaskRequest(bool bForceExit);

/**
 * @brief Called by UI mode task to check whether is not required to exit
 *
 * In some cases UI top layer can request UI mode task to terminate itself.
 * Since there can be some processing, this allow task to close all interfaces
 * properly.
 *
 * @return True if current task should terminate itself, false otherwise
 */
bool taskGetForceCloseFlag(void);

/**
 * @brief Send request to switch to service mode
 *
 * When service mode is activated, it need access to all functionality.
 * Therefore it sent this request and UI top layer will try to stop all user
 * tasks.
 */
void requestUiEnableServiceMode(void);

/**
 * @brief Send request to switch out from service mode
 *
 * When service mode exiting, it report UI top layer that it can switch back
 * to the normal mode.
 */
void requestUiDisableServiceMode(void);

/**
 * @brief Tell if UI top layer is in service mode or not
 * @return True if in service mode, otherwise false
 */
bool isUiInServiceMode(void);

/**
 * @brief UI top layer get request type from service mode
 * @return One of the request type
 */
teUiReq uiGetReq(void);

/**
 * @brief UI top layer reports that successfully switched to service mode
 */
void uiSwithedToServiceMode(void);

/**
 * @brief UI top layer reports that successfully exit from service mode
 */
void uiExitedServiceMode(void);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Return mode name as string
 * @param eCurrentMode Mode as enumeration
 * @return Pointer to string value
 */
const char *uiGetModeName(teUiModes eCurrentMode);

uint8_t ui_detector_wifi_rssi_to_u8(const int8_t i8_rssi);
#endif  // __UI_MODES_COMMON_H__
