/**
 * @file
 * @author Martin Stejskal
 * @brief High level keyboard functionality for UI
 */
#ifndef __UI_KEYBOARD_H__
#define __UI_KEYBOARD_H__
// ===============================| Includes |================================
#include <esp_err.h>
#include <stdbool.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef struct {
  esp_err_t (*pfPreLoop)(void *pvParam);
  void *pvPreLoopParam;

  esp_err_t (*pfEnter)(void *pvParam);
  void *pvEnterParam;
  bool bExitAfterEnterCallback;

  esp_err_t (*pfUp)(void *pvParam);
  void *pvUpParam;

  esp_err_t (*pfDown)(void *pvParam);
  void *pvDownParam;

  esp_err_t (*pfAnyBtn)(void *pvParam);
  void *pvAnyBtnParam;

  esp_err_t (*pfPostLoop)(void *pvParam);
  void *pvPostLoopParam;
} tsKbrdMenuParams;

typedef enum { KBRD_OK, KBRD_CANCEL } teKbrdOkCancel;
// ===============================| Functions |===============================

// =========================| High level functions |==========================
/**
 * @brief Generic menu which react to keyboard input
 *
 * @param psParams Parameter structure
 * @return ESP_OK if no error
 */
esp_err_t uiKbrdMenu(tsKbrdMenuParams *psParams);

/**
 * @brief Standard "OK"/"Cancel" interface
 * @param peSelectedOption Selected option will be written here. Must be set
 *                         to one of enumeration value - will be used as
 *                         predefined default
 * @param pacMsg Message which should be displayed
 * @return ESP_OK if no error
 */
esp_err_t uiKbrdOkCancel(teKbrdOkCancel *peSelectedOption, const char *pacMsg);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

#endif  // __UI_KEYBOARD_H__
