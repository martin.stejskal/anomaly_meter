/**
 * @file
 * @author Martin Stejskal
 * @brief Display functions for display
 */
// ===============================| Includes |================================
// Standard
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

// ESP specific
#include <driver/spi_common.h>
#include <esp_log.h>

// Local settings
#include "cfg.h"

// Display things
#include "bitmaps.h"
#include "ui_display.h"
#include "ui_display_ex.h"

// Utilities
#include "err_handlers.h"
#include "math_fast.h"
#include "time_utils.h"

// Define maximum value for value from magnetic sensor
#include "sensors.h"

// WiFi minimum and maximum RSSI values
#include "wifi.h"

// Play with LED
#include "led_rgb.h"
// ================================| Defines |================================
/**
 * @brief Position of some information for flashlight
 */
#define FLSHLGHT_X_POS (27)

/**
 * @brief Define compass arrow center position
 *
 * @{
 */
#define COMPASS_CENTER_X_POS (PCD8544_X_RES / 2 - 1)
// First line is reserved for "N" or other symbol -> 8 pixels are out of game
#define COMPASS_CENTER_Y_POS ((((PCD8544_Y_RES - 8) / 2) - 1) + 8)
#define COMPASS_ARROW_LENGTH (20)

#define COMPASS_SMALL_CIRCLE (4)
/**
 * @}
 */

/**
 * @brief Define display center in Y axe
 *
 * Note that display resolution is even, so there is no perfectly centered
 * pixel, but "close enough" one.
 *
 * @{
 */
#define DISP_Y_CENTER_POS ((PCD8544_Y_RES / 2) - 1)
/**
 * @}
 */

/**
 * @brief Tells which value mean "high magnetic intensity"
 *
 * Value was decided by trial and error. Simply this configuration "looks
 * good" in real conditions. Deal with it.
 */
#define MAG_FIELD_SATURATION (MAG_SENS_MAX / 80)

/**
 * @brief Threshold when magnetic field intensity is above average
 *
 * When this threshold is reached, some magnet is probably near and it cause
 * significant error
 */
#define MAG_FIELD_MAGNET_DETECTED (3500)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "UI_DISP";

static const char *pac1of10 = " 1/10";
static const char *pac2of10 = " 2/10";
static const char *pac3of10 = " 3/10";
static const char *pac4of10 = " 4/10";
static const char *pac5of10 = " 5/10";
static const char *pac6of10 = " 6/10";
static const char *pac7of10 = " 7/10";
static const char *pac8of10 = " 8/10";
static const char *pac9of10 = " 9/10";
static const char *pac10of10 = "10/10";

// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static esp_err_t _writeLogoHeader(void);
static esp_err_t _writeLogo(const uint16_t u16waitDelayMs);
static esp_err_t _cleanDisp(void);
// =========================| High level functions |==========================
esp_err_t uiDispInit(void) {
  // Reset pin is connected with ESP reset -> not need to control
  static pcd8544_control_pin_config_t sCntrlLcdPin = {
      .reset_io_num = -1,
      .dc_io_num = IO_LCD_DC,
      .backlight_io_num = IO_LCD_BCK_LGHT,
  };

  ESP_LOGI(TAG, "Display init");
  static pcd8544_config_t config = {
      .spi_host = IO_LCD_SPI_HOST,
      .is_backlight_common_anode = true,
      .bControlReset = false,
      .control_pin = (pcd8544_control_pin_config_t *)&sCntrlLcdPin};
  pcd8544init(&config);

  // Clean display
  ESP_ERROR_CHECK(_cleanDisp());

  // Small delay
  vTaskDelay(200 / portTICK_PERIOD_MS);

  // Enable all segments
  pcd8544setDisplayMode(PCD8544_DISPLAY_MODE_ALL_SEGMENTS_ON);

  // Small delay
  vTaskDelay(500 / portTICK_PERIOD_MS);

  // Restore data in controller memory
  pcd8544setDisplayMode(PCD8544_DISPLAY_MODE_NORMAL);

  ESP_LOGI(TAG, "Display ready");

  // Write logo
  return (_writeLogo(1500));
}

esp_err_t uiDispDeInit(void) {
  pcd8544free();

  ESP_LOGI(TAG, "Display deinitialized");
  return ESP_OK;
}

esp_err_t uiDispShowMode(teUiModes eMode,
                         tsUiModeCfg asModesCfg[static UI_MODE_MAX]) {
  // List of pointers to options. Need to allocate memory for all just in
  // case
  const char *pacOptions[UI_MODE_MAX];

  // Feed list of options (pointers to options). This reflects real number
  // of options in list
  uint8_t u8optIdx = 0;

  // Current mode index in freshly created options list
  uint8_t u8currentModeIdx;

  // Check if current mode is enabled. If not, it is internal error. Upper
  // layer should not give such parameter
  if (!asModesCfg[eMode].bEnabled) {
    return ESP_ERR_INVALID_ARG;
  }

  for (teUiModes eModeIdx = (teUiModes)0; eModeIdx < UI_MODE_MAX; eModeIdx++) {
    // If selected mode is the one we're processing now
    if (eMode == eModeIdx) {
      u8currentModeIdx = u8optIdx;
    }

    // If current mode is enabled, add it to list
    if (asModesCfg[eModeIdx].bEnabled) {
      pacOptions[u8optIdx++] = uiGetModeName(eModeIdx);
    }
  }
  // Since last time it was increased by one, but it was last write, so
  // we need to decrease it by 1 again
  u8optIdx--;

  // Maximum index - do not forget that it is index -> UI_MODE_MAX is
  // actually 1 ahead of last valid index
  return uiDispDrawMenu("Select mode", pacOptions, u8currentModeIdx, u8optIdx);
}

esp_err_t uiDispDetector(tsUiDispDetector *psDetectorData,
                         tsBattStatus *psBattStatus) {
  // Pointers should not be empty
  assert(psDetectorData);
  assert(psDetectorData->pacSSIDorDescription);

  esp_err_t eErrCode = ESP_FAIL;
  int8_t i8rssi, i8rssiLatest;
  int32_t i32rssiSum = 0;

  // 3 Bytes for 3 line graph
  uint8_t u8bottom, u8mid, u8top;

  RET_WHEN_ERR(pcd8544clearFb());

  if (!psDetectorData->bDetected) {
    // Anomaly not detected -> no meaning to show RSSI or SSID.
    // Show battery status at least
    // Uptime or how long charging
    char acTxtTimerBuffer[30];
    char acTxtRemainingTimeBuffer[30];

    if (psBattStatus->bCharging) {
      time_util_sec_to_hh_mm_ss(acTxtTimerBuffer,
                                psBattStatus->u32howLongChargingSec);
    } else {
      time_util_sec_to_hh_mm_ss(acTxtTimerBuffer,
                                psBattStatus->u32runOnBatterySec);
    }

    // It does not make sense to calculate remaining time with second accuracy.
    time_util_sec_to_hh_mm(acTxtRemainingTimeBuffer,
                           psBattStatus->u32batteryEstimateSec);

    // Show last value on battery. Also show window size in minutes
    RET_WHEN_ERR(pcd8544printfFb(0, 0, "%.2fV %d%%\n  %s\n  %s:00",
                                 psBattStatus->fVoltage,
                                 psBattStatus->u8percentage, acTxtTimerBuffer,
                                 acTxtRemainingTimeBuffer));

    // If charging, add icon too
    if (psBattStatus->bCharging) {
      RET_WHEN_ERR(uiDispExDrawPowerPlug(1, 0));
    } else {
      // Add battery icon
      RET_WHEN_ERR(uiDispExDrawBattery(1, 0, psBattStatus->u8percentage));
    }
  } else {
    // Get latest RSSI
    ringBuffPullLatest(&psDetectorData->sRssiRingBuff, &i8rssiLatest);

    // Start at line 0, maximum value is 255 (8 bit number)
    RET_WHEN_ERR(pcd8544drawBarGraphHorizontalSimpleInFb(
        0, ui_detector_wifi_rssi_to_u8(i8rssiLatest), 0xFF));

    // Write SSID. Since one line have limit 14 characters
    // (84 px / 6 px font)
    if (strlen(psDetectorData->pacSSIDorDescription) > (PCD8544_X_RES / 6)) {
      // Write null on index 14 (15th character) -> string will be cut to
      // 14 characters
      psDetectorData->pacSSIDorDescription[PCD8544_X_RES / 6] = 0;
    }
    // X = 0; row = 1
    RET_WHEN_ERR(pcd8544putsFb(psDetectorData->pacSSIDorDescription, 0, 1));
  }

  // Always draw graph
  // Draw bottom line - bottom of LCD
  RET_WHEN_ERR(pcd8544drawLineInFb(0, PCD8544_Y_RES - 1, PCD8544_X_RES - 1,
                                   PCD8544_Y_RES - 1, WRITE_SET));
  for (uint8_t u8xPos = 0; u8xPos < PCD8544_X_RES; u8xPos++) {
    ringBuffPull(&psDetectorData->sRssiRingBuff, &i8rssi);

    // If buffer was cleared (zeros) it would actually
    // mean that "signal is good" -> need to check and if such value
    // was loaded, tell "signal is very bad" since there was nothing
    // found yet
    if (i8rssi == 0) {
      i8rssi = WIFI_MIN_RSSI;
    }

    // For calculating average (later), add current value to
    // accumulator.
    i32rssiSum += i8rssi;

    RET_WHEN_ERR(uiDispEx8bitValTo24bitGraphInfill(
        ui_detector_wifi_rssi_to_u8(i8rssi), &u8top, &u8mid, &u8bottom));

    // Write Bytes to appropriate position. Bottom is line 5, middle is
    // line 4 and top is line 3
    RET_WHEN_ERR(pcd8544setByteInFb(u8top, u8xPos, 3, WRITE_SET));
    RET_WHEN_ERR(pcd8544setByteInFb(u8mid, u8xPos, 4, WRITE_SET));
    RET_WHEN_ERR(pcd8544setByteInFb(u8bottom, u8xPos, 5, WRITE_SET));
  }

  // Show latest RSSI and average value is any anomaly was found
  if (psDetectorData->bDetected) {
    // Calculate average RSSI over whole window
    i32rssiSum = i32rssiSum / PCD8544_X_RES;

    RET_WHEN_ERR(pcd8544printfFb(10, 2, "%d (%d)", i8rssiLatest, i32rssiSum));
  }

  if (psDetectorData->eWarningLvl > UI_ANOMALY_DETECTOR_NO_WARNING) {
    uint8_t u8level;
    switch (psDetectorData->eWarningLvl) {
      case UI_ANOMALY_DETECTOR_1ST_WARNING:
        u8level = 1;
        break;
      case UI_ANOMALY_DETECTOR_2ND_WARNING:
        u8level = 2;
        break;
      case UI_ANOMALY_DETECTOR_3RD_WARNING:
        u8level = 3;
        break;
      case UI_ANOMALY_DETECTOR_DEAD:
        u8level = 4;
        break;
      default:
        // Should not happen.
        ESP_LOGE(TAG, "Unexpected warning level: %d",
                 psDetectorData->eWarningLvl);
        assert(0);
    }
    ///@todo Draw something graphical, but for now there is text
    RET_WHEN_ERR(pcd8544printfFb(70, 2, "!%d", u8level));
  }

  // Render it
  RET_WHEN_ERR(pcd8544writeFbToLCD());

  return eErrCode;
}

esp_err_t uiDispBattInfo(tsBattStatus *psStatus, uint32_t u32zoom) {
  esp_err_t eErrCode = ESP_FAIL;

  RET_WHEN_ERR(pcd8544clearFb());

  // Battery voltage as raw ADC value
  uint16_t u16adcValue;

  // 3 Bytes for 3 line graph
  uint8_t u8bottom, u8mid, u8top;

  // For more precise calculations (fractional)
  uint32_t u32tmp;

  // Reset read index, so this function can read it correctly
  ringBuffResetReadIdx(psStatus->psRingBuffRaw);

  // Uptime or how long charging
  char acTxtTimerBuffer[30];
  char acTxtRemainingTimeBuffer[30];

  if (psStatus->bCharging) {
    time_util_sec_to_hh_mm_ss(acTxtTimerBuffer,
                              psStatus->u32howLongChargingSec);
  } else {
    time_util_sec_to_hh_mm_ss(acTxtTimerBuffer, psStatus->u32runOnBatterySec);
  }

  // Seconds have no meaning, since whole estimation is not accurate enough
  time_util_sec_to_hh_mm(acTxtRemainingTimeBuffer,
                         psStatus->u32batteryEstimateSec);

  // Show last value on battery. Also show window size in minutes
  RET_WHEN_ERR(pcd8544printfFb(0, 0, "%.2fV %d%%\n  %s\n  %s:00",
                               psStatus->fVoltage, psStatus->u8percentage,
                               acTxtTimerBuffer, acTxtRemainingTimeBuffer));

  // If charging, add icon too
  if (psStatus->bCharging) {
    RET_WHEN_ERR(uiDispExDrawPowerPlug(1, 0));
  } else {
    // Add battery icon
    RET_WHEN_ERR(uiDispExDrawBattery(1, 0, psStatus->u8percentage));
  }

  // Draw bottom line - bottom of LCD
  RET_WHEN_ERR(pcd8544drawLineInFb(0, PCD8544_Y_RES - 1, PCD8544_X_RES - 1,
                                   PCD8544_Y_RES - 1, WRITE_SET));

  uint16_t u16numOfItemsRingBuff =
      ringBuffGetNumOfItems(psStatus->psRingBuffRaw);
  // Index in ring buffer as integer, but multiplied by 1000, since steps does
  // not have to be exactly multiplier of 1 (index 1.2, 3.6, etc...). Initial
  // index is "latest value", which means highest relative index. Note that
  // this is index -> -1
  int32_t i32ringBuffIdx = ((int32_t)u16numOfItemsRingBuff - 1) * 1000;

  // Expect positive number
  assert(i32ringBuffIdx > 0);

  // (Window size)/(display resolution)
  uint32_t u32idxStep =
      ((((uint32_t)u16numOfItemsRingBuff * 1000) / u32zoom) * 1000) /
      PCD8544_X_RES;

  // Draw charge/discharge graph
  for (uint8_t u8xPos = PCD8544_X_RES - 1; u8xPos < PCD8544_X_RES; u8xPos--) {
    // Get value from ring buffer
    ringBuffPullRelative(psStatus->psRingBuffRaw, &u16adcValue,
                         (uint16_t)(i32ringBuffIdx / 1000));
    // Update relative index
    i32ringBuffIdx -= u32idxStep;

    // Expect only 10 bit value
    assert(u16adcValue < 1024);

    if (i32ringBuffIdx < 0) {
      i32ringBuffIdx = 0;
    }

    // Adjust value. Value from ADC is 10 bit (1024), but typically we
    // need to know battery range 3.5 V to 4.2 V -> ADC value 689 ~ 827
    // -> 690 is basically offset. Difference is at about 137 (integer) ->
    // need to multiply by 1.8613 to get full range (0~255)

    // Fail-safe check. If lower than minimum threshold, it does not make much
    // sense to continue calculating anything
    if (u16adcValue < 690) {
      u16adcValue = 0;
    } else {
      // Overall multiply current value by 10 000 -> move decimal point a bit
      // higher
      u32tmp = ((int32_t)(u16adcValue - 690) * 18613) / 10000;

      assert(u32tmp <= 0xFFFF);

      u16adcValue = (uint16_t)(u32tmp);

      // Limit it to 8 bit value - just in case
      if (u16adcValue > 255) {
        u16adcValue = 255;
      }
    }

    assert(u16adcValue <= 255);

    RET_WHEN_ERR(uiDispEx8bitValTo24bitGraphInfill((uint8_t)u16adcValue, &u8top,
                                                   &u8mid, &u8bottom));

    // Write Bytes to appropriate position. Bottom is line 5, middle is
    // line 4 and top is line 3
    RET_WHEN_ERR(pcd8544setByteInFb(u8top, u8xPos, 3, WRITE_SET));
    RET_WHEN_ERR(pcd8544setByteInFb(u8mid, u8xPos, 4, WRITE_SET));
    RET_WHEN_ERR(pcd8544setByteInFb(u8bottom, u8xPos, 5, WRITE_SET));
  }

  // Render it
  RET_WHEN_ERR(pcd8544writeFbToLCD());

  return eErrCode;
}

esp_err_t uiDispFlashlight(teLedRGBlevel eLedBrightness) {
  esp_err_t eErrCode = ESP_OK;

  // Clear display
  RET_WHEN_ERR(pcd8544clearFb());

  switch (eLedBrightness) {
    case LED_LVL_0:
      eErrCode = pcd8544putsFb(pac1of10, FLSHLGHT_X_POS, 0);
      break;
    case LED_LVL_1:
      eErrCode = pcd8544putsFb(pac2of10, FLSHLGHT_X_POS, 0);
      break;
    case LED_LVL_2:
      eErrCode = pcd8544putsFb(pac3of10, FLSHLGHT_X_POS, 0);
      break;
    case LED_LVL_3:
      eErrCode = pcd8544putsFb(pac4of10, FLSHLGHT_X_POS, 0);
      break;
    case LED_LVL_4:
      eErrCode = pcd8544putsFb(pac5of10, FLSHLGHT_X_POS, 0);
      break;
    case LED_LVL_5:
      eErrCode = pcd8544putsFb(pac6of10, FLSHLGHT_X_POS, 0);
      break;
    case LED_LVL_6:
      eErrCode = pcd8544putsFb(pac7of10, FLSHLGHT_X_POS, 0);
      break;
    case LED_LVL_7:
      eErrCode = pcd8544putsFb(pac8of10, FLSHLGHT_X_POS, 0);
      break;
    case LED_LVL_8:
      eErrCode = pcd8544putsFb(pac9of10, FLSHLGHT_X_POS, 0);
      break;
    case LED_LVL_9:
      eErrCode = pcd8544putsFb(pac10of10, FLSHLGHT_X_POS, 0);
      break;
    default:
      eErrCode = pcd8544putsFb(" ?/?", FLSHLGHT_X_POS, 0);
  }

  if (eErrCode) {
    return eErrCode;
  }

  // Render it
  RET_WHEN_ERR(pcd8544writeFbToLCD());

  return eErrCode;
}

esp_err_t uiDispCompass(tsVector32 sSumMagDataVector,
                        const uint16_t u16numOfMagSamples,
                        tsVector16 sAccVector, const teLedRGBlevel eLedLevel) {
  // ESP_LOGI(TAG, "MAG: x: %d y: %d z: %d", psSumMagDataVector->i32x,
  //        psSumMagDataVector->i32y, psSumMagDataVector->i32z);

  esp_err_t eErrCode;

  int32_t i32angle, i32arrowEndX, i32arrowEndY, i32arrowStartX, i32arrowStartY,
      i32vectLength;

  // Store magnitude values for rendering
  tsVector32 sVect32magnitude, sVect32accTmp;

  RET_WHEN_ERR(pcd8544clearFb());

  // ================================| N, W, E |============================
  // Center "N" -> font width /2
  RET_WHEN_ERR(pcd8544putsFb(
      "N", COMPASS_CENTER_X_POS - (PCD8544_FONT_WIDTH_PX / 2), 0));
  RET_WHEN_ERR(pcd8544putsFb(
      "W",
      COMPASS_CENTER_X_POS - COMPASS_ARROW_LENGTH - (PCD8544_FONT_WIDTH_PX),
      PCD8544_LINES / 2));
  RET_WHEN_ERR(pcd8544putsFb(
      "E", COMPASS_CENTER_X_POS + COMPASS_ARROW_LENGTH + 1, PCD8544_LINES / 2));

  // Nice round - bit smaller than arrow size due to cool effect
  RET_WHEN_ERR(uiDispDrawCircle(COMPASS_CENTER_X_POS, COMPASS_CENTER_Y_POS,
                                COMPASS_ARROW_LENGTH - 7));
  RET_WHEN_ERR(uiDispDrawCircle(COMPASS_CENTER_X_POS, COMPASS_CENTER_Y_POS,
                                COMPASS_ARROW_LENGTH - 4));

  // ==========================| Draw compass arrow |=======================
  // Need to consider physical alignment of magnetometer and display.
  // Display have inverted Y logic. The "bottom" means higher value ->
  // invert it
  sSumMagDataVector.i32y *= -1;

  i32angle =
      mf_get_angle_from_xy(sSumMagDataVector.i32x, sSumMagDataVector.i32y);

  // The 180 degree compensate physical location of sensor itself
  i32angle += 180;

  // Initial point should be in center (well,
  mf_get_xy_coordinates(COMPASS_CENTER_X_POS, COMPASS_CENTER_Y_POS,
                        COMPASS_ARROW_LENGTH, i32angle, &i32arrowEndX,
                        &i32arrowEndY);

  mf_get_xy_coordinates(COMPASS_CENTER_X_POS, COMPASS_CENTER_Y_POS,
                        COMPASS_ARROW_LENGTH - 10, i32angle, &i32arrowStartX,
                        &i32arrowStartY);

  ESP_LOGV(
      TAG, "Compass: x: %d y: %d z: %d (disp: angle: %d (%d) - x: %d y: %d)",
      sSumMagDataVector.i32x, sSumMagDataVector.i32y, sSumMagDataVector.i32z,
      i32angle, i32angle - 180, i32arrowEndX, i32arrowEndY);

  assert(i32arrowEndX >= 0);
  assert(i32arrowEndX < PCD8544_X_RES);
  assert(i32arrowEndY >= 0);
  assert(i32arrowEndY < PCD8544_Y_RES);

  RET_WHEN_ERR(pcd8544drawLineInFb(
      (uint8_t)i32arrowStartX, (uint8_t)i32arrowStartY, (uint8_t)i32arrowEndX,
      (uint8_t)i32arrowEndY, WRITE_SET));

  // ===========================| Angle in degrees |========================
  i32angle = mf_get_360_deg_range(i32angle + 90);

  RET_WHEN_ERR(pcd8544printfFb(
      COMPASS_CENTER_X_POS + (PCD8544_FONT_WIDTH_PX * 3), 0, "%03d", i32angle));
  // Degree symbol
  RET_WHEN_ERR(uiDispExDrawDegreeSymbol(
      COMPASS_CENTER_X_POS + (PCD8544_FONT_WIDTH_PX * (3 + 3)), 0));

  // ============================| Intensity graph |========================
  // For every axe is required to scale magnitude. Maximum size is 23 (need to
  // fit on display -> 48 px -> 2x23 will still fit. 2x24 would not, since
  // value basically means index and maximum index is 47
  RET_WHEN_ERR(vect32scaleSymetric(&sVect32magnitude, sSumMagDataVector,
                                   MAG_FIELD_SATURATION * u16numOfMagSamples,
                                   23));
  // x, y, height, width
  RET_WHEN_ERR(
      uiDispExBargraph(0, DISP_Y_CENTER_POS, sVect32magnitude.i32x, 2));
  RET_WHEN_ERR(
      uiDispExBargraph(3, DISP_Y_CENTER_POS, sVect32magnitude.i32y, 2));
  RET_WHEN_ERR(
      uiDispExBargraph(6, DISP_Y_CENTER_POS, sVect32magnitude.i32z, 2));

  // ============================| Pitch of sensor |========================
  // The sensor should be aligned with ground. To indicate pitch we can
  // use accelerometer data and display them. User then can balance with
  // device in order to improve compass accuracy. Put acceleration data
  // to 32 bit long vector and scale them
  sVect32accTmp.i32x = (int32_t)sAccVector.i16x;
  sVect32accTmp.i32y = (int32_t)sAccVector.i16y;
  sVect32accTmp.i32z = (int32_t)sAccVector.i16z;

  RET_WHEN_ERR(vect32scaleSymetric(&sVect32magnitude, sVect32accTmp,
                                   ACC_SENS_1G_VALUE / 3, 11));
  // Acceleration Y compensation display (X coordination on display)
  RET_WHEN_ERR(pcd8544drawLineInFb(COMPASS_CENTER_X_POS, COMPASS_CENTER_Y_POS,
                                   COMPASS_CENTER_X_POS + sVect32magnitude.i32y,
                                   COMPASS_CENTER_Y_POS, WRITE_SET));
  RET_WHEN_ERR(pcd8544drawLineInFb(
      COMPASS_CENTER_X_POS, COMPASS_CENTER_Y_POS, COMPASS_CENTER_X_POS,
      COMPASS_CENTER_Y_POS - sVect32magnitude.i32x, WRITE_SET));

  // Just nice circle about it
  RET_WHEN_ERR(uiDispDrawCircle(COMPASS_CENTER_X_POS, COMPASS_CENTER_Y_POS,
                                COMPASS_SMALL_CIRCLE));

  // Render it
  RET_WHEN_ERR(pcd8544writeFbToLCD());

  // ==============================| LED effect |===========================
  // Signalize horizontal status - if out of small circle, warn user
  // Get absolute value of magnitude vector
  sVect32accTmp = sVect32magnitude;
  vect32abs(&sVect32accTmp);

  if ((sVect32accTmp.i32x < COMPASS_SMALL_CIRCLE) &&
      (sVect32accTmp.i32y < COMPASS_SMALL_CIRCLE)) {
    // on green, off red
    ledRGBsetR(LED_LVL_MIN);
    ledRGBsetG(eLedLevel);
  } else {
    // Create yellow: red + green
    ledRGBsetR(eLedLevel);
    ledRGBsetG(eLedLevel);
  }

  // Magnetic flux status - turn on blue if over limit in any direction
  if (vect32length(&sSumMagDataVector, &i32vectLength)) {
    // Calculation should not fail, but if so...
    ESP_LOGE(TAG, "Calculating magnetic field vector length failed");
    return ESP_FAIL;
  }

  if (i32vectLength > MAG_FIELD_MAGNET_DETECTED) {
    ledRGBsetB(eLedLevel);
  } else {
    ledRGBsetB(LED_LVL_MIN);
  }

  return eErrCode;
}

esp_err_t uiDispVectGraphRow(tsRingBuffer *psRingBuff,
                             tsVector16 *psVectDivFactor) {
  esp_err_t eErrCode = ESP_FAIL;

  // Temporary vector for calculating new value for graph
  tsVector16 sVect16tmp, sVect16raw;

  // For calculating average values
  tsVector32 sVect32avg = {.i32x = 0, .i32y = 0, .i32z = 0};

  // Define Y start position for graph showing "X" value from ring buffer.
  // Note that every row have 8 bits and most "bottom one" is the last one
  // -> +7 to get 8th bit position
  uint8_t au8btmLnePos[] = {(PCD8544_Y_RES_ROW * (3 + 0)) + 7,
                            (PCD8544_Y_RES_ROW * (3 + 1)) + 7,
                            (PCD8544_Y_RES_ROW * (3 + 2)) + 7};

  // Clean up display
  RET_WHEN_ERR(pcd8544clearFb());
  RET_WHEN_ERR(_writeLogoHeader());

  // Prepare basic ground for graphs
  for (uint8_t u8graphCnt = 0; u8graphCnt < sizeof(au8btmLnePos);
       u8graphCnt++) {
    // Bottom lines
    RET_WHEN_ERR(pcd8544drawLineInFb(0, au8btmLnePos[u8graphCnt],
                                     (PCD8544_X_RES - 1),
                                     au8btmLnePos[u8graphCnt], WRITE_SET));
  }

  // Add pixels
  for (uint8_t u8xPos = 0; u8xPos < PCD8544_X_RES; u8xPos++) {
    // Get vector from ring buffer
    ringBuffPull(psRingBuff, &sVect16raw);

    // need to keep original value due to debug messages
    sVect16tmp = sVect16raw;

    // Divide by given vector
    vect16div(&sVect16tmp, &sVect16tmp, psVectDivFactor);

    // Get only absolute values
    vect16abs(&sVect16tmp);

    // Add it to average
    sVect32avg.i32x += sVect16tmp.i16x;
    sVect32avg.i32y += sVect16tmp.i16y;
    sVect32avg.i32z += sVect16tmp.i16z;

    // Check range. We should not exceed 8 bit values
    vect16cutOff(&sVect16tmp, 0xFF);

    RET_WHEN_ERR(uiDispEx8bitValTo8bitGraphInfill(sVect16tmp.i16x,
                                                  (uint8_t *)&sVect16tmp.i16x));
    RET_WHEN_ERR(uiDispEx8bitValTo8bitGraphInfill(sVect16tmp.i16y,
                                                  (uint8_t *)&sVect16tmp.i16y));
    RET_WHEN_ERR(uiDispEx8bitValTo8bitGraphInfill(sVect16tmp.i16z,
                                                  (uint8_t *)&sVect16tmp.i16z));

    // Write bytes for X, Y, and Z
    RET_WHEN_ERR(pcd8544setByteInFb(sVect16tmp.i16x, u8xPos, 3 + 0, WRITE_SET));
    RET_WHEN_ERR(pcd8544setByteInFb(sVect16tmp.i16y, u8xPos, 3 + 1, WRITE_SET));
    RET_WHEN_ERR(pcd8544setByteInFb(sVect16tmp.i16z, u8xPos, 3 + 2, WRITE_SET));

    // For debug purpose, print only last item (newest sample)
    if (u8xPos >= (PCD8544_X_RES - 1)) {
      ESP_LOGV(TAG, "Graph: Orig: %d %d %d ; Mod: %x %x %x", sVect16raw.i16x,
               sVect16raw.i16y, sVect16raw.i16z, sVect16tmp.i16x,
               sVect16tmp.i16y, sVect16tmp.i16z);
    }
  }

  // Calculate average
  sVect32avg.i32x /= PCD8544_X_RES;
  sVect32avg.i32y /= PCD8544_X_RES;
  sVect32avg.i32z /= PCD8544_X_RES;

  // x pos, row, average vector value
  RET_WHEN_ERR(pcd8544printfFb(0, 1, "X: %03d Y: %03d\nZ: %03d",
                               sVect32avg.i32x, sVect32avg.i32y,
                               sVect32avg.i32z));

  // Render it
  RET_WHEN_ERR(pcd8544writeFbToLCD());

  return eErrCode;
}
// ========================| Middle level functions |=========================
esp_err_t uiDispDrawMenu(const char *pacHead, const char **pacOptions,
                         uint8_t u8currentOptIdx, uint8_t u8maxOptIdx) {
  assert(pacHead);
  assert(pacOptions);
  assert(u8currentOptIdx <= u8maxOptIdx);

  esp_err_t eErrCode = ESP_OK;

  // Clear display
  RET_WHEN_ERR(pcd8544clearFb());

  // Write header
  RET_WHEN_ERR(pcd8544putsFb(pacHead, 0, 0));

  // Minimum option index is 0. Maximum is variable. Calculate "show minimum"
  // and "show maximum" index.
  uint8_t u8showMin, u8showMax;

  if (u8maxOptIdx <= 4) {
    // Will fit without any adjusting
    u8showMin = 0;
    u8showMax = u8maxOptIdx;
  } else {
    /* Need to calculate showing range more intelligently. Theory says
     * that on display fits 5 options (1st line is occupied by header).
     * Therefore we can display 2 options before and 2 after current index.
     * That sounds easy, but still need to handle cases when there are
     * no more options.
     */
    if (u8currentOptIdx < 2) {
      // 0, 1 -> minimum is 0
      u8showMin = 0;
    } else {
      u8showMin = u8currentOptIdx - 2;
    }

    // Can display 5 options only
    u8showMax = u8showMin + 4;
    if (u8showMax > u8maxOptIdx) {
      u8showMax = u8maxOptIdx;
    }
  }

  assert(u8showMax <= u8maxOptIdx);
  assert(u8showMin <= u8maxOptIdx);
  assert(u8showMin <= u8showMax);

  uint8_t u8lineCnt = 1;
  // Write down modes. Valid modes are after service mode
  for (uint8_t u8idxCnt = u8showMin; u8idxCnt < (u8showMax + 1); u8idxCnt++) {
    // Initial X coordination 5 - give space for cursor
    // initial line is based on current mode. The line number "0" is
    // already used -> need to keep that in mind
    RET_WHEN_ERR(pcd8544putsFb(pacOptions[u8idxCnt], 5, u8lineCnt));

    if (u8idxCnt == u8currentOptIdx) {
      // Write cursor on specific line - based on line, X position is 0
      RET_WHEN_ERR(uiDispExDrawCursorRight4px(0, u8lineCnt));
    }

    u8lineCnt++;
  }

  // Render it
  RET_WHEN_ERR(pcd8544writeFbToLCD());

  return eErrCode;
}
// ==========================| Low level functions |==========================
esp_err_t uiDispDrawCircle(const uint8_t u8centerX, const uint8_t u8centerY,
                           const uint8_t u8size) {
  esp_err_t eErrCode;
  int32_t i32endX, i32endY;

  for (int32_t i32angle = 0; i32angle < 360; i32angle++) {
    mf_get_xy_coordinates((int32_t)u8centerX, (int32_t)u8centerY,
                          (int32_t)u8size, i32angle, &i32endX, &i32endY);
    assert(i32endX >= 0);
    assert(i32endX < PCD8544_X_RES);
    assert(i32endY >= 0);
    assert(i32endY < PCD8544_Y_RES);

    RET_WHEN_ERR(
        pcd8544setPixelInFb((uint8_t)i32endX, (uint8_t)i32endY, WRITE_SET));
  }

  return ESP_OK;
}
// ==========================| Internal functions |===========================
static esp_err_t _writeLogoHeader(void) {
  esp_err_t eErrCode = ESP_FAIL;
  ;
  RET_WHEN_ERR(pcd8544putsFb("=<| ", 0, 0));

  // Write "D" in azbuka - special character
  const uint8_t au8azbukaD[] = {0b01100000, 0b00111110, 0b00100001,
                                0b00111111, 0b01100000, 0};
  // 4 characters with length 6 (5 font + 1 space) was already written
  RET_WHEN_ERR(pcd8544setBytesInFb(au8azbukaD, sizeof(au8azbukaD), 4 * (5 + 1),
                                   0, WRITE_SET));

  // Move behind azbuka symbol
  RET_WHEN_ERR(pcd8544putsFb("A3-20 |>=", (4 + 1) * (5 + 1), 0));

  return eErrCode;
}

static esp_err_t _writeLogo(const uint16_t u16waitDelayMs) {
  esp_err_t eErrCode = ESP_FAIL;
  RET_WHEN_ERR(pcd8544setBytesInFb(gu8logo, sizeof(gu8logo), 0, 0, WRITE_SET));

  RET_WHEN_ERR(pcd8544writeFbToLCD());

  vTaskDelay(u16waitDelayMs / portTICK_PERIOD_MS);

  return eErrCode;
}

static esp_err_t _cleanDisp(void) {
  pcd8544clearFb();
  return (pcd8544writeFbToLCD());
}
