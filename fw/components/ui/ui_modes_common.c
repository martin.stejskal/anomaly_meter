/**
 * @file
 * @author Martin Stejskal
 * @brief Kind of IPC for UI and separated modes
 */
// ===============================| Includes |================================
// Standard
#include "ui_modes_common.h"

#include <assert.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef struct {
  bool bRequestSwitchServiceMode;
  bool bSwitchedToServiceMode;
  bool bRequestLeaveServiceMode;
} tsServiceModeSwitcher;
// ===========================| Global variables |============================
/**
 * @brief Flag is set to true when task itself decide to exit
 */
static bool mbExit = false;

static bool mbForceExit = false;

static tsServiceModeSwitcher msServiceModeSwitcher = {
    .bRequestSwitchServiceMode = 0,
    .bSwitchedToServiceMode = 0,
    .bRequestLeaveServiceMode = 0,
};
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
void taskSetModeExit(void) { mbExit = true; }

bool uiGetIsModeExited(void) { return mbExit; }

void uiClearModeExitFlag(void) { mbExit = false; }

void uiForceCloseTaskRequest(bool bForceExit) { mbForceExit = bForceExit; }

bool taskGetForceCloseFlag(void) { return mbForceExit; }

void requestUiEnableServiceMode(void) {
  // There should not be any pending request yet
  assert(!msServiceModeSwitcher.bRequestSwitchServiceMode);
  assert(!msServiceModeSwitcher.bRequestLeaveServiceMode);

  // Also we should not be switched to service mode already
  assert(!msServiceModeSwitcher.bSwitchedToServiceMode);

  msServiceModeSwitcher.bRequestSwitchServiceMode = true;
}

void requestUiDisableServiceMode(void) {
  // There should not be any pending request yet
  assert(!msServiceModeSwitcher.bRequestSwitchServiceMode);
  assert(!msServiceModeSwitcher.bRequestLeaveServiceMode);

  // We should be switched to service mode
  assert(msServiceModeSwitcher.bSwitchedToServiceMode);

  msServiceModeSwitcher.bRequestLeaveServiceMode = true;
}

bool isUiInServiceMode(void) {
  return msServiceModeSwitcher.bSwitchedToServiceMode;
}

teUiReq uiGetReq(void) {
  // There should not be 2 requests
  assert(!(msServiceModeSwitcher.bRequestSwitchServiceMode &&
           msServiceModeSwitcher.bRequestLeaveServiceMode));

  if (msServiceModeSwitcher.bRequestSwitchServiceMode) {
    return UI_REQ_EN_SERVICE_MODE;
  }
  if (msServiceModeSwitcher.bRequestLeaveServiceMode) {
    return UI_REQ_DIS_SERVICE_MODE;
  }
  return UI_REQ_NONE;
}

void uiSwithedToServiceMode(void) {
  assert(msServiceModeSwitcher.bRequestSwitchServiceMode);
  assert(!msServiceModeSwitcher.bRequestLeaveServiceMode);
  assert(!msServiceModeSwitcher.bSwitchedToServiceMode);

  // Clean pending request
  msServiceModeSwitcher.bRequestSwitchServiceMode = false;
  msServiceModeSwitcher.bSwitchedToServiceMode = true;
}

void uiExitedServiceMode(void) {
  assert(msServiceModeSwitcher.bRequestLeaveServiceMode);
  assert(!msServiceModeSwitcher.bRequestSwitchServiceMode);
  assert(msServiceModeSwitcher.bSwitchedToServiceMode);

  // Clean pending request
  msServiceModeSwitcher.bRequestLeaveServiceMode = false;
  msServiceModeSwitcher.bSwitchedToServiceMode = false;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
const char *uiGetModeName(teUiModes eCurrentMode) {
  const static char *pacNone = "Dummy";
  const static char *pacService = "Service";
  const static char *pacAnomDet = "Detector";
  const static char *pacAccGraph = "Acceleration";
  const static char *pacBatt = "Battery";
  const static char *pacFlashlight = "Flashlight";
  const static char *pacSettings = "Settings";
  const static char *pacCompass = "Compass";
  const static char *pacMAX = "MAX";

  switch (eCurrentMode) {
    case UI_MODE_NONE:
      return pacNone;
    case UI_MODE_SERVICE:
      return pacService;
    case UI_MODE_ANOMALY_DETECTOR:
      return pacAnomDet;
    case UI_MODE_ACC_GRAPH:
      return pacAccGraph;
    case UI_MODE_BATT:
      return pacBatt;
    case UI_MODE_FLASHLIGHT:
      return pacFlashlight;
    case UI_MODE_SETTINGS:
      return pacSettings;
    case UI_MODE_COMPASS:
      return pacCompass;
    case UI_MODE_MAX:
      return pacMAX;
  }

  // This should not happen
  return "ERROR";
}

uint8_t ui_detector_wifi_rssi_to_u8(const int8_t i8_rssi) {
  // Adjust value. In practice we can expect RSSI from -20 to -100.
  // RSSI can be zero, if nothing was found
  assert(i8_rssi <= 0);

  const int32_t i32_max = UI_ANOMALY_DETECTOR_DEAD;
  const int32_t i32_min = UI_ANOMALY_DETECTOR_NO_WARNING;

  // Check boundaries
  if (i8_rssi > i32_max) {
    // Maximum signal reached
    return 0xFF;

  } else if (i8_rssi < i32_min) {
    // Can not go lower actually
    return 0;
  }

  // Algorithm is based on wifiRssiToU8maxRange()
  int32_t i32_x = (255000) / (i32_max - i32_min);
  int32_t i32_y = (-1 * i32_min * 255000) / (i32_max - i32_min);

  int32_t i32_res = i8_rssi * i32_x + i32_y;
  // Rounding
  i32_res += 500;

  // Everything is multiplied by 1000 -> divide it
  i32_res /= 1000;

  assert(i32_res >= 0);
  assert(i32_res < 256);

  return (uint8_t)(i32_res);
}
