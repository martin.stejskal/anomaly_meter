/**
 * @file
 * @author Martin Stejskal
 * @brief Battery mode
 */
#ifndef __UI_MODE_BATT_H__
#define __UI_MODE_BATT_H__
// ===============================| Includes |================================
// ================================| Defines |================================
/**
 * @brief Minimum allowed zoom multiplied by 1000
 *
 * To avoid working with float, algorithms using integer values, but multiplied
 * by 1000. That allow to transfer 3 fractional digits in decimal format
 */
#define UI_MODE_BATT_MIN_ZOOM_1000X (1 * 1000)

/**
 * @brief Maximum allowed zoom multiplied by 1000
 *
 *To avoid working with float, algorithms using integer values, but multiplied
 * by 1000. That allow to transfer 3 fractional digits in decimal format
 */
#define UI_MODE_BATT_MAX_ZOOM_1000X (100 * 1000)

/**
 * @brief Zoom multiplier multiplied by 1000
 *
 * This value define zoom "step" multiplied by 1000.
 * To avoid working with float, algorithms using integer values, but multiplied
 * by 1000. That allow to transfer 3 fractional digits in decimal format
 */
#define UI_MODE_BATT_MUL_ZOOM_1000X (1500)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
void uiModeBattTaskRTOS(void *pvParameters);
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
#endif  // __UI_MODE_BATT_H__
