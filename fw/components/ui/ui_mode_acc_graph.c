/**
 * @file
 * @author Martin Stejskal
 * @brief Acceleration mode with graph
 */
// ===============================| Includes |================================
#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ESP specific
#include <esp_log.h>

// Local modules
#include "cfg.h"
#include "keyboard.h"
#include "ring_buff.h"
#include "sensors.h"
#include "ui_display.h"
#include "ui_mode_acc_graph.h"
#include "ui_modes_common.h"

// ================================| Defines |================================
#define _LCD_REFRESH_PERIOD_MS (250)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================

// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "ACC mode";
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
void uiModeAccTaskRTOS(void *pvParameters) {
  ESP_LOGI(TAG, "Executing ACC mode...");

  // Ring buffer which will keep all the data
  tsVector16 asBufferSamples[UI_DISP_X_RES];

  // Configure ring buffer accordingly
  tsRingBuffer sAccDispRingBuff = {
      .pvBuffer = asBufferSamples,
      .u16bufferSizeBytes = sizeof(asBufferSamples),
      .u8itemSizeBytes = sizeof(tsVector16)};
  ringBuffClean(&sAccDispRingBuff);

  tsVector16 sVector16tmp;

  // Division vector - take original value, divide by these and result
  // will be displayed. This allow dynamically change graph Y range
  tsVector16 sVectorDiv = {
      .i16x = 67,
      .i16y = 67,
      .i16z = 67,
  };

  // Register pressed buttons
  tsKbrdBtns sButtons = {0};

  uint32_t u32actualTimeMs = GET_CLK_MS_32BIT();
  uint32_t u32nextLcdRefreshMs = u32actualTimeMs + _LCD_REFRESH_PERIOD_MS;

  uint8_t u8runTask = 1;

  while (u8runTask) {
    // Get accelerometer data and push them to buffer
    ESP_ERROR_CHECK(sensAccGetInt(&sVector16tmp));
    ringBuffPush(&sAccDispRingBuff, &sVector16tmp);

    // Set read index
    ringBuffResetReadIdx(&sAccDispRingBuff);

    // And display data - row 3 is starting point
    ESP_ERROR_CHECK(uiDispVectGraphRow(&sAccDispRingBuff, &sVectorDiv));

    while (u32actualTimeMs < u32nextLcdRefreshMs) {
      // Keyboard test
      kbrPressedReleased(&sButtons);
      if (kbrdAnyButtonPressed(&sButtons)) {
        kbrdShowPressedButtons(&sButtons);

        // Get out from this mode
        if (sButtons.bLeft) {
          u8runTask = 0;
          break;
        }
      }

      // Check if UI request exit this task
      if (taskGetForceCloseFlag()) {
        u8runTask = 0;
        break;
      }

      // Keep scanning keyboard within some intervals. Take a nap
      vTaskDelay(KBRD_RECOMMENDED_SCAN_TIME_MS / portTICK_PERIOD_MS);

      u32actualTimeMs = GET_CLK_MS_32BIT();
    }
    // Setup new timeout
    u32nextLcdRefreshMs = u32actualTimeMs + _LCD_REFRESH_PERIOD_MS;
  }

  taskSetModeExit();
  // Terminate itself
  vTaskDelete(NULL);
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
