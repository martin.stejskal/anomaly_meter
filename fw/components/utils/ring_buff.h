/**
 * @file
 * @author Martin Stejskal
 * @brief Ring buffer utility
 */
#ifndef __RING_BUFF_H__
#define __RING_BUFF_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
/**
 * @brief Generic structure for ring buffer
 */
typedef struct {
  // Pointer to buffer
  void *pvBuffer;

  // Total buffer size in Bytes
  const uint16_t u16bufferSizeBytes;

  // Item size in Bytes
  const uint8_t u8itemSizeBytes;

  // Read index
  uint16_t u16readIdx;

  // Write index
  uint16_t u16writeIdx;
} tsRingBuffer;

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Push data into ring buffer
 * @param[in, out] psRingBuffer Pointer to ring buffer structure
 * @param[in] pvData Pointer to input data
 */
void ringBuffPush(tsRingBuffer *psRingBuffer, void *pvData);

/**
 * @brief Pull data from ring buffer
 * @param[in, out] psRingBuffer Pointer to ring buffer structure
 * @param[out] pvData Data will be pulled into this memory space
 */
void ringBuffPull(tsRingBuffer *psRingBuffer, void *pvData);
// ========================| Middle level functions |=========================
/**
 * @brief Reset ring buffer include content
 * @param[in, out] psRingBuffer Pointer to ring buffer structure
 */
void ringBuffClean(tsRingBuffer *psRingBuffer);

/**
 * @brief Set given value across whole ring buffer
 * @param[in, out] psRingBuffer Pointer to ring buffer structure
 * @param[in] pvValue Input value
 */
void ringBuffSet(tsRingBuffer *psRingBuffer, void *pvValue);

/**
 * @brief Read out latest value while not touch read index
 * @param[in, out] psRingBuffer Pointer to ring buffer structure
 * @param[out] pvData Data will be pulled into this memory space
 */
void ringBuffPullLatest(tsRingBuffer *psRingBuffer, void *pvData);

/**
 * @brief Read item from ring buffer without touching read index
 * @param[in, out] psRingBuffer Pointer to ring buffer structure
 * @param[out] pvData Data will be pulled into this memory space
 * @param[in] u16relativeIdx Relative index where 0 means "oldest value"
 */
void ringBuffPullRelative(tsRingBuffer *psRingBuffer, void *pvData,
                          const uint16_t u16relativeIdx);
// ==========================| Low level functions |==========================
/**
 * @brief Set read index to "beginning" relative to write index
 * @param[in, out] psRingBuffer Pointer to ring buffer structure
 */
void ringBuffResetReadIdx(tsRingBuffer *psRingBuffer);

/**
 * @brief Returns number of items in ring buffer
 * @param[in] psRingBuffer Pointer to ring buffer structure
 * @return Number of items in given ring buffer
 */
uint16_t ringBuffGetNumOfItems(tsRingBuffer *psRingBuffer);
#endif  // __RING_BUFF_H__
