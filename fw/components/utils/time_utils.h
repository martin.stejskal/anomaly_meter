/**
 * @file
 * @author Martin Stejskal
 * @brief Mainly convert time in integer forms to another
 */
#ifndef __TIME_UTILS_H__
#define __TIME_UTILS_H__
// ===============================| Includes |================================
#include <stdint.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
void time_util_sec_to_hh_mm_ss(char *pac_txt_buffer, uint32_t u32_seconds);
void time_util_sec_to_hh_mm(char *pac_txt_buffer, uint32_t u32_seconds);
// ==========================| Low level functions |==========================

#endif  // __TIME_UTILS_H__
