/**
 * @file
 * @author Martin Stejskal
 * @brief Tests for time utilities
 */
// ===============================| Includes |================================
#include "time_utils.h"

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================
static uint32_t _timeConvTest(uint32_t u32sec) {
  // Just buffer for storing data
  char acTxtOutput[100];

  time_util_sec_to_hh_mm_ss(acTxtOutput, u32sec);
  printf("%d -> %s\n", u32sec, acTxtOutput);

  // Right now very simple processing - human should check it
  return 0;
}

// =========================| High level functions |==========================
int main(void) {
  uint32_t i32numOfProblems = 0;

  // Define test cases
  i32numOfProblems += _timeConvTest(35);     // 0:35
  i32numOfProblems += _timeConvTest(63);     // 1:03
  i32numOfProblems += _timeConvTest(6301);   // 1:45:01
  i32numOfProblems += _timeConvTest(11143);  // 3:05:43
  i32numOfProblems += _timeConvTest(57943);  // 16:05:43

  if (i32numOfProblems == 0) {
    printf("%s : No problems detected\n", __func__);
  } else {
    printf("%s : %d problems detected\n", __func__, i32numOfProblems);
  }

  return i32numOfProblems;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
