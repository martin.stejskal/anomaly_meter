/**
 * @file
 * @author Martin Stejskal
 * @brief Ring buffer utility
 */
// ===============================| Includes |================================
#include "ring_buff.h"

#include <assert.h>
#include <string.h>
// ================================| Defines |================================
#define _GET_NUM_OF_ITEMS(ringBuffer) \
  (ringBuffer->u16bufferSizeBytes / ringBuffer->u8itemSizeBytes)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static void _checkRingBuffer(tsRingBuffer *psRingBuffer);
static void _incIdxAndcheck(uint16_t *pu16idx, size_t uMaxIdx);
static void _decIdxAndCheck(uint16_t *pu16idx, size_t uMaxIdx);
// =========================| High level functions |==========================
void ringBuffPush(tsRingBuffer *psRingBuffer, void *pvData) {
  // Pointer to data should not be empty
  assert(pvData);

  _checkRingBuffer(psRingBuffer);

  // Calculate correct position in buffer. Need to work at Byte level.
  // First set beginning of buffer and later add offset.
  uint8_t *pu8data = (uint8_t *)psRingBuffer->pvBuffer;

  pu8data += psRingBuffer->u16writeIdx * psRingBuffer->u8itemSizeBytes;

  // Write data to given position
  memcpy(pu8data, pvData, (size_t)psRingBuffer->u8itemSizeBytes);

  _incIdxAndcheck(&psRingBuffer->u16writeIdx, _GET_NUM_OF_ITEMS(psRingBuffer));
}

void ringBuffPull(tsRingBuffer *psRingBuffer, void *pvData) {
  // Pointer to data should not be empty
  assert(pvData);

  _checkRingBuffer(psRingBuffer);

  // Calculate correct position in buffer. Need to work at Byte level.
  // First set beginning of buffer and later add offset.
  uint8_t *pu8data = (uint8_t *)psRingBuffer->pvBuffer;

  pu8data += psRingBuffer->u16readIdx * psRingBuffer->u8itemSizeBytes;

  // Write data to given position
  memcpy(pvData, pu8data, (size_t)psRingBuffer->u8itemSizeBytes);

  _incIdxAndcheck(&psRingBuffer->u16readIdx, _GET_NUM_OF_ITEMS(psRingBuffer));
}

// ========================| Middle level functions |=========================
void ringBuffClean(tsRingBuffer *psRingBuffer) {
  _checkRingBuffer(psRingBuffer);

  // Clean content
  memset(psRingBuffer->pvBuffer, 0, (size_t)psRingBuffer->u16bufferSizeBytes);

  // Reset read & write index
  psRingBuffer->u16readIdx = psRingBuffer->u16writeIdx = 0;
}

void ringBuffSet(tsRingBuffer *psRingBuffer, void *pvValue) {
  // Calculate correct position in buffer. Need to work at Byte level.
  // First set beginning of buffer and later add offset.
  uint8_t *pu8data = (uint8_t *)psRingBuffer->pvBuffer;

  for (uint16_t u16itemIdx = 0; u16itemIdx < _GET_NUM_OF_ITEMS(psRingBuffer);
       u16itemIdx++) {
    memcpy(pu8data, pvValue, psRingBuffer->u8itemSizeBytes);

    pu8data += psRingBuffer->u8itemSizeBytes;
  }
}

void ringBuffPullLatest(tsRingBuffer *psRingBuffer, void *pvData) {
  // Pointer to data should not be empty
  assert(pvData);

  _checkRingBuffer(psRingBuffer);

  // Latest value is "behind" write index
  uint16_t u16idx = psRingBuffer->u16writeIdx;
  _decIdxAndCheck(&u16idx, _GET_NUM_OF_ITEMS(psRingBuffer));

  // Calculate correct position in buffer. Need to work at Byte level.
  // First set beginning of buffer and later add offset.
  uint8_t *pu8data = (uint8_t *)psRingBuffer->pvBuffer;

  pu8data += u16idx * psRingBuffer->u8itemSizeBytes;

  memcpy(pvData, pu8data, (size_t)psRingBuffer->u8itemSizeBytes);
}

void ringBuffPullRelative(tsRingBuffer *psRingBuffer, void *pvData,
                          const uint16_t u16relativeIdx) {
  // Pointer to data should not be empty
  assert(pvData);

  uint16_t u16numOfItems = _GET_NUM_OF_ITEMS(psRingBuffer);

  // Relative index should not be bigger than number of items
  assert(u16relativeIdx < u16numOfItems);

  _checkRingBuffer(psRingBuffer);

  // The "latest item" is on "write index" position
  uint16_t u16idx = psRingBuffer->u16writeIdx + u16relativeIdx;

  if (u16idx >= u16numOfItems) {
    // Index is bigger than maximum -> need to start from beginning, while
    // deduct "overlap"
    u16idx = u16idx - u16numOfItems;
  }

  // Get pointer for given index
  uint8_t *pu8data = (uint8_t *)psRingBuffer->pvBuffer;

  pu8data += u16idx * psRingBuffer->u8itemSizeBytes;

  memcpy(pvData, pu8data, (size_t)psRingBuffer->u8itemSizeBytes);
}
// ==========================| Low level functions |==========================

void ringBuffResetReadIdx(tsRingBuffer *psRingBuffer) {
  _checkRingBuffer(psRingBuffer);

  psRingBuffer->u16readIdx = psRingBuffer->u16writeIdx;
}

inline uint16_t ringBuffGetNumOfItems(tsRingBuffer *psRingBuffer) {
  _checkRingBuffer(psRingBuffer);

  return _GET_NUM_OF_ITEMS(psRingBuffer);
}

// ==========================| Internal functions |===========================
/**
 * @brief Check ring buffer structure
 * @param psRingBuffer Pointer to ring buffer structure
 */
static void _checkRingBuffer(tsRingBuffer *psRingBuffer) {
  // Pointer to rung buffer should not be empty
  assert(psRingBuffer);
  // Write index should not overlap maximum address
  assert(psRingBuffer->u16writeIdx < _GET_NUM_OF_ITEMS(psRingBuffer));

  // Items should be aligned to buffer size
  assert((psRingBuffer->u16bufferSizeBytes % psRingBuffer->u8itemSizeBytes) ==
         0);
}

/**
 * @brief Increase index number and check if not overflow. If so, reset index
 * @param pu16idx Pointer to index
 * @param uMaxIdx When reached this value, index will be reset to zero
 */
static void _incIdxAndcheck(uint16_t *pu16idx, size_t uMaxIdx) {
  *pu16idx += 1;

  if (*pu16idx >= uMaxIdx) {
    *pu16idx = 0;
  } else {
    // Keep value as it is
  }
}

/**
 * @brief Decrease index number. If value is lower than 1, then set maximum
 *        index value
 * @param pu16idx Pointer to index
 * @param uMaxIdx If current index is zero, this value will be used
 */
static void _decIdxAndCheck(uint16_t *pu16idx, size_t uMaxIdx) {
  if (*pu16idx >= 1) {
    *pu16idx -= 1;
  } else {
    // Max index is actually "sizeof(array)" so actually as index we have
    // to use 1 lower
    *pu16idx = uMaxIdx - 1;
  }
}
