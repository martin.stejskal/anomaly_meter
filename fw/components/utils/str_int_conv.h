/**
 * @file
 * @author Martin Stejskal
 * @brief Provide basic conversion functionality between string and integer
 */
#ifndef __STR_INT_CONV_H__
#define __STR_INT_CONV_H__
// ===============================| Includes |================================
// Standard
#include <stdint.h>

// ESP specific
#include <esp_err.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Convert string to 32 bit unsigned integer
 * @param pacStr Pointer to string
 * @param pu32result Result will be written here
 * @return ESP_OK if no error
 */
esp_err_t strToUint32(const char *pacStr, uint32_t *pu32result);

/**
 * @brief Convert 2 bytes from string into integer
 * @param pacStr Pointer to string with 2 Bytes of HEX value as string
 * @param pu8result Converted value will be written there
 * @return ESP_OK if no error
 */
esp_err_t strHex2ByteToInt(const char *pacStr, uint8_t *pu8result);

// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
/**
 * @brief Convert character ASCII HEX value to integer
 * @param cHex HEX value in ASCII
 * @param piResult Result will be written here
 * @return ESP_OK if no error
 */
esp_err_t charHexToInt(const char cHex, uint8_t *pu8result);
#endif  // __STR_INT_CONV_H__
