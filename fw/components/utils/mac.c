/**
 * @file
 * @author Martin Stejskal
 * @brief Library that deal with MAC functions
 */
// ===============================| Includes |================================
// Standard
#include "mac.h"

#include <assert.h>
#include <string.h>

#include "str_int_conv.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
char *macBinToStr(uint8_t au8mac[static MAC_SIZE_BYTES]) {
  // Check for empty pointer
  assert(au8mac);
  // Expecting 6 Bytes
  assert(MAC_SIZE_BYTES == 6);

  // Every Byte need 2 digits, 5x ":" + null character.
  // Variable is static, so when pointer is returned, value is kept until
  // rewritten
  static char acMac[MAC_SIZE_BYTES * 2 + 5 + 1] = "??:??:??:??:??:??";

  sprintf(acMac, "%02x:%02x:%02x:%02x:%02x:%02x", au8mac[0], au8mac[1],
          au8mac[2], au8mac[3], au8mac[4], au8mac[5]);
  return acMac;
}

esp_err_t macStrToBin(char *pacStr, uint8_t au8mac[static MAC_SIZE_BYTES]) {
  esp_err_t eErrCode = ESP_FAIL;

  // Expected format: xx:xx:xx:xx:xx:xx -> 17 chars
  if (strlen(pacStr) != (MAC_SIZE_BYTES * 2 + 5)) {
    // Well, there still can be optional argument, so need to check if there
    // is space
    char *pacOptionalDescription = pacStr + (MAC_SIZE_BYTES * 2 + 5);
    if (*pacOptionalDescription == ' ') {
      // OK, there is space, so that is expected
    } else {
      // Something unexpected -> raise error
      return ESP_ERR_INVALID_MAC;
    }
  }

  // Expecting 6 Bytes split by ":"
  for (int iByteCnt = 0; iByteCnt < 6; iByteCnt++) {
    // Pass pointer to MAC array as "base address" + offset
    eErrCode = strHex2ByteToInt(pacStr, au8mac + iByteCnt);
    if (eErrCode) {
      return eErrCode;
    }
    // And move string pointer by 3 (format is "xx:")
    pacStr += 3;
  }

  return eErrCode;
}

inline bool areMacSame(const uint8_t au8mac1[static MAC_SIZE_BYTES],
                       const uint8_t au8mac2[static MAC_SIZE_BYTES]) {
  if (memcmp(au8mac1, au8mac2, MAC_SIZE_BYTES) == 0) {
    // They're same
    return true;
  } else {
    // They differ
    return false;
  }
}

bool macIsEmpty(uint8_t au8mac[static MAC_SIZE_BYTES]) {
  for (uint8_t u8idx = 0; u8idx < MAC_SIZE_BYTES; u8idx++) {
    // Empty MAC is FF:FF:FF:FF:FF:FF -> all have to be 0xFF otherwise not
    // empty
    if (au8mac[u8idx] != 0xFF) {
      return false;
    }
  }

  return true;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
