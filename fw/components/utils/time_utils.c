/**
 * @file
 * @author Martin Stejskal
 * @brief Mainly convert time in integer forms to another
 */
// ===============================| Includes |================================
#include "time_utils.h"

#include <stdio.h>
// ================================| Defines |================================

// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================

// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
void time_util_sec_to_hh_mm_ss(char *pac_txt_buffer, uint32_t u32_seconds) {
  uint8_t u8_sec, u8_min = 0;
  uint32_t u32_hour = 0;

  u8_sec = u32_seconds % 60;
  u8_min = (u32_seconds % (60 * 60)) / 60;

  u32_hour = u32_seconds / (60 * 60);

  if ((u8_min == 0) && (u32_hour == 0)) {
    // Only seconds here
    sprintf(pac_txt_buffer, "%d", u8_sec);

  } else if (u32_hour == 0) {
    // Minutes + seconds
    sprintf(pac_txt_buffer, "%d:%02d", u8_min, u8_sec);

  } else {
    // Hours, minutes, seconds
    sprintf(pac_txt_buffer, "%d:%02d:%02d", u32_hour, u8_min, u8_sec);
  }
}

void time_util_sec_to_hh_mm(char *pac_txt_buffer, uint32_t u32_seconds) {
  uint8_t u8_min = 0;
  uint32_t u32_hour = 0;

  u8_min = (u32_seconds % (60 * 60)) / 60;

  u32_hour = u32_seconds / (60 * 60);

  if ((u8_min == 0) && (u32_hour == 0)) {
    // Nothing to display -> NULL
    *pac_txt_buffer = 0;

  } else if (u32_hour == 0) {
    // Minutes
    sprintf(pac_txt_buffer, "%d", u8_min);

  } else {
    // Hours, minutes
    sprintf(pac_txt_buffer, "%d:%02d", u32_hour, u8_min);
  }
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
