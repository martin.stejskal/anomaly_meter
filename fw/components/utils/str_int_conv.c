/**
 * @file
 * @author Martin Stejskal
 * @brief Provide basic conversion functionality between string and integer
 */
// ===============================| Includes |================================
#include "str_int_conv.h"

#include <errno.h>
#include <stdlib.h>
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
esp_err_t strToUint32(const char *pacStr, uint32_t *pu32result) {
  char *pDummy;

  long lConverted = strtol(pacStr, &pDummy, 10);

  if (errno == ERANGE) {
    // Value out of range
    return ESP_ERR_INVALID_ARG;
  }

  // If does not fit into 32 bit value
  if (lConverted > 0xFFFFFFFF) {
    return ESP_ERR_INVALID_ARG;
  }

  // Else everything went well
  *pu32result = (uint32_t)lConverted;

  return ESP_OK;
}

esp_err_t strHex2ByteToInt(const char *pacStr, uint8_t *pu8result) {
  // Take two characters (low and high 4 bits) and convert them
  esp_err_t eErrCode;
  uint8_t u8high4bits, u8low4bits;

  eErrCode = charHexToInt(pacStr[0], &u8high4bits);
  if (eErrCode) {
    return eErrCode;
  }

  eErrCode = charHexToInt(pacStr[1], &u8low4bits);
  if (eErrCode) {
    return eErrCode;
  }

  // If program get there, conversions were successful -> calculate result
  *pu8result = 16 * u8high4bits + u8low4bits;

  return ESP_OK;
}

// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
esp_err_t charHexToInt(const char cHex, uint8_t *pu8result) {
  // 0~9
  if ((cHex >= '0') && (cHex <= '9')) {
    // Char is just integer -> decrease ACSII "0" and get integer result
    *pu8result = cHex - '0';
    return ESP_OK;
  } else if ((cHex >= 'A') && (cHex <= 'F')) {
    // Same as before - "Minus A" is the key. Since A means 10 -> +10
    *pu8result = cHex - 'A' + 10;
    return ESP_OK;
  } else if ((cHex >= 'a') && (cHex <= 'f')) {
    *pu8result = cHex - 'a' + 10;
    return ESP_OK;
  }

  return ESP_ERR_INVALID_ARG;
}
// ==========================| Internal functions |===========================
