/**
 * @file
 * @author Martin Stejskal
 * @brief Test for some WiFi functionality
 */
// ===============================| Includes |================================
#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
// ================================| Defines |================================
#define WIFI_MIN_RSSI (-100)
#define WIFI_MAX_RSSI (-20)
// ============================| Default values |=============================

// ==========================| Preprocessor checks |==========================

// =======================| Structures, enumerations |========================

// ===========================| Global variables |============================

// ===============================| Functions |===============================
uint8_t wifiRssiToU8maxRangeOriginal(int8_t i8rssi) {
  // Temporary calculations, which can not be easily done with integer
  // (actually they can - multiply it 1000x, but let's make it simple now)
  float fTmp;

  // RSSI value suitable for graph
  uint8_t u8rssi;

  // Adjust value. In practice we can expect RSSI from -20 to -100.
  // RSSI can be zero, if nothing was found
  assert(i8rssi <= 0);

  // Check boundaries
  if (i8rssi > WIFI_MAX_RSSI) {
    // Maximum signal reached
    return 0xFF;
    i8rssi = WIFI_MAX_RSSI;

  } else if (i8rssi < WIFI_MIN_RSSI) {
    // Can not go lower actually
    return 0;
  }

  // From -20 ~ -100 range we need to get 255 ~ 0 range.
  // Based on that, We can write down 2 equations:
  //  255 = RSSImax * x + y
  //    0 = RSSImin * x + y
  //
  //  -----------------------
  //  y = - RSSImin * x // From 2nd equation
  //
  //  255 = RSSImax * x - RSSImin * x
  //
  //  ------------------------------
  //  x = (255)/(RSSImax - RSSImin)
  //  y = (- RSSImin * 255)/(RSSImax - RSSImin)
  //
  //  Once you have the x and y constants, following equation can be used
  //  out = RSSI * x + y
  //
  // The +0.5 is for rounding purposes
  ///@todo Optimize - use 32 bit integer instead
  fTmp = ((float)i8rssi) *
             (255 / ((float)WIFI_MAX_RSSI - 1 * (float)WIFI_MIN_RSSI)) +
         (((float)WIFI_MIN_RSSI * -255) /
          ((float)WIFI_MAX_RSSI - 1 * (float)WIFI_MIN_RSSI)) +
         0.5;
  assert(fTmp >= 0);
  assert(fTmp < 256);  // Should not be be bigger than 8 bits -> 255

  u8rssi = (uint8_t)fTmp;

  return u8rssi;
}

uint8_t wifiRssiToU8maxRangeSimplified(int8_t i8rssi) {
  // Adjust value. In practice we can expect RSSI from -20 to -100.
  // RSSI can be zero, if nothing was found
  assert(i8rssi <= 0);

  // Check boundaries
  if (i8rssi > WIFI_MAX_RSSI) {
    // Maximum signal reached
    return 0xFF;
    i8rssi = WIFI_MAX_RSSI;

  } else if (i8rssi < WIFI_MIN_RSSI) {
    // Can not go lower actually
    return 0;
  }

  // From -20 ~ -100 range we need to get 255 ~ 0 range.
  // Based on that, We can write down 2 equations:
  //  255 = RSSImax * x + y
  //    0 = RSSImin * x + y
  //
  //  -----------------------
  //  y = - RSSImin * x // From 2nd equation
  //
  //  255 = RSSImax * x - RSSImin * x
  //
  //  ------------------------------
  //  x = (255)/(RSSImax - RSSImin)
  //  y = (- RSSImin * 255)/(RSSImax - RSSImin)
  //
  //  Once you have the x and y constants, following equation can be used
  //  out = RSSI * x + y
  //
  // The +0.5 is for rounding purposes
  int iX = (255000) / (WIFI_MAX_RSSI - WIFI_MIN_RSSI);
  int iY = (-1 * WIFI_MIN_RSSI * 255000) / (WIFI_MAX_RSSI - WIFI_MIN_RSSI);

  int iRes = i8rssi * iX + iY;

  // Rounding (0.5 * 1000)
  iRes += 500;

  // Everything was multiplied by 1000 -> divide it
  iRes /= 1000;

  assert(iRes >= 0);
  assert(iRes < 256);

  return (uint8_t)(iRes);
}
// ===============| Internal function prototypes: high level |================
// ==============| Internal function prototypes: middle level |===============
// ================| Internal function prototypes: low level |================

// =========================| High level functions |==========================
int main(void) {
  uint8_t u8original, u8simplified = 0;
  int iDiff;

  // Use full "negative" range for 8 bit value.
  for (int iRunIdx = 0; iRunIdx >= (-128); iRunIdx--) {
    u8original = wifiRssiToU8maxRangeOriginal(iRunIdx);
    u8simplified = wifiRssiToU8maxRangeSimplified(iRunIdx);

    printf("Input: %d | Original: %d | Modified: %d\n", iRunIdx, u8original,
           u8simplified);

    iDiff = u8original - u8simplified;
    if (iDiff != 0) {
      printf("Diff: %d", iDiff);
      return 1;
    }
  }

  return 0;
}
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================

// ====================| Internal functions: high level |=====================
// ===================| Internal functions: middle level |====================
// =====================| Internal functions: low level |=====================
