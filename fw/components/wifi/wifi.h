/**
 * @file
 * @author Martin Stejskal
 * @brief WiFi driver for ESP32
 */
#ifndef __WIFI_H__
#define __WIFI_H__
// ===============================| Includes |================================
// Standard
#include <stdint.h>

// ESP specific
#include <esp_err.h>
#include <esp_wifi.h>

// ================================| Defines |================================
/**
 * @brief Default channel used for scanning
 */
#define WIFI_DEFAULT_CH (6)

/**
 * @brief Default scan type (active/passive)
 */
#define WIFI_DEFAULT_SCAN_TYPE (WIFI_SCAN_TYPE_PASSIVE)

/**
 * @brief Default scan time per channel
 */
#define WIFI_DEFAULT_SCAN_TIME_MS (240)

/**
 * @brief Size of scan list
 */
#define WIFI_SCAN_LIST_SIZE (CONFIG_ESP32_WIFI_STATIC_RX_BUFFER_NUM)

/**
 * @brief Practical minimum RSSI value which can occur
 *
 * This value can be used by higher layers in order to determine "minimum"
 * value
 */
#define WIFI_MIN_RSSI (-100)
/**
 * @brief Practical maximum RSSI value which can occur
 *
 * This value can be used by higher layers in order to determine "maximum"
 * value
 */
#define WIFI_MAX_RSSI (-20)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef struct {
  /**@brief Scan the specific channel */
  uint8_t u8channel;

  /**@brief Active or passive */
  wifi_scan_type_t eScan_type;

  /**@brief Maximum scan time in ms */
  uint32_t u32maxScanMs;
} tsWiFiScanCfg;
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Initialize WiFi module
 *
 * @return ESP_OK if no error
 */
esp_err_t wifiInit(void);

/**
 * @brief Perform WiFi scan
 * @param[out] pu16apCount Return number of found APs
 * @param[out] asApInfo Return list of found APs
 * @param[in] psWiFiConfig Optional configuration structure. If default is OK,
 *            then pointer can be null
 * @return
 */
esp_err_t wifiScan(uint16_t* pu16apCount,
                   wifi_ap_record_t asApInfo[static WIFI_SCAN_LIST_SIZE],
                   const tsWiFiScanCfg* psWiFiConfig);

/**
 * @brief Deinitialize WiFi module
 * @return ESP_OK if no error
 */
esp_err_t wifiDeInit(void);
// ========================| Middle level functions |=========================
/**
 * @brief Load WiFi configuration from NVS
 * @param[out] psWiFiCfg Pointer to configuration structure
 * @return ESP_OK if no error
 */
esp_err_t wifiGetScanCfgNvs(tsWiFiScanCfg* psWiFiCfg);

/**
 * @brief Save WiFi configuration to NVS
 * @param[in] psWiFiCfg Pointer to configuration structure
 * @return ESP_OK if no error
 */
esp_err_t wifiSetScanCfgNvs(tsWiFiScanCfg* psWiFiCfg);

/**
 * @brief Delete WiFi configuration stored in the NVS
 *
 * When called this function and then wifiGetScanCfgNvs(), hard-coded defaults
 * will be returned
 *
 * @return ESP_OK if no error
 */
esp_err_t wifiDelScanCfgNvs(void);
// ==========================| Low level functions |==========================
/**
 * @brief Tells if WiFi was initialized or not
 * @return True if WiFi was initialized, false otherwise
 */
bool wifiIsInitialized(void);

/**
 * @brief Tells if WiFi module is scanning now or not
 * @return True if scanning, false otherwise
 */
bool wifiIsScanning(void);

/**
 * @brief Transform signed RSSI value to 0~255 value suitable for processing
 *
 * RSSI value itself have quite limited range (WIFI_MIN_RSSI, WIFI_MAX_RSSI).
 * This range does not suits display, DAC and other digital things. This
 * function simply takes signed RSSi value and transform it to 0~255 range
 * where 0 is lowest signal value and 255 is maximum signal value.
 *
 * @param i8rssi Original RSSI from WiFi stack.
 * @return Transformed value for 0~255 range, where 0 is lowest RSSI and
 *         255 means highest value
 */
uint8_t wifiRssiToU8maxRange(int8_t i8rssi);
#endif  // __WIFI_H__
