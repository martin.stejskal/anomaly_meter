/**
 * @file
 * @author Martin Stejskal
 * @brief WiFi driver for ESP32
 */
// ===============================| Includes |================================
// Standard
#include <string.h>

// ESP specific
#include <esp_event.h>
#include <esp_log.h>
#include <esp_netif.h>
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/task.h>

// Project specific
#include "cfg.h"
#include "err_handlers.h"
#include "nvs_settings.h"
#include "wifi.h"
// ================================| Defines |================================
/**
 * @brief ID for NVS driver for WiFi configuration
 */
#define _NVS_WIFI_ID "WiFi"
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "WiFi";

static bool mbInitialized = false;
static bool mbScanningInProgress = false;

static uint32_t mu32numOfScans = 0;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
// =========================| High level functions |==========================
esp_err_t wifiInit(void) {
  static uint8_t u8wifiEventsSet = 0;
  esp_err_t eErrCode = ESP_FAIL;

  // If already initialized, no need to do again
  if (mbInitialized) {
    return ESP_OK;
  }
  mbInitialized = true;

  // This have to be done only once
  if (!u8wifiEventsSet) {
    // Initialize the underlying TCP/IP stack
    RET_WHEN_ERR(esp_netif_init());

    RET_WHEN_ERR(esp_event_loop_create_default());

    esp_netif_t *sta_netif = esp_netif_create_default_wifi_sta();
    assert(sta_netif);

    u8wifiEventsSet = +1;
  }

  wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
  RET_WHEN_ERR(esp_wifi_init(&cfg));

  // STA things
  RET_WHEN_ERR(esp_wifi_set_mode(WIFI_MODE_STA));
  RET_WHEN_ERR(esp_wifi_start());

  // Reset number of scans
  mu32numOfScans = 0;

  return eErrCode;
}

esp_err_t wifiScan(uint16_t *pu16apCount,
                   wifi_ap_record_t asApInfo[static WIFI_SCAN_LIST_SIZE],
                   const tsWiFiScanCfg *psWiFiConfig) {
  if (!mbInitialized) {
    return ESP_ERR_WIFI_NOT_INIT;
  }

  uint16_t u16numOfAps = WIFI_SCAN_LIST_SIZE;
  esp_err_t eErrCode = ESP_FAIL;
  wifi_scan_config_t sWiFiEspCfg = {0};

  memset(asApInfo, 0, sizeof(wifi_ap_record_t) * WIFI_SCAN_LIST_SIZE);

  // If configuration is not empty, setup structure accordingly
  if (psWiFiConfig) {
    sWiFiEspCfg.channel = psWiFiConfig->u8channel;
    sWiFiEspCfg.scan_type = psWiFiConfig->eScan_type;

    switch (sWiFiEspCfg.scan_type) {
      case WIFI_SCAN_TYPE_ACTIVE:
        sWiFiEspCfg.scan_time.active.max = psWiFiConfig->u32maxScanMs;
        break;
      case WIFI_SCAN_TYPE_PASSIVE:
        sWiFiEspCfg.scan_time.passive = psWiFiConfig->u32maxScanMs;
        sWiFiEspCfg.show_hidden = 1;
    }
  }

  // Make it task friendly
  while (mbScanningInProgress) {
    // Give 10 more RTOS cycles to wait
    vTaskDelay(10);
  }
  mbScanningInProgress = true;
  mu32numOfScans++;

  ESP_LOGV(TAG, "Scanning... (%u)", mu32numOfScans);

  if (psWiFiConfig) {
    // If configuration was not empty
    RET_WHEN_ERR(esp_wifi_scan_start(&sWiFiEspCfg, true));
  } else {
    // Configuration was empty - use default
    RET_WHEN_ERR(esp_wifi_scan_start(0x00, true));
  }
  ESP_LOGV(TAG, "Scan done");

  RET_WHEN_ERR(esp_wifi_scan_get_ap_records(&u16numOfAps, asApInfo));
  RET_WHEN_ERR(esp_wifi_scan_get_ap_num(pu16apCount));

  ESP_LOGV(TAG, "Total APs scanned = %u", *pu16apCount);
  for (int i = 0; (i < WIFI_SCAN_LIST_SIZE) && (i < *pu16apCount); i++) {
    ESP_LOGV(TAG, "SSID \t\t%s", asApInfo[i].ssid);
    ESP_LOGV(TAG, "RSSI \t\t%d", asApInfo[i].rssi);
    ESP_LOGV(TAG, "Channel \t\t%d\n", asApInfo[i].primary);
  }

  mbScanningInProgress = false;

  return eErrCode;
}

esp_err_t wifiDeInit(void) {
  // If no de-initialized, no need to deinitialize again
  if (!mbInitialized) {
    return ESP_OK;
  }
  mbInitialized = false;

  while (mbScanningInProgress) {
    // Wait for scan finish
    vTaskDelay(10);
  }

  esp_err_t eErrCode;
  RET_WHEN_ERR(esp_wifi_stop());
  RET_WHEN_ERR(esp_wifi_deinit());

  return eErrCode;
}
// ========================| Middle level functions |=========================
esp_err_t wifiGetScanCfgNvs(tsWiFiScanCfg *psWiFiCfg) {
  // Pre-load default
  tsWiFiScanCfg sDefaultWiFiCfg = {.u8channel = WIFI_DEFAULT_CH,
                                   .eScan_type = WIFI_DEFAULT_SCAN_TYPE,
                                   .u32maxScanMs = WIFI_DEFAULT_SCAN_TIME_MS};
  return nvsSettingsGet(_NVS_WIFI_ID, psWiFiCfg, &sDefaultWiFiCfg,
                        sizeof(tsWiFiScanCfg));
}

esp_err_t wifiSetScanCfgNvs(tsWiFiScanCfg *psWiFiCfg) {
  return nvsSettingsSet(_NVS_WIFI_ID, psWiFiCfg, sizeof(tsWiFiScanCfg));
}

esp_err_t wifiDelScanCfgNvs(void) { return nvsSettingsDel(_NVS_WIFI_ID); }
// ==========================| Low level functions |==========================
inline bool wifiIsInitialized(void) { return mbInitialized; }

inline bool wifiIsScanning(void) { return mbScanningInProgress; }

uint8_t wifiRssiToU8maxRange(int8_t i8rssi) {
  // Adjust value. In practice we can expect RSSI from -20 to -100.
  // RSSI can be zero, if nothing was found
  assert(i8rssi <= 0);

  // Check boundaries
  if (i8rssi > WIFI_MAX_RSSI) {
    // Maximum signal reached
    return 0xFF;

  } else if (i8rssi < WIFI_MIN_RSSI) {
    // Can not go lower actually
    return 0;
  }

  // From -20 ~ -100 range we need to get 255 ~ 0 range.
  // Based on that, We can write down 2 equations:
  //  255 = RSSImax * x + y
  //    0 = RSSImin * x + y
  //
  //  -----------------------
  //  y = - RSSImin * x // From 2nd equation
  //
  //  255 = RSSImax * x - RSSImin * x
  //
  //  ------------------------------
  //  x = (255)/(RSSImax - RSSImin)
  //  y = (- RSSImin * 255)/(RSSImax - RSSImin)
  //
  //  Once you have the x and y constants, following equation can be used
  //  out = RSSI * x + y
  //
  // The +0.5 is for rounding purposes
  int iX = (255000) / (WIFI_MAX_RSSI - WIFI_MIN_RSSI);
  int iY = (-1 * WIFI_MIN_RSSI * 255000) / (WIFI_MAX_RSSI - WIFI_MIN_RSSI);

  int iRes = i8rssi * iX + iY;

  // Rounding (0.5 * 1000)
  iRes += 500;

  // Everything was multiplied by 1000 -> divide it
  iRes /= 1000;

  assert(iRes >= 0);
  assert(iRes < 256);

  return (uint8_t)(iRes);
}
// ==========================| Internal functions |===========================
