/**
 * @file
 * @author Martin Stejskal
 * @brief Service sensors
 */
// ===============================| Includes |================================
// Standard ESP32 libraries
#include "sensors.h"

#include <driver/i2c.h>
#include <esp_log.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include "mpu9250.h"
#include "sensors_common.h"
// ================================| Defines |================================
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "Sensors";
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
esp_err_t sensorsInit(void) {
  ESP_LOGD(TAG, "Initializing sensors...");
  i2c_config_t conf = {.mode = I2C_MODE_MASTER,
                       .sda_io_num = IO_I2C_SDA,
                       .scl_io_num = IO_I2C_SCL,
                       .sda_pullup_en = GPIO_PULLUP_ENABLE,
                       .scl_pullup_en = GPIO_PULLUP_ENABLE,
                       .master.clk_speed = I2C_SCL_SPEED,
                       .clk_flags = 0};

  // Following 2 should not fail. If so, it is quite serious
  ESP_ERROR_CHECK(i2c_param_config(I2C_NUM_0, &conf));
  ESP_ERROR_CHECK(i2c_driver_install(I2C_NUM_0, I2C_MODE_MASTER, 0, 0, 0));

  return (mpu9250setup());
}
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
// ==========================| Internal functions |===========================
