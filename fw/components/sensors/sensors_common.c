/**
 * @file
 * @author Martin Stejskal
 * @brief Common routines & macros for sensor logic
 */
// ===============================| Includes |================================
#include "sensors_common.h"

#include <assert.h>
#include <string.h>

#include "math_fast.h"
// ================================| Defines |================================
/**
 * @brief Maximum value which can be used when calculating vector length
 *
 * Vector have 3 compounds: x,y, z. And all need to fit into 64 bit buffer
 * -> 2^31 / 3 (calculating vector length: sqrt(x*x + y*y + z*z))
 */
#define VECT_LEN_INT32_MAX_VALUE (715827881)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static uint16_t _reverseBitOrder16(uint16_t u16src);
// =========================| High level functions |==========================
esp_err_t vect32scaleSymetric(tsVector32 *psRes, tsVector32 sVectSrc,
                              const int32_t i32sourceMax,
                              const int32_t i32targetMax) {
  if (sVectSrc.i32x > i32sourceMax) {
    sVectSrc.i32x = i32sourceMax;
  }
  if (sVectSrc.i32x < (-1 * i32sourceMax)) {
    sVectSrc.i32x = -1 * i32sourceMax;
  }

  if (sVectSrc.i32y > i32sourceMax) {
    sVectSrc.i32y = i32sourceMax;
  }
  if (sVectSrc.i32y < (-1 * i32sourceMax)) {
    sVectSrc.i32y = -1 * i32sourceMax;
  }

  if (sVectSrc.i32z > i32sourceMax) {
    sVectSrc.i32z = i32sourceMax;
  }
  if (sVectSrc.i32z < (-1 * i32sourceMax)) {
    sVectSrc.i32z = -1 * i32sourceMax;
  }

  // Clean result structure
  memset(psRes, 0, sizeof(tsVector32));

  // It is required some margin to avoid overflow
  if ((i32sourceMax > (INT32_MAX / 1000)) ||
      (sVectSrc.i32x > (INT32_MAX / 1000)) ||
      (sVectSrc.i32x < (INT32_MIN / 1000)) ||

      (sVectSrc.i32y > (INT32_MAX / 1000)) ||
      (sVectSrc.i32y < (INT32_MIN / 1000)) ||

      (sVectSrc.i32z > (INT32_MAX / 1000)) ||
      (sVectSrc.i32z < (INT32_MIN / 1000))) {
    return ESP_ERR_INVALID_ARG;
  }

  // Division ratio is: source/target. To get more precise results multiply
  // it by 1000
  int32_t i32divRatio = (i32sourceMax * 1000) / i32targetMax;

  psRes->i32x = (sVectSrc.i32x * 1000) / i32divRatio;
  psRes->i32y = (sVectSrc.i32y * 1000) / i32divRatio;
  psRes->i32z = (sVectSrc.i32z * 1000) / i32divRatio;

  return ESP_OK;
}

void vect16cutOff(tsVector16 *psVect, uint16_t u16maxValue) {
  // Check pointers
  assert(psVect);

  if (psVect->i16x > u16maxValue) {
    psVect->i16x = u16maxValue;
  }

  if (psVect->i16y > u16maxValue) {
    psVect->i16y = u16maxValue;
  }

  if (psVect->i16z > u16maxValue) {
    psVect->i16z = u16maxValue;
  }
}

void vect16swapBits(tsVector16 *psVect) {
  psVect->i16x = _reverseBitOrder16(psVect->i16x);
  psVect->i16y = _reverseBitOrder16(psVect->i16y);
  psVect->i16z = _reverseBitOrder16(psVect->i16z);
}
// ========================| Middle level functions |=========================
esp_err_t vect32length(tsVector32 *psVect, int32_t *pi32length) {
  if ((psVect->i32x > VECT_LEN_INT32_MAX_VALUE) ||
      (psVect->i32x < (-1 * VECT_LEN_INT32_MAX_VALUE)) ||
      (psVect->i32y > VECT_LEN_INT32_MAX_VALUE) ||
      (psVect->i32y < (-1 * VECT_LEN_INT32_MAX_VALUE)) ||
      (psVect->i32z > VECT_LEN_INT32_MAX_VALUE) ||
      (psVect->i32z < (-1 * VECT_LEN_INT32_MAX_VALUE))) {
    return ESP_ERR_INVALID_ARG;
  }

  // Calculating length: sqrt(x*x + y*y + z*z)
  // Do sum first
  int64_t i64sum = (int64_t)psVect->i32x * (int64_t)psVect->i32x +
                   (int64_t)psVect->i32y * (int64_t)psVect->i32y +
                   (int64_t)psVect->i32z * (int64_t)psVect->i32z;

  *pi32length = (int32_t)mf_sqrt_64((uint64_t)i64sum);
  return ESP_OK;
}

void vect16abs(tsVector16 *psVect) {
  assert(psVect);

  if (psVect->i16x < 0) {
    psVect->i16x = -1 * psVect->i16x;
  }
  if (psVect->i16y < 0) {
    psVect->i16y = -1 * psVect->i16y;
  }
  if (psVect->i16z < 0) {
    psVect->i16z = -1 * psVect->i16z;
  }
}

void vect32abs(tsVector32 *psVect) {
  assert(psVect);

  if (psVect->i32x < 0) {
    psVect->i32x = -1 * psVect->i32x;
  }
  if (psVect->i32y < 0) {
    psVect->i32y = -1 * psVect->i32y;
  }
  if (psVect->i32z < 0) {
    psVect->i32z = -1 * psVect->i32z;
  }
}

void vect16getMinimum(tsVector16 *psVectResult, tsVector16 *psVectA,
                      tsVector16 *psVectB) {
  assert(psVectResult && psVectA && psVectB);

  if (psVectA->i16x < psVectB->i16x) {
    psVectResult->i16x = psVectA->i16x;
  } else {
    psVectResult->i16x = psVectB->i16x;
  }

  if (psVectA->i16y < psVectB->i16y) {
    psVectResult->i16y = psVectA->i16y;
  } else {
    psVectResult->i16y = psVectB->i16y;
  }

  if (psVectA->i16z < psVectB->i16z) {
    psVectResult->i16z = psVectA->i16z;
  } else {
    psVectResult->i16z = psVectB->i16z;
  }
}

void vect16getMaximum(tsVector16 *psVectResult, tsVector16 *psVectA,
                      tsVector16 *psVectB) {
  assert(psVectResult && psVectA && psVectB);

  if (psVectA->i16x > psVectB->i16x) {
    psVectResult->i16x = psVectA->i16x;
  } else {
    psVectResult->i16x = psVectB->i16x;
  }

  if (psVectA->i16y > psVectB->i16y) {
    psVectResult->i16y = psVectA->i16y;
  } else {
    psVectResult->i16y = psVectB->i16y;
  }

  if (psVectA->i16z > psVectB->i16z) {
    psVectResult->i16z = psVectA->i16z;
  } else {
    psVectResult->i16z = psVectB->i16z;
  }
}
// ==========================| Low level functions |==========================
void vect16add(tsVector16 *psVectResult, tsVector16 *psVectA,
               tsVector16 *psVectB) {
  assert(psVectResult && psVectA && psVectB);

  psVectResult->i16x = psVectA->i16x + psVectB->i16x;
  psVectResult->i16y = psVectA->i16y + psVectB->i16y;
  psVectResult->i16z = psVectA->i16z + psVectB->i16z;
}

void vect16sub(tsVector16 *psVectResult, tsVector16 *psVectA,
               tsVector16 *psVectB) {
  assert(psVectResult && psVectA && psVectB);

  psVectResult->i16x = psVectA->i16x - psVectB->i16x;
  psVectResult->i16y = psVectA->i16y - psVectB->i16y;
  psVectResult->i16z = psVectA->i16z - psVectB->i16z;
}

void vect16mul(tsVector16 *psVectResult, tsVector16 *psVectA,
               tsVector16 *psVectB) {
  assert(psVectResult && psVectA && psVectB);

  psVectResult->i16x = psVectA->i16x * psVectB->i16x;
  psVectResult->i16y = psVectA->i16y * psVectB->i16y;
  psVectResult->i16z = psVectA->i16z * psVectB->i16z;
}

void vect16div(tsVector16 *psVectResult, tsVector16 *psVectNumerator,
               tsVector16 *psVectDenominator) {
  // Check pointers
  assert(psVectNumerator);
  assert(psVectDenominator);
  assert(psVectResult);

  // We should not divide by zero. If so, raise error
  assert(psVectDenominator->i16x);
  assert(psVectDenominator->i16y);
  assert(psVectDenominator->i16z);

  psVectResult->i16x = psVectNumerator->i16x / psVectDenominator->i16x;
  psVectResult->i16y = psVectNumerator->i16y / psVectDenominator->i16y;
  psVectResult->i16z = psVectNumerator->i16z / psVectDenominator->i16z;
}

// ==========================| Internal functions |===========================
static uint16_t _reverseBitOrder16(uint16_t u16src) {
  u16src = (u16src & 0xFF00) >> 8 | (u16src & 0x00FF) << 8;
  u16src = (u16src & 0xF0F0) >> 4 | (u16src & 0x0F0F) << 4;
  u16src = (u16src & 0xCCCC) >> 2 | (u16src & 0x3333) << 2;
  u16src = (u16src & 0xAAAA) >> 1 | (u16src & 0x5555) << 1;

  return u16src;
}
