/**
 * @file
 * @author Martin Stejskal
 * @brief Common routines & macros for sensor logic
 */
#ifndef __SENSORS_COMMON_H__
#define __SENSORS_COMMON_H__
// ===============================| Includes |================================
#include <esp_err.h>
#include <stdint.h>

#include "cfg.h"
// ================================| Defines |================================
// ============================| Default values |=============================
#ifndef GET_CLK_MS_32BIT
#define GET_CLK_MS_32BIT() esp_log_timestamp()
#endif  // GET_CLK_MS_32BIT
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef union {
  struct {
    // Little endian is expected
    uint8_t u8low;
    uint8_t u8high;
  } s;
  uint16_t u16;
} tu16bit;

typedef struct {
  int16_t i16x;
  int16_t i16y;
  int16_t i16z;
} tsVector16;

typedef struct {
  int32_t i32x;
  int32_t i32y;
  int32_t i32z;
} tsVector32;

typedef struct {
  int64_t i64x;
  int64_t i64y;
  int64_t i64z;
} tsVector64;

typedef struct {
  float fx;
  float fy;
  float fz;
} tsVectorFloat;

// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Scale symmetrically given vector
 *
 * Ratio of i32sourceMax and i32targetMax tells how vector should be scaled.
 * Note that full 32 bit range can not be used, to avoid overflow during
 * intermediate calculations. But most of data from sensors should fit into
 * range without problems.
 *
 * @param psRes Pointer to result vector structure
 * @param sVectSrc Source vector
 * @param i32sourceMax Maximum value in source vector
 * @param i32targetMax Maximum value in result vector
 * @return ESP_OK if no error. ESP_ERR_INVALID_ARG if value is out of range
 */
esp_err_t vect32scaleSymetric(tsVector32 *psRes, tsVector32 sVectSrc,
                              const int32_t i32sourceMax,
                              const int32_t i32targetMax);

/**
 * @brief Cut off values in vector
 * @param psVect Pointer to vector data
 * @param u16maxValue Maximum value. When any part of vector exceed this value,
 *        value will be changed to this maximum
 */
void vect16cutOff(tsVector16 *psVect, uint16_t u16maxValue);

/**
 * @brief Swap bits in every vector item
 * @param psVect Pointer to vector structure
 */
void vect16swapBits(tsVector16 *psVect);
// ========================| Middle level functions |=========================
/**
 * @brief Calculate vector length
 * @param psVect Pointer to source vector
 * @param pi32length Result will be written here.
 * @return ESP_OK if no error. ESP_ERR_INVALID_ARG if value is out of range
 *
 * @note Length can not be negative. Signed integer is used for easier
 *       operations later on
 */
esp_err_t vect32length(tsVector32 *psVect, int32_t *pi32length);

/**
 * @brief Get absolute value for every component in vector
 * @param psVect Pointer to vector structure
 */
void vect16abs(tsVector16 *psVect);

/**
 * @brief Get absolute value for every component in vector
 * @param psVect Pointer to vector structure
 */
void vect32abs(tsVector32 *psVect);

/**
 * @brief Get minimum value from both vectors
 * @param psVectResult Pointer to result structure
 * @param psVectA Source vector A
 * @param psVectB Source vector B
 */
void vect16getMinimum(tsVector16 *psVectResult, tsVector16 *psVectA,
                      tsVector16 *psVectB);

/**
 * @brief Get maximum value from both vectors
 * @param psVectResult Pointer to result structure
 * @param psVectA Source vector A
 * @param psVectB Source vector B
 */
void vect16getMaximum(tsVector16 *psVectResult, tsVector16 *psVectA,
                      tsVector16 *psVectB);
// ==========================| Low level functions |==========================
/**
 * @brief Sum vector A and B (A+B)
 * @param psVectResult Pointer to result structure
 * @param psVectA Source vector A
 * @param psVectB Source vector B
 */
void vect16add(tsVector16 *psVectResult, tsVector16 *psVectA,
               tsVector16 *psVectB);
/**
 * @brief Subtract vector B from vector A (A-B)
 * @param psVectResult Pointer to result structure
 * @param psVectA Source vector A
 * @param psVectB Source vector B
 */
void vect16sub(tsVector16 *psVectResult, tsVector16 *psVectA,
               tsVector16 *psVectB);

/**
 * @brief Multiply vector A by vector B (A*B)
 * @param psVectResult Pointer to result structure
 * @param psVectA Source vector A
 * @param psVectB Source vector B
 */
void vect16mul(tsVector16 *psVectResult, tsVector16 *psVectA,
               tsVector16 *psVectB);

/**
 * @brief Divide two 16 bit long vectors
 *
 * @param psVectResult Pointer to result structure
 * @param psVectNumerator Pointer to numerator vector ("top" part)
 * @param psVectDenominator Pointer to denominator vector ("bottom" part)
 *
 * @note When one of value of denominator is zero, assert will be raised.
 */
void vect16div(tsVector16 *psVectResult, tsVector16 *psVectNumerator,
               tsVector16 *psVectDenominator);

#endif  // __SENSORS_COMMON_H__
