/**
 * @file
 * @author Martin Stejskal
 * @brief Service for sensors
 */
#ifndef __SENSORS_H__
#define __SENSORS_H__
// ===============================| Includes |================================
#include "mpu9250.h"
// ================================| Defines |================================
#define ACC_SENS_1G_VALUE MPU9250_ACC_RES

#define MAG_SENS_MIN MPU9250_MAG_14_BIT_MIN
#define MAG_SENS_MAX MPU9250_MAG_14_BIT_MAX
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Initialize sensors
 * @return ESP_OK if no problem detected
 */
esp_err_t sensorsInit(void);

/**
 * @brief Map acceleration sensor functions
 *
 * @{
 */
#define sensAccGetInt(psVector16) mpu9250getAccInt(psVector16)
/**
 * @}
 */

/**
 * @brief Map temperature sensor functions
 *
 * @{
 */
#define sensTempGetInt(pi8temp) mpu9250getTempInt(pi8temp)
/**
 * @}
 */

/**
 * @brief Map magnetometer sensor functions
 *
 * @{
 */
#define sensMagGetInt(psVector16) mpu9250getMagInt(psVector16)

#define sensMagCalibrate(u16numOfIterations) \
  mpu9250magCalibrate(u16numOfIterations)

#define sensMagClearCalibration() mpu9250magDelCalibration()
/**
 * @}
 */
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
#endif  // __SENSORS_H__
