/**
 * @file
 * @author Martin Stejskal
 * @brief Low level driver for MPU6050
 */
// ===============================| Includes |================================
#include <assert.h>
#include <stdint.h>
#include <string.h>

// Basically most sensors are I2C based
#include <driver/i2c.h>
// For button events mainly
#include <driver/gpio.h>
#include <esp_log.h>

#include "mpu9250.h"
#include "sensors_common.h"
// Calibration values
#include "nvs_settings.h"
// ================================| Defines |================================
/**
 * @brief Check if error code is 0, and if not, return error code
 * @return Non-zero error code. Otherwise pass
 */
#define MPU9250_RET_WHEN_ERR(errCodeFromFunction) \
  do {                                            \
    esp_err_t __err_rc = (errCodeFromFunction);   \
    if (__err_rc != 0) {                          \
      return (__err_rc);                          \
    }                                             \
  } while (0)

/**
 * @brief Minimal register address
 */
#define MPU9250_REG_MIN (13)
#define MPU9250_MAG_REG_MIN (0)

/**
 * @
 * @return Maximal register address
 */
#define MPU9250_REG_MAX (117)
#define MPU9250_MAG_REG_MAX (0x13)

/**
 * @brief Pseudo functions simplify writing in code
 *
 * @{
 */
#define ACC_GYRO_READ_REG(u8regAddr, pau8buffer, u8numOfBytes) \
  _read_reg(MPU9250_I2C_ADDR, u8regAddr, pau8buffer, u8numOfBytes)

#define ACC_GYRO_WRITE_REG(u8regAddr, pau8buffer) \
  _write_reg(MPU9250_I2C_ADDR, u8regAddr, pau8buffer)

#define MAG_READ_REG(u8regAddr, pau8buffer, u8numOfBytes) \
  _read_reg(MPU9250_I2C_MAGN_ADDR, u8regAddr, pau8buffer, u8numOfBytes)

#define MAG_WRITE_REG(u8regAddr, pau8buffer) \
  _write_reg(MPU9250_I2C_MAGN_ADDR, u8regAddr, pau8buffer)
/**
 * @}
 */

/**
 * @brief The ID for NVS calibration data
 */
#define _MAG_CALIB_ID "MagCal"
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef struct {
  ///@brief Correction data for Gyroscope
  tsVector16 sGyroCorrection;

  ///@brief Calibration data from magnetometer IC (provided by manufacturer)
  tsVector16 sMagFactoryCalib;

  ///@brief Calibration data obtained manually
  ///
  /// These data are obtained when calibration function for magnetometer
  /// is called. Then user have to move device around all axes multiple
  /// times to get calibration data. This can not be done by manufacturer
  /// since during soldering MEMS characteristic are changed
  tsMagRuntimeCalibration sMagRuntimeCalib;
} tsRuntime;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *MPU_TAG = "MPU6050";

/**
 * @brief Runtime data under one hood
 */
static tsRuntime msRuntime;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static esp_err_t _getCalibrationData(
    tsMagRuntimeCalibration *psMagRuntimeCalib);

static esp_err_t _accGyroCheckWhoAmI(void);

static esp_err_t _accGyroReadXYZ(const uint8_t u8regAddr, tsVector16 *psVector);
static esp_err_t _magReadXYZ(tsVector16 *psVector,
                             const bool bWithFactoryCalibration,
                             const bool bWithRuntimeCalibration);

static esp_err_t _read_reg(const uint8_t u8devAddr, const uint8_t u8regAddr,
                           uint8_t *pau8buffer, const uint8_t u8numOfBytes);
static esp_err_t _write_reg(const uint8_t u8devAddr, const uint8_t u8regAddr,
                            const uint8_t u8value);

static void _swapBytes(tu16bit *puVariable);
static uint8_t _isNot32bitLong(int64_t i64input);
// =========================| High level functions |==========================
esp_err_t mpu9250setup(void) {
  // ================| Confirm chip - read "who am I" register |============
  // If this fails, exit now
  MPU9250_RET_WHEN_ERR(_accGyroCheckWhoAmI());

  // =============================| Reset device |==========================
  tuPowerMgmnt_107 uPwrMgmnt = {
      .s.DEVICE_RESET = 1,
  };

  ESP_LOGD(MPU_TAG, "Reset IC... (%x)", uPwrMgmnt.u8raw);
  ESP_ERROR_CHECK(
      ACC_GYRO_WRITE_REG(MPU9250_REG_POWER_MANAMEMENT_1, uPwrMgmnt.u8raw));
  vTaskDelay(200 / portTICK_PERIOD_MS);

  // ====================| Wait for reset - check if zero |=================
  // While this flag is "1", device is resetting

  while (uPwrMgmnt.s.DEVICE_RESET) {
    // Load register value
    ESP_ERROR_CHECK(ACC_GYRO_READ_REG(MPU9250_REG_POWER_MANAMEMENT_1,
                                      &uPwrMgmnt.u8raw,
                                      sizeof(uPwrMgmnt.u8raw)));
    ESP_LOGD(MPU_TAG, "Read reset flag (%d)", sizeof(uPwrMgmnt.u8raw));
  }
  // ===========================| Remove sleep flag |=======================
  uPwrMgmnt.s.SLEEP = 0;
  ESP_LOGD(MPU_TAG, "Removing sleep flag");
  ESP_ERROR_CHECK(
      ACC_GYRO_WRITE_REG(MPU9250_REG_POWER_MANAMEMENT_1, uPwrMgmnt.u8raw));

  // ========================| Dummy read sensors data |====================
  // Since after reset all registers are set to 0, including sensor
  // registers, one dummy read is necessary to let internal logic overwrite
  // these values
  ESP_LOGD(MPU_TAG, "First reading from sensors");
  tsVector16 sAccVector;
  int8_t i8temperature = 0;

  MPU9250_RET_WHEN_ERR(mpu9250getAccInt(&sAccVector));

  // Wait for first valid reading from sensor. If register value is 0,
  // sensor is not ready and it returns MPU6050_INVALID_TEMP value
  do {
    MPU9250_RET_WHEN_ERR(mpu9250getTempInt(&i8temperature));
  } while (i8temperature == MPU9250_INVALID_TEMP);

  // ==========================| Calibrate gyroscope |======================
  // Gyroscope is tricky - need some time after reset. According to register
  // reference (pg 41), there should be 100 ms delay after power on for SPI,
  // but it seems this 100 ms should be there even in case of I2C
  // vTaskDelay(100 / portTICK_PERIOD_MS);
  // mpu6050gyroCalibrate(GYRO_CALIBRATION_ITERATIONS);

  // =====================| Setup route for magnetometer |==================

  tuIntPinBypassEnCfg_55 uIntPinBypassCfg = {
      .s.BYPASS_EN = 1,
  };
  tuMagControl1_0x0A uMagControl = {.s.MODE = R_0x0A_MODE_FUSE_ROM_ACCESS};

  MPU9250_RET_WHEN_ERR(ACC_GYRO_WRITE_REG(MPU9250_REG_INT_PIN_BYPASS_EN_CFG,
                                          uIntPinBypassCfg.u8raw));

  // ================| Load calibration data for magnetometer |=============
  // Switch to "ROM access" in order to load calibration data
  MPU9250_RET_WHEN_ERR(MAG_WRITE_REG(MPU9250_MAG_REG_CNTL1, uMagControl.u8raw));
  // There are 3 coefficients (axe X, Y, Z)
  uint8_t au8coefRaw[3];
  MPU9250_RET_WHEN_ERR(
      MAG_READ_REG(MPU9250_MAG_REG_ASAX, au8coefRaw, sizeof(au8coefRaw)));

  // Calculate coefficients. Note that output is fractional value ->
  // multiplied by 1000 so fractional information is kept although integer
  // is still used
  float fTmp = ((((float)au8coefRaw[0] - 128) * (0.5) / 128) + 1) * 1000;
  msRuntime.sMagFactoryCalib.i16x = (int16_t)fTmp;

  fTmp = ((((float)au8coefRaw[1] - 128) * (0.5) / 128) + 1) * 1000;
  msRuntime.sMagFactoryCalib.i16y = (int16_t)fTmp;

  fTmp = ((((float)au8coefRaw[1] - 128) * (0.5) / 128) + 1) * 1000;
  msRuntime.sMagFactoryCalib.i16z = (int16_t)fTmp;

  // Load runtime calibration data
  MPU9250_RET_WHEN_ERR(_getCalibrationData(&msRuntime.sMagRuntimeCalib));

  // Switch mode to "single measurement"
  uMagControl.s.MODE = R_0x0A_MODE_SINGLE_MEASUREMENT;
  MPU9250_RET_WHEN_ERR(MAG_WRITE_REG(MPU9250_MAG_REG_CNTL1, uMagControl.u8raw));

  return ESP_OK;
}

esp_err_t mpu9250getAccInt(tsVector16 *psVector) {
  MPU9250_RET_WHEN_ERR(_accGyroReadXYZ(MPU9250_REG_ACCEL_XOUT_H, psVector));

  ESP_LOGV(MPU_TAG, "AccInt: X: %d, Y: %d, Z: %d", psVector->i16x,
           psVector->i16y, psVector->i16z);

  return ESP_OK;
}

esp_err_t mpu9250getAccFloat(tsVectorFloat *psVector) {
  tsVector16 sVector;

  MPU9250_RET_WHEN_ERR(_accGyroReadXYZ(MPU9250_REG_ACCEL_XOUT_H, &sVector));

  psVector->fx = (float)sVector.i16x / MPU9250_ACC_RES;
  psVector->fy = (float)sVector.i16y / MPU9250_ACC_RES;
  psVector->fz = (float)sVector.i16z / MPU9250_ACC_RES;
  ESP_LOGV(MPU_TAG, "AccFloat: X: %f (%d), Y: %f (%d), Z: %f (%d)",
           psVector->fx, sVector.i16x, psVector->fy, sVector.i16y, psVector->fz,
           sVector.i16z);

  return ESP_OK;
}

esp_err_t mpu9250getTempInt(int8_t *pi8temp) {
  // Value is 16 bit long. When loading Bytes, MSB is loaded first -> need
  // to swap these bytes later on
  tu16bit uTempRaw;

  ESP_ERROR_CHECK(ACC_GYRO_READ_REG(MPU9250_REG_TEMP_OUT_H,
                                    (uint8_t *)&uTempRaw, sizeof(uTempRaw)));
  _swapBytes(&uTempRaw);

  // Need to recalculate raw value to "real integer"
  if (uTempRaw.u16 == 0) {
    // Not ready yet
    *pi8temp = MPU9250_INVALID_TEMP;
  } else {
    // ready, provide data
    *pi8temp = (((int16_t)uTempRaw.u16) / 340) + 36.53;
  }

  ESP_LOGV(MPU_TAG, "TempInt: %d (raw: %d)", *pi8temp, uTempRaw.u16);

  return ESP_OK;
}

esp_err_t mpu9250getTempFloat(float *pfTemp) {
  // Value is 16 bit long. When loading Bytes, MSB is loaded first -> need
  // to swap these bytes later on
  tu16bit uTempRaw;

  ESP_ERROR_CHECK(ACC_GYRO_READ_REG(MPU9250_REG_TEMP_OUT_H,
                                    (uint8_t *)&uTempRaw, sizeof(uTempRaw)));
  _swapBytes(&uTempRaw);

  // Need to recalculate raw value to "real integer"
  if (uTempRaw.u16 == 0) {
    // Not ready yet
    *pfTemp = MPU9250_INVALID_TEMP;
  } else {
    *pfTemp = (((float)uTempRaw.u16) / 340) + 36.53;
  }

  ESP_LOGV(MPU_TAG, "TempFloat: %f (raw: %d)", *pfTemp, uTempRaw.u16);

  return ESP_OK;
}

esp_err_t mpu9250getGyroDegInt(tsVector32 *psVectorDeg) {
  static uint32_t u32timePrevMs;
  uint32_t u32timeActualMs = GET_CLK_MS_32BIT();
  uint32_t u32timeDiffMs = u32timeActualMs - u32timePrevMs;

  // Temporary vector for calculations (do not overwrite original directly)
  tsVector16 sVect16tmp;
  tsVector64 sVect64tmp;

  // If this is first sample (previous time is 0) -> return zeros
  if (u32timePrevMs == 0) {
    // Reset vector
    memset(psVectorDeg, 0, sizeof(tsVector32));
    u32timePrevMs = u32timeActualMs;
    return ESP_OK;
  } else {
    // Do not forget about updating time
    u32timePrevMs = u32timeActualMs;
  }

  // Load value with correction
  MPU9250_RET_WHEN_ERR(mpu9250getGyroDegPerSecInt(&sVect16tmp));

  // In order to be precise, we can not "cut" milliseconds and count with
  // only second resolution. Temporary need to work with 64 bit long
  // integers. Since we need to get back values referred to second, division
  // 1000 is there
  sVect64tmp.i64x = ((int64_t)u32timeDiffMs * (int64_t)sVect16tmp.i16x) / 1000;
  sVect64tmp.i64y = ((int64_t)u32timeDiffMs * (int64_t)sVect16tmp.i16y) / 1000;
  sVect64tmp.i64z = ((int64_t)u32timeDiffMs * (int64_t)sVect16tmp.i16z) / 1000;

  // Should fir to 32 bit long register
  assert(sVect64tmp.i64x <= 0xFFFFFFFF);
  assert(sVect64tmp.i64y <= 0xFFFFFFFF);
  assert(sVect64tmp.i64z <= 0xFFFFFFFF);

  // Add to original value. If that would overflow 32 bits, return error
  sVect64tmp.i64x += (int64_t)psVectorDeg->i32x;
  sVect64tmp.i64y += (int64_t)psVectorDeg->i32y;
  sVect64tmp.i64z += (int64_t)psVectorDeg->i32z;

  if (_isNot32bitLong(sVect64tmp.i64x) || _isNot32bitLong(sVect64tmp.i64y) ||
      _isNot32bitLong(sVect64tmp.i64z)) {
    return ESP_ERR_INVALID_ARG;
  } else {
    // Nothing
  }

  // Write it back
  psVectorDeg->i32x = (int32_t)sVect64tmp.i64x;
  psVectorDeg->i32y = (int32_t)sVect64tmp.i64y;
  psVectorDeg->i32z = (int32_t)sVect64tmp.i64z;

  ESP_LOGV(MPU_TAG, "Gyro Cur: %d %d %d", psVectorDeg->i32x, psVectorDeg->i32y,
           psVectorDeg->i32z);

  return ESP_OK;
}

esp_err_t mpu9250getMagInt(tsVector16 *psVector) {
  assert(psVector);

  // Load data with calibration (factory + runtime)
  MPU9250_RET_WHEN_ERR(_magReadXYZ(psVector, true, true));

  ESP_LOGV(MPU_TAG, "MagInt: X: %d, Y: %d, Z: %d", psVector->i16x,
           psVector->i16y, psVector->i16z);

  return ESP_OK;
}
// ========================| Middle level functions |=========================
esp_err_t mpu9250getGyroDegPerSecInt(tsVector16 *psVectorDegPerSec) {
  MPU9250_RET_WHEN_ERR(mpu9250getGyroDegPerSecRawInt(psVectorDegPerSec));

  // Add offset
  psVectorDegPerSec->i16x += msRuntime.sGyroCorrection.i16x;
  psVectorDegPerSec->i16y += msRuntime.sGyroCorrection.i16y;
  psVectorDegPerSec->i16z += msRuntime.sGyroCorrection.i16z;

  return ESP_OK;
}

esp_err_t mpu9250getGyroDegPerSecFloat(tsVectorFloat *psVectorDegPerSec) {
  tsVector16 sVector16;

  // Get integer with correction
  MPU9250_RET_WHEN_ERR(mpu9250getGyroDegPerSecInt(&sVector16));

  psVectorDegPerSec->fx = (float)sVector16.i16x / MPU9250_GYRO_RES;
  psVectorDegPerSec->fy = (float)sVector16.i16y / MPU9250_GYRO_RES;
  psVectorDegPerSec->fz = (float)sVector16.i16z / MPU9250_GYRO_RES;
  ESP_LOGV(MPU_TAG, "GyroFloat: X: %f (%d), Y: %f (%d), Z: %f (%d)",
           psVectorDegPerSec->fx, sVector16.i16x, psVectorDegPerSec->fy,
           sVector16.i16y, psVectorDegPerSec->fz, sVector16.i16z);

  return ESP_OK;
}
// ==========================| Low level functions |==========================
esp_err_t mpu9250gyroCalibrate(const uint16_t u16numOfIterations) {
  ESP_LOGI(MPU_TAG, "Calibrating gyroscope....");
  // Number of iterations should not be zero
  assert(u16numOfIterations);

  // Reset current calibration data
  memset(&msRuntime.sGyroCorrection, 0, sizeof(msRuntime.sGyroCorrection));

  // Need to collect data in 32-bit long value due to add operation to
  // avoid overflow
  tsVector32 sCalib32 = {
      .i32x = 0,
      .i32y = 0,
      .i32z = 0,
  };

  // Store raw data from gyroscope here
  tsVector16 sGyroRaw;

  // For showing progress in %. First print at 10% of completed task
  uint8_t u8percVal = 10;

  // To avoid calculating threshold all the time (dividing), calculate it
  // in advance
  uint16_t u16percShowThreshold = u16numOfIterations / 10;

  for (uint16_t u16cnt = 0; u16cnt < u16numOfIterations; u16cnt++) {
    MPU9250_RET_WHEN_ERR(mpu9250getGyroDegPerSecRawInt(&sGyroRaw));

    sCalib32.i32x += sGyroRaw.i16x;
    sCalib32.i32y += sGyroRaw.i16y;
    sCalib32.i32z += sGyroRaw.i16z;

    if (u16cnt >= u16percShowThreshold) {
      ESP_LOGI(MPU_TAG, "%d %%", u8percVal);

      // Set new threshold - at next 10 %
      u16percShowThreshold = u16percShowThreshold + (u16numOfIterations / 10);
      // Update percentage value
      u8percVal += 10;
    }
  }

  // Check if result is 16 bit long (should be)
  assert((sCalib32.i32x / u16numOfIterations) <= 0xFFFF);
  assert((sCalib32.i32y / u16numOfIterations) <= 0xFFFF);
  assert((sCalib32.i32z / u16numOfIterations) <= 0xFFFF);

  // Correction is inverted value -> -1 * (offset)
  msRuntime.sGyroCorrection.i16x = -1 * sCalib32.i32x / u16numOfIterations;
  msRuntime.sGyroCorrection.i16y = -1 * sCalib32.i32y / u16numOfIterations;
  msRuntime.sGyroCorrection.i16z = -1 * sCalib32.i32z / u16numOfIterations;

  ESP_LOGI(MPU_TAG, "Calibration completed (Correction: x:%d y:%d z:%d)",
           msRuntime.sGyroCorrection.i16x, msRuntime.sGyroCorrection.i16y,
           msRuntime.sGyroCorrection.i16z);

  return ESP_OK;
}

esp_err_t mpu9250magCalibrate(const uint16_t u16numOfIterations) {
  // Big thanks to following author:
  // https://appelsiini.net/2018/calibrate-magnetometer/
  // Originally written in Python, but well explained

  // Recorded minimum
  tsVector16 sMin = {.i16x = INT16_MAX, .i16y = INT16_MAX, .i16z = INT16_MAX};

  // Recorded maximum
  tsVector16 sMax = {.i16x = INT16_MIN, .i16y = INT16_MIN, .i16z = INT16_MIN};

  // Actual magnetic vector
  tsVector16 sActual;

  // Average delta vector & average over axes for soft iron correction
  tsVector16 sAvgDelta;
  int16_t i16avgDelta;

  // For some intermediate calculations is required to use more bits to
  // avoid overflow at 16 bit long variables
  int32_t i32tmp;

  // For showing progress
  uint16_t u16showProgressCnt = 0;

  for (uint16_t u16iterationCnt = 0; u16iterationCnt < u16numOfIterations;
       u16iterationCnt++) {
    // Show progress once a while
    if (u16iterationCnt >= u16showProgressCnt) {
      ESP_LOGD(MPU_TAG, "Mag calibration: %u/%u", u16iterationCnt,
               u16numOfIterations);
      u16showProgressCnt += 25;
    }

    // Load magnetic data only with factory calibration data
    MPU9250_RET_WHEN_ERR(_magReadXYZ(&sActual, true, false));

    // Get minimum and maximum value
    vect16getMinimum(&sMin, &sActual, &sMin);
    vect16getMaximum(&sMax, &sActual, &sMax);
    vTaskDelay(1);
  }

  // Calculate offset (hard iron correction). In general, output from
  // magnetometer should be relatively small numbers -> no need to calculate
  // it inside 32 bit variable
  assert((sMin.i16x < 10000) && (sMin.i16x > -10000));
  assert((sMin.i16y < 10000) && (sMin.i16y > -10000));
  assert((sMin.i16z < 10000) && (sMin.i16z > -10000));
  assert((sMax.i16x < 10000) && (sMax.i16x > -10000));
  assert((sMax.i16y < 10000) && (sMax.i16y > -10000));
  assert((sMax.i16z < 10000) && (sMax.i16z > -10000));

  // =========================| Hard iron correction |======================
  // Calculate offset: (maximum + minimum)/2
  msRuntime.sMagRuntimeCalib.sOffset.i16x = (sMax.i16x + sMin.i16x) >> 1;
  msRuntime.sMagRuntimeCalib.sOffset.i16y = (sMax.i16y + sMin.i16y) >> 1;
  msRuntime.sMagRuntimeCalib.sOffset.i16z = (sMax.i16z + sMin.i16z) >> 1;

  // =========================| Soft iron correction |======================
  sAvgDelta.i16x = (sMax.i16x - sMin.i16x) / 2;
  sAvgDelta.i16y = (sMax.i16y - sMin.i16y) / 2;
  sAvgDelta.i16z = (sMax.i16z - sMin.i16z) / 2;

  i16avgDelta = (sAvgDelta.i16x + sAvgDelta.i16y + sAvgDelta.i16z) / 3;

  // And finally scale. Since integer can not store fractional values, result
  // is multiplied by 1000
  i32tmp = (1000 * (int32_t)i16avgDelta) / ((int32_t)sAvgDelta.i16x);
  assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
  msRuntime.sMagRuntimeCalib.sScale.i16x = (int16_t)i32tmp;

  i32tmp = (1000 * (int32_t)i16avgDelta) / ((int32_t)sAvgDelta.i16y);
  assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
  msRuntime.sMagRuntimeCalib.sScale.i16y = (int16_t)i32tmp;

  i32tmp = (1000 * (int32_t)i16avgDelta) / ((int32_t)sAvgDelta.i16z);
  assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
  msRuntime.sMagRuntimeCalib.sScale.i16z = (int16_t)i32tmp;

  if (nvsSettingsSet(_MAG_CALIB_ID, &msRuntime.sMagRuntimeCalib,
                     sizeof(msRuntime.sMagRuntimeCalib))) {
    return ESP_FAIL;
  } else {
    return ESP_OK;
  }
}

esp_err_t mpu9250magDelCalibration(void) {
  esp_err_t eErrCode = nvsSettingsDel(_MAG_CALIB_ID);
  // And reload data by default values
  eErrCode |= _getCalibrationData(&msRuntime.sMagRuntimeCalib);

  return eErrCode;
}

esp_err_t mpu9250getGyroDegPerSecRawInt(tsVector16 *psVector) {
  MPU9250_RET_WHEN_ERR(_accGyroReadXYZ(MPU9250_REG_GYRO_XOUT_H, psVector));

  // Due to calibration, this function is called quite heavily when call
  // setup function -> commented

  ESP_LOGV(MPU_TAG, "GyroInt: X: %d, Y: %d, Z: %d", psVector->i16x,
           psVector->i16y, psVector->i16z);

  return ESP_OK;
}

// ==========================| Internal functions |===========================
static esp_err_t _getCalibrationData(
    tsMagRuntimeCalibration *psMagRuntimeCalib) {
  // Default data are pretty straightforward - can generate here
  tsMagRuntimeCalibration sDefaultCalibrationdata = {
      .sOffset = {.i16x = 0, .i16y = 0, .i16z = 0},
      // Since this is not float, scale is 1000 for "1.000"
      .sScale = {.i16x = 1000, .i16y = 1000, .i16z = 1000}};

  return (nvsSettingsGet(_MAG_CALIB_ID, psMagRuntimeCalib,
                         &sDefaultCalibrationdata,
                         sizeof(tsMagRuntimeCalibration)));
}

static esp_err_t _accGyroCheckWhoAmI() {
  uint8_t u8regValue = 0;
  // Load register value and compare
  esp_err_t eErrCode =
      ACC_GYRO_READ_REG(MPU9250_REG_WHO_I_AM, &u8regValue, sizeof(u8regValue));
  if (eErrCode) {
    ESP_LOGE(MPU_TAG, "Can not communicate with sensors");
    return eErrCode;
  }

  if (u8regValue != MPU9250_REG_WHO_I_AM_117_DEFAULT) {
    ESP_LOGE(MPU_TAG, "Invalid value in register Who I am: 0x%2x", u8regValue);
    return ESP_ERR_INVALID_RESPONSE;
  } else {
    ESP_LOGD(MPU_TAG, "Who I am: 0x%2x", u8regValue);
  }

  return ESP_OK;
}

/**
 * @brief Read X, Y and Z from accelerometer or gyroscope
 * @param u8regAddr Start register address
 * @param psVector Pointer to structure with 16 bit values
 * @return ESP_OK if no problem
 */
static esp_err_t _accGyroReadXYZ(const uint8_t u8regAddr,
                                 tsVector16 *psVector) {
  assert(psVector);
  // Only accelerometer and gyroscope contains X, Y and Z coordinates
  assert((u8regAddr == MPU9250_REG_ACCEL_XOUT_H) ||
         (u8regAddr == MPU9250_REG_GYRO_XOUT_H));

  // X, Y, Z values are 16 bit long
  uint8_t au8dataRaw[3 * 2];

  MPU9250_RET_WHEN_ERR(
      ACC_GYRO_READ_REG(u8regAddr, au8dataRaw, sizeof(au8dataRaw)));

  psVector->i16x = ((au8dataRaw[0] << 8) | au8dataRaw[1]);
  psVector->i16y = ((au8dataRaw[2] << 8) | au8dataRaw[3]);
  psVector->i16z = ((au8dataRaw[4] << 8) | au8dataRaw[5]);

  return ESP_OK;
}

/**
 * @brief Read X, Y and Z from magnetometer
 * @param psVector Pointer to structure with 16 bit values
 * @param bWithFactoryCalibration If true, factory calibration data will be
 *                                taken into account
 * @param bWithRuntimeCalibration If true, runtime calibration data will be
 *                                taken into account
 * @return ESP_OK if no problem
 */
static esp_err_t _magReadXYZ(tsVector16 *psVector,
                             const bool bWithFactoryCalibration,
                             const bool bWithRuntimeCalibration) {
  assert(psVector);

  // Want 16 bit output
  tuMagControl1_0x0A uCtrlReg = {.s.MODE = R_0x0A_MODE_SINGLE_MEASUREMENT,
                                 .s.BIT = 0};
  tuMagStatus1_0x02 uStatus1reg = {0};

  // Need to read magnetometer data and "reset" in order to signal
  // magnetometer that data were read. Data are 16 bit long and there are
  // 3 axes (2x3) plus reset register (+1) -> 7
  uint8_t au8magData[7];

  // Temporary accumulator. Due to multiplication by high value the
  // 16 bit value could overflow -> using 32 bit long value is safe
  int32_t i32tmp;

  // Request one measurement
  MPU9250_RET_WHEN_ERR(MAG_WRITE_REG(MPU9250_MAG_REG_CNTL1, uCtrlReg.u8raw));

  // Wait until not ready
  while (!uStatus1reg.s.DRDY) {
    MPU9250_RET_WHEN_ERR(MAG_READ_REG(MPU9250_MAG_REG_ST1, &uStatus1reg.u8raw,
                                      sizeof(uStatus1reg)));
  }

  // Read data. Order is X, Y, Z. LSB first
  MPU9250_RET_WHEN_ERR(
      MAG_READ_REG(MPU9250_MAG_REG_HXL, au8magData, sizeof(au8magData)));

  // Fit data to vector structure
  psVector->i16x = ((au8magData[1] << 8) | au8magData[0]);
  psVector->i16y = ((au8magData[3] << 8) | au8magData[2]);
  psVector->i16z = ((au8magData[5] << 8) | au8magData[4]);

  // If calibration data should be taken into account, bit more calculation
  // is needed to be done
  if (bWithFactoryCalibration) {
    // Note that calibration data are multiplied by 1000 -> need to divide
    // by 1000 to get real value

    i32tmp =
        ((int32_t)psVector->i16x * (int32_t)msRuntime.sMagFactoryCalib.i16x) /
        1000;
    assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
    psVector->i16x = (int16_t)i32tmp;

    i32tmp =
        ((int32_t)psVector->i16y * (int32_t)msRuntime.sMagFactoryCalib.i16y) /
        1000;
    assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
    psVector->i16y = (int16_t)i32tmp;

    i32tmp =
        ((int32_t)psVector->i16z * (int32_t)msRuntime.sMagFactoryCalib.i16z) /
        1000;
    assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
    psVector->i16z = (int16_t)psVector->i16z;
  }

  if (bWithRuntimeCalibration) {
    // Scale is 1000x multiplied -> need to divide in final step
    i32tmp =
        ((int32_t)(psVector->i16x - msRuntime.sMagRuntimeCalib.sOffset.i16x) *
         (int32_t)msRuntime.sMagRuntimeCalib.sScale.i16x) /
        1000;
    assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
    psVector->i16x = (int16_t)i32tmp;

    i32tmp =
        ((int32_t)(psVector->i16y - msRuntime.sMagRuntimeCalib.sOffset.i16y) *
         (int32_t)msRuntime.sMagRuntimeCalib.sScale.i16y) /
        1000;
    assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
    psVector->i16y = (int16_t)i32tmp;

    i32tmp =
        ((int32_t)(psVector->i16z - msRuntime.sMagRuntimeCalib.sOffset.i16z) *
         (int32_t)msRuntime.sMagRuntimeCalib.sScale.i16z) /
        1000;
    assert((i32tmp >= INT16_MIN) && (i32tmp <= INT16_MAX));
    psVector->i16z = (int16_t)i32tmp;
  }

  return ESP_OK;
}

/**
 * @brief Read data from register(s)
 * @param u8regAddr Start register address
 * @param pau8buffer Pointer to buffer, where read data will be written
 * @param u8numOfBytes Number of Bytes to read
 * @param u8devAddr Device address
 * @return ESP_OK if no error
 */
static esp_err_t _read_reg(const uint8_t u8devAddr, const uint8_t u8regAddr,
                           uint8_t *pau8buffer, const uint8_t u8numOfBytes) {
  // Pointer check
  assert(pau8buffer);
  assert(u8numOfBytes);

  // Register range
  assert((u8devAddr == MPU9250_I2C_ADDR) ||
         (u8devAddr == MPU9250_I2C_MAGN_ADDR));
  if (u8devAddr == MPU9250_I2C_ADDR) {
    assert((u8regAddr >= MPU9250_REG_MIN) && (u8regAddr <= MPU9250_REG_MAX));
  } else {
    // The MPU9250_MAG_REG_MIN is zero -> when compare it throws warning
    assert(u8regAddr <= MPU9250_MAG_REG_MAX);
  }

  i2c_cmd_handle_t cmd;

  cmd = i2c_cmd_link_create();

  MPU9250_RET_WHEN_ERR(i2c_master_start(cmd));
  MPU9250_RET_WHEN_ERR(
      i2c_master_write_byte(cmd, (u8devAddr << 1) | I2C_MASTER_WRITE, 1));
  MPU9250_RET_WHEN_ERR(i2c_master_write_byte(cmd, u8regAddr, 1));
  MPU9250_RET_WHEN_ERR(i2c_master_stop(cmd));
  MPU9250_RET_WHEN_ERR(
      i2c_master_cmd_begin(I2C_NUM_0, cmd, 100000 / portTICK_PERIOD_MS));
  i2c_cmd_link_delete(cmd);
  // =================================| /Link |==========================

  // =================================| Link |===========================
  cmd = i2c_cmd_link_create();
  MPU9250_RET_WHEN_ERR(i2c_master_start(cmd));
  MPU9250_RET_WHEN_ERR(
      i2c_master_write_byte(cmd, (u8devAddr << 1) | I2C_MASTER_READ, 1));

  for (uint8_t u8byteCnt = 0; u8byteCnt < u8numOfBytes; u8byteCnt++) {
    // If this is last Byte
    if (u8byteCnt == (u8numOfBytes - 1)) {
      MPU9250_RET_WHEN_ERR(i2c_master_read_byte(cmd, pau8buffer, 1));
    } else {
      MPU9250_RET_WHEN_ERR(i2c_master_read_byte(cmd, pau8buffer, 0));
    }

    pau8buffer++;
  }

  MPU9250_RET_WHEN_ERR(i2c_master_stop(cmd));
  MPU9250_RET_WHEN_ERR(
      i2c_master_cmd_begin(I2C_NUM_0, cmd, 100000 / portTICK_PERIOD_MS));
  i2c_cmd_link_delete(cmd);

  // If program went here, everything is cool
  return ESP_OK;
}

/**
 * @brief Write one Byte to register address
 * @param u8regAddr Register address
 * @param u8value Value which will be written into given register
 * @param u8devAddr Device address
 * @return ESP_OK if no error
 */
static esp_err_t _write_reg(const uint8_t u8devAddr, const uint8_t u8regAddr,
                            const uint8_t u8value) {
  // Register range
  assert((u8devAddr == MPU9250_I2C_ADDR) ||
         (u8devAddr == MPU9250_I2C_MAGN_ADDR));
  if (u8devAddr == MPU9250_I2C_ADDR) {
    assert((u8regAddr >= MPU9250_REG_MIN) && (u8regAddr <= MPU9250_REG_MAX));
  } else {
    // The MPU9250_MAG_REG_MIN is zero -> when compare it throws warning
    assert(u8regAddr <= MPU9250_MAG_REG_MAX);
  }

  i2c_cmd_handle_t cmd;

  cmd = i2c_cmd_link_create();
  // Start bit (queue)
  MPU9250_RET_WHEN_ERR(i2c_master_start(cmd));

  // Write (queue)
  MPU9250_RET_WHEN_ERR(
      i2c_master_write_byte(cmd, (u8devAddr << 1) | I2C_MASTER_WRITE, 1));
  // Write (queue)
  MPU9250_RET_WHEN_ERR(i2c_master_write_byte(cmd, u8regAddr, 1));
  // Write (queue)
  MPU9250_RET_WHEN_ERR(i2c_master_write_byte(cmd, u8value, 1));

  // Stop bit
  MPU9250_RET_WHEN_ERR(i2c_master_stop(cmd));

  // Actually send data
  i2c_master_cmd_begin(I2C_NUM_0, cmd, 1000 / portTICK_PERIOD_MS);
  i2c_cmd_link_delete(cmd);

  return ESP_OK;
}

static void _swapBytes(tu16bit *puVariable) {
  uint8_t u8temp = puVariable->s.u8high;
  puVariable->s.u8high = puVariable->s.u8low;
  puVariable->s.u8low = u8temp;
}

static uint8_t _isNot32bitLong(int64_t i64input) {
  // If negative, get absolute value
  if (i64input < 0) {
    i64input = i64input * -1;
  }

  // Signed -> 31 bits actually
  if (i64input > 0x7FFFFFFF) {
    return 1;
  } else {
    return 0;
  }
}
