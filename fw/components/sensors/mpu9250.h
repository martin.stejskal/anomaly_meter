/**
 * @file
 * @author Martin Stejskal
 * @brief Low level driver for MPU6050
 */
#ifndef __MPU6050_H__
#define __MPU6050_H__
// ===============================| Includes |================================
#include <stdint.h>

#include "sensors_common.h"
// ================================| Defines |================================
/**
 * @brief I2C address of MPU9250
 */
#define MPU9250_I2C_ADDR (0x68)

/**
 * @brief I2C address of AK8963
 *
 * The magnetometer is on same chip with MPU9250, but it is separated IP core.
 * Therefore it have own address
 */
#define MPU9250_I2C_MAGN_ADDR (0x0C)

/**
 * @brief Register numbers for accelerometer and gyroscope
 *
 * @{
 */
#define MPU9250_REG_INT_PIN_BYPASS_EN_CFG (55)
#define MPU9250_REG_ACCEL_XOUT_H (59)
#define MPU9250_REG_TEMP_OUT_H (65)
#define MPU9250_REG_GYRO_XOUT_H (67)
#define MPU9250_REG_POWER_MANAMEMENT_1 (107)
#define MPU9250_REG_WHO_I_AM (117)
/**
 * @}
 */

/**
 * @brief Register numbers for magnetometer
 *
 * @{
 */
#define MPU9250_MAG_REG_WIA (0x00)
#define MPU9250_MAG_REG_ST1 (0x02)
#define MPU9250_MAG_REG_HXL (0x03)
#define MPU9250_MAG_REG_CNTL1 (0x0A)
#define MPU9250_MAG_REG_ASAX (0x10)

/**
 * @}
 */

/**
 * @brief Default value for register 107 (Power Management 1)
 */
#define MPU6050_REG_POWER_MANAGEMENT_1_107_DEFAULT (0x40)

/**
 * @brief Default value for register 117 (Who I am)
 */
#define MPU9250_REG_WHO_I_AM_117_DEFAULT (0x71)

/**
 * @brief Some unnatural value when checking if temperature sensor is ready
 *
 * When temperature sensor is initializing, value in register is zero and due
 * to nature of formula, result is non-zero value. So when sensor is simply
 * not ready yet, function return something ridiculous to make it clean, that
 * this is not typical.
 */
#define MPU9250_INVALID_TEMP (-123)

/**
 * @brief Gyroscope resolution (number of LSB per degree)
 */
#define MPU9250_GYRO_RES (131)

/**
 * @brief Accelerometer resolution (number of LSB per 1g)
 */
#define MPU9250_ACC_RES (16384)

/**
 * @brief Magnetic sensor value range in 14 bit resolution
 *
 * @{
 */
#define MPU9250_MAG_14_BIT_MIN (-8190)
#define MPU9250_MAG_14_BIT_MAX (8190)
/**
 * @}
 */
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===============================| Registers |===============================
// ======================| Accelerometer and Gyroscope |======================
// Expected Little Endian - LSB is "first" in list
typedef union {
  struct {
    uint8_t CLKSEL : 3;
    uint8_t TEMP_DIS : 1;
    uint8_t : 1;
    uint8_t CYCLE : 1;
    uint8_t SLEEP : 1;
    uint8_t DEVICE_RESET : 1;
  } s;
  uint8_t u8raw;
} tuPowerMgmnt_107;

typedef enum {
  R107_CLKSEL_INTERNAL_8MHZ_OSCILLATOR = 0,
  R107_CLKSEL_PLL_WITH_X_AXIS_GYRO_REF = 1,
  R107_CLKSEL_PLL_WITH_Y_AXIS_GYRO_REF = 2,
  R107_CLKSEL_PLL_WITH_Z_AXIS_GYRO_REF = 3,
  R107_CLKSEL_EXT_32768_HZ_REF = 4,
  R107_CLKSEL_EXT_19200_KHZ_REF = 5,
  R107_CLKSEL_RESERVED = 6,
  R107_CLKSEL_STOP_CLK_KEEP_TIMING_GEN_IN_RESET = 7
} tePowerMgmnt_107_clksel;

typedef union {
  struct {
    uint8_t reserved : 1;
    uint8_t BYPASS_EN : 1;
    uint8_t FSYNC_INT_MODE_EN : 1;
    uint8_t ACTL_FSYNC : 1;
    uint8_t INT_ANYRD_2CLEAR : 1;
    uint8_t LATCH_INT_EN : 1;
    uint8_t OPEN : 1;
    uint8_t ACTL;
  } s;
  uint8_t u8raw;
} tuIntPinBypassEnCfg_55;

// =============================| Magnetometer |==============================
typedef union {
  struct {
    uint8_t DRDY : 1;
    uint8_t DOR : 1;
    uint8_t reserved : 6;
  } s;
  uint8_t u8raw;
} tuMagStatus1_0x02;

typedef union {
  struct {
    uint8_t MODE : 4;
    uint8_t BIT : 1;  // 0: 14 bit output ; 1: 16 bit output
    uint8_t reserved : 3;
  } s;
  uint8_t u8raw;
} tuMagControl1_0x0A;

typedef enum {
  R_0x0A_MODE_POWER_DOWN = 0,
  R_0x0A_MODE_SINGLE_MEASUREMENT = 1,
  R_0x0A_MODE_CONTINUOUS_1 = 2,
  R_0x0A_MODE_CONTINUOUS_2 = 6,
  R_0x0A_MODE_EXTERNAL_TRIGGER_MEASUREMENT = 4,
  R_0x0A_MODE_SELF_TEST = 8,
  R_0x0A_MODE_FUSE_ROM_ACCESS = 15
} teMagControl1_0x0A_mode;
// ==============================| /Registers |===============================
typedef struct {
  // Hard iron offset
  tsVector16 sOffset;

  // Soft iron correction. Multiplied by 1000x (because value is fractional
  // and we're working with integer and not float).
  tsVector16 sScale;
} tsMagRuntimeCalibration;
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Setup hardware, calibrate gyroscope data
 * @return ESP_OK if no error
 */
esp_err_t mpu9250setup(void);

/**
 * @brief Get data from accelerometer as 16 bit vector
 * @param psVector Pointer to vector structure
 * @return ESP_OK if no error
 */
esp_err_t mpu9250getAccInt(tsVector16 *psVector);

/**
 * @brief Get data from accelerometer as 16 bit vector
 * @param psVector Pointer to vector structure
 * @return ESP_OK if no error
 */
esp_err_t mpu9250getAccFloat(tsVectorFloat *psVector);

/**
 * @brief Get temperature on chip in degrees of Celsius
 * @param pi8temp Result will be written here
 * @return ESP_OK if no error
 */
esp_err_t mpu9250getTempInt(int8_t *pi8temp);

/**
 * @brief Get temperature on chip in degrees of Celsius as float
 * @param pfTemp Result will be written here
 * @return ESP_OK if no error
 */
esp_err_t mpu9250getTempFloat(float *pfTemp);

esp_err_t mpu9250getGyroDegInt(tsVector32 *psVectorDeg);

/**
 * @brief Get raw integer data from magnetometer
 * @param psVector Pointer to vector structure
 * @return ESP_OK if no error
 */
esp_err_t mpu9250getMagInt(tsVector16 *psVector);
// ========================| Middle level functions |=========================
/**
 * @brief Get gyroscope data with calibration offset
 *
 * Output data are in degrees per second
 *
 * @param psVectorDegPerSec Result data will be written here
 * @return ESP_OK if no error
 */
esp_err_t mpu9250getGyroDegPerSecInt(tsVector16 *psVectorDegPerSec);

esp_err_t mpu9250getGyroDegPerSecFloat(tsVectorFloat *psVectorDegPerSec);
// ==========================| Low level functions |==========================
/**
 * @brief Calibrate gyroscope value while sensor is still
 *
 * @note Function {@link mpu6050setup()} automatically call this function
 *
 * @param u16numOfIterations Number of iterations.
 * @return ESP_OK if there is no problem
 */
esp_err_t mpu9250gyroCalibrate(const uint16_t u16numOfIterations);

/**
 * @brief Calibrate magnetometer and store setting in NVS
 *
 * Calibration is typically needed to be done only once when device is
 * assembled. reason is that surrounding wires affect magnetic field
 *
 * @param u16numOfIterations Number of iterations. Every iteration take at
 *        about 1 RTOS cycle. It should be few hundreds at least. Thousands
 *        ideally
 * @return ESP_OK if there is no problem
 */
esp_err_t mpu9250magCalibrate(const uint16_t u16numOfIterations);

/**
 * @brief Delete calibration data from NVS
 * @return ESP_OK if no problem
 */
esp_err_t mpu9250magDelCalibration(void);

/**
 * @brief Return raw read from gyroscope (degrees per second)
 * @param psVector Result data will be written here
 * @return ESP_OK if no error
 */
esp_err_t mpu9250getGyroDegPerSecRawInt(tsVector16 *psVector);

#endif  // __MPU6050_H__
