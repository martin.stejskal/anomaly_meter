/**
 * @file
 * @author Martin Stejskal
 * @brief Keep settings in non-volatile-storage memory
 */
#ifndef __NVS_SETTINGS_H__
#define __NVS_SETTINGS_H__
//================================| Includes |================================
#include <stddef.h>

// Modules from which structures are taken
// ================================| Defines |================================

// ============================| Default values |=============================
//===========================| Preprocessor checks |==========================
//============================| Structures, enums |===========================
typedef enum {
  NVS_STTNGS_OK = 0,                  //!< NVS_STTNGS_OK
  NVS_STTNGS_OPEN_ERR,                //!< NVS_STTNGS_OPEN_ERR
  NVS_STTNGS_READ_ERR,                //!< NVS_STTNGS_READ_ERR
  NVS_STTNGS_WRITE_ERR,               //!< NVS_STTNGS_WRITE_ERR
  NVS_STTNGS_VARIABLE_NOT_EXIST_YET,  //!< NVS_STTNGS_VARIABLE_NOT_EXIST_YET
  NVS_STTNGS_CAN_NOT_ERASE_KEY,       //!< NVS_STTNGS_CAN_NOT_ERASE_KEY
} teNvsSttngsErr;

//============================| Global variables |============================
//================================| Functions |===============================
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
teNvsSttngsErr nvsSettingsGet(const char *pacKey, void *pOutValue,
                              const void *pDefaultValue, size_t iSize);
teNvsSttngsErr nvsSettingsSet(const char *pacKey, const void *pInValue,
                              size_t iSize);
teNvsSttngsErr nvsSettingsDel(const char *pacKey);
#endif  // __NVS_SETTINGS_H__
