/**
 * @file
 * @author Martin Stejskal
 * @brief Keep settings in non-volatile-storage memory
 */
//================================| Includes |================================
#include "nvs_settings.h"

#include <assert.h>
#include <string.h>

#include "nvs_settings_config.h"

// Standard ESP libraries
#include <esp_err.h>
#include <esp_log.h>
#include <esp_system.h>
#include <nvs_flash.h>
// ================================| Defines |================================
#define STORAGE_NAMESPACE "storage"
#define NVS_STTNGS_GET "NVS sttngs get"
#define NVS_STTNGS_SET "NVS sttngs set"
// ============================| Default values |=============================
//===========================| Preprocessor checks |==========================
//============================| Structures, enums |===========================
//============================| Global variables |============================
//================================| Functions |===============================

// =====================| Internal function prototypes |======================
static teNvsSttngsErr _nvsGet(const char *pacKey, void *pOutValue,
                              const void *pDefaultValue, size_t *piSize);
static teNvsSttngsErr _nvsSet(const char *pacKey, const void *pInValue,
                              size_t iSize);
static teNvsSttngsErr _nvsDel(const char *pacKey);
// =========================| High level functions |==========================
// ========================| Middle level functions |=========================
// ==========================| Low level functions |==========================
teNvsSttngsErr nvsSettingsGet(const char *pacKey, void *pOutValue,
                              const void *pDefaultValue, size_t iSize) {
  return _nvsGet(pacKey, pOutValue, pDefaultValue, &iSize);
}

teNvsSttngsErr nvsSettingsSet(const char *pacKey, const void *pInValue,
                              size_t iSize) {
  return _nvsSet(pacKey, pInValue, iSize);
}

teNvsSttngsErr nvsSettingsDel(const char *pacKey) { return _nvsDel(pacKey); }
// ==========================| Internal functions |===========================
/**
 * @brief Get value from NVM
 * @param pacKey Key identificator
 * @param pOutValue Result will be written there
 * @param pDefaultValue Default value for case that value was not written to
 *                      EEPROM yet
 * @param piSize Size of value in Bytes
 * @return Zero if there is no problem
 */
inline static teNvsSttngsErr _nvsGet(const char *pacKey, void *pOutValue,
                                     const void *pDefaultValue,
                                     size_t *piSize) {
  // Pointers should not be empty
  assert(pacKey);
  assert(pOutValue);
  assert(pDefaultValue);
  assert(piSize);

  // Initialize NVS
  esp_err_t err = nvs_flash_init();
  if (err == ESP_ERR_NVS_NO_FREE_PAGES ||
      err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
    // NVS partition was truncated and needs to be erased
    // Retry nvs_flash_init
    ESP_ERROR_CHECK(nvs_flash_erase());
    err = nvs_flash_init();
  }
  ESP_ERROR_CHECK(err);

  nvs_handle_t sNvsHandler;
  teNvsSttngsErr eRetCode = NVS_STTNGS_OK;

  esp_err_t eErr = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &sNvsHandler);
  if (eErr) {
    // Use default & exit function - can not read anyway
    ESP_LOGE(NVS_STTNGS_GET, "Opening NVS failed: %d", eErr);
    return NVS_STTNGS_OPEN_ERR;
  }

  eErr = nvs_get_blob(sNvsHandler, pacKey, pOutValue, piSize);
  if (eErr) {
    // Use default
    memcpy(pOutValue, pDefaultValue, *piSize);

    if (eErr == ESP_ERR_NVS_NOT_FOUND) {
      ESP_LOGW(NVS_STTNGS_GET, "Reading failed - variable not set yet");
      eRetCode = NVS_STTNGS_VARIABLE_NOT_EXIST_YET;
    } else {
      ESP_LOGE(NVS_STTNGS_GET, "Reading from NVS failed: %d", eErr);
      eRetCode = NVS_STTNGS_READ_ERR;
    }
  }
  // Close in any case
  nvs_close(sNvsHandler);

#if NVS_STTNGS_VAL_NOT_EXIST_IS_OK
  if (eRetCode == NVS_STTNGS_VARIABLE_NOT_EXIST_YET) {
    return NVS_STTNGS_OK;
  }
#endif

  return eRetCode;
}

/**
 * @brief Write key and value to NVS
 * @param pacKey Key identificator
 * @param pInValue Key value
 * @param iSize Value size in Bytes
 * @return Zero if there is no problem
 */
inline static teNvsSttngsErr _nvsSet(const char *pacKey, const void *pInValue,
                                     size_t iSize) {
  // Pointers should not be empty
  assert(pacKey);
  assert(pInValue);

  // Size should not non-zero value
  assert(iSize);

  nvs_handle_t sNvsHandler;
  teNvsSttngsErr eRetCode = NVS_STTNGS_OK;

  esp_err_t eErr = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &sNvsHandler);
  if (eErr) {
    ESP_LOGE(NVS_STTNGS_SET, "Opening NVS failed: %d", eErr);
    return NVS_STTNGS_OPEN_ERR;
  }

  eErr = nvs_set_blob(sNvsHandler, pacKey, pInValue, iSize);
  if (eErr) {
    ESP_LOGE(NVS_STTNGS_SET, "Writing to NVS failed: %d", eErr);
    eRetCode = NVS_STTNGS_WRITE_ERR;
  } else {
    // Commit write
    eErr = nvs_commit(sNvsHandler);
    if (eErr) {
      eRetCode = NVS_STTNGS_WRITE_ERR;
    }
  }
  // Close anyway
  nvs_close(sNvsHandler);

  return eRetCode;
}

inline static teNvsSttngsErr _nvsDel(const char *pacKey) {
  nvs_handle_t sNvsHandler;
  teNvsSttngsErr eRetCode = NVS_STTNGS_OK;

  esp_err_t eErr = nvs_open(STORAGE_NAMESPACE, NVS_READWRITE, &sNvsHandler);
  if (eErr) {
    ESP_LOGE(NVS_STTNGS_SET, "Opening NVS failed: %d", eErr);
    return NVS_STTNGS_OPEN_ERR;
  }

  eErr = nvs_erase_key(sNvsHandler, pacKey);

  if (eErr == ESP_ERR_NVS_NOT_FOUND) {
    // Key does not exists -> kind of OK
    eRetCode = NVS_STTNGS_OK;
  } else if (eErr == ESP_OK) {
    // Delete successful. Commit changes
    eErr = nvs_commit(sNvsHandler);
    if (eErr) {
      eRetCode = NVS_STTNGS_WRITE_ERR;
    }
  } else if (eErr != ESP_OK) {
    // Some another error
    eRetCode = NVS_STTNGS_CAN_NOT_ERASE_KEY;
  }

  // Close anyway
  nvs_close(sNvsHandler);

  return eRetCode;
}
