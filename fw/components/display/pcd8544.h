/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for LCD controller 8544 (aka Nokia 5110 LCD)
 *
 * Based on https://github.com/yanbe/esp32-pcd8544-examples
 * Display resolution 84x48 (width x height)
 */
#ifndef _PCD8544_H_
#define _PCD8544_H_
// ===============================| Includes |================================
#include <driver/spi_common.h>
#include <stdbool.h>
#include <stdint.h>

#include "pcd8544_pin.h"

// ================================| Defines |================================
/**
 * @brief Display resolution in X axe (width)
 */
#define PCD8544_X_RES (84)

/**
 * @brief Display resolution in Y axe (height)
 */
#define PCD8544_Y_RES (48)

/**
 * @brief Height resolution per row
 */
#define PCD8544_Y_RES_ROW (8)

/**
 * @brief Number of lines
 *
 * Also this number match for font 5x7 px. Basically every line is 1 Byte
 * (8 bits) height.
 */
#define PCD8544_LINES (PCD8544_Y_RES / PCD8544_Y_RES_ROW)

/**
 * @brief Font width in pixels including extra space
 */
#define PCD8544_FONT_WIDTH_PX (6)

/**
 * @brief Font height in pixels including extra space
 */
#define PCD8544_FONT_HEIGHT_PX (8)

/**
 * @brief When setting data/command bit, these can be used to make code more
 *        easy to read
 *
 * @{
 */
#define PCD8544_DC_DATA (1)
#define PCD8544_DC_CMD (0)
/**
 * @}
 */

/**
 * @brief Internal frame buffer size
 *
 * Since data to LCD are written Byte by Byte, there are 6 lines (48 / 8).
 * So total size of frame buffer is 84 columns * 6 lines
 */
#define PCD8544_FRAME_BUF_SIZE (PCD8544_X_RES * PCD8544_LINES)

/**
 * @brief Bias System parameter for PCD8544
 *
 * It affects results of the contrast setting via `pcd8544setContrast()`
 *
 */
#define PCD8544_BIAS_SYSTEM (7)

/**
 * @brief Temperature coefficient parameter for PCD8544.
 *
 * It affects results of the contrast setting via `pcd8544setContrast()`, in
 * low temperature environment.
 */
#define PCD8544_TEMPERATURE_COEFFICIENT (2)

// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  /**
   * @brief Make DDRAM address pointer to move from (row,column) like
   *        (0,0), (1,0), (2,0) ... (83,0), (0,1), (1,1)
   *        order for each data transfer.
   */
  PCD8544_ADDRESSING_MODE_HORIZONTAL,  //!< PCD8544_ADDRESSING_MODE_HORIZONTAL
  /**
   * @brief Make DDRAM address pointer to move from (row,column) like
   *        (0,0), (0,1), (0,2) ... (0,5), (1,0), (1,1)
   *        order for each data transfer.
   */
  PCD8544_ADDRESSING_MODE_VERTICAL  //!< PCD8544_ADDRESSING_MODE_VERTICAL
} pcd8544_addressing_mode;

typedef enum {
  /**
   * @brief Make LCD content to blank
   */
  PCD8544_DISPLAY_MODE_BLANK = 0b00,

  /**
   * @brief Make all LCD segments on (for pin connection check)
   */
  PCD8544_DISPLAY_MODE_ALL_SEGMENTS_ON = 0b01,

  /**
   * @brief Make LCD segments correspond to DDRAM data
   */
  PCD8544_DISPLAY_MODE_NORMAL = 0b10,

  /**
   * @brief Make LCD segments opposite to DDRAM data
   */
  PCD8544_DISPLAY_MODE_INVERSE = 0b11,
} pcd8544_display_mode;

/**
 * @brief Pixel options
 */
typedef enum {
  WRITE_SET,  //!< Write pixel/byte/data by setting value to 1
  WRITE_CLR,  //!< Write pixel/byte/data by setting value to 0
} tePcd8544writeMode;

/**
 * @brief Configuration for PCD8544
 *
 *  Configurations for PCD8544 and Nokia 5110 LCD board.
 *
 */
typedef struct pcd8544_config_t {
  /**
   * @brief Configure PCD8544 board PCBs
   *
   * There are two type of PCB is selling store for Nokia 5100 LCD module.
   * They can be determined from its color:  red one and green one.
   *
   * Red one comes with common-anode LED so it this should be set to `true`.
   *
   * Blue one comes with common-cathode LED so it this should be set to
   * `false`.
   */
  bool is_backlight_common_anode;

  /**
   * @brief SPI host to be used for PCD8544.
   *
   * You can choose from ESP32 logical 3 SPI channels `SPI_HOST`, `HSPI_HOST`
   * and `VSPI_HOST`.
   *
   * @attention Different SPI host have different native pin assignment for
   *            fast and low latency IO_MUX. So ESP32-PCD8544 offers
   *            different pin assignment according to `.spi_host` member.
   *             You can confirm which pins are assigned via UART serial
   *             output log in INFO label.
   */
  spi_host_device_t spi_host;

  /**
   * @brief Manual SPI pin assignment configuration.
   *
   * Although native SPI pin assignment for SPI host is recommended as it
   * enables low latency SPI transfer by IOMMU, you can still specify any IO
   * pin numbers for each SPI pins  one by one, via setting pointer to
   * `*spi_pin` member.
   *
   * @attention All specified GPIO pins must be able to configured output
   *            pins. (see "2.2 Pin Description" in the ESP32 Datasheet)
   *
   * @attention Keeping this member `NULL` allows ESP32-PCD8544 to
   *            auto-configure native SPI pins corresponding to selected
   *            SPI host. You can confirm which pins are assigned to control
   *            pins from  UART serial output log (In INFO log level).
   */
  pcd8544_spi_pin_config_t *spi_pin;

  /**
   * @brief Manual control pin assignment configuration.
   *
   * To drive PCD8544, it requires some additional control pins from
   * transmission SPI pins; `DC`(Data/Command) pin, `RST`(Reset) pin and
   * `BL`(Backlight) pin. You can specify them manually alternative to
   * automatic configuration by ESP32-PCD8544.
   *
   * @attention All specified IO pins should be able to configured output
   *            pins.
   *
   * @attention Keeping this member `NULL` allows auto configuration of
   *            recommended control pin assignment by ESP32-PCD8544. You can
   *            confirm which pins are assigned to control pins from  UART
   *            serial output log (In INFO log level).
   */
  pcd8544_control_pin_config_t *control_pin;

  bool bControlReset;
} pcd8544_config_t;
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Initialize PCD8544 device
 *
 *  Initialize PCD8544 as SPI slave. You can make some operations via APIs
 *  offered from ESP32-PCD8544 until pcd8544_free() is called.
 *
 * @param pcd8544_config Configuration of PCD8544 SPI
 *
 * @return ESP_OK if no error
 */
esp_err_t pcd8544init(pcd8544_config_t *pcd8544_config);

/**
 * @brief Clear framebuffer
 *
 *  This function clear content in frame buffer
 *
 * @return ESP_OK if no error
 *
 * @attention  To confirm result on the LCD, you have to call
 *             pcd8544writeFbToLCD() after frame buffer writing operations
 *             are finished.
 */
esp_err_t pcd8544clearFb(void);

/**
 * @brief Transfer framebuffer content to LCD
 *
 * To avoid meaningless data transfers to LCD while using clear, write line,
 * putc and so on, data are kept in framebuffer. Once framebuffer is ready,
 * this function will transfer framebuffer data to LCD. In most cases this
 * technique reduce communication time between LCD and MCU
 *
 * @return ESP_OK if no error
 */
esp_err_t pcd8544writeFbToLCD(void);

/**
 * @brief Print text to the LCD by using framebuffer
 *
 * As this library includes 8x5 dots font, you can write up to 14 characters
 * per row.
 *
 * @param pacText Text to print
 * @param u8xPos Starting X position. Valid range: 0~84
 * @param u8row Starting row (line). Valid range 0~5
 *
 * @return ESP_OK if no error
 *
 * @attention  To write line data to the DDRAM and update segments on the LCD,
 *             you have to call pcd8544writeFbToLCD() after all frame
 *             buffer writing operation is finished.
 */
esp_err_t pcd8544putsFb(const char *pacText, const uint8_t u8xPos,
                        uint8_t u8row);

esp_err_t pcd8544printfFb(const uint8_t u8xPos, uint8_t u8row,
                          const char *format, ...);

/**
 * @brief Draw rectangle to the LCD.
 *
 *  This function draw rectangle on the frame buffer. You can specify
 *  upper-left and bottom-right points in geometry.
 *
 * @param u8x0  x of the upper-left corner.
 *
 * @param u8y0  y of the upper-left corner.
 *
 * @param u8x1  x of the bottom-right corner.
 *
 * @param u8y1  y of the bottom-right corner.
 *
 * @param eMode Set or clear pixel
 *
 * @return ESP_OK if no error
 *
 * @attention  To write line data to the DDRAM and update segments on the LCD,
 * you have to call pcd8544writeFbToLCD() after all frame buffer writing
 * operation is finished.
 */
esp_err_t pcd8544drawRectangleInFb(uint8_t u8x0, uint8_t u8y0, uint8_t u8x1,
                                   uint8_t u8y1,
                                   const tePcd8544writeMode eMode);

/**
 * @brief Draw simple horizontal bar graph
 *
 * Beginning is assumed very left pixel on given row.
 *
 * @param u8row Starring row
 * @param u16actualValue Current value for bar graph. Can be from range
 *        0 ~ u16maxValue
 * @param u16maxValue Maximum value for this bar graph. You can use full
 *        16 bit range (0 ~ 0xFFFF)
 * @return ESP_OK if no error
 */
esp_err_t pcd8544drawBarGraphHorizontalSimpleInFb(const uint8_t u8row,
                                                  const uint16_t u16actualValue,
                                                  const uint16_t u16maxValue);

/**
 * @brief Draw line to the LCD.
 *
 *  This function draw an line on the frame buffer. You can specify start and
 *  end points.
 *
 * @param u8x0  x of the start point.
 *
 * @param u8y0  y of the start point.
 *
 * @param u8x1  x of the end point.
 *
 * @param u8y1  y of the end point.
 *
 * @param eMode Set or clear pixel
 *
 * @return ESP_OK if no error
 *
 * @attention  To confirm result on the LCD, you have to call
 *             pcd8544writeFbToLCD() after frame buffer writing operation
 *             is finished.
 */
esp_err_t pcd8544drawLineInFb(uint8_t u8x0, uint8_t u8y0, uint8_t u8x1,
                              uint8_t u8y1, const tePcd8544writeMode eMode);

/**
 * @brief Clear selected row in framebuffer
 * @param u8row Row number. Range 0~6
 * @return ESP_OK if no error
 */
esp_err_t pcd8544clearRowInFb(uint8_t u8row);

/**
 * @brief Free SPI related resource completely.
 *
 * ESP32-PCD8544 utilize single SPI host of ESP32 to drive PCD8544.
 * Developer may want to re-use SPI host after some content has been written
 * to the LCD. In that case, SPI device and bus occupied by `pcd8544init()`
 * can be freed with this function.
 *
 * @return ESP_OK if no error
 *
 * @attention Once `pcd8544free()` is called, you have to call `pcd8544init()`
 *            again to later use.
 */
esp_err_t pcd8544free(void);

// ========================| Middle level functions |=========================
/**
 * @brief Set N Bytes in framebuffer
 * @param pu8bytes Pointer to data array
 * @param u16numOfBytes NUmber of Bytes which will be written to framebuffer
 * @param u8xPos Initial X position. Range 0~83
 * @param u8row Initial Row. Range 0~5
 * @param eMode Write mode ("pen color")
 * @return ESP_OK if no error
 */
esp_err_t pcd8544setBytesInFb(const uint8_t *pu8bytes,
                              const uint16_t u16numOfBytes,
                              const uint8_t u8xPos, const uint8_t u8row,
                              const tePcd8544writeMode eMode);

/**
 * @brief Set Byte in framebuffer
 * @param u8byte Byte value which will be written to framebuffer
 * @param u8xPos X position. Range 0~83
 * @param u8row Row number. Range 0~5
 * @param eMode Set or clear set pixels. Kind of "black" or "while" pencil
 * @return ESP_OK if no error
 */
esp_err_t pcd8544setByteInFb(const uint8_t u8byte, const uint8_t u8xPos,
                             const uint8_t u8row,
                             const tePcd8544writeMode eMode);

/**
 * @brief Set pixel in framebuffer
 * @param u8xPos X position. Range 0~83
 * @param u8yPos Y position. Range 0~47
 * @param eMode Set or clear pixel
 * @return ESP_OK if no error
 */
esp_err_t pcd8544setPixelInFb(const uint8_t u8xPos, const uint8_t u8yPos,
                              const tePcd8544writeMode eMode);

/**
 * @brief Control power-down mode / chip-active mode status of PCD8544.
 *
 *  PCD8544 provides `power-down` mode that consume only very low current on
 *  inactive status. This function control its status. In power-down mode,
 *  only 1.5uA current is consumed based on datasheet.
 *
 * @param bPowerdown Activate or deactivate power-down mode.
 *
 * @attention On entering  power-down mode, backlight goes off for to reduce
 *            current. Similarly, when back to chip-active mode backlight goes
 *            on.
 *
 * @attention To enter power-down mode correctly, content of frame buffer and
 *            LCD DRAM is cleared before entering power-down mode. Thus you
 *            have to recover LCD contents by yourself after back to
 *            chip-active mode.
 */
esp_err_t pcd8544setPowerdownMode(bool bPowerdown);

/**
 * @brief Print text to the LCD directly. No framebuffer is used
 *
 * Like standard `puts()` function, you can write arbitrary text to the LCD
 * via this function. Position of the text is determined from last write. LCD
 * keep track about that. You can override initial position by using
 * `pcd8544setPos()` function.
 *
 * As this library includes 8x5 dots font, you can write up to 14 characters
 * per row.
 *
 * @param pacText Text to print.
 *
 * @return ESP_OK if no error
 *
 * @attention If writing position of the text reached right-end of the LCD,
 * writing position is automatically moves to the next column. When it was the
 * bottom column (column 5), the next column is column 0.
 */
esp_err_t pcd8544puts(const char *pacText);

/**
 * @brief Print formatted text to the LCD without framebuffer
 *
 * Like standard `printf()` function, you can write arbitrary text with format.
 * Position of the text is determined from last write to LCD. Module itself
 * keep track about that. To override initial position, you need to call
 * `pcd8544setPos()` first.
 *
 * Format string is full compatible with standard `printf()` as this function
 * uses `spritntf()` internally.
 *
 * @param *format  format string to print.
 *
 * @param ...  arbitrary arguments for format string.
 *
 * @return ESP_OK if no error
 *
 * @attention If writing position of the text reached right-end of the LCD,
 *            writing position is automatically moves to the next column. When
 *            it was the bottom column (column 5), the next column is column 0.
 */
esp_err_t pcd8544printf(const char *format, ...);

// ==========================| Low level functions |==========================
/**
 * @brief Allow to check whether display is already initialized or not
 * @return True is display is initialized, false othewise
 */
bool pcd8544isInitialized(void);

/**
 * @brief Set LCD backlight status
 *
 *  Set LCD backlight enabled / disabled.
 *
 * @param bOn  Activate or deactivate LED for LCD.
 * @param bSaveSetting If true, save setting to NVS
 *
 * @return ESP_OK if no error
 *
 * @attention To use this function correctly you have to initialize
 *            `.is_backlight_common_anode` member in
 *            pcd8544_config_t according to the model of your Nokia 5100 LCD
 *            Module (red one or blue one, or you will get opposite result.
 */
esp_err_t pcd8544setBacklight(bool bOn, bool bSaveSetting);

/**
 * @brief Returns status of backlight
 * @return True if backlight is on, false otherwise
 */
bool pcd8544getBacklightStatus(void);

/**
 * @brief Set (row, col) position operation start position.
 *
 * This function set start position of the next operation. But when using
 * framebuffer, you do not need this one.
 *
 * @param u8column  start row of the next operation (0-83)
 *
 * @param u8row  start column of the next operation (0-6)
 *
 * @return ESP_OK if no error
 */
esp_err_t pcd8544setPos(uint8_t u8column, uint8_t u8row);

/**
 * @brief Control display mode of the LCD
 *
 *  PCD8544 provides four display modes. `normal`, `all segments on`,
 *  `inverted` and `blank`. This function controls PCD8544 between modes which
 *  defined on pcd8544_display_mode.
 *
 *  `normal` mode, DDRAM (Display Data RAM) as-is applied to LCD content.
 *
 *  `inverted` mode makes opposite on / off state of LCD segments.
 *
 *  In `all segments on` mode, you can confirm  electrical connection by
 *  setting and very high contrast setting.
 *
 *  `blank` mode make make all segments off.
 *
 * @param eMode  pcd8544_display_mode value for entering other display mode.
 *
 * @return ESP_OK if no error
 *
 * @attention  After pcd8544init() called, display mode goes `normal`.
 *
 * @attention  `blank` mode does not erase LCD contents. It is hold in DDRAM
 *             (Display Data RAM) and still and can be recovered by switching
 *             to other modes.
 */
esp_err_t pcd8544setDisplayMode(pcd8544_display_mode eMode);

/**
 * @brief Control contrast of the LCD
 *
 *  PCD8544 have contrast control functionality. You can access this by setting
 *  contrast value. Optimal parameter may different from each individuals.
 *  The pcd8544init() resets contrast to empirically good default but you can
 *  tune via this function.
 *
 * @param u8ovp Operation voltage parameter for LCD (0~127). In default
 *              configuration options, range between 20 to 24 will be good
 *              choice.
 *
 * @return ESP_OK if no error
 *
 * @attention  pcd8544init() calls this function internally with empirically
 *             good default (u8vop=20) so you don't need to tune contrast
 *             manually. You can still tune contrast against different
 *             individual characteristics.
 */
esp_err_t pcd8544setContrast(uint8_t u8ovp);

/**
 * @brief Send raw data to LCD
 *
 * @note Data are simply send. No check, no parsing.
 *
 * @param pu8data Pointer to data array
 * @param u16numOfBytes NUmber of Bytes which will be sent
 * @param bDc Data (1) or command (0)
 * @return ESP_OK if no error
 */
esp_err_t pcd8544sendRawData(const uint8_t *pu8data,
                             const uint16_t u16numOfBytes, const bool bDc);

esp_err_t pcd8544getActualRow(uint16_t u16frameBufferAbsIdx,
                              uint8_t *pu8rowIdx);

/**
 * @brief Swap bit position in Byte
 * @param u8data Source data
 * @return Swapped Byte
 */
uint8_t pcd8544swapBitsInByte(uint8_t u8data);

#endif /* _PCD8544_H_ */
