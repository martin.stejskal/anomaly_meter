/**
 * @file
 * @author Martin Stejskal
 * @brief Driver for LCD controller 8544 (aka Nokia 5110 LCD)
 *
 * Based on https://github.com/yanbe/esp32-pcd8544-examples
 * Display resolution 84x48 (width x height)
 *
 * Original source was quite nice, but there were some memory leak which was
 * not successfully fixed, so this is simplified version. In general,
 * framebuffer is used because it allow pre-render image in RAM and then
 * push whole image as 1 update. Partial writing would be possible, but
 * when mixed graphic and text, it would not be effective.
 *
 * Also framebuffer storage logic changed. Now it is addressed by lines ->
 * first 84 Bytes are for 1st line, next 84 Bytes for second and so on
 */
// ===============================| Includes |================================
#include "pcd8544.h"

#include <assert.h>
#include <driver/gpio.h>
#include <driver/spi_master.h>
#include <esp_err.h>
#include <esp_log.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include "font5x7.h"
#include "nvs_settings.h"
#include "pcd8544_pin.h"
// ================================| Defines |================================
/**
 * @brief This constant reflect HW capabilities of SPI buffer
 *
 * After some investigation, trial and error this seems to be working
 * configuration. Following is from technical reference:
 *  * Chapter 7.3.2 -> received and/or sent data: length of 0 ~ 512 bits
 *                     (64 bytes)
 */
#define _PCD8544_MAX_BITS_PER_SPI_TRANSFER (16 * 32)

#define _PCD8544_RET_WHEN_ERR(func) \
  eErrCode = func;                  \
  if (eErrCode) {                   \
    return eErrCode;                \
  }

#define _NVS_BCKLGHT_ID "dispBckl"
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
static const char *TAG = "PCD8544";

/**
 * @brief SPI handler
 *
 * Used for de-initialization (when needed)
 */
static spi_device_handle_t msSpi;

/**
 * @brief Display specific settings from upper layer
 */
static pcd8544_config_t mpsConfig;

/**
 * @brief Framebuffer itself
 */
static uint8_t mau8frameBuff[PCD8544_FRAME_BUF_SIZE];

/**
 * @brief When initialization is done, this flag is set
 */
static bool mbInitDone = false;

/**
 * @brief Keep backlight status safe and sound
 */
static bool mbBacklightOn = false;
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static esp_err_t _spiInit(void);
static esp_err_t _reset(void);

static esp_err_t _finalizeFrameBuf(void);

static esp_err_t _checkXpos(const uint8_t u8xPos);
static esp_err_t _checkYpos(const uint8_t u8yPos);
static esp_err_t _checkRow(const uint8_t u8row);
static esp_err_t _checkWriteMode(const tePcd8544writeMode eMode);
// =========================| High level functions |==========================
esp_err_t pcd8544init(pcd8544_config_t *pcd8544_config) {
  esp_err_t eErrCode = ESP_FAIL;

  mpsConfig = *pcd8544_config;
  if (mpsConfig.control_pin == NULL) {
    mpsConfig.control_pin =
        (void *)pcd8544_default_control_pin_config(mpsConfig.spi_host);
  }

  // Enable backlight - just to simply verify if it works or not. Do not
  // save setting yet
  _PCD8544_RET_WHEN_ERR(pcd8544setBacklight(true, false));

  // If reset feature is enabled
  if (pcd8544_config->bControlReset) {
    _PCD8544_RET_WHEN_ERR(gpio_set_direction(
        mpsConfig.control_pin->reset_io_num, GPIO_MODE_OUTPUT));
  }
  _PCD8544_RET_WHEN_ERR(
      gpio_set_direction(mpsConfig.control_pin->dc_io_num, GPIO_MODE_OUTPUT));
  _PCD8544_RET_WHEN_ERR(gpio_set_direction(
      mpsConfig.control_pin->backlight_io_num, GPIO_MODE_OUTPUT));

  ESP_LOGI(TAG, "Control pins: RST=IO%d, DC=IO%d, BL=IO%d",
           mpsConfig.control_pin->reset_io_num,
           mpsConfig.control_pin->dc_io_num,
           mpsConfig.control_pin->backlight_io_num);

  _PCD8544_RET_WHEN_ERR(_spiInit());

  // If reset feature is enabled
  if (pcd8544_config->bControlReset) {
    _PCD8544_RET_WHEN_ERR(_reset());
  }

  _PCD8544_RET_WHEN_ERR(pcd8544setDisplayMode(PCD8544_DISPLAY_MODE_NORMAL));
  _PCD8544_RET_WHEN_ERR(pcd8544setContrast(20));

  // Restore backlight setting
  bool bBacklightOn;
  bool bDefault = true;

  _PCD8544_RET_WHEN_ERR(nvsSettingsGet(_NVS_BCKLGHT_ID, &bBacklightOn,
                                       &bDefault, sizeof(bBacklightOn)));

  // Fresh load, no need to save again
  _PCD8544_RET_WHEN_ERR(pcd8544setBacklight(bBacklightOn, false));

  mbInitDone = true;

  return eErrCode;
}

esp_err_t pcd8544clearFb() {
  memset(mau8frameBuff, 0, sizeof(mau8frameBuff));
  return ESP_OK;
}

esp_err_t pcd8544writeFbToLCD(void) { return _finalizeFrameBuf(); }

esp_err_t pcd8544putsFb(const char *pacText, const uint8_t u8xPos,
                        uint8_t u8row) {
  esp_err_t eErrCode = ESP_FAIL;

  // Input parameter check
  if (!pacText) {
    ESP_LOGE(TAG, "%s Pointer to text is empty", __func__);
    return ESP_ERR_INVALID_ARG;
  }
  if (u8xPos >= PCD8544_X_RES) {
    ESP_LOGE(TAG, "%s X position over limit (%d >= %d)", __func__, u8xPos,
             PCD8544_X_RES);
    return ESP_ERR_INVALID_ARG;
  }
  if (u8row >= (PCD8544_Y_RES / 8)) {
    ESP_LOGE(TAG, "%s Line number over limit (%d >= %d)", __func__, u8row,
             PCD8544_Y_RES / 8);
    return ESP_ERR_INVALID_ARG;
  }
  ///@todo Break font to next line if needed

  uint8_t u8textLen = strlen(pacText);

  // Font is hollow without margin space -> +1
  uint8_t u8charWidth = sizeof(font5x7[0]) + 1;

  // Initial position in frame buffer
  const int16_t i16initFbPos = (u8xPos) + (PCD8544_X_RES * u8row);

  // Absolute position in frame buffer. Automatically increased after writing
  // every Byte
  int16_t i16absFbPos = i16initFbPos;

  for (uint8_t u8charIdx = 0; u8charIdx < u8textLen; u8charIdx++) {
    if (pacText[u8charIdx] == '\n') {
      // Get current row
      uint8_t u8currentRowIdx;
      _PCD8544_RET_WHEN_ERR(pcd8544getActualRow(i16absFbPos, &u8currentRowIdx));

      // Jump to next row, check if need to jump back to line 0
      u8currentRowIdx++;
      if (u8currentRowIdx >= PCD8544_LINES) {
        u8currentRowIdx = 0;
      }

      i16absFbPos = u8currentRowIdx * PCD8544_X_RES;

    } else {
      for (uint8_t u8fontPx = 0; u8fontPx < (u8charWidth - 1); u8fontPx++) {
        mau8frameBuff[i16absFbPos] =
            font5x7[pacText[u8charIdx] - FONT5X7_CHAR_CODE_OFFSET][u8fontPx];

        // Increase absolute position in frame buffer
        i16absFbPos++;
      }
      // Add space (blank 8 bit), which is not defined as part of character
      i16absFbPos++;

      // Need to check for end of buffer
      if (i16absFbPos >= sizeof(mau8frameBuff)) {
        // Need to start from beginning of display
        i16absFbPos = 0;
        // No need to add any 1px space here
      } else {
        // Add space
        mau8frameBuff[i16absFbPos] = 0;
      }
    }
  }

  return ESP_OK;
}

esp_err_t pcd8544printfFb(const uint8_t u8xPos, uint8_t u8row,
                          const char *format, ...) {
  // When font 7x5 px us used -> character take 8x6 px
  char acBuffer[(PCD8544_X_RES / 6) * PCD8544_LINES];

  va_list ap;
  va_start(ap, format);

  vsprintf(acBuffer, format, ap);
  va_end(ap);

  return (pcd8544putsFb(acBuffer, u8xPos, u8row));
}

esp_err_t pcd8544drawRectangleInFb(uint8_t u8x0, uint8_t u8y0, uint8_t u8x1,
                                   uint8_t u8y1,
                                   const tePcd8544writeMode eMode) {
  esp_err_t eErrCode;

  _PCD8544_RET_WHEN_ERR(_checkXpos(u8x0))
  _PCD8544_RET_WHEN_ERR(_checkXpos(u8x1))
  _PCD8544_RET_WHEN_ERR(_checkYpos(u8y0))
  _PCD8544_RET_WHEN_ERR(_checkYpos(u8y1))
  _PCD8544_RET_WHEN_ERR(_checkWriteMode(eMode))

  eErrCode = pcd8544drawLineInFb(u8x0, u8y0, u8x1, u8y0, eMode);
  eErrCode |= pcd8544drawLineInFb(u8x1, u8y0, u8x1, u8y1, eMode);
  eErrCode |= pcd8544drawLineInFb(u8x1, u8y1, u8x0, u8y1, eMode);
  eErrCode |= pcd8544drawLineInFb(u8x0, u8y0, u8x0, u8y1, eMode);

  return eErrCode;
}

esp_err_t pcd8544drawBarGraphHorizontalSimpleInFb(const uint8_t u8row,
                                                  const uint16_t u16actualValue,
                                                  const uint16_t u16maxValue) {
  esp_err_t eErrCode;

  _PCD8544_RET_WHEN_ERR(_checkRow(u8row));
  if (u16actualValue > u16maxValue) {
    return ESP_ERR_INVALID_ARG;
  }

  // Get starting point in framebuffer
  uint16_t u16fbIdx = PCD8544_X_RES * u8row;

  assert(u16fbIdx < sizeof(mau8frameBuff));

  // Get bar graph length (relative ratio between display X resolution, maximum
  // value and current value
  uint32_t u32relLength =
      ((uint32_t)u16actualValue * PCD8544_X_RES) / ((uint32_t)u16maxValue);

  assert(u32relLength <= PCD8544_X_RES);

  for (uint8_t u8xPos = 0; u8xPos < u32relLength; u8xPos++) {
    // Fill "top" 7 pixels.
    mau8frameBuff[u16fbIdx++] = 0x7F;
  }

  return eErrCode;
}

esp_err_t pcd8544drawLineInFb(uint8_t u8x0, uint8_t u8y0, uint8_t u8x1,
                              uint8_t u8y1, const tePcd8544writeMode eMode) {
  // Inspirated by
  // http://read.pudn.com/downloads212/sourcecode/asm/997996/lcd_pcd8544/pcd8544.c__.htm

  esp_err_t eErrCode;
  // ========================| Input parameters check |=====================
  _PCD8544_RET_WHEN_ERR(_checkXpos(u8x0))
  _PCD8544_RET_WHEN_ERR(_checkXpos(u8x1))
  _PCD8544_RET_WHEN_ERR(_checkYpos(u8y0))
  _PCD8544_RET_WHEN_ERR(_checkYpos(u8y1))
  _PCD8544_RET_WHEN_ERR(_checkWriteMode(eMode))

  // Step in X and Y axes
  int8_t i8stepX, i8stepY;

  // Fraction for temporary calculations
  int16_t i16fraction;

  // Processing itself. Calculate directions in X and Y axe
  int16_t i16dy = u8y1 - u8y0;
  int16_t i16dx = u8x1 - u8x0;

  // Direction in Y axe
  if (i16dy < 0) {
    i16dy = -i16dy;
    i8stepY = -1;
  } else {
    i8stepY = 1;
  }

  // Direction in Y axe
  if (i16dx < 0) {
    i16dx = -i16dx;
    i8stepX = -1;
  } else {
    i8stepX = 1;
  }

  // Double values
  i16dx <<= 1;
  i16dy <<= 1;

  // Draw initial position
  ESP_ERROR_CHECK(pcd8544setPixelInFb(u8x0, u8y0, eMode));

  // Draw next position until done
  if (i16dx > i16dy) {
    // Take fraction
    i16fraction = i16dy - (i16dx >> 1);
    while (u8x0 != u8x1) {
      if (i16fraction >= 0) {
        u8y0 += i8stepY;
        i16fraction -= i16dx;
      }
      u8x0 += i8stepX;
      i16fraction += i16dy;

      // Draw point
      ESP_ERROR_CHECK(pcd8544setPixelInFb(u8x0, u8y0, eMode));
    }
  } else {
    i16fraction = i16dx - (i16dy >> 1);
    while (u8y0 != u8y1) {
      if (i16fraction >= 0) {
        u8x0 += i8stepX;
        i16fraction -= i16dy;
      }
      u8y0 += i8stepY;
      i16fraction += i16dx;

      ESP_ERROR_CHECK(pcd8544setPixelInFb(u8x0, u8y0, eMode));
    }
  }

  return ESP_OK;
}

esp_err_t pcd8544clearRowInFb(uint8_t u8row) {
  esp_err_t eErrCode = ESP_FAIL;
  // Check parameter
  _PCD8544_RET_WHEN_ERR(_checkRow(u8row))

  // Get starting point in framebuffer
  uint16_t u16fbIdx = PCD8544_X_RES * u8row;

  assert(u16fbIdx < sizeof(mau8frameBuff));

  for (uint8_t u8xPos = 0; u8xPos < PCD8544_X_RES; u8xPos++) {
    mau8frameBuff[u16fbIdx++] = 0x00;
  }

  return eErrCode;
}

esp_err_t pcd8544free(void) {
  mbInitDone = false;

  esp_err_t eErrCode = ESP_FAIL;
  _PCD8544_RET_WHEN_ERR(spi_bus_remove_device(msSpi));
  _PCD8544_RET_WHEN_ERR(spi_bus_free(mpsConfig.spi_host));

  return eErrCode;
}

// ========================| Middle level functions |=========================
esp_err_t pcd8544setBytesInFb(const uint8_t *pu8bytes,
                              const uint16_t u16numOfBytes,
                              const uint8_t u8xPos, const uint8_t u8row,
                              const tePcd8544writeMode eMode) {
  esp_err_t eErrCode;
  // ========================| Input parameters check |=====================
  assert(pu8bytes);
  _PCD8544_RET_WHEN_ERR(_checkXpos(u8xPos))
  _PCD8544_RET_WHEN_ERR(_checkRow(u8row))
  _PCD8544_RET_WHEN_ERR(_checkWriteMode(eMode))

  // Store pointer to source data (will be changed)
  uint8_t *pu8src = (uint8_t *)pu8bytes;

  // Check number of Bytes. Should not overflow framebuffer.

  // Get Byte position in framebuffer
  uint16_t u16fbByteIdx = u8xPos + (PCD8544_X_RES * u8row);

  assert(u16fbByteIdx < sizeof(mau8frameBuff));

  const uint16_t u16reminingBytes = sizeof(mau8frameBuff) - u16fbByteIdx;

  // Check if data fit
  if (u16reminingBytes < u16numOfBytes) {
    // Out of memory -> couldn't fit into framebuffer
    return ESP_ERR_NO_MEM;
  }

  // Else can fit -> fill framebuffer
  for (uint16_t u16numOfBytesSet = 0; u16numOfBytesSet < u16numOfBytes;
       u16numOfBytesSet++) {
    switch (eMode) {
      case WRITE_SET:
        // Add pixel
        mau8frameBuff[u16fbByteIdx++] |= *pu8src;
        break;
      case WRITE_CLR:
        // Remove pixel
        mau8frameBuff[u16fbByteIdx++] &= ~(*pu8src);
        break;
      default:
        // Should not happen
        ESP_LOGE(TAG, "%s Unexpected mode: %d", __func__, eMode);
        assert(0);
        return ESP_FAIL;
    }

    pu8src++;
  }

  return ESP_OK;
}

esp_err_t pcd8544setByteInFb(const uint8_t u8byte, const uint8_t u8xPos,
                             const uint8_t u8row,
                             const tePcd8544writeMode eMode) {
  esp_err_t eErrCode;
  // ========================| Input parameters check |=====================
  _PCD8544_RET_WHEN_ERR(_checkXpos(u8xPos))
  _PCD8544_RET_WHEN_ERR(_checkRow(u8row))
  _PCD8544_RET_WHEN_ERR(_checkWriteMode(eMode))

  // Get Byte position in framebuffer
  const uint16_t u16fbByteIdx = u8xPos + (PCD8544_X_RES * u8row);

  assert(u16fbByteIdx < sizeof(mau8frameBuff));

  switch (eMode) {
    case WRITE_SET:
      // Add pixel
      mau8frameBuff[u16fbByteIdx] |= u8byte;
      break;
    case WRITE_CLR:
      // Remove pixel
      mau8frameBuff[u16fbByteIdx] &= ~(u8byte);
      break;
    default:
      // Should not happen
      ESP_LOGE(TAG, "%s Unexpected mode: %d", __func__, eMode);
      assert(0);
      return ESP_FAIL;
  }

  return ESP_OK;
}

esp_err_t pcd8544setPixelInFb(const uint8_t u8xPos, const uint8_t u8yPos,
                              const tePcd8544writeMode eMode) {
  esp_err_t eErrCode;
  // ========================| Input parameters check |=====================
  _PCD8544_RET_WHEN_ERR(_checkXpos(u8xPos))
  _PCD8544_RET_WHEN_ERR(_checkYpos(u8yPos))
  _PCD8544_RET_WHEN_ERR(_checkWriteMode(eMode))

  // Get correct Byte from framebuffer. X position + rows*X resolution.
  // Since rows are 8 bit height, need to divide by 8 to get correct row
  const uint8_t u8rowIdx = u8yPos / 8;
  const uint16_t u16fbByteIdx = u8xPos + (PCD8544_X_RES * u8rowIdx);

  assert(u16fbByteIdx < sizeof(mau8frameBuff));

  // So now we know which byte to read, but still need to know which bit.
  // Simply calculate difference between given Y position
  uint8_t u8bitPos = u8yPos - (u8rowIdx * 8);

  // ESP_LOGV(TAG, "%s FB idx: %d ; row: %d ; bit: %d -> Loaded: 0x%02x",
  //          __func__, u16fbByteIdx, u8rowIdx, u8bitPos, u8loaded);

  switch (eMode) {
    case WRITE_SET:
      // Add pixel
      mau8frameBuff[u16fbByteIdx] |= (1 << u8bitPos);
      break;
    case WRITE_CLR:
      // Remove pixel
      mau8frameBuff[u16fbByteIdx] &= ~(1 << u8bitPos);
      break;
    default:
      // Should not happen
      ESP_LOGE(TAG, "%s Unexpected mode: %d", __func__, eMode);
      assert(0);
      return ESP_FAIL;
  }

  // ESP_LOGV(TAG, "%s mod Byte: 0x%02x", __func__,
  //          mau8frameBuff[u16fbByteIdx]);

  return ESP_OK;
}

esp_err_t pcd8544setPowerdownMode(bool bPowerdown) {
  esp_err_t eErrCode = ESP_FAIL;
  // Disable backlight, do not save
  _PCD8544_RET_WHEN_ERR(pcd8544setBacklight(!bPowerdown, false));

  if (bPowerdown) {
    _PCD8544_RET_WHEN_ERR(pcd8544clearFb());
    _PCD8544_RET_WHEN_ERR(_finalizeFrameBuf());
  }

  // power-down mode / chip is active
  uint8_t u8cmd = 0b00100000 | bPowerdown ? (1 << 2) : 0;
  return (pcd8544sendRawData(&u8cmd, sizeof(u8cmd), PCD8544_DC_CMD));
}

esp_err_t pcd8544puts(const char *pacText) {
  size_t uTextLen = strlen(pacText);
  uint8_t u8charWidth = sizeof(font5x7[0]) + 1;
  uint16_t u16dataLen = u8charWidth * uTextLen;

  // Need to limit buffer. Trying to avoid malloc() whatever is the price
  if (u16dataLen > (PCD8544_LINES * PCD8544_X_RES)) {
    return ESP_ERR_NO_MEM;
  }
  uint8_t au8data[PCD8544_LINES * PCD8544_X_RES];

  for (uint8_t u8charIdx = 0; u8charIdx < uTextLen; u8charIdx++) {
    for (uint8_t u8charPixel = 0; u8charPixel < u8charWidth - 1;
         u8charPixel++) {
      au8data[u8charIdx * u8charWidth + u8charPixel] =
          font5x7[pacText[u8charIdx] - FONT5X7_CHAR_CODE_OFFSET][u8charPixel];
    }
    au8data[u8charIdx * u8charWidth + u8charWidth - 1] = 0;
  }
  return (pcd8544sendRawData(au8data, u16dataLen, PCD8544_DC_DATA));
}

esp_err_t pcd8544printf(const char *format, ...) {
  // When font 7x5 px us used -> character take 8x6 px
  char acBuffer[(PCD8544_X_RES / 6) * PCD8544_LINES];

  va_list ap;
  va_start(ap, format);

  vsprintf(acBuffer, format, ap);
  va_end(ap);

  return (pcd8544puts(acBuffer));
}

// ==========================| Low level functions |==========================
inline bool pcd8544isInitialized(void) { return mbInitDone; }

esp_err_t pcd8544setBacklight(bool bOn, bool bSaveSetting) {
  esp_err_t eErrCode = ESP_FAIL;
  mbBacklightOn = bOn;

  bool flag = mpsConfig.is_backlight_common_anode ? !bOn : bOn;
  eErrCode = gpio_set_level(mpsConfig.control_pin->backlight_io_num, flag);

  if (bSaveSetting) {
    if (nvsSettingsSet(_NVS_BCKLGHT_ID, &bOn, sizeof(bOn))) {
      // If fails, return error code
      return ESP_FAIL;
    }
  }

  return eErrCode;
}

inline bool pcd8544getBacklightStatus(void) { return mbBacklightOn; }

esp_err_t pcd8544setPos(uint8_t u8column, uint8_t u8row) {
  uint8_t au8cmds[2];
  au8cmds[0] = 0b10000000 | u8column, au8cmds[1] = 0b01000000 | u8row;

  return (pcd8544sendRawData(au8cmds, sizeof(au8cmds), PCD8544_DC_CMD));
}

esp_err_t pcd8544setDisplayMode(pcd8544_display_mode eMode) {
  uint8_t u8cmd = 0b00001000 | ((eMode & 2) << 1) | (eMode & 1);

  return (pcd8544sendRawData(&u8cmd, sizeof(u8cmd), PCD8544_DC_CMD));
}

esp_err_t pcd8544setContrast(uint8_t u8ovp) {
  uint8_t au8cmds[5];
  // extended instruction set
  au8cmds[0] = 0b00100001;
  // Temperature coefficient
  au8cmds[1] = 0b00000100 | PCD8544_TEMPERATURE_COEFFICIENT;
  au8cmds[2] = 0b00010000 | PCD8544_BIAS_SYSTEM;  // bias system
  au8cmds[3] = 0b10000000 | u8ovp;                // Vop (contrast)
  au8cmds[4] = 0b00100000;                        // basic instruction set

  return (pcd8544sendRawData(au8cmds, sizeof(au8cmds), PCD8544_DC_CMD));
}

esp_err_t pcd8544sendRawData(const uint8_t *pu8data,
                             const uint16_t u16numOfBytes, const bool bDc) {
  // Check for empty pointer and number of Bytes to be sent
  assert(pu8data);
  assert(u16numOfBytes);

  esp_err_t eErrCode = ESP_FAIL;

  // Set Data/!command GPIO
  _PCD8544_RET_WHEN_ERR(gpio_set_level(mpsConfig.control_pin->dc_io_num, bDc));

  spi_transaction_t sTransaction;

  // Size in bits -> 8x
  const uint32_t u32numOfBits = u16numOfBytes * 8;

  // If we want transmit more than ~4 kbits, function
  // spi_device_polling_transmit() will not process it. Unfortunately
  // framebuffer is bit bigger than 4 kbit, so process needs to be split
  // into smaller chunks. For no reason, let's transmit N bits max
  // at the transaction
  uint32_t u32numOfReminingBits = u32numOfBits;

  // Number of bits to be transmitted
  uint16_t u16bitsTx;

  while (u32numOfReminingBits) {
    // Required to clean-up transaction structure after every transaction
    memset(&sTransaction, 0, sizeof(sTransaction));

    // If there is more than we want to handle
    if (u32numOfReminingBits > _PCD8544_MAX_BITS_PER_SPI_TRANSFER) {
      u16bitsTx = _PCD8544_MAX_BITS_PER_SPI_TRANSFER;
    } else {
      // Else it is OK to transmit all now
      u16bitsTx = u32numOfReminingBits;
    }

    sTransaction.length = u16bitsTx;
    // Need to move pointer to buffer every time
    sTransaction.tx_buffer =
        pu8data + ((u32numOfBits - u32numOfReminingBits) / 8);

    _PCD8544_RET_WHEN_ERR(spi_device_polling_transmit(msSpi, &sTransaction));

    // Recalculate number of remaining bits
    u32numOfReminingBits -= u16bitsTx;
  }

  return eErrCode;
}

esp_err_t pcd8544getActualRow(uint16_t u16frameBufferAbsIdx,
                              uint8_t *pu8rowIdx) {
  // Pointer should not be empty
  assert(pu8rowIdx);

  // Calculated row index
  uint8_t u8rowIdx;

  if (u16frameBufferAbsIdx >= sizeof(mau8frameBuff)) {
    return ESP_ERR_INVALID_ARG;
  }

  for (u8rowIdx = 0; u8rowIdx < PCD8544_LINES; u8rowIdx++) {
    if (u16frameBufferAbsIdx < ((u8rowIdx + 1) * PCD8544_X_RES)) {
      break;
    }
  }

  *pu8rowIdx = u8rowIdx;

  return ESP_OK;
}

inline uint8_t pcd8544swapBitsInByte(uint8_t u8data) {
  u8data = (u8data & 0xF0F0) >> 4 | (u8data & 0x0F0F) << 4;
  u8data = (u8data & 0xCCCC) >> 2 | (u8data & 0x3333) << 2;
  u8data = (u8data & 0xAAAA) >> 1 | (u8data & 0x5555) << 1;

  return u8data;
}

// ==========================| Internal functions |===========================
esp_err_t _spiInit(void) {
  esp_err_t eErrCode = ESP_FAIL;

  if (mpsConfig.spi_pin == NULL) {
    mpsConfig.spi_pin =
        (void *)pcd8544_native_spi_pin_config(mpsConfig.spi_host);
  }

  spi_bus_config_t sBuscfg = {.mosi_io_num = mpsConfig.spi_pin->mosi_io_num,
                              .miso_io_num = mpsConfig.spi_pin->miso_io_num,
                              .sclk_io_num = mpsConfig.spi_pin->sclk_io_num,
                              .quadwp_io_num = -1,
                              .quadhd_io_num = -1};

  spi_device_interface_config_t sDevcfg = {
      .clock_speed_hz = 4 * 1000 * 1000,
      .mode = 0,
      .spics_io_num = mpsConfig.spi_pin->spics_io_num,
      .queue_size = 3,
      .pre_cb = 0,
  };

  ESP_LOGI(TAG, "SPI pins: Din=IO%d, Clk=IO%d, CE=IO%d",
           mpsConfig.spi_pin->mosi_io_num, mpsConfig.spi_pin->sclk_io_num,
           mpsConfig.spi_pin->spics_io_num);

  _PCD8544_RET_WHEN_ERR(spi_bus_initialize(mpsConfig.spi_host, &sBuscfg, 0));
  _PCD8544_RET_WHEN_ERR(
      spi_bus_add_device(mpsConfig.spi_host, &sDevcfg, &msSpi));

  return eErrCode;
}

esp_err_t _reset() {
  esp_err_t eErrCode;

  _PCD8544_RET_WHEN_ERR(gpio_set_level(mpsConfig.control_pin->reset_io_num, 1));
  _PCD8544_RET_WHEN_ERR(gpio_set_level(mpsConfig.control_pin->reset_io_num, 0));
  // Need super short delay -> set GPIO again
  _PCD8544_RET_WHEN_ERR(gpio_set_level(mpsConfig.control_pin->reset_io_num, 0));
  _PCD8544_RET_WHEN_ERR(gpio_set_level(mpsConfig.control_pin->reset_io_num, 0));
  _PCD8544_RET_WHEN_ERR(gpio_set_level(mpsConfig.control_pin->reset_io_num, 0));

  _PCD8544_RET_WHEN_ERR(gpio_set_level(mpsConfig.control_pin->reset_io_num, 1));

  return eErrCode;
}

static esp_err_t _finalizeFrameBuf(void) {
  esp_err_t eErrCode = ESP_FAIL;

  // Set position to X=0, row=0
  _PCD8544_RET_WHEN_ERR(pcd8544setPos(0, 0));

  return (pcd8544sendRawData(mau8frameBuff, sizeof(mau8frameBuff),
                             PCD8544_DC_DATA));
}

static esp_err_t _checkXpos(const uint8_t u8xPos) {
  if (u8xPos >= PCD8544_X_RES) {
    ESP_LOGE(TAG, "%s X position over limit (%d >= %d)", __func__, u8xPos,
             PCD8544_X_RES);
    return ESP_ERR_INVALID_ARG;
  } else {
    return ESP_OK;
  }
}

static esp_err_t _checkYpos(const uint8_t u8yPos) {
  if (u8yPos >= PCD8544_Y_RES) {
    ESP_LOGE(TAG, "%s Y position over limit (%d >= %d)", __func__, u8yPos,
             PCD8544_Y_RES);
    return ESP_ERR_INVALID_ARG;
  } else {
    return ESP_OK;
  }
}

static esp_err_t _checkRow(const uint8_t u8row) {
  if (u8row >= (PCD8544_Y_RES / 8)) {
    ESP_LOGE(TAG, "%s Line number over limit (%d >= %d)", __func__, u8row,
             PCD8544_Y_RES / 8);
    return ESP_ERR_INVALID_ARG;
  } else {
    return ESP_OK;
  }
}

static esp_err_t _checkWriteMode(const tePcd8544writeMode eMode) {
  if ((eMode != WRITE_SET) && (eMode != WRITE_CLR)) {
    ESP_LOGE(TAG, "%s Unknown mode: %d", __func__, eMode);
    return ESP_ERR_INVALID_ARG;
  } else {
    return ESP_OK;
  }
}
