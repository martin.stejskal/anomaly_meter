/**
 * @file
 * @author Martin Stejskal
 * @brief Battery manager module
 *
 * Periodically read battery status and record it
 */
// ===============================| Includes |================================
#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <string.h>

// ESP specific
#include <driver/adc.h>
#include <driver/gpio.h>
#include <esp_log.h>

// Local
#include "battery.h"
#include "cfg.h"
#include "err_handlers.h"
#include "math_utils.h"
#include "nvs_settings.h"
// ================================| Defines |================================
/**
 * @brief Value 400 is approximately 3.5 V on USB input
 *
 * Battery voltage. ADC is 10 bit. Reference is 1.1V.
 * Vcc - 8k2 - ADC - 2k2 - GND
 */
#define _CHARGE_THRESHOLD (400)

/**
 * @brief NVS ID for different variables
 *
 * @{
 */
#define _NVS_STATUS_DATA_ID "BattStatusD"
#define _NVS_STATUS_IDX_ID "BattStatusI"
#define _NVS_STATUS_UPTIME_ID "BattStatusT"
#define _NVS_STATUS_PERC_ID "BattStatusP"
#define _NVS_CALIB_ID "BattCalib"
#define _NVS_STATIC_OFFSET "BattStatOff"
#define _NVS_INITIAL_ESTIMATION_ID "BattInitE"
/**
 * @}
 */

/**
 * @brief Tells when battery measurement is still not accurate enough
 *
 * When battery is not discharged enough, any estimations will be quite
 * inaccurate
 */
#define _INACCURATE_WHEN_BATT_OVER_PERCENTAGE (80)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
/**
 * @brief Thresholds for battery capacity estimation
 *
 * Values based on
 * https://www.richtek.com/Design%20Support/Technical%20Document/AN024
 *
 */
typedef enum {
  // Vbatt = 4.20 V ; Ideal value on ADC: 827
  BATTERY_PERC_100 = 827,  //!< BATTERY_PERC_100

  // Vbatt = 4.1 V ; Ideal value on ADC: 807
  BATTERY_PERC_90 = 807,  //!< BATTERY_PERC_90

  // Vbatt = 4.0 V ; Ideal value on ADC: 787
  BATTERY_PERC_80 = 787,  //!< BATTERY_PERC_80

  // Vbatt = 3.94 V ; Ideal value on ADC: 775
  BATTERY_PERC_70 = 775,  //!< BATTERY_PERC_70

  // Vbatt = 3.82 V ; Ideal value on ADC: 752
  BATTERY_PERC_60 = 752,  //!< BATTERY_PERC_60

  // Vbatt = 3.76 V ; Ideal value on ADC: 740
  BATTERY_PERC_50 = 740,  //!< BATTERY_PERC_50

  // Vbatt = 3.71 V ; Ideal value on ADC: 734
  BATTERY_PERC_40 = 734,  //!< BATTERY_PERC_40

  // Vbatt = 3.71 V ; Ideal value on ADC: 730
  BATTERY_PERC_30 = 730,  //!< BATTERY_PERC_30

  // Vbatt = 3.69 V ; Ideal value on ADC: 726
  BATTERY_PERC_20 = 726,  //!< BATTERY_PERC_20

  // Vbatt = 3.67 V ; Ideal value on ADC: 722
  BATTERY_PERC_10 = 722,  //!< BATTERY_PERC_10

  // Vbatt = 3.65 V ; Ideal value on ADC: 718
  BATTERY_PERC_0 = 718,  //!< BATTERY_PERC_0

  // Approximations. These just allow to calculate estimation bit more
  // accurately
  BATTERY_PERC_95 =                                         //!< BATTERY_PERC_95
  (BATTERY_PERC_100 + BATTERY_PERC_90) / 2,                 //!< BATTERY_PERC_95
  BATTERY_PERC_85 =                                         //!< BATTERY_PERC_85
  (BATTERY_PERC_90 + BATTERY_PERC_80) / 2,                  //!< BATTERY_PERC_85
  BATTERY_PERC_75 =                                         //!< BATTERY_PERC_75
  (BATTERY_PERC_80 + BATTERY_PERC_70) / 2,                  //!< BATTERY_PERC_75
  BATTERY_PERC_65 =                                         //!< BATTERY_PERC_65
  (BATTERY_PERC_70 + BATTERY_PERC_60) / 2,                  //!< BATTERY_PERC_65
  BATTERY_PERC_55 =                                         //!< BATTERY_PERC_55
  (BATTERY_PERC_60 + BATTERY_PERC_50) / 2,                  //!< BATTERY_PERC_55
  BATTERY_PERC_45 =                                         //!< BATTERY_PERC_45
  (BATTERY_PERC_50 + BATTERY_PERC_40) / 2,                  //!< BATTERY_PERC_45
  BATTERY_PERC_35 =                                         //!< BATTERY_PERC_35
  (BATTERY_PERC_40 + BATTERY_PERC_30) / 2,                  //!< BATTERY_PERC_35
  BATTERY_PERC_25 =                                         //!< BATTERY_PERC_25
  (BATTERY_PERC_30 + BATTERY_PERC_20) / 2,                  //!< BATTERY_PERC_25
  BATTERY_PERC_15 =                                         //!< BATTERY_PERC_15
  (BATTERY_PERC_20 + BATTERY_PERC_10) / 2,                  //!< BATTERY_PERC_15
  BATTERY_PERC_5 = (BATTERY_PERC_10 + BATTERY_PERC_0) / 2,  //!< BATTERY_PERC_5
} teBattPercentage;

typedef struct {
  /**
   * @brief Store measured data under one hood
   */
  uint16_t au16measuredData[BATT_BUFFER_SIZE_ITEMS];

  /**
   * @brief Store default data in static part of RAM
   *
   * In non-RTOS it could be used malloc, but when RTOS is used, that means
   * that a lot of memory have to be allocated but used only once. And that
   * will have direct impact on performance. Therefore for RTOS on embedded
   * is better static variable.
   */
  uint16_t au16defaultData[BATT_BUFFER_SIZE_ITEMS];

  struct {
    /**
     * @brief Calibration data for ADC
     *
     * In order to show battery voltage more precisely, calibration data are
     * required. Here is list of measured values for every battery level
     */
    uint16_t au16real[BATTERY_CALIB_MAX];

    /**
     * @brief Correction coefficients
     *
     * If calibration data are correct, offset and multiplication coefficients
     * have to be calculated. Then it is possible to compensate error on ADC.
     *
     * These coefficients are used for ranges -> actually there is required to
     * keep one coefficient extra.
     * @{
     */
    int16_t i16offset[BATTERY_CALIB_MAX + 1];
    int32_t i32multiplier[BATTERY_CALIB_MAX + 1];
    /**
     * @}
     */

    /**
     * @brief This is list of ideal values on ADC for every voltage level
     */
    const uint16_t au16ideal[BATTERY_CALIB_MAX];

    /**
     * @brief Static offset for output as raw value
     *
     * When calibrating, there can be still some voltage drop on wires. This
     * value basically allow to add one more offset on the top
     */
    int8_t i8_static_offset;

  } calib;

  struct {
    /**
     * @brief Ring buffer settings
     */
    tsRingBuffer sRingBuff;

    /**
     * @brief Up time for last power cycle in seconds
     *
     * This value is basically value which is loaded from NVS after power on.
     * Later on it can be reset if charging cycle is finished.
     */
    uint32_t u32prevPwrOnUptimeSec;

    /**
     * @brief Tells how many percent of battery was used
     *
     * This is additive value. It means it can be bigger than 100 in case
     * that battery is charged. Example: 100% -> 20%. Burned 80%. Then charged
     * to 70%. Then drops to 30% - burns another 40%. Total will be 120%.
     * But it is also necessary to calculate with time.
     */
    uint32_t u32totalDischargePercentage;

    /**
     * @brief After power on, battery percentage is stored here
     *
     * This is used as reference later on, to figure out how much was battery
     * drained during this power cycle and therefore calculate estimation
     * from time and voltage drop.
     */
    uint8_t u8powerOnBatteryPercentage;
  } status;

} tsRuntime;

typedef struct {
  /**
   * @brief Timestamp when battery was fully charged during this power cycle
   *
   * Need to know if charging cycle was finished at this power cycle or some
   * previous. This is necessary due to estimation correct up time
   */
  uint32_t u32fullyChargedTimestampSec;

  /**
   * @brief Tells how many percent battery gained when charging
   */
  uint8_t u8percCharging;
} tsStatusMetadata;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "BATT";

/**
 * @brief Runtime variables
 *
 * @note Need to fill in "ideal" values for ADC for given voltage range
 *       (3.6V ~ 4.3V). ADC is 10 bit. Reference is 1.1V.
 *       Vcc - 8k2 - ADC - 2k2 - GND
 */
static tsRuntime msRuntime = {
    .calib.au16ideal = {708, 728, 748, 768, 787, 807, 827, 846},

    // Data from ADC are 16 bit long -> 2 Bytes
    .status.sRingBuff.u8itemSizeBytes = sizeof(uint16_t),
    // Every item consume 2 Bytes (as mentioned above). Simply multiply by
    // number of required items in buffer
    .status.sRingBuff.u16bufferSizeBytes =
        sizeof(uint16_t) * BATT_BUFFER_SIZE_ITEMS};

// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static bool _isCharging(void);
static uint32_t _calcUptimeOnBattery(tsBattStatus *psStatus,
                                     tsStatusMetadata *psMetadata);
static uint32_t _calcLifeEstimation(tsBattStatus *psStatus,
                                    tsStatusMetadata *psMetadata);
static uint32_t _getFullyChargedTimestamp(tsBattStatus *psStatus);
static uint32_t _howLongCharging(tsBattStatus *psStatus);
static uint8_t _howManyPercCharging(tsBattStatus *psStatus);
static uint8_t _howManyPercDischarged(tsBattStatus *psStatus);

static void _updateInitialEstimateIfNeeded(tsBattStatus *psStatus);

static esp_err_t _adcInit(void);
static uint16_t _adcGetRawAverage(uint16_t u16numOfReadings);
static uint32_t _getNextWriteRingBuffToNvsTimeMs(void);
static teNvsSttngsErr _saveRingBufferToNvs(tsRingBuffer *psRingBuffRaw);
static teNvsSttngsErr _loadRingBufferFromNvs(tsRingBuffer *psRingBuffRaw);
static bool _areCalibrationDataValid(void);
static void _calculateCalibCoef(void);
// =========================| High level functions |==========================
void batteryTaskRTOS(void *pvParameters) {
  // Setup ADC. If fails, exit task
  if (_adcInit()) {
    vTaskDelete(NULL);
    return;
  }

  // Set new time when data should be write to NVS. Note that write should
  // not be done immediately in first "round" due to possible reboot loop.
  // That would cause intensive writing to NVS and later on failure.
  // Therefore even first write is postponed
  uint32_t u32writeToNvsMs = _getNextWriteRingBuffToNvsTimeMs();

  // Initialize buffer for storing measured values
  msRuntime.status.sRingBuff.pvBuffer = msRuntime.au16measuredData;

  // Status structure for battery. Only for this instance
  tsBattStatus sStatus;

  ESP_LOGI(TAG, "Battery manager started");

  // Load data from NVS
  batteryForceReloadValuesFromNvs();

  // infinite loop - log only battery voltage
  while (1) {
    // Read battery status
    batteryGetStatus(&sStatus);

    ringBuffPush(sStatus.psRingBuffRaw, &sStatus.u16rawAdc);

    ESP_LOGI(TAG, "Battery voltage raw: %d | %.3fV | Chrg: %d",
             sStatus.u16rawAdc, sStatus.fVoltage, sStatus.bCharging);

    // If there is time to backup data to NVS
    if (GET_CLK_MS_32BIT() >= u32writeToNvsMs) {
      // Store data
      _saveRingBufferToNvs(sStatus.psRingBuffRaw);
      nvsSettingsSet(_NVS_STATUS_UPTIME_ID, &sStatus.u32runOnBatterySec,
                     sizeof(sStatus.u32runOnBatterySec));
      nvsSettingsSet(_NVS_STATUS_PERC_ID,
                     &msRuntime.status.u32totalDischargePercentage,
                     sizeof(msRuntime.status.u32totalDischargePercentage));

      // Calculate new timeout
      u32writeToNvsMs = _getNextWriteRingBuffToNvsTimeMs();
    }

    vTaskDelay((BATT_DIAG_PERIOD_S * 1000) / portTICK_PERIOD_MS);
  }
}

void batteryGetStatus(tsBattStatus *psStatus) {
  assert(psStatus);

  // Some additional data required for calculating how long is device running
  // on battery
  static tsStatusMetadata sStatusMetadata;

  // Load current time. Will be re-used multiple times later on
  psStatus->u32timestampSec = GET_CLK_MS_32BIT() / 1000;

  // Load data from ADC
  psStatus->bCharging = _isCharging();
  psStatus->u16rawAdc =
      batteryGetCurrentValueFromAdc(BATT_DEFAULT_NUM_OF_SAMPLES);

  // Give pointer to that ring buffer
  psStatus->psRingBuffRaw = &msRuntime.status.sRingBuff;

  // Do conversions
  psStatus->u8percentage = batteryRawToPercentage(psStatus->u16rawAdc);
  psStatus->fVoltage = batteryRawToVoltage(psStatus->u16rawAdc);

  // Get timestamp of when was battery fully charged
  sStatusMetadata.u32fullyChargedTimestampSec =
      _getFullyChargedTimestamp(psStatus);

  // Get number of seconds which was device running on charger
  psStatus->u32howLongChargingSec = _howLongCharging(psStatus);

  sStatusMetadata.u8percCharging = _howManyPercCharging(psStatus);

  // Figure out how many percent was discharged since this power on cycle
  msRuntime.status.u32totalDischargePercentage +=
      _howManyPercDischarged(psStatus);

  // Figure out how long is device running on the battery (relative
  // variable)
  psStatus->u32runOnBatterySec =
      _calcUptimeOnBattery(psStatus, &sStatusMetadata);

  // Calculate battery life time estimation
  psStatus->u32batteryEstimateSec =
      _calcLifeEstimation(psStatus, &sStatusMetadata);

  // Print data for debug purposes
  ESP_LOGD(TAG, "Status. %d%% Chrg: %d OnBatt: %d OnChrg: %d ET: %d ",
           psStatus->u8percentage, psStatus->bCharging,
           psStatus->u32runOnBatterySec, psStatus->u32howLongChargingSec,
           psStatus->u32batteryEstimateSec);

  // Update initial battery estimation value if necessary
  _updateInitialEstimateIfNeeded(psStatus);
}

// ========================| Middle level functions |=========================

float batteryRawToVoltage(uint16_t u16rawValue) {
  // Battery voltage. ADC is 10 bit. Reference is 1.1V.
  // Vcc - 8k2 - ADC - 2k2 - GND
  float fBattVoltage = (float)u16rawValue;
  fBattVoltage = ((1.1 * (8200 + 2200)) * fBattVoltage) / (1024 * 2200);

  return fBattVoltage;
}

uint16_t battery_voltage_to_raw(float f_voltage) {
  // Battery voltage. ADC is 10 bit. Reference is 1.1V.
  // Vcc - 8k2 - ADC - 2k2 - GND
  // Convert to raw float and then convert to raw integer due to keeping
  // accuracy
  float f_raw = (f_voltage * 1024) / ((1.1 / 2200) * (8200 + 2200));

  // Check upper limit
  if ((float)f_raw > (float)0xFFFF) {
    f_raw = (float)0xFFFF;
  }

  return (uint16_t)f_raw;
}

uint8_t batteryRawToPercentage(uint16_t u16rawValue) {
  if (u16rawValue >= BATTERY_PERC_100) {
    return 100;
  } else if (u16rawValue >= BATTERY_PERC_95) {
    return 95;
  } else if (u16rawValue >= BATTERY_PERC_90) {
    return 90;
  } else if (u16rawValue >= BATTERY_PERC_85) {
    return 85;
  } else if (u16rawValue >= BATTERY_PERC_80) {
    return 80;
  } else if (u16rawValue >= BATTERY_PERC_75) {
    return 75;
  } else if (u16rawValue >= BATTERY_PERC_70) {
    return 70;
  } else if (u16rawValue >= BATTERY_PERC_65) {
    return 65;
  } else if (u16rawValue >= BATTERY_PERC_60) {
    return 60;
  } else if (u16rawValue >= BATTERY_PERC_55) {
    return 55;
  } else if (u16rawValue >= BATTERY_PERC_50) {
    return 50;
  } else if (u16rawValue >= BATTERY_PERC_45) {
    return 45;
  } else if (u16rawValue >= BATTERY_PERC_40) {
    return 40;
  } else if (u16rawValue >= BATTERY_PERC_35) {
    return 35;
  } else if (u16rawValue >= BATTERY_PERC_30) {
    return 30;
  } else if (u16rawValue >= BATTERY_PERC_25) {
    return 25;
  } else if (u16rawValue >= BATTERY_PERC_20) {
    return 20;
  } else if (u16rawValue >= BATTERY_PERC_15) {
    return 15;
  } else if (u16rawValue >= BATTERY_PERC_10) {
    return 10;
  } else if (u16rawValue >= BATTERY_PERC_5) {
    return 5;
  } else {
    return 0;
  }
}
// ==========================| Low level functions |==========================
tsRingBuffer *batteryGetRawAdcData(void) {
  return (&msRuntime.status.sRingBuff);
}

uint16_t batteryGetCurrentValueFromAdc(uint16_t u16numOfReadings) {
  uint16_t u16adcRaw = 0;

  // If not defined number of readings, use default
  if (u16numOfReadings == 0) {
    u16numOfReadings = BATT_NUM_OF_SAMPLES_PER_MEASUREMENT;
  }

  u16adcRaw = _adcGetRawAverage(u16numOfReadings);

  // If calibration data are set
  if (_areCalibrationDataValid()) {
    /* Calibration data should be used
     *
     * Simple linear approximation will be used. Simply there are few operating
     * ranges:
     *   * 0 ~ 3.6V
     *   * 3.6 V ~ 3.7V
     *   * ...
     *   * 4.3V ~ above
     *
     * According to the current ADC value select appropriate range and process
     * it
     */
    bool bValueCorrected = false;

    // Range 0 ~ 4.3 V
    for (teBattCalibPoints eCalibPoints = (teBattCalibPoints)0;
         eCalibPoints < BATTERY_CALIB_MAX; eCalibPoints++) {
      // If value fall into given range, recalculate value from ADC
      if (u16adcRaw < msRuntime.calib.au16real[eCalibPoints]) {
        u16adcRaw = mu_linear_approx_get_corrected_value_u16(
            u16adcRaw, msRuntime.calib.i16offset[eCalibPoints],
            msRuntime.calib.i32multiplier[eCalibPoints]);
        bValueCorrected = true;
        break;
      }
    }

    // Range 4.3 V and above
    if (!bValueCorrected) {
      // Last option
      u16adcRaw = mu_linear_approx_get_corrected_value_u16(
          u16adcRaw, msRuntime.calib.i16offset[BATTERY_CALIB_MAX],
          msRuntime.calib.i32multiplier[BATTERY_CALIB_MAX]);
    }
  } else {
    // Calibration data are not available - nothing more to do
  }

  assert(u16adcRaw < 0xFFFF);

  // Apply static offset
  // Need to check if offset will not cause value overflow
  int32_t i32_adc_raw =
      (int32_t)u16adcRaw + (int32_t)msRuntime.calib.i8_static_offset;

  if (i32_adc_raw > 0xFFFF) {
    i32_adc_raw = 0xFFFF;
  }

  u16adcRaw = (uint16_t)i32_adc_raw;

  return (0xFFFF & u16adcRaw);
}

esp_err_t batteryCalibrateAdc(teBattCalibPoints eCalibrationType) {
  if (eCalibrationType >= BATTERY_CALIB_MAX) {
    ESP_LOGE(TAG, "Can not calibrate. Invalid argument");
    return ESP_ERR_INVALID_ARG;
  }

  ESP_LOGI(TAG, "Calibrating %s",
           batteryGetCalibrationTypeName(eCalibrationType));

  // Value of enumeration should be higher than maximum index in calib.au16adc
  assert(eCalibrationType <= ((sizeof(msRuntime.calib.au16real) /
                               sizeof(msRuntime.calib.au16real[0])) -
                              1));

  // Get value and write it to array. In case of calibration, make tons of
  // measurement
  msRuntime.calib.au16real[eCalibrationType] =
      _adcGetRawAverage(BATT_NUM_OF_SAMPLES_WHEN_CALIBRATING);

  // Recalculate coefficients, so values will be applied immediately
  _calculateCalibCoef();

  // Store settings to NVS
  return nvsSettingsSet(_NVS_CALIB_ID, &msRuntime.calib.au16real,
                        sizeof(msRuntime.calib.au16real));
}

int8_t battery_get_static_offset_mv(void) {
  // Recalculate raw offset to mV
  bool b_is_negative = false;
  int8_t i8_raw_value = msRuntime.calib.i8_static_offset;

  if (i8_raw_value < 0) {
    b_is_negative = true;
    // Get absolute value (need for correct conversion)
    i8_raw_value *= -1;
  }

  // We expect mV range -> x 1000
  float f_voltage_mv = batteryRawToVoltage(i8_raw_value) * 1000;

  // Limit of 8 bit signed number
  if (f_voltage_mv > 127) {
    f_voltage_mv = 127;
  }

  int8_t i8_offset_mv;
  if (b_is_negative) {
    i8_offset_mv = -1 * (int8_t)f_voltage_mv;
  } else {
    // Positive number
    i8_offset_mv = (int8_t)f_voltage_mv;
  }

  return i8_offset_mv;
}

esp_err_t battery_set_static_offset_mv(int8_t i8_static_offset_mv) {
  bool b_is_negative = false;

  // Need to work with absolute value, but later on sign will be needed again
  if (i8_static_offset_mv < 0) {
    b_is_negative = true;
    i8_static_offset_mv *= -1;
  }

  // Value is in mV, but it should be converted to V -> divided by 1000
  uint16_t u16_abs_offset_raw =
      battery_voltage_to_raw(((float)i8_static_offset_mv) / 1000);

  // Have to fit into 8 bit signed value
  assert(u16_abs_offset_raw < 127);

  if (b_is_negative) {
    msRuntime.calib.i8_static_offset = -1 * (int8_t)u16_abs_offset_raw;
  } else {
    msRuntime.calib.i8_static_offset = (int8_t)u16_abs_offset_raw;
  }

  // Store value also in NVS, so it will not be forgotten
  return nvsSettingsSet(_NVS_STATIC_OFFSET, &msRuntime.calib.i8_static_offset,
                        sizeof(msRuntime.calib.i8_static_offset));
}

const char *batteryGetCalibrationTypeName(teBattCalibPoints eCalibrationType) {
  const static char *pac36V = "3.6 V";
  const static char *pac37V = "3.7 V";
  const static char *pac38V = "3.8 V";
  const static char *pac39V = "3.9 V";
  const static char *pac40V = "4.0 V";
  const static char *pac41V = "4.1 V";
  const static char *pac42V = "4.2 V";
  const static char *pac43V = "4.3 V";
  const static char *pacMax = "MAX";

  switch (eCalibrationType) {
    case BATTERY_CALIB_3_6V:
      return pac36V;
    case BATTERY_CALIB_3_7V:
      return pac37V;
    case BATTERY_CALIB_3_8V:
      return pac38V;
    case BATTERY_CALIB_3_9V:
      return pac39V;
    case BATTERY_CALIB_4_0V:
      return pac40V;
    case BATTERY_CALIB_4_1V:
      return pac41V;
    case BATTERY_CALIB_4_2V:
      return pac42V;
    case BATTERY_CALIB_4_3V:
      return pac43V;
    case BATTERY_CALIB_MAX:
      return pacMax;
  }

  // This should not happen
  return "ERROR";
}

void batteryGetCalibrationRaw(
    uint16_t au16calibData[static BATTERY_CALIB_MAX]) {
  // Default value are just zeros - no need to explicitly define from upper
  // layer
  uint16_t au16defaultCalibration[BATTERY_CALIB_MAX] = {0};

  nvsSettingsGet(_NVS_CALIB_ID, au16calibData, au16defaultCalibration,
                 sizeof(au16defaultCalibration));
}

esp_err_t batteryOverwriteCalibrationRaw(
    uint16_t au16calibData[static BATTERY_CALIB_MAX]) {
  teNvsSttngsErr eErrCode = nvsSettingsSet(
      _NVS_CALIB_ID, au16calibData, sizeof(uint16_t) * BATTERY_CALIB_MAX);

  if (eErrCode) {
    // Writing failed -> abort
    return ESP_FAIL;
  }

  // Else recalculate coefficients and copy data to current runtime variables
  memcpy(msRuntime.calib.au16real, au16calibData,
         sizeof(msRuntime.calib.au16real));

  // Recalculate coefficients, so values will be applied immediately
  _calculateCalibCoef();

  return ESP_OK;
}

void batteryForceReloadValuesFromNvs(void) {
  // Load battery status related things
  _loadRingBufferFromNvs(&msRuntime.status.sRingBuff);

  uint32_t u32defaultPowerOnTime = 0;
  nvsSettingsGet(_NVS_STATUS_UPTIME_ID, &msRuntime.status.u32prevPwrOnUptimeSec,
                 &u32defaultPowerOnTime,
                 sizeof(msRuntime.status.u32prevPwrOnUptimeSec));

  uint32_t u32defaultTotalDischarge = 0;
  nvsSettingsGet(_NVS_STATUS_PERC_ID,
                 &msRuntime.status.u32totalDischargePercentage,
                 &u32defaultTotalDischarge,
                 sizeof(msRuntime.status.u32totalDischargePercentage));

  // Default value are just zeros - no need to explicitly define from upper
  // layer
  uint16_t au16defaultCalibration[BATTERY_CALIB_MAX] = {0};
  nvsSettingsGet(_NVS_CALIB_ID, msRuntime.calib.au16real,
                 au16defaultCalibration, sizeof(msRuntime.calib.au16real));

  // Additional raw offset value. Actually this value can be zero (optional
  // calibration information)
  int8_t i8_static_offset = 0;
  nvsSettingsGet(_NVS_STATIC_OFFSET, &msRuntime.calib.i8_static_offset,
                 &i8_static_offset, sizeof(msRuntime.calib.i8_static_offset));

  // If calibration data are valid, calculate correction coefficients
  if (_areCalibrationDataValid()) {
    _calculateCalibCoef();
  }

  // Get last stored value from ring buffer, so we can estimate if battery
  // was charged to 100% while device was off
  uint16_t u16adcRawPrevious;
  ringBuffPullLatest(&msRuntime.status.sRingBuff, &u16adcRawPrevious);

  // If data in NVS are empty, then there will be nothing to display. This
  // might be bit confusing -> do at least one measurement, to have something
  // in buffer
  // Raw value from ADC
  uint16_t u16adcRaw =
      batteryGetCurrentValueFromAdc(BATT_DEFAULT_NUM_OF_SAMPLES);

  ringBuffPush(&msRuntime.status.sRingBuff, &u16adcRaw);

  // If battery is charged now, but previously was not, this signalize that
  // battery was charged while device was off -> reset power on up time counter
  if ((u16adcRaw >= BATTERY_PERC_100) &&
      (u16adcRawPrevious < BATTERY_PERC_90)) {
    msRuntime.status.u32prevPwrOnUptimeSec = 0;
    msRuntime.status.u32totalDischargePercentage = 0;
  }

  // Store early power-on battery status
  msRuntime.status.u8powerOnBatteryPercentage =
      batteryRawToPercentage(u16adcRaw);
}

esp_err_t batteryDeleteStatisticData(void) {
  teNvsSttngsErr eErrCode = nvsSettingsDel(_NVS_STATUS_DATA_ID);

  eErrCode |= nvsSettingsDel(_NVS_STATUS_IDX_ID);
  eErrCode |= nvsSettingsDel(_NVS_STATUS_UPTIME_ID);
  eErrCode |= nvsSettingsDel(_NVS_STATUS_PERC_ID);
  eErrCode |= nvsSettingsDel(_NVS_INITIAL_ESTIMATION_ID);

  // The _NVS_CALIB_ID should not be deleted. That would require repeat whole
  // calibration process -> super annoying.

  if (eErrCode) {
    return ESP_FAIL;
  } else {
    return ESP_OK;
  }
}
// ==========================| Internal functions |===========================
static bool _isCharging(void) {
  // Returned value
  bool bCharging = false;

  int iAdcRaw = adc1_get_raw((adc1_channel_t)IO_BATT_ADC1_EXT_PWR);

  if (iAdcRaw > _CHARGE_THRESHOLD) {
    // Charging
    bCharging = true;
  } else {
    // Not charging
    bCharging = false;
  }

  return bCharging;
}

static uint32_t _calcUptimeOnBattery(tsBattStatus *psStatus,
                                     tsStatusMetadata *psMetadata) {
  // This does not store total time when running on battery. It is only
  // intermediate variable that increase only when charger is plugged.
  static uint32_t u32onBatteryAccTimeSec = 0;

  // Keep track when was the last time when device was unplugged from charger
  static uint32_t u32lastSwitchToBattery = 0;

  // Keep status of previous charging state
  static bool bPrevCharging = false;

  assert(psStatus);
  assert(psMetadata);

  // If just finished charging
  if (psStatus->u32timestampSec == psMetadata->u32fullyChargedTimestampSec) {
    // Current time running on the battery is 0
    msRuntime.status.u32prevPwrOnUptimeSec = 0;
    msRuntime.status.u32totalDischargePercentage = 0;
    msRuntime.status.u8powerOnBatteryPercentage = psStatus->u8percentage;

    // Also reset accumulator
    u32onBatteryAccTimeSec = 0;
  }

  // Note: If in this power cycle is device connected to charger since power
  // on, it will cause that u32absBattTimeThisPwrCycleSec will be 1 or 2
  // seconds, depends when this function will be executed. Anyway, this
  // deviation is negligible.

  // Total up time which is required
  uint32_t u32uptimeSec;

  // Calculate absolute time on battery
  uint32_t u32absBattTimeThisPwrCycleSec;

  if (bPrevCharging != psStatus->bCharging) {
    // Charging started or stopped
    if (psStatus->bCharging) {
      // Started charging
      // Update accumulator. Current time - last known switch to battery
      u32onBatteryAccTimeSec +=
          (psStatus->u32timestampSec - u32lastSwitchToBattery);

    } else {
      // Stop charging
      // Note when switched back to battery
      u32lastSwitchToBattery = psStatus->u32timestampSec;
    }

    // Either way, current up time for this power cycle is in the accumulator
    u32absBattTimeThisPwrCycleSec = u32onBatteryAccTimeSec;

  } else {
    // Keep charging or discharging
    if (psStatus->bCharging) {
      // Just use what is in the accumulator
      u32absBattTimeThisPwrCycleSec = u32onBatteryAccTimeSec;
    } else {
      // Discharging
      // Accumulator value is not updated yet, so use whatever is there and
      // calculate "how long is running on battery since last charging"
      u32absBattTimeThisPwrCycleSec =
          u32onBatteryAccTimeSec +
          (psStatus->u32timestampSec - u32lastSwitchToBattery);
    }
  }

  // Update "previous" state
  bPrevCharging = psStatus->bCharging;

  // Simplified situation. Add up time from previous power cycles
  u32uptimeSec =
      u32absBattTimeThisPwrCycleSec + msRuntime.status.u32prevPwrOnUptimeSec;

  return u32uptimeSec;
}

static uint32_t _calcLifeEstimation(tsBattStatus *psStatus,
                                    tsStatusMetadata *psMetadata) {
  static bool bFirstRun = true;

  static uint32_t
      u32prevBattLifeEstimation[BATT_ESTIMATION_AVERAGE_NUM_OF_SAMPLES];

  static tsRingBuffer sRingBuff = {
      .pvBuffer = &u32prevBattLifeEstimation,
      .u16bufferSizeBytes = sizeof(u32prevBattLifeEstimation),
      .u8itemSizeBytes = sizeof(u32prevBattLifeEstimation[0]),
      .u16readIdx = 0,
      .u16writeIdx = 0,
  };

  uint32_t u32battLifeEstimation;

  // Load initial estimation - might be used later on
  uint32_t u32initialEstimationSec;
  uint32_t u32defaultEstimation = BATT_INITIAL_ESTIMATE_S;

  nvsSettingsGet(_NVS_INITIAL_ESTIMATION_ID, &u32initialEstimationSec,
                 &u32defaultEstimation, sizeof(u32initialEstimationSec));

  // Handle exception
  if (psStatus->u8percentage == 100) {
    // Can not divide by 0. Also there is not much from what we can estimate,
    // since battery is fully charged. Return initial estimate.
    u32battLifeEstimation = u32initialEstimationSec;

    // Need to distribute value through all records
    ringBuffSet(&sRingBuff, &u32battLifeEstimation);
  } else {
    u32battLifeEstimation =
        (psStatus->u32runOnBatterySec * psStatus->u8percentage) /
        (100 - psStatus->u8percentage);

    // If battery is not discharged enough, even with average estimation
    // will be really rough. In that case it is needed to take calculated
    // estimation more like correction and base estimation on initial value
    if (psStatus->u8percentage >= _INACCURATE_WHEN_BATT_OVER_PERCENTAGE) {
      // If battery status is 95%, only 5% of it's value will be used as
      // correction
      uint32_t u32correction =
          (u32battLifeEstimation * (100 - psStatus->u8percentage)) / 100;

      // Estimate battery voltage as proportionally decreasing with initial
      // estimate. Then add correction
      u32battLifeEstimation =
          (u32initialEstimationSec * psStatus->u8percentage) / 100;

      u32battLifeEstimation += u32correction;
    }

    // Calculate life time estimation as average. Only 1 exception is there.
    // If this is very first run, calculated value have to be copied through
    // whole ring buffer.
    if (bFirstRun) {
      bFirstRun = false;
      // Need to distribute value through all records
      ringBuffSet(&sRingBuff, &u32battLifeEstimation);
    } else {
      // Calculate average value

      // Push new value to buffer
      ringBuffPush(&sRingBuff, &u32battLifeEstimation);

      u32battLifeEstimation = 0;

      uint32_t u32prevValueFromBuffer;

      for (uint16_t u16itemCnt = 0;
           u16itemCnt < BATT_ESTIMATION_AVERAGE_NUM_OF_SAMPLES; u16itemCnt++) {
        ringBuffPull(&sRingBuff, &u32prevValueFromBuffer);

        u32battLifeEstimation += u32prevValueFromBuffer;
      }

      u32battLifeEstimation /= BATT_ESTIMATION_AVERAGE_NUM_OF_SAMPLES;
    }
  }

  return u32battLifeEstimation;
}

static uint32_t _getFullyChargedTimestamp(tsBattStatus *psStatus) {
  static uint32_t u32fullyChargedTimeSec = 0;

  if (psStatus->bCharging && (psStatus->u8percentage == 100)) {
    // Current time is stored in "timestamp"
    u32fullyChargedTimeSec = psStatus->u32timestampSec;
  } else {
    // Keep current/last value
  }

  return u32fullyChargedTimeSec;
}

static uint32_t _howLongCharging(tsBattStatus *psStatus) {
  // Register when charging was initiated
  static uint32_t u32chargingStartSec = 0;

  // Keep status of previous charging state
  static bool bPrevCharging = false;

  uint32_t u32howLongChargingSec = 0;

  if (bPrevCharging != psStatus->bCharging) {
    // Charging started or stopped
    if (psStatus->bCharging) {
      // Started charging
      u32chargingStartSec = psStatus->u32timestampSec;

      // We just started -> zero
      u32howLongChargingSec = 0;
    } else {
      // Stop charging. Calculate as normal, but reset "start time" (due to
      // easier debugging)
      assert(u32chargingStartSec <= psStatus->u32timestampSec);
      u32howLongChargingSec = psStatus->u32timestampSec - u32chargingStartSec;

      u32chargingStartSec = 0;
    }
  } else {
    // Keep charging or discharging
    if (psStatus->bCharging) {
      assert(u32chargingStartSec <= psStatus->u32timestampSec);

      u32howLongChargingSec = psStatus->u32timestampSec - u32chargingStartSec;
    } else {
      // Discharging -> zero charging time
      u32howLongChargingSec = 0;
    }
  }

  bPrevCharging = psStatus->bCharging;

  return u32howLongChargingSec;
}

static uint8_t _howManyPercCharging(tsBattStatus *psStatus) {
  static uint8_t u8perctNotCharging = 100;

  uint8_t u8percCharging = 0;

  if (psStatus->bCharging) {
    // Charging
    if (u8perctNotCharging == 100) {
      // It was already charged
      u8percCharging = 0;
    } else {
      u8percCharging = psStatus->u8percentage - u8perctNotCharging;
    }
  } else {
    // Not charging
    u8percCharging = 0;

    // Store how many percent have battery when not charging
    u8perctNotCharging = psStatus->u8percentage;
  }

  return u8percCharging;
}

static uint8_t _howManyPercDischarged(tsBattStatus *psStatus) {
  static bool bFirstRun = true;
  static uint8_t u8previousPercentage = 0;
  static bool bPreviousCharging = false;

  uint8_t u8recentPercDischarged = 0;

  if (bFirstRun) {
    bFirstRun = false;

    u8previousPercentage = msRuntime.status.u8powerOnBatteryPercentage;
    bPreviousCharging = psStatus->bCharging;
  }

  // If charging process was finished
  if (!psStatus->bCharging && bPreviousCharging) {
    u8previousPercentage = msRuntime.status.u8powerOnBatteryPercentage;
  }

  // Reports only if battery percentage drops at once. If there is some glitch
  // (ADC is on edge -> get 90%, 95%, 90%, 95%), then no drop have to be
  // reported until value is stable.
  if (psStatus->bCharging) {
    // Charging. Calculate discharge from last "remembered" percentage
    u8recentPercDischarged =
        msRuntime.status.u8powerOnBatteryPercentage - u8previousPercentage;
  } else {
    // Discharging
    if (psStatus->u8percentage < u8previousPercentage) {
      // Discharged a bit
      u8recentPercDischarged = u8previousPercentage - psStatus->u8percentage;

      u8previousPercentage = psStatus->u8percentage;
    } else {
      // Discharged, but hardly to recognize
      u8recentPercDischarged = 0;
    }
  }  // /discharging

  bPreviousCharging = psStatus->bCharging;

  return u8recentPercDischarged;
}

static void _updateInitialEstimateIfNeeded(tsBattStatus *psStatus) {
  assert(psStatus);

  // Tells when next write to NVS can be done
  static uint32_t u32nextUpdateToNvs = 0;
  static uint8_t u8previousPercentage = 0;

  if (psStatus->u8percentage > BATT_RECALIBRATE_INITIAL_ESTIMATE_BATT_PERC) {
    // Still charged too much
    return;
  } else if (psStatus->u8percentage <= 5) {
    // Battery is nearly dead -> better not save to NVS
    return;
  } else {
    // Battery is discharged "enough"

    // If this is first time when detected battery discharged "enough", mark
    // current battery percentage and calculate when it will be OK to write
    // data to NVS
    if (u8previousPercentage == 0) {
      u8previousPercentage = psStatus->u8percentage;
      u32nextUpdateToNvs = psStatus->u32timestampSec +
                           (BATT_RECALIBRATE_INITIAL_ESTIMATE_STABLE_FOR_SEC);
      return;
    }
  }

  // Timeout should be set
  assert(u32nextUpdateToNvs);

  // Battery percentage should not be zero
  assert(u8previousPercentage);

  if (psStatus->u32timestampSec < u32nextUpdateToNvs) {
    // Still not running long enough
    return;
  } else {
    // It is time to update initial estimation

    // Check battery percentage. If changed, we need to wait again
    if (psStatus->u8percentage != u8previousPercentage) {
      // Mark down when it will be fine to check again and update battery
      // level
      u8previousPercentage = psStatus->u8percentage;
      u32nextUpdateToNvs =
          psStatus->u32timestampSec +
          (BATT_RECALIBRATE_INITIAL_ESTIMATE_STABLE_FOR_SEC * 60);
      return;
    }

    // First calculate time for next write in order to avoid overwriting in
    // next call
    u32nextUpdateToNvs =
        psStatus->u32timestampSec +
        (BATT_RECALIBRATE_INITIAL_ESTIMATE_NVS_UPDATE_MIN * 60);

    // Now calculate estimation for 100% of battery -> need to consider
    // current percentage and proportionally increase estimated time
    uint32_t u32initialEstimation =
        (psStatus->u32batteryEstimateSec * 100) / psStatus->u8percentage;

    // Note - all status data should be printed to debug output before this
    // function, so not necessary to print them again
    ESP_LOGI(TAG, "Storing new initial battery estimation: %d s",
             u32initialEstimation);

    nvsSettingsSet(_NVS_INITIAL_ESTIMATION_ID, &u32initialEstimation,
                   sizeof(u32initialEstimation));
  }
}

/**
 * @brief Initialize ADC periphery
 * @return ESP_OK if no error
 */
static esp_err_t _adcInit(void) {
  esp_err_t eErrCode = ESP_FAIL;

  RET_WHEN_ERR(adc1_config_channel_atten(IO_BATT_ADC1_EXT_PWR, ADC_ATTEN_DB_0));
  RET_WHEN_ERR(adc1_config_channel_atten(IO_BATT_ADC1_BATT, ADC_ATTEN_DB_0));

  return eErrCode;
}

/**
 * @brief Read raw data from ADC and calculate average
 * @param u16numOfReadings Number of readings
 * @return
 */
static uint16_t _adcGetRawAverage(uint16_t u16numOfReadings) {
  uint32_t u32adcRawSum = 0;

  for (uint16_t u16cnt = 0; u16cnt < u16numOfReadings; u16cnt++) {
    // For debug purposes uncomment
    // ESP_LOGV(TAG, "%d of %d", u16cnt, u16numOfReadings);
    u32adcRawSum += adc1_get_raw((adc1_channel_t)IO_BATT_ADC1_BATT);
    vTaskDelay(BATT_SAMPLES_TIMEOUT_MS / portTICK_PERIOD_MS);
  }

  // Calculate average
  u32adcRawSum /= u16numOfReadings;

  assert(u32adcRawSum < 0xFFFF);

  return u32adcRawSum & 0xFFFF;
}

/**
 * @brief Return time when data from ring buffer suppose to be written into NVS
 * @return Time in ms
 */
inline static uint32_t _getNextWriteRingBuffToNvsTimeMs(void) {
  return GET_CLK_MS_32BIT() + (1000 * 60 * BATT_STORE_DATA_MINUTES);
}

static teNvsSttngsErr _saveRingBufferToNvs(tsRingBuffer *psRingBuffRaw) {
  teNvsSttngsErr eErrCode =
      nvsSettingsSet(_NVS_STATUS_DATA_ID, psRingBuffRaw->pvBuffer,
                     psRingBuffRaw->u16bufferSizeBytes);

  // Also set indexes
  uint16_t au16idx[2] = {psRingBuffRaw->u16readIdx, psRingBuffRaw->u16writeIdx};
  eErrCode |= nvsSettingsSet(_NVS_STATUS_IDX_ID, au16idx, sizeof(au16idx));

  return eErrCode;
}

static teNvsSttngsErr _loadRingBufferFromNvs(tsRingBuffer *psRingBuffRaw) {
  teNvsSttngsErr eErrCode = nvsSettingsGet(
      _NVS_STATUS_DATA_ID, psRingBuffRaw->pvBuffer, msRuntime.au16defaultData,
      psRingBuffRaw->u16bufferSizeBytes);

  uint16_t au16idx[2];
  uint16_t au16defaultIdx[sizeof(au16idx)] = {0};
  eErrCode |= nvsSettingsGet(_NVS_STATUS_IDX_ID, au16idx, au16defaultIdx,
                             sizeof(au16idx));

  psRingBuffRaw->u16readIdx = au16idx[0];
  psRingBuffRaw->u16writeIdx = au16idx[1];

  return eErrCode;
}

/**
 * @brief Check whether calibration data are valid or not
 * @return True if calibration data are valid, false otherwise
 */
static bool _areCalibrationDataValid(void) {
  // If any of calibration value is still zero, consider all data as invalid

  for (uint8_t u8idx = 0; u8idx < (sizeof(msRuntime.calib.au16real) /
                                   sizeof(msRuntime.calib.au16real[0]));
       u8idx++) {
    ESP_LOGV(TAG, "Are calibration data valid: idx: %d | val: %d", u8idx,
             msRuntime.calib.au16real[u8idx]);

    // If only one record is empty, calibration data are discarded as whole
    if (msRuntime.calib.au16real[u8idx] == 0) {
      // Calibration is required before device is at field -> warning at least
      ESP_LOGW(TAG, "Calibration data NOT valid");
      return false;
    }
  }

  // If no zero value was found, assume that calibration data are valid
  return true;
}

/**
 * @brief Calculate coefficients from calibration data
 */
static void _calculateCalibCoef(void) {
  // Calculate coefficients for every "item"

  // Range 0 ~ 3.6 V
  mu_linear_approx_get_coef_u16(
      0, msRuntime.calib.au16ideal[BATTERY_CALIB_3_6V], 0,
      msRuntime.calib.au16real[BATTERY_CALIB_3_6V],
      &msRuntime.calib.i16offset[BATTERY_CALIB_3_6V],
      &msRuntime.calib.i32multiplier[BATTERY_CALIB_3_6V]);

  // Range 3.6 ~ 3.7 V
  mu_linear_approx_get_coef_u16(
      msRuntime.calib.au16ideal[BATTERY_CALIB_3_6V],
      msRuntime.calib.au16ideal[BATTERY_CALIB_3_7V],
      msRuntime.calib.au16real[BATTERY_CALIB_3_6V],
      msRuntime.calib.au16real[BATTERY_CALIB_3_7V],
      &msRuntime.calib.i16offset[BATTERY_CALIB_3_7V],
      &msRuntime.calib.i32multiplier[BATTERY_CALIB_3_7V]);

  // Range 3.7 ~ 3.8 V
  mu_linear_approx_get_coef_u16(
      msRuntime.calib.au16ideal[BATTERY_CALIB_3_7V],
      msRuntime.calib.au16ideal[BATTERY_CALIB_3_8V],
      msRuntime.calib.au16real[BATTERY_CALIB_3_7V],
      msRuntime.calib.au16real[BATTERY_CALIB_3_8V],
      &msRuntime.calib.i16offset[BATTERY_CALIB_3_8V],
      &msRuntime.calib.i32multiplier[BATTERY_CALIB_3_8V]);

  // Range 3.8 ~ 3.9 V
  mu_linear_approx_get_coef_u16(
      msRuntime.calib.au16ideal[BATTERY_CALIB_3_8V],
      msRuntime.calib.au16ideal[BATTERY_CALIB_3_9V],
      msRuntime.calib.au16real[BATTERY_CALIB_3_8V],
      msRuntime.calib.au16real[BATTERY_CALIB_3_9V],
      &msRuntime.calib.i16offset[BATTERY_CALIB_3_9V],
      &msRuntime.calib.i32multiplier[BATTERY_CALIB_3_9V]);

  // Range 3.9 ~ 4.0 V
  mu_linear_approx_get_coef_u16(
      msRuntime.calib.au16ideal[BATTERY_CALIB_3_9V],
      msRuntime.calib.au16ideal[BATTERY_CALIB_4_0V],
      msRuntime.calib.au16real[BATTERY_CALIB_3_9V],
      msRuntime.calib.au16real[BATTERY_CALIB_4_0V],
      &msRuntime.calib.i16offset[BATTERY_CALIB_4_0V],
      &msRuntime.calib.i32multiplier[BATTERY_CALIB_4_0V]);

  // Range 4.0 ~ 4.1 V
  mu_linear_approx_get_coef_u16(
      msRuntime.calib.au16ideal[BATTERY_CALIB_4_0V],
      msRuntime.calib.au16ideal[BATTERY_CALIB_4_1V],
      msRuntime.calib.au16real[BATTERY_CALIB_4_0V],
      msRuntime.calib.au16real[BATTERY_CALIB_4_1V],
      &msRuntime.calib.i16offset[BATTERY_CALIB_4_1V],
      &msRuntime.calib.i32multiplier[BATTERY_CALIB_4_1V]);

  // Range 4.1 ~ 4.2 V
  mu_linear_approx_get_coef_u16(
      msRuntime.calib.au16ideal[BATTERY_CALIB_4_1V],
      msRuntime.calib.au16ideal[BATTERY_CALIB_4_2V],
      msRuntime.calib.au16real[BATTERY_CALIB_4_1V],
      msRuntime.calib.au16real[BATTERY_CALIB_4_2V],
      &msRuntime.calib.i16offset[BATTERY_CALIB_4_2V],
      &msRuntime.calib.i32multiplier[BATTERY_CALIB_4_2V]);
  // Range 4.2 ~ 4.3 V
  mu_linear_approx_get_coef_u16(
      msRuntime.calib.au16ideal[BATTERY_CALIB_4_2V],
      msRuntime.calib.au16ideal[BATTERY_CALIB_4_3V],
      msRuntime.calib.au16real[BATTERY_CALIB_4_2V],
      msRuntime.calib.au16real[BATTERY_CALIB_4_3V],
      &msRuntime.calib.i16offset[BATTERY_CALIB_4_3V],
      &msRuntime.calib.i32multiplier[BATTERY_CALIB_4_3V]);

  // Range 4.3 V and above. Maximum (theoretical and real) is 1023 anyway ->
  // ley's use it as maximum
  mu_linear_approx_get_coef_u16(
      msRuntime.calib.au16ideal[BATTERY_CALIB_4_3V], 1023,
      msRuntime.calib.au16real[BATTERY_CALIB_4_3V], 1023,
      &msRuntime.calib.i16offset[BATTERY_CALIB_MAX],
      &msRuntime.calib.i32multiplier[BATTERY_CALIB_MAX]);
}
