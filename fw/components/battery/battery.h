/**
 * @file
 * @author Martin Stejskal
 * @brief Battery manager module
 *
 * Periodically read battery status and record it
 */
#ifndef __BATTERY_H__
#define __BATTERY_H__
// ===============================| Includes |================================
// Standard
#include <stdbool.h>

// ESP specific
#include <esp_err.h>

// "local"
#include "battery_cfg.h"
#include "ring_buff.h"
// ================================| Defines |================================

/**
 * @brief This is kind of "symbolic link" to default number of samples
 *
 * Can be used by higher layers when "they just want value" and do not care
 * about how it is received.
 */
#define BATT_DEFAULT_NUM_OF_SAMPLES (0)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  // Vadc = 0.762 V ; Ideal value on ADC: 708
  BATTERY_CALIB_3_6V,  //!< BATTERY_CALIB_3_6V 3.6 V

  // Vadc = 0.783 V ; Ideal value on ADC: 728
  BATTERY_CALIB_3_7V,  //!< BATTERY_CALIB_3_7V 3.7 V

  // Vadc = 0.804 V ; Ideal value on ADC: 748
  BATTERY_CALIB_3_8V,  //!< BATTERY_CALIB_3_8V 3.8 V

  // Vadc = 0.825 V ; Ideal value on ADC: 768
  BATTERY_CALIB_3_9V,  //!< BATTERY_CALIB_3_9V 3.9 V

  // Vadc = 0.846 V ; Ideal value on ADC: 787
  BATTERY_CALIB_4_0V,  //!< BATTERY_CALIB_4_0V 4.0 V

  // Vadc = 0.867 V ; Ideal value on ADC: 807
  BATTERY_CALIB_4_1V,  //!< BATTERY_CALIB_4_1V 4.1 V

  // Vadc = 0.888 V ; Ideal value on ADC: 827
  BATTERY_CALIB_4_2V,  //!< BATTERY_CALIB_4_2V 4.2 V

  // Vadc = 0.910 V ; Ideal value on ADC: 846
  BATTERY_CALIB_4_3V,  //!< BATTERY_CALIB_4_3V 4.3 V

  // Last item - used for indexing
  BATTERY_CALIB_MAX,  //!< BATTERY_CALIB_MAX End of enumeration
} teBattCalibPoints;

/**
 * @brief Battery status structure
 */
typedef struct {
  // Battery level as percentage. Range: 0 ~ 100
  uint8_t u8percentage;

  // True is charging. False otherwise.
  bool bCharging;

  // Voltage on battery
  float fVoltage;

  // How long device run on battery since last full charge
  uint32_t u32runOnBatterySec;

  // Telling how long is device charging in current charge cycle
  uint32_t u32howLongChargingSec;

  // Estimated battery lifetime in seconds. Note that this is estimation.
  uint32_t u32batteryEstimateSec;

  // Voltage on battery in raw form from ADC
  uint16_t u16rawAdc;

  // Pointer to measured data. Can be used for visualization
  tsRingBuffer* psRingBuffRaw;

  // System time in seconds. This might help you to show power on time and
  // relate to current data.
  uint32_t u32timestampSec;
} tsBattStatus;
// ===============================| Functions |===============================
// =========================| High level functions |==========================
/**
 * @brief Task which take care about sampling battery status
 * @param pvParameters Pointer to pointers - can be void
 */
void batteryTaskRTOS(void* pvParameters);

/**
 * @brief Report battery status at once
 * @param[out] psStatus Pointer to status structure
 */
void batteryGetStatus(tsBattStatus* psStatus);
// ========================| Middle level functions |=========================
/**
 * @brief Recalculate raw value to voltage
 *
 * Function loads calibration data. Before very first measurement calibration
 * have to be done.
 *
 * @param u16rawValue Raw value from ring buffer
 * @return Battery voltage in volts as float
 */
float batteryRawToVoltage(uint16_t u16rawValue);

/**
 * @brief Convert input voltage into raw value
 *
 * @param f_voltage Voltage in V
 * @return Raw value
 */
uint16_t battery_voltage_to_raw(float f_voltage);

/**
 * @brief Calculate battery capacity from actual voltage
 *
 * @param u16rawValue Raw value from ring buffer
 * @return Battery capacity in percentage
 */
uint8_t batteryRawToPercentage(uint16_t u16rawValue);
// ==========================| Low level functions |==========================
/**
 * @brief Measure voltage on battery and return raw value
 *
 * This will not store result into ring buffer. This is independent logic. It
 * allows upper layer to get current value, while ring buffer allow to receive
 * measured history
 *
 * @param u16numOfReadings Number of readings. If value is zero, default value
 *                         is used. More readings basically help to eliminate
 *                         noise, but takes more time.
 *
 * @return Raw value form ADC
 */
uint16_t batteryGetCurrentValueFromAdc(uint16_t u16numOfReadings);

/**
 * @brief Calibrate ADC for given value
 *
 * Before calling this function, make sure that correct voltage is set on
 * battery connector
 *
 * @note It is required to do calibration for all values. Until that point
 *       calibration data can not be used.
 *
 * @param eCalibrationType Select calibration type (voltage)
 * @return ESP_OK if no error
 */
esp_err_t batteryCalibrateAdc(teBattCalibPoints eCalibrationType);

/**
 * @brief Get current static offset for battery in mV
 *
 * This offset is applied on the top of measured and data. This is kind of last
 * dirty calibration value which should help to correct final calculated voltage
 * due to voltage drop on wires.
 *
 * @return Static offset in mV
 */
int8_t battery_get_static_offset_mv(void);

/**
 * @brief Set new static offset for battery in mV
 *
 * @note Set and get value might differ due to internal calculation processing
 * and limited accuracy of integer values.
 *
 * @note Value is saved into NVS
 *
 * This offset is applied on the top of measured and data. This is kind of last
 * dirty calibration value which should help to correct final calculated voltage
 * due to voltage drop on wires.
 *
 * @param i8_static_offset New offset in mV
 * @return ESP_OK if no error
 */
esp_err_t battery_set_static_offset_mv(int8_t i8_static_offset);

/**
 * @brief Returns calibration type as string
 * @param eCalibrationType Calibration type as enumeration
 * @return Calibration type as string
 */
const char* batteryGetCalibrationTypeName(teBattCalibPoints eCalibrationType);

/**
 * @brief Returns raw calibration data, so user can make backup
 *
 * Typically backup might be needed in case that whole device flash is erased
 * or when partition table is changed. Then it might be good to have those
 * data and set them directly instead of repeating whole calibration process
 *
 * @param au16calibData Data structure where calibration data will be stored
 */
void batteryGetCalibrationRaw(uint16_t au16calibData[static BATTERY_CALIB_MAX]);

/**
 * @brief Allow to set calibration data directly, without measurement
 *
 * This can be handy when previously measured data were backup and flash was
 * re-written or partitions were changed.
 *
 * @param au16calibData Raw calibration data in same order as returned via
 *                     batteryGetCalibrationRaw()
 * @return ESP_OK if no error
 */
esp_err_t batteryOverwriteCalibrationRaw(
    uint16_t au16calibData[static BATTERY_CALIB_MAX]);

/**
 * @brief Throw current data in RAM and load data from NVS
 *
 * This allow to get "fresh" data when NVS is erased for example. In such case
 * it is necessary to get rid of measured data and load what is in NVS. Thanks
 * to this mechanism it is possible to reload values "on the fly" without
 * resetting device.
 */
void batteryForceReloadValuesFromNvs(void);

/**
 * @brief Remove all statistics data from NVS
 * @return ESP_OK if no error
 */
esp_err_t batteryDeleteStatisticData(void);
#endif  // __BATTERY_H__
