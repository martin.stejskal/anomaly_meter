idf_component_register(SRCS "battery.c"
  INCLUDE_DIRS "."
  INCLUDE_DIRS "../../config"
  REQUIRES utils
  REQUIRES math_utils
  REQUIRES nvs_settings)
