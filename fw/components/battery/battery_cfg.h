/**
 * @file
 * @author Martin Stejskal
 * @brief Configuration for battery manager
 */
#ifndef __BATTERY_CFG_H__
#define __BATTERY_CFG_H__
// ===============================| Includes |================================

// When compiler support it, check if user file exists and if yes, include it
#if defined __has_include
#if __has_include("cfg.h")
#include "cfg.h"
#endif
#endif
// ================================| Defines |================================

// ============================| Default values |=============================
#ifndef BATT_INITIAL_ESTIMATE_S
/**
 * @brief Initial estimation when battery is 100% charged
 *
 * This value is brainlessly passed to user as initial estimation if battery
 * is "fully" charged for the first time. There is some inaccuracy. By time
 * device will "learn" real estimation, so this value will not be used anymore.
 *
 * @note This depends on your application and battery. Set it wisely
 */
#define BATT_INITIAL_ESTIMATE_S (8 * 60 * 60)
#endif  // BATT_INITIAL_ESTIMATE_S

#ifndef BATT_DIAG_PERIOD_S
/**
 * @brief How often should battery data should be sampled in seconds
 *
 * More often samples means higher accuracy in time, but it will reduce total
 * measure window.
 */
#define BATT_DIAG_PERIOD_S (30)
#endif  // BATT_DIAG_PERIOD_S

#ifndef BATT_BUFFER_SIZE_ITEMS
/**
 * @brief Define how many measured samples should be stored in memory
 *
 * Higher number of samples allows more often sampling and hold sampled data
 * longer. Negative impact is higher memory usage (RAM and NVS)
 */
#define BATT_BUFFER_SIZE_ITEMS (12 * 60 * 60 / (BATT_DIAG_PERIOD_S))
#endif  // BATT_BUFFER_SIZE_ITEMS

#ifndef BATT_STORE_DATA_MINUTES
/**
 * @brief How often should be stored data to NVS in minutes
 */
#define BATT_STORE_DATA_MINUTES (1)
#endif  // BATT_STORE_DATA_MINUTES

#ifndef BATT_NUM_OF_SAMPLES_PER_MEASUREMENT
/**
 * @brief How many samples should be used when measuring
 *
 * Due to noise reduction it is better to repeatedly measure and then make
 * simple average. In theory this should suppress noise.
 */
#define BATT_NUM_OF_SAMPLES_PER_MEASUREMENT (15)
#endif  // BATT_NUM_OF_SAMPLES_PER_MEASUREMENT

#ifndef BATT_NUM_OF_SAMPLES_WHEN_CALIBRATING
/**
 * @brief Define how many samples will be used when calibrating ADC
 *
 * This value should be "huge" in order to eliminate noise as much as possible
 */
#define BATT_NUM_OF_SAMPLES_WHEN_CALIBRATING (150)
#endif  // BATT_NUM_OF_SAMPLES_WHEN_CALIBRATING

#ifndef BATT_ESTIMATION_AVERAGE_NUM_OF_SAMPLES
/**
 * @brief When estimating battery lifetime, do average
 *
 * Estimation might "jump" from left to right due to inaccuracy on ADC.
 * Therefore some averaging is done. This number telling from how many samples
 * should be average calculated
 */
#define BATT_ESTIMATION_AVERAGE_NUM_OF_SAMPLES (50)
#endif  // BATT_ESTIMATION_AVERAGE_NUM_OF_SAMPLES

#ifndef BATT_SAMPLES_TIMEOUT_MS
/**
 * @brief Timeout in ms between samples when measuring
 *
 * It is not good take samples very quickly. There can be temporary load or
 * some noise and this could easily influence measurement data. Some minimal
 * timeout should be considered.
 */
#define BATT_SAMPLES_TIMEOUT_MS (20)
#endif  // BATT_SAMPLES_TIMEOUT_MS

#ifndef BATT_RECALIBRATE_INITIAL_ESTIMATE_BATT_PERC
/**
 * @brief Initial estimation will be overwritten if battery level will be below
 *
 * In order to be able accurately estimate how long battery will be functional,
 * it is good idea time to time update base initial estimation. This should
 * be done when battery is discharged enough, so we can assume that current
 * estimation is reliable enough. Value is in percentage of battery capacity.
 * Recommended is 60 or bit lower.
 */
#define BATT_RECALIBRATE_INITIAL_ESTIMATE_BATT_PERC (50)
#endif  // BATT_RECALIBRATE_INITIAL_ESTIMATE_BATT_PERC

#ifndef BATT_RECALIBRATE_INITIAL_ESTIMATE_STABLE_FOR_SEC
/**
 * @brief Additional condition for calibration initial estimation
 *
 * Device have to "stable" for some time in order to decide that it is OK to
 * store current estimation. By "stable" is meant that battery did not drop
 * (there is ring buffer and have some inertia) while device is running already
 * for some time.
 */
#define BATT_RECALIBRATE_INITIAL_ESTIMATE_STABLE_FOR_SEC (5 * 60)
#endif  // BATT_RECALIBRATE_INITIAL_ESTIMATE_STABLE_FOR_SEC

#ifndef BATT_RECALIBRATE_INITIAL_ESTIMATE_NVS_UPDATE_MIN
/**
 * @brief Minimum time when storing initial calibration can be done
 *
 * Writing too often to NVS is not good. Also here it is not required actually.
 * Time to time is fine.
 */
#define BATT_RECALIBRATE_INITIAL_ESTIMATE_NVS_UPDATE_MIN (10)
#endif  // BATT_RECALIBRATE_INITIAL_ESTIMATE_NVS_UPDATE_MIN
// ==========================| Preprocessor checks |==========================

#endif  // __BATTERY_CFG_H__
