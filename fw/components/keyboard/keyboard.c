/**
 * @file
 * @author Martin Stejskal
 * @brief Keyboard driver
 */
// ===============================| Includes |================================
#include <assert.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <stdbool.h>
#include <string.h>

// ESP specific
#include <driver/adc.h>
#include <driver/gpio.h>
#include <esp_log.h>

#include "err_handlers.h"
#include "keyboard.h"
#include "nvs_settings.h"

// ================================| Defines |================================
#define _KBRD_GET_THRSHLD(res, low, high) res = low + ((high - low) >> 1)

/**
 * @brief When calibrating buttons, how many times will be read from ADC
 */
#define _KBRD_NUM_OF_CAL_CYCLES (150)

/**
 * @brief Scan period for keyboard in ms
 */
#define _KBRD_SCAN_PERIOD_MS (50)

/**
 * @brief Number of ADC reading before value is processed
 *
 * Because on ADC can occur some temporary spikes, it is advised to do
 * multiple measurements and average value. This value basically says how many
 * samples should be used for averaging
 */
#define _KBRD_NUM_OF_ADC_READING (20)

/**
 * @brief Show pressed buttons in log
 *
 * This is typically not needed, but it is suitable for debug
 */
#define _KBRD_SHOW_PRESSED_BTNS (0)

/**
 * @brief Show raw values from ADC - only for debug
 */
#define _KBRD_SHOW_ADC_RAW (0)

/**
 * @brief Calibration ID for NVS
 */
#define _KBRD_CALIBRATION_ID "KbrdCal"

/**
 * @brief Backlight ID for NVS
 */
#define _KBRD_AUTO_BCKLGHT_ID "KbrdAbckl"

// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef union {
  tsKbrdBtns sBtns;
  uint8_t u8raw;
} tuKbrd;

typedef struct {
  uint16_t u16left;
  uint16_t u16right;
  uint16_t u16up;
  uint16_t u16down;
  uint16_t u16enter;
} tsNvsSttngsKbrdCal;

typedef struct {
  /**
   * @brief Button threshold values
   *
   * This may change on the fly due to calibration, so that is why it is kept
   * as static variable
   */
  tsNvsSttngsKbrdCal sBtnThresholds;

  /**
   * @brief Previous button state
   */
  tuKbrd uBtnsPrevious;

  /**
   * @brief Current button state
   */
  tuKbrd uBtnsCurrent;

  /**
   * @brief Registers which buttons were pressed and released
   */
  tuKbrd uBtnsPressedReleased;

  /**
   * @brief Enable/disable automatic turning off backlight
   */
  bool bAutoOffBacklight;
} tsRuntime;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "KBRD";

/**
 * @brief Initial calibration data used after first flash
 *
 * List of this values reflect average value on ADC on reference board when
 * given button is pressed. Value represent bottom range value when given
 * button is considered as pressed.
 *
 * @note Values very roughly corresponds with voltage in mV on ADC pin
 *
 */
static const tsNvsSttngsKbrdCal msDefaultCalibDdata = {.u16enter = 10,
                                                       .u16right = 202,
                                                       .u16down = 337,
                                                       .u16up = 540,
                                                       .u16left = 838};

/**
 * @brief Keep runtime variables at one place
 */
static tsRuntime msRuntime;

// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static esp_err_t _kbrdInit(void);
static esp_err_t _kbrdRead(tsKbrdBtns *psButtons);

static char *_getBtnName(teKbrdSelBtn eButton);
// =========================| High level functions |==========================
void kbrdTaskRTOS(void *pvParameters) {
  // If initialization fail, exit task
  if (_kbrdInit()) {
    vTaskDelete(NULL);
    return;
  }

  // When button pressed, calculate turn off timeout for backlight
  uint32_t u32bclghtOffTimeMs =
      GET_CLK_MS_32BIT() + (1000 * KBRD_DISP_BCKLGHT_ON_TIME_S);
  // Assume that backlight is on (after start up)
  bool bBcklghtOn = true;

  // Load auto-off feature from NVM.
  bool bDefaultAutoBacklight = KBRD_AUTO_BCKLGHT_OFF_FEATURE;

  if (nvsSettingsGet(_KBRD_AUTO_BCKLGHT_ID, &msRuntime.bAutoOffBacklight,
                     &bDefaultAutoBacklight,
                     sizeof(msRuntime.bAutoOffBacklight))) {
    // If this fails, application can still work -> just log error
    ESP_LOGE(TAG, "Can not load backlight settings");
  }

  // Only when automatic backlight shutdown feature is enabled
  if (msRuntime.bAutoOffBacklight) {
    if (KBRD_IS_DISP_INIT_DONE()) {
      KBRD_DISP_BCKLGHT(1);
    } else {
      // Nevermind. Let's hope/pretend that backlight is on
    }
  }

  while (1) {
    // Read buttons, if something went wrong, exit loop
    if (_kbrdRead(&msRuntime.uBtnsCurrent.sBtns)) {
      break;
    }

#if _KBRD_SHOW_PRESSED_BTNS
    // If any button was pressed
    if (msRuntime.uBtnsCurrent.u8raw) {
      kbrdShowPressedButtons(&msRuntime.uBtnsCurrent.sBtns);
    }
#endif

    // Check pressed and released buttons
    // XOR - do "diff" ; AND - only previously pressed buttons
    msRuntime.uBtnsPressedReleased.u8raw |=
        (msRuntime.uBtnsPrevious.u8raw ^ msRuntime.uBtnsCurrent.u8raw) &
        msRuntime.uBtnsPrevious.u8raw;

    // Current value became previous
    msRuntime.uBtnsPrevious = msRuntime.uBtnsCurrent;

    // If any button pressed, update "turn off timer", enable backlight
    if (msRuntime.bAutoOffBacklight) {
      if (msRuntime.uBtnsCurrent.u8raw && KBRD_IS_DISP_INIT_DONE()) {
        KBRD_DISP_BCKLGHT(1);
        bBcklghtOn = true;

        u32bclghtOffTimeMs =
            GET_CLK_MS_32BIT() + (1000 * KBRD_DISP_BCKLGHT_ON_TIME_S);
      } else {
        // No button pressed of display driver not ready - do nothing
      }
    }

    vTaskDelay(_KBRD_SCAN_PERIOD_MS / portTICK_PERIOD_MS);

    if (msRuntime.bAutoOffBacklight) {
      // If backlight is on and it is time to power it off
      if (KBRD_IS_DISP_INIT_DONE()) {
        if (bBcklghtOn && (GET_CLK_MS_32BIT() >= u32bclghtOffTimeMs)) {
          KBRD_DISP_BCKLGHT(0);
          bBcklghtOn = false;
        } else {
          // backlight is off or time has not come yet
        }
      } else {
        // Display driver is not ready - do not mess with it
      }
    }
  }

  // If we get out of the cycle, something went wrong
  vTaskDelete(NULL);
}

void kbrPressedReleased(tsKbrdBtns *psButtons) {
  assert(psButtons);

  assert(sizeof(tsKbrdBtns) == sizeof(tuKbrd));

  *psButtons = msRuntime.uBtnsPressedReleased.sBtns;

  // Clear pressed/released buttons
  msRuntime.uBtnsPressedReleased.u8raw = 0;
}

void kbrdRead(tsKbrdBtns *psButtons) {
  *psButtons = msRuntime.uBtnsCurrent.sBtns;
}

// ========================| Middle level functions |=========================
esp_err_t kbrdCalibrateButton(teKbrdSelBtn eButton) {
  esp_err_t eErrCode = ESP_FAIL;

  // Calibration data
  tsNvsSttngsKbrdCal sCalData;

  // Load calibration data
  RET_WHEN_ERR(nvsSettingsGet(_KBRD_CALIBRATION_ID, &sCalData,
                              &msDefaultCalibDdata, sizeof(sCalData)));

  ESP_LOGI(TAG,
           "%s Calibrating button %s\n"
           "Make sure correct button is pressed",
           __FUNCTION__, _getBtnName(eButton));

  // Accumulate values from ADC - will be used later on for calculation
  // average
  int32_t i32adcSum = 0;

  // Keep calculated offset
  int16_t i16offset;

  for (uint16_t u16calCnt = 0; u16calCnt < _KBRD_NUM_OF_CAL_CYCLES;
       u16calCnt++) {
    i32adcSum += adc1_get_raw((adc1_channel_t)IO_KBRD_ADC1_CHANNEL);
  }

  // Get average
  i32adcSum = i32adcSum / _KBRD_NUM_OF_CAL_CYCLES;
  assert(i32adcSum < 0xFFFF);

  // Default is expected OK
  eErrCode = ESP_OK;
  switch (eButton) {
    case KBRD_BTN_NONE:
      // Nothing to do. Can not calibrate - zero have to be zero
      break;
    case KBRD_BTN_ENTER:
      // This is kind of exception. Recommended to not change default
      // hard-coded value to avoid false positive press.
      break;
    case KBRD_BTN_RIGHT:
      // Calculate offset from ideal value on key with 3.3 V power supply.
      // Pressed button should generate 0.332 V on Vadc -> 309 on ADC ideally.
      // Difference is offset
      i16offset = (int16_t)i32adcSum - 309;

      // Take default value and add offset
      sCalData.u16right =
          (uint16_t)((int16_t)msDefaultCalibDdata.u16right + i16offset);
      break;
    case KBRD_BTN_DOWN:
      // 0.527 V on Vadc -> 490
      i16offset = (int16_t)i32adcSum - 490;

      // Default + offset = calibrated value
      sCalData.u16down =
          (uint16_t)((int16_t)msDefaultCalibDdata.u16down + i16offset);
      break;
    case KBRD_BTN_UP:
      // 0.853 V on Vadc -> 792
      i16offset = (int16_t)i32adcSum - 792;

      // Default + offset = calibrated value
      sCalData.u16up =
          (uint16_t)((int16_t)msDefaultCalibDdata.u16up + i16offset);

      // Since calibration for "left" is not possible (out of ADC range),
      // simply same offset will be used also for left in this step
      sCalData.u16left =
          (uint16_t)((int16_t)msDefaultCalibDdata.u16left + i16offset);
      break;
    case KBRD_BTN_LEFT:
      // This is tricky, because it is out of ADC range. Part of calibration
      // button "up"
      break;
    default:
      // Undefined button
      eErrCode = ESP_FAIL;
  }
  RET_WHEN_ERR(eErrCode);

  // Save calibration data
  RET_WHEN_ERR(
      nvsSettingsSet(_KBRD_CALIBRATION_ID, &sCalData, sizeof(sCalData)));
  ESP_LOGI(TAG, "%s Calibration data saved", __FUNCTION__);

  return eErrCode;
}

esp_err_t kbrdDeleteCalibration(void) {
  return nvsSettingsDel(_KBRD_CALIBRATION_ID);
}

esp_err_t kbrdSetBacklightAutoOff(bool bAutoOff, bool bSaveSetting) {
  msRuntime.bAutoOffBacklight = bAutoOff;

  if (bSaveSetting) {
    return nvsSettingsSet(_KBRD_AUTO_BCKLGHT_ID, &msRuntime.bAutoOffBacklight,
                          sizeof(msRuntime.bAutoOffBacklight));
  } else {
    return ESP_OK;
  }
}

esp_err_t kbrdResetBacklightAutoOff(void) {
  // Load default and reset settings in NVS
  msRuntime.bAutoOffBacklight = KBRD_AUTO_BCKLGHT_OFF_FEATURE;

  return nvsSettingsDel(_KBRD_AUTO_BCKLGHT_ID);
}

inline bool kbrdGetBacklightAutoOff(void) {
  return msRuntime.bAutoOffBacklight;
}
// ==========================| Low level functions |==========================
uint8_t kbrdNumOfButtonPressed(tsKbrdBtns *psButtons) {
  uint8_t u8btnsPrssd = 0;

  if (psButtons->bEnter) {
    u8btnsPrssd++;
  }

  if (psButtons->bRight) {
    u8btnsPrssd++;
  }

  if (psButtons->bDown) {
    u8btnsPrssd++;
  }

  if (psButtons->bUp) {
    u8btnsPrssd++;
  }

  if (psButtons->bLeft) {
    u8btnsPrssd++;
  }

  return u8btnsPrssd;
}

bool kbrdAnyButtonPressed(tsKbrdBtns *psButtons) {
  assert(sizeof(tsKbrdBtns) <= sizeof(uint8_t));

  if (*((uint8_t *)psButtons)) {
    return 1;
  } else {
    return 0;
  }
}
// ============================| Print functions |============================
void kbrdShowPressedButtons(tsKbrdBtns *psButtons) {
  assert(psButtons);

  // 5 buttons on board + NULL for string
  char acButtons[6] = "     ";

  if (psButtons->bLeft) {
    acButtons[0] = '<';
  }

  if (psButtons->bUp) {
    acButtons[1] = '^';
  }

  if (psButtons->bDown) {
    acButtons[2] = 'v';
  }

  if (psButtons->bEnter) {
    acButtons[3] = 'x';
  }

  if (psButtons->bRight) {
    acButtons[4] = '>';
  }

  ESP_LOGD(TAG, "Pressed buttons: %s", acButtons);
}

// ==========================| Internal functions |===========================
static esp_err_t _kbrdInit(void) {
  esp_err_t eErrCode = ESP_FAIL;

  RET_WHEN_ERR(adc1_config_channel_atten(IO_KBRD_ADC1_CHANNEL, ADC_ATTEN_DB_0));

  // Load calibration data
  RET_WHEN_ERR(nvsSettingsGet(_KBRD_CALIBRATION_ID, &msRuntime.sBtnThresholds,
                              &msDefaultCalibDdata,
                              sizeof(msRuntime.sBtnThresholds)));

  ESP_LOGI(TAG,
           "Initialized.\nCal data: %d %d %d %d %d\n"
           "Thresholds: %d %d %d %d %d",
           msRuntime.sBtnThresholds.u16enter, msRuntime.sBtnThresholds.u16right,
           msRuntime.sBtnThresholds.u16down, msRuntime.sBtnThresholds.u16up,
           msRuntime.sBtnThresholds.u16left, msRuntime.sBtnThresholds.u16enter,
           msRuntime.sBtnThresholds.u16right, msRuntime.sBtnThresholds.u16down,
           msRuntime.sBtnThresholds.u16up, msRuntime.sBtnThresholds.u16left);

  assert(msRuntime.sBtnThresholds.u16enter < msRuntime.sBtnThresholds.u16right);
  assert(msRuntime.sBtnThresholds.u16right < msRuntime.sBtnThresholds.u16down);
  assert(msRuntime.sBtnThresholds.u16down < msRuntime.sBtnThresholds.u16up);
  assert(msRuntime.sBtnThresholds.u16up < msRuntime.sBtnThresholds.u16left);

  return eErrCode;
}

static esp_err_t _kbrdRead(tsKbrdBtns *psButtons) {
  assert(sizeof(int) >= sizeof(uint16_t));
  assert(psButtons);

  // Variable for calculating average value
  uint32_t u32adcAcc = 0;
  int iAdcVal;

  for (uint8_t u8readingIdx = 0; u8readingIdx < _KBRD_NUM_OF_ADC_READING;
       u8readingIdx++) {
    iAdcVal = adc1_get_raw((adc1_channel_t)IO_KBRD_ADC1_CHANNEL);
    u32adcAcc += (uint32_t)iAdcVal;
  }
  u32adcAcc = u32adcAcc / _KBRD_NUM_OF_ADC_READING;

  assert(u32adcAcc <= 0xFFFF);
  uint16_t u16adcVal = (uint16_t)u32adcAcc;

  // Clean-up buttons
  memset(psButtons, 0, sizeof(tsKbrdBtns));

#if _KBRD_SHOW_ADC_RAW
  ESP_LOGD(TAG, "ADC avg: %u ; Last raw: %u", u16adcVal, iAdcVal);
#endif

  // Start from highest value and go down. This is directly tied with
  // hardware

  if (u16adcVal > msRuntime.sBtnThresholds.u16left) {
    psButtons->bLeft = 1;
  } else if (u16adcVal > msRuntime.sBtnThresholds.u16up) {
    psButtons->bUp = 1;
  } else if (u16adcVal > msRuntime.sBtnThresholds.u16down) {
    psButtons->bDown = 1;
  } else if (u16adcVal > msRuntime.sBtnThresholds.u16right) {
    psButtons->bRight = 1;
  } else if (u16adcVal > msRuntime.sBtnThresholds.u16enter) {
    psButtons->bEnter = 1;
  } else {
    // Nothing pressed
  }

  return ESP_OK;
}

static char *_getBtnName(teKbrdSelBtn eButton) {
  // Need static, since it will return pointer to this string (have to
  // persist)
  static char cName[] = "Unknown";

  switch (eButton) {
    case KBRD_BTN_NONE:
      strcpy(cName, "None");
      break;
    case KBRD_BTN_LEFT:
      strcpy(cName, "Left");
      break;
    case KBRD_BTN_RIGHT:
      strcpy(cName, "Right");
      break;
    case KBRD_BTN_UP:
      strcpy(cName, "Up");
      break;
    case KBRD_BTN_DOWN:
      strcpy(cName, "Down");
      break;
    case KBRD_BTN_ENTER:
      strcpy(cName, "Enter");
      break;
      // There is no "default" since this is enumeration and compiler would
      // report it warning/error when not defined value would be used
  }

  return cName;
}
