/**
 * @file
 * @author Martin Stejskal
 * @brief Keyboard driver
 */
#ifndef __KEYBOARD_H__
#define __KEYBOARD_H__
// ===============================| Includes |================================
#include <esp_err.h>
#include <stdbool.h>
#include <stdint.h>

#include "cfg.h"

// Allow enable/disable backlight
#include "ui_display.h"
// ================================| Defines |================================
#ifndef KBRD_DISP_BCKLGHT
/**
 * @brief Map function which enable and disable display backlight
 */
#define KBRD_DISP_BCKLGHT(bOn) uiDispSetBacklight(bOn, false)
#endif  // KBRD_DISP_BCKLGHT

#ifndef KBRD_IS_DISP_INIT_DONE
/**
 * @brief Map function which check if display driver is ready or not
 */
#define KBRD_IS_DISP_INIT_DONE() uiDispIsHwInitialized()
#endif  // KBRD_IS_DISP_INIT_DONE

#ifndef KBRD_DISP_BCKLGHT_ON_TIME_S
/**
 * @brief When button pressed, it will turn on backlight for this time in sec
 */
#define KBRD_DISP_BCKLGHT_ON_TIME_S (4)
#endif  // KBRD_DISP_BCKLGHT_ON_TIME_S

#ifndef KBRD_AUTO_BCKLGHT_OFF_FEATURE
/**
 * @brief Default setting for automatic disabling display backlight
 *
 * Non zero value enable "auto off" feature. Zero disable it
 */
#define KBRD_AUTO_BCKLGHT_OFF_FEATURE (1)
#endif  // KBRD_AUTO_BCKLGHT_OFF_FEATURE
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef struct {
  uint8_t bLeft : 1;
  uint8_t bRight : 1;
  uint8_t bUp : 1;
  uint8_t bDown : 1;
  uint8_t bEnter : 1;
} tsKbrdBtns;

typedef enum {
  KBRD_BTN_NONE,
  KBRD_BTN_LEFT,
  KBRD_BTN_RIGHT,
  KBRD_BTN_UP,
  KBRD_BTN_DOWN,
  KBRD_BTN_ENTER,
} teKbrdSelBtn;
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
void kbrdTaskRTOS(void *pvParameters);

/**
 * @brief Report which buttons were pressed and released
 *
 * In most of cases we need to wait not only for pressing button, but also for
 * releasing it. This function can handle it.
 *
 * @note This function keep track about pressed button and until is called,
 *       information about pressed buttons will be additively increasing. This
 *       means if user press button "left" and then "right" and function will
 *       be called after these two operations, it return 2 pressed buttons.
 *
 * @param psButtons Pointer to button array
 */
void kbrPressedReleased(tsKbrdBtns *psButtons);

/**
 * @brief Read from keyboard
 *
 * Return last button state from keyboard
 *
 * @param psButtons Pointer to structure where button states will be set
 */
void kbrdRead(tsKbrdBtns *psButtons);
// ========================| Middle level functions |=========================
/**
 * @brief Calibrate selected button
 * @param eButton One of button which suppose to be calibrated
 * @return ESP_OK if no error
 */
esp_err_t kbrdCalibrateButton(teKbrdSelBtn eButton);

/**
 * @brief Delete all calibration data
 * @return ESP_OK if no error
 */
esp_err_t kbrdDeleteCalibration(void);

/**
 * @brief Set backlight auto off feature
 * @param bAutoOff False disable feature, true enable it
 * @param bSaveSetting True will save setting to NVM, otherwise just keep it
 *                     in RAM
 * @return ESP_OK if no error
 */
esp_err_t kbrdSetBacklightAutoOff(bool bAutoOff, bool bSaveSetting);

/**
 * @brief Reset auto backlight feature to factory settings
 * @return ESP_OK if no error
 */
esp_err_t kbrdResetBacklightAutoOff(void);

/**
 * @brief Get backlight auto off feature
 * @return True if feature is enabled, false otherwise
 */
bool kbrdGetBacklightAutoOff(void);
// ==========================| Low level functions |==========================
/**
 * @brief Return number of pressed buttons in structure
 * @param psButtons Pointer to structure with buttons
 * @return Number of pressed buttons as unsigned value
 */
uint8_t kbrdNumOfButtonPressed(tsKbrdBtns *psButtons);

/**
 * @brief Check if any button is pressed
 * @param psButtons Pointer to button structure
 * @return Zero if no button pressed, non-zero otherwise
 */
bool kbrdAnyButtonPressed(tsKbrdBtns *psButtons);

// ============================| Print functions |============================
/**
 * @brief Print pressed buttons in human-readable form
 * @param psButtons Pointer to button structure
 */
void kbrdShowPressedButtons(tsKbrdBtns *psButtons);
#endif  // __KEYBOARD_H__
