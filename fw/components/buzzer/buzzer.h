/**
 * @file
 * @author Martin Stejskal
 * @brief Buzzer driver
 */
#ifndef __BUZZER_H__
#define __BUZZER_H__
// ===============================| Includes |================================
#include <esp_err.h>
#include <stdbool.h>
#include <stdint.h>
// ================================| Defines |================================
/**
 * @brief Initial settings for "power on beep"
 *
 * Beep duration in ms
 */
#define BUZZER_INITIAL_BEEP_DEFAULT_MS (500)
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
// ===========================| Global variables |============================
// ===============================| Functions |===============================
// =========================| High level functions |==========================
void buzzerTaskRTOS(void *pvParameters);
// ========================| Middle level functions |=========================
void buzzerModeContinuous(bool bEnable);

void buzzerModeGeigerWiFiRssi(int8_t i8rssi);

void buzzerOff(void);
// ==========================| Low level functions |==========================
uint16_t buzzerGetInitialBeepPeriod(void);
void buzzerSetInitialBeepPeriod(uint16_t u16periodMs);
void buzzerResetInitialBeepPeriod(void);

esp_err_t buzzerSetLevel(uint8_t u8level);
#endif  // __BUZZER_H__
