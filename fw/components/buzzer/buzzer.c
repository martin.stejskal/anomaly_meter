/**
 * @file
 * @author Martin Stejskal
 * @brief Buzzer driver
 */
// ===============================| Includes |================================
// Standard
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// ESP specific
#include <driver/dac.h>
#include <esp32/rom/ets_sys.h>  // Precise delays
#include <esp_log.h>
#include <esp_system.h>  // Random number generator

// Project specific
#include "buzzer.h"
#include "cfg.h"
#include "err_handlers.h"
#include "nvs_settings.h"
#include "wifi.h"
// ================================| Defines |================================
/**
 * @brief define how long should geiger "beep" pulse should be long
 *
 * Lower periods (ms domain) make more characteristic sound, but less loud.
 * Longer periods (20 ms and above) make sound much louder, but should will not
 * be as we know from geiger counter
 */
#define GEIGER_BEEP_DURATION_MS (3)

/**
 * @brief NVS identificator for initial beep length
 */
#define _NVS_INIT_BEEP "BzrInit"
// ============================| Default values |=============================
// ==========================| Preprocessor checks |==========================
// ===========================| Structures, enums |===========================
typedef enum {
  BUZZER_OFF,         //!< BUZZER_OFF
  BUZZER_CONTINUOUS,  //!< BUZZER_CONTINUOUS
  BUZZER_GEIGER,      //!< BUZZER_GEIGER
  BUZZER_MAX          //!< BUZZER_MAX
} teMode;

typedef struct {
  teMode eMode;
  uint8_t u8volumeRaw;
  int8_t i8geigerWiFiRssi;
} tsRuntime;
// ===========================| Global variables |============================
/**
 * @brief Tag for logger
 */
static const char *TAG = "Buzzer";

static tsRuntime msRuntime = {
    .eMode = BUZZER_OFF,
    .u8volumeRaw = 255,
    .i8geigerWiFiRssi = WIFI_MIN_RSSI,
};
// ===============================| Functions |===============================
// =====================| Internal function prototypes |======================
static void _enableBuzzer(bool bEnable);
static void _geigerTask(void);

// =========================| High level functions |==========================
void buzzerTaskRTOS(void *pvParameters) {
  esp_err_t eErrCode = ESP_FAIL;

  eErrCode = dac_output_enable(IO_BUZZER_DAC);
  eErrCode |= dac_output_voltage(IO_BUZZER_DAC, 0);

  // Initialize HW
  if (eErrCode) {
    // This should not happen, but if so, we can keep live without buzzer
    ESP_LOGE(TAG, "Initialization failed");
    vTaskDelete(NULL);
    return;
  }

  /* Load beep period. If beep period is higher than zero, then do annoying
   * sound :D Idea is that in service mode is possible to configure this value,
   * so eventually it can be off.
   */
  uint16_t u16beepPeriodMs = buzzerGetInitialBeepPeriod();

  if (esp_cpu_in_ocd_debug_mode()) {
    // In debug mode - do not do annoying sound
  } else if (u16beepPeriodMs) {
    dac_output_voltage(IO_BUZZER_DAC, 255);
    vTaskDelay(u16beepPeriodMs / portTICK_PERIOD_MS);
    dac_output_voltage(IO_BUZZER_DAC, 0);
  }

  // Current mode is off
  msRuntime.eMode = BUZZER_OFF;

  for (;;) {
    switch (msRuntime.eMode) {
      case BUZZER_OFF:
        // Nothing to do. Keep it shut for another 250 ms
        vTaskDelay(250 / portTICK_PERIOD_MS);
        break;
      case BUZZER_CONTINUOUS:
        vTaskDelay(250 / portTICK_PERIOD_MS);
        break;
      case BUZZER_GEIGER:
        _geigerTask();
        break;
      default:
        ESP_LOGE(TAG, "Unknown buzzer mode %d. Turning off...",
                 msRuntime.eMode);
        ESP_ERROR_CHECK(dac_output_voltage(IO_BUZZER_DAC, 0));
        msRuntime.eMode = BUZZER_OFF;
    }
  }

  ESP_LOGE(TAG, "Buzzer task failed");
}

// ========================| Middle level functions |=========================
void buzzerModeContinuous(bool bEnable) {
  if (bEnable) {
    _enableBuzzer(true);
    msRuntime.eMode = BUZZER_CONTINUOUS;
  } else {
    buzzerOff();
  }
}

void buzzerModeGeigerWiFiRssi(int8_t i8rssi) {
  // Turn off buzzer first
  _enableBuzzer(false);

  msRuntime.i8geigerWiFiRssi = i8rssi;

  // Change mode
  msRuntime.eMode = BUZZER_GEIGER;
}

void buzzerOff(void) {
  _enableBuzzer(false);
  msRuntime.eMode = BUZZER_OFF;
}
// ==========================| Low level functions |==========================
uint16_t buzzerGetInitialBeepPeriod(void) {
  uint16_t u16beepPeriodMs;
  const uint16_t u16defaultBeepPeriodMs = BUZZER_INITIAL_BEEP_DEFAULT_MS;

  nvsSettingsGet(_NVS_INIT_BEEP, &u16beepPeriodMs, &u16defaultBeepPeriodMs,
                 sizeof(u16beepPeriodMs));

  return u16beepPeriodMs;
}

void buzzerSetInitialBeepPeriod(uint16_t u16periodMs) {
  nvsSettingsSet(_NVS_INIT_BEEP, &u16periodMs, sizeof(u16periodMs));
}

void buzzerResetInitialBeepPeriod(void) { nvsSettingsDel(_NVS_INIT_BEEP); }

esp_err_t buzzerSetLevel(uint8_t u8level) {
  return (dac_output_voltage(IO_BUZZER_DAC, u8level));
}
// ==========================| Internal functions |===========================
static void _enableBuzzer(bool bEnable) {
  if (bEnable) {
    buzzerSetLevel(msRuntime.u8volumeRaw);
  } else {
    buzzerSetLevel(0);
  }
}

static void _geigerTask(void) {
  if (msRuntime.i8geigerWiFiRssi <= WIFI_MIN_RSSI) {
    // No beep -> return with some minimal delay
    vTaskDelay(10 / portTICK_PERIOD_MS);
    return;
  }

  // Store final delay value (in ms)
  uint32_t u32delayMs;

  // Else non-zero value -> it have to be kind of random
  // Get random value
  uint32_t u32rand = esp_random();

  /* Problem in real world is that nothing is linear -> for calculating delay
   * we could use linear equation, but in real life test it would be quite
   * synthetic and not cool. Therefore also calculating delay is bit
   * "cheated"
   */
  if (msRuntime.i8geigerWiFiRssi > -50) {
    // Delay 10 ms
    // Random factor: 20 ms
    u32delayMs = 10 + (u32rand / (0xFFFFFFFF / 20));
  } else if (msRuntime.i8geigerWiFiRssi > -60) {
    // Delay between 10 and 60 ms
    // Random factor: 50
    u32delayMs = 10 + (u32rand / (0xFFFFFFFF / 50));
  } else if (msRuntime.i8geigerWiFiRssi > -77) {
    // delay between 20 and 100 ms
    // Random factor: 80 ms
    u32delayMs = 20 + (u32rand / (0xFFFFFFFF / 80));
  } else if (msRuntime.i8geigerWiFiRssi > -87) {
    // delay between 50 and 170 ms
    // Random factor: 120 ms
    u32delayMs = 50 + (u32rand / (0xFFFFFFFF / 120));
  } else if (msRuntime.i8geigerWiFiRssi > -92) {
    // delay between 100 and 260 ms
    // Random factor: 160 ms
    u32delayMs = 100 + (u32rand / (0xFFFFFFFF / 160));
  } else if (msRuntime.i8geigerWiFiRssi > -95) {
    // delay between 170 and 350 ms
    // Random factor: 180 ms
    u32delayMs = 170 + (u32rand / (0xFFFFFFFF / 180));
  } else {
    // "far" - high chance to loose signal ; delay between 270 and 700 ms
    // Random factor: 430 ms
    u32delayMs = 270 + (u32rand / (0xFFFFFFFF / 430));
  }

  ESP_LOGV(TAG, "Geiger counter: %u (%u)", u32delayMs,
           msRuntime.i8geigerWiFiRssi);

  _enableBuzzer(true);
  ets_delay_us(1000 * GEIGER_BEEP_DURATION_MS);
  _enableBuzzer(false);

  vTaskDelay(u32delayMs / portTICK_PERIOD_MS);
}
