#!/bin/bash
# Run flake8 check from same Docker image as in CI job.
# Sometimes system flake8 might differ a bit and it can cause problems. While
# locally everything seems to be fine, it can fail in CI.

# Get project directory, so everything can be done through absolute paths ->
# -> can call this script from anywhere
this_dir=$( dirname $0 )

# Backup actual directory
user_dir=$( pwd )

# Go to project root
cd "$this_dir"/../../

# Get image name
img_name=$( cat .gitlab-ci.yml | grep image | grep flake8 | awk '{print $2}')

# Run container, mount project root folder into "/project" and check it
docker run --rm -v ${PWD}:/project ${img_name} flake8 /project

# Go back
cd $user_dir
