# About
 * This application ease managing MAC address in device.
 * Application communicate with device over *service UI* (UART).

# Installation
 * Application need few Python packages. Just run `install_dependencies.sh`

# Executing application
 * Recommended way of running application is over virtual environment.
   In short call `run.sh`.
 * Still, you're free to run it in your host Python environment as well, but
   you will need to install dependencies manually.

# Configuration
 * If your device is using default password, you do not need to configure
   anything. If password was changed, take a look into `cfg/app.yaml` ;)

# Connection
 * By default, application will try to access all available UART interfaces.
 * If you have connected only 1 device, you do not have to care about anything.
   Otherwise you need to specify UART interface from drop down menu.

