#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
Module for loading configuration from file
=============================================

Load configuration and store it in structure

``author`` Martin Stejskal
"""
# Playing with configuration files
import yaml

# Our local libraries
# tuned logger
from lib.common import CmnLgrCls


class LoadCfgCls:
    def __init__(self, cfg_file='cfg/app.yaml'):
        self.logger = CmnLgrCls('Load configuration')

        # Load file and store settings in this instance
        self.reload(cfg_file)
    # /__init__()

    def get(self):
        """Get loaded settings

        :returns: Complete settings structure
        :rtype: struct
        """
        return self.cfg
    # /get()

    def reload(self, cfg_file='cfg/app.yaml'):
        """Reload all settings from configuration files.

        Local settings structure will be updated as well

        :returns: complete settings
        :rtype: struct
        """
        with open(cfg_file, 'r') as f:
            try:
                cfg = yaml.load(f, Loader=yaml.CLoader)
            except yaml.YAMLError as e:
                msg = f'Can not parse data in {cfg_file} file:\n{e}'
                self.logger.error(msg)
                raise yaml.YAMLError(msg)
        # /Read configuration file

        self.logger.debug('Loaded configuration:\n{}'.format(cfg))

        self.cfg = cfg
        return cfg
    # /reload()
# /LoadCfgCls
