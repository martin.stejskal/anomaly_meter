#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
GUI which allow user to setup connection
=============================================

Allow to select port, check status and so on

``author`` Martin Stejskal
"""
from lib.common import CmnLgrCls


class ConnectionCls:
    def __init__(self):
        self.lgr = CmnLgrCls('Password - GUI')
    # /__init__()
# /ConnectionCls
