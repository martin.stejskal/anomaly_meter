#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
Main control window
=============================================

Handle other frames and functionality.

``author`` Martin Stejskal
"""
import tkinter
from typing import Type

from lib.common import CmnLgrCls, DummyCls
from lib.service_ui_core import ServiceUiCore
from lib.load_cfg import LoadCfgCls

from .common import CmnSubmoduleParamsCls
from .connection import ConnectionCls
from .mac import MacCls


class ControlWCls:
    def __init__(
        self,
        core: Type[ServiceUiCore],  # Service UI core
        cfg_handler: Type[LoadCfgCls]  # Initialized structure
    ):
        self.lgr = CmnLgrCls('Control window')

        # Store given parameters into instance
        self.core = core
        self.cfg_handler = cfg_handler  # Whole handler
        self.cfg = cfg_handler.get()    # Only configuration as dictionary

        # Initialize runtime variables
        self.rt = _ControlWRtCls()

        self.control_w = tkinter.Tk()
        self.control_w.title("...:: Loading ::..")

        # Windows should be automatically changed, but it does not work always
        # well
        self.control_w.geometry("200x150")

        # Define "on close" action
        self.control_w.protocol("WM_DELETE_WINDOW", self.close)

        # Create menu bar - will be filled later on
        self.rt.menu_bar = tkinter.Menu(self.control_w)
        self.control_w.config(menu=self.rt.menu_bar)

        # Add frames
        self._add_gui_frames()

        # Put submodules into frames and menu bar
        self._init_submodules()

        self.control_w.title("MAC Klikator")
        # Show it!
        self.update()
    # /__init__()

    def is_closing(self) -> bool:
        """Tells upper layer is GUI is closing or not
        """
        return self.rt.is_closing
    # /is_closing()

    def update(self):
        """Update GUI widgets

        Need to be called periodically in order to figure out if user did
        some action
        """
        if(self.is_closing()):
            self.lgr.warn(
                "Received request to update, but GUI is in closing phase")
            return
        # /if closing

        # Resize master windows as needed
        self.control_w.geometry("")

        # And update widgets - this will render changes
        self.control_w.update()
    # /update()

    def close(self):
        """Called when main windows is closing or externally"""

        """Just for reference
        if(messagebox.askokcancel("Quit", "Do you want quit application?")):
            self.rt.is_closing = True
            # And close console API
            self.close()
        """
        self.rt.is_closing = True

        # if auto log out was enabled and device is connected
        if(self.cfg['basic']['auto_logout'] and self.core.is_connected()):
            # If device was connected, gracefully disconnect
            self.core.disconnect()
    # /close()

    # ===========================| Internal functions |=======================
    def _add_gui_frames(self):
        """Add required frames to GUI
        """
        self.rt.f.mac = tkinter.Frame(
            self.control_w, borderwidth=2, relief=tkinter.RIDGE)
        self.rt.f.mac.grid(column=0, row=0, columnspan=1, rowspan=1)
    # /_add_gui_frames()

    def _init_submodules(self):
        """Initialize GUI submodules
        """
        # MAC have to be initialized before connection in order to allow
        # connection module pass callback when connection state changes
        # No need to pass "menu bar" = it should not be used anyway
        self.rt.m.mac = MacCls(params=CmnSubmoduleParamsCls(
            service_ui_core=self.core,
            cfg=self.cfg,
            parent_frame=self.rt.f.mac
        ))

        # No need to pass "parent frame" - it should not use it anyway
        self.rt.m.connection = ConnectionCls(params=CmnSubmoduleParamsCls(
            service_ui_core=self.core,
            cfg=self.cfg,
            menu_bar=self.rt.menu_bar
        ), connection_change_callback=self.rt.m.mac.connection_state_changed)

    # /_init_submodules()
# /ControlWCls


class _ControlWRtCls:
    """Most of runtime variables under same hood
    """

    def __init__(self):
        # Changed to True when there is request to close window
        self.is_closing = False

        # Keeps GUI frames
        self.f = DummyCls()

        # Keep GUI modules
        self.m = DummyCls()
    # /__init__()
# /_ControlWRtCls
