#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
MAC related GUI component
=============================================

Handle all operations with MAC

``author`` Martin Stejskal
"""
import tkinter
from tkinter import messagebox
from tkinter import filedialog
import yaml
from typing import Type, Optional

from lib.common import CmnLgrCls
from .common import CmnSubmoduleParamsCls


class MacCls:
    def __init__(self, params: Type[CmnSubmoduleParamsCls]):
        self.lgr = CmnLgrCls('MAC GUI')

        self.params = params

        self._init_gui_widgets()
    # /__init__()

    def connection_state_changed(self, connected: bool):
        """Call when connection state was changed
        """
        self.lgr.info(f"State changed to connected: {connected}")

        # Collect all widgets that are related
        widgets = [self.tk_load_from_dev, self.tk_save_to_dev,
                   self.tk_del_all_in_dev]

        if(connected):
            state = tkinter.NORMAL
        else:
            state = tkinter.DISABLED

        # Apply state for all widgets
        for widget in widgets:
            widget['state'] = state
    # /connection_state_changed()
    # ===========================| Internal functions |=======================

    def _init_gui_widgets(self):
        """Initialize GUI widgets
        """
        parent_frame = self.params.parent_frame

        # First initialize action buttons
        row = 0

        module_name = tkinter.Label(parent_frame, text="MAC")
        module_name.grid(column=0, row=row, columnspan=10, rowspan=1)
        row += 1

        # Require to be connected -> by default disabled
        self.tk_load_from_dev = tkinter.Button(
            parent_frame, text="Load from device",
            command=self._load_from_device, state=tkinter.DISABLED)
        self.tk_load_from_dev.grid(column=0, row=row)
        row += 1

        # Require to be connected -> by default disabled
        self.tk_save_to_dev = tkinter.Button(
            parent_frame, text="Save to device", command=self._save_to_device,
            state=tkinter.DISABLED)
        self.tk_save_to_dev.grid(column=0, row=row)
        row += 1

        tk_load_from_file = tkinter.Button(
            parent_frame, text="Load from file", command=self._load_from_file)
        tk_load_from_file.grid(column=0, row=row)
        row += 1

        tk_save_to_file = tkinter.Button(
            parent_frame, text="Save to file", command=self._save_to_file)
        tk_save_to_file.grid(column=0, row=row)
        row += 1

        # Require to be connected -> by default disabled
        self.tk_del_all_in_dev = tkinter.Button(
            parent_frame, text="Delete all from device",
            command=self._delete_all_in_device, state=tkinter.DISABLED)
        self.tk_del_all_in_dev.grid(column=0, row=row)
        row += 1

        # Now setup text input.
        self.tk_mac_list_edit = tkinter.Text(
            parent_frame, height=30, width=40)
        self.tk_mac_list_edit.grid(column=1, row=1, rowspan=row, sticky='nsew')

        scrollbar = tkinter.Scrollbar(
            parent_frame, command=self.tk_mac_list_edit.yview)
        scrollbar.grid(column=2, row=1, rowspan=row, sticky='nsew')

        self.tk_mac_list_edit['yscrollcommand'] = scrollbar.set
    # /_init_gui_widgets()

    def _load_from_device(self):
        """Load list of MAC from device
        """
        mac_list = self.params.service_ui_core.get_mac_list()

        self.lgr.info(
            f"MAC list:\n-----------------\n{mac_list}\n-----------------")

        # Delete what is already there
        self.tk_mac_list_edit.delete('1.0', tkinter.END)
        self.tk_mac_list_edit.insert('1.0', mac_list)
    # /_load_from_device()

    def _save_to_device(self):
        """Save current MAC list to device
        """
        # Dump all data into string
        mac_list_txt = self.tk_mac_list_edit.get('1.0', tkinter.END)
        is_mac_list_valid, mac_list = self._is_mac_list_text_valid(
            mac_list=mac_list_txt)

        if(is_mac_list_valid):
            # MAC list is valid -> can store it inside device
            self.params.service_ui_core.set_mac_list(
                mac_list=mac_list, erase_all_stored_mac=True)
    # /save_to_dev()

    def _load_from_file(self):
        """Load MAC list from file
        """
        filename = filedialog.askopenfilename(
            title="Load MAC list",
            filetypes=(("MAC list", "*.yaml"),),)

        if(len(filename) == 0):
            # Nothing selected -> just exit
            return

        with open(filename, 'r') as f:
            try:
                yaml_string = yaml.load(f, Loader=yaml.CLoader)
            except yaml.YAMLError as e:
                msg = f'Can not parse data in {filename}:\n{e}'
                self.logger.error(msg)
                messagebox.showerror(
                    title="Invalid input data",
                    message=msg)
                return
        # /try to load file

        mac_list = self._dict_to_mac_list(input_dict=yaml_string)

        is_mac_list_valid, mac_list = self._is_mac_list_text_valid(
            mac_list=mac_list, show_error_box=True)

        if(is_mac_list_valid):
            # MAC is valid -> load to text field. Delete current
            self.tk_mac_list_edit.delete('1.0', tkinter.END)
            self.tk_mac_list_edit.insert('1.0', mac_list)
        else:
            # user was warned that file is not OK already
            pass

    # /_load_from_file()

    def _save_to_file(self):
        """Save MAC list to file
        """
        # Dump all data into string
        mac_list_txt = self.tk_mac_list_edit.get('1.0', tkinter.END)
        is_mac_list_valid, mac_list = self._is_mac_list_text_valid(
            mac_list=mac_list_txt, show_error_box=True)

        # Function above take care about message box
        if(not is_mac_list_valid):
            return
        # if MAC is not valid
        filename = filedialog.asksaveasfilename(
            title="Save MAC list",
            filetypes=(("MAC list", "*.yaml"),),
            confirmoverwrite=True)

        if(len(filename) == 0):
            self.lgr.info("No file selected. MAC list is not stored")
            return
        # /if clicked to cancel or not selected any file

        self.lgr.debug("Save as {}".format(filename))

        yaml_string = self._mac_list_to_dict(mac_list)

        with open(filename, 'w') as f:
            yaml.dump(yaml_string, f, indent=4)
    # _save_to_file()

    def _delete_all_in_device(self):
        """Delete all MAC in device
        """
        # Clean up text box also
        self.tk_mac_list_edit.delete('1.0', tkinter.END)

        self.params.service_ui_core.del_mac_list()
    # /_delete_all_in_device()

    def _is_mac_list_text_valid(
        self, mac_list, show_error_box: Optional[bool] = True)\
            -> [bool, str]:
        """Check if data in text box are valid

        :returns: True if data are valid. Second parameter is string from text
                  box
        """
        is_mac_list_valid = False

        # Check if input MAC list is valid
        error_description = self.params.service_ui_core.check_mac_list(
            mac_list=mac_list)

        # If there is some error when verifying, inform user
        if(len(error_description)):
            # Invalid MAC list -> tell user
            self.lgr.info(f"Invalid user input. {error_description}")

            if(show_error_box):
                messagebox.showerror(
                    title="Invalid input data",
                    message=error_description
                )
            # /if required to show error to user
        else:
            # Everything is fine
            is_mac_list_valid = True

        return is_mac_list_valid, mac_list
    # /_is_mac_list_text_valid()

    def _mac_list_to_dict(self, mac_list: str) -> dict:
        """Convert MAC list string into dictionary form for YAML file
        """
        mac_list_array = []

        for line in mac_list.split('\n'):
            # Ignore empty lines
            if(len(line) <= 1):
                continue

            mac_list_array.append(line)
        # /for all lines

        output = {'mac_list': mac_list_array}

        return output
    # /_mac_list_to_dict()

    def _dict_to_mac_list(self, input_dict: dict) -> str:
        """Convert MAC list in dictionary into list as string
        """
        output = ""

        if(not ('mac_list' in input_dict)):
            # There is no keyword -> return "nothing"
            return output
        # /if there is no MAC list keyword

        # OK, something should be there
        for line in input_dict['mac_list']:
            output = f'{output}\n{line}'

        # Remove leading new line
        output = output[1:]

        return output
    # /_dict_to_mac_list()
# /MacCls
