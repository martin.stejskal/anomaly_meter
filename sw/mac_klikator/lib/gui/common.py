#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
Common parts for GUI submodules
=============================================

Mainly "structures", which can be easily passed as one argument into submodules

``author`` Martin Stejskal
"""
import tkinter
from typing import Type, Optional

from lib.service_ui_core import ServiceUiCore


class CmnSubmoduleParamsCls:
    def __init__(
            self,
            service_ui_core: Type[ServiceUiCore],  # Service UI core
            cfg: dict,  # Loaded configuration
            parent_frame: Optional[tkinter.Frame] = None,  # GUI parent frame
            menu_bar: Optional[tkinter.Menu] = None,   # GUI Menu bar object
    ):
        # Store parameters into instance
        self.service_ui_core = service_ui_core
        self.cfg = cfg
        self.parent_frame = parent_frame
        self.menu_bar = menu_bar
    # /__init__()
# /CmnSubmoduleParamsCls
