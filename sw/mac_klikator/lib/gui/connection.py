#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
GUI which allow user to setup connection
=============================================

Allow to select port, check status and so on

``author`` Martin Stejskal
"""
import tkinter
from tkinter import messagebox
from typing import Type, Optional, Callable

from lib.common import CmnLgrCls
from .common import CmnSubmoduleParamsCls


class ConnectionCls:
    def __init__(
            self,
            params: Type[CmnSubmoduleParamsCls],
            connection_change_callback:
            Optional[Callable[[bool], None]] = None):
        self.lgr = CmnLgrCls('Connection - GUI')

        self.params = params
        self.conn_change_callback = connection_change_callback

        if(self.params.cfg['basic']['auto_connect']):
            self._check_if_uart_port_is_avilable()

        self.rt = _ConnectionRtCls(self.params)

        self._init_gui_widgets()

        # If auto connection is enabled, let's try to connect
        if(self.params.cfg['basic']['auto_connect']):
            self._show_dialog(text="Searching for device...")

            self._connect_to_and_update_status(
                port=self.rt.selected_port.get())

            self._close_dialog()
        # /try connect
    # /__init__()

    # ===========================| Internal functions |=======================
    def _init_gui_widgets(self):
        """Initialize GUI widgets
        """
        self._regenerate_submenu()

        self.params.menu_bar.add_cascade(label="Connection", menu=self.submenu)
    # /_init_gui_widgets()

    def _regenerate_submenu(self):
        """Refesh submenu according to current settings
        """
        self.submenu = tkinter.Menu(self.params.menu_bar, tearoff=0)

        self.submenu.add_radiobutton(
            label="Auto", value="auto", variable=self.rt.selected_port,
            command=self._on_port_change)

        self.submenu.add_separator()
        # Rest will be automatically generated
        available_ports = self.params.service_ui_core.get_available_ports()

        for port in available_ports:
            self.submenu.add_radiobutton(
                label=port, value=port, variable=self.rt.selected_port,
                command=self._on_port_change)
        # /for all available ports

        self.submenu.add_separator()

        self.submenu.add_command(
            label="Refresh port list", command=self._update_port_list)

        # Allow to disconnect if connected
        self.submenu.add_command(
            label="Disconnect", command=self._disconnect_and_update_status,
            state=tkinter.DISABLED)
        # Need to extract item index for status, so it can be changed later
        self.rt.disable_button_idx = self.submenu.index(tkinter.END)

        # Add check button, which reports status (connected/disconnected)
        self.submenu.add_checkbutton(
            label="Loading...", variable=self.rt.connected, onvalue=True,
            offvalue=False, state=tkinter.DISABLED)
        # Need to extract item index for status, so it can be changed later
        self.rt.status_checkbutton_idx = self.submenu.index(tkinter.END)

        # And update status according to the current state
        self._update_status_in_submenu()
    # /_regenerate_submenu()

    def _show_dialog(self, text: str,):
        """Show dialog with some text. Can be closed later
        """
        self.top_dialog = tkinter.Toplevel()
        self.top_dialog.title('Status')
        tkinter.Message(self.top_dialog, text=text).grid()

        self.top_dialog.update()
    # /_show_connecting_dialog()

    def _close_dialog(self):
        """Close previously opened dialog

        If dialog was not created yet, it will cause error
        """
        self.top_dialog.destroy()
    # /_close_dialog()

    def _connect_to_and_update_status(self, port: str):
        """Try to connect to given port
        """
        core = self.params.service_ui_core

        # If already connected, then it is good idea to disconnect first before
        # trying to connect to the another port
        if(core.is_connected()):
            core.disconnect()

        try:
            core.connect(port=self.rt.selected_port.get())
        except ConnectionError as e:
            msg = f'Connection failed :(\n{e}'
            self.lgr.error(msg)
            messagebox.showerror(
                title="Connection failed", message=msg)
        except PermissionError as e:
            msg = f'Device found, but login failed :(\n{e}'
            self.lgr.error(msg)
            messagebox.showerror(
                title="Can not log in", message=msg)
        else:
            # Successfully connected
            pass

        # Does not matter if connected or not. Check status either way
        self._update_status_in_submenu()

        # As last step, call callback function (if not empty)
        if(self.conn_change_callback):
            self.conn_change_callback(core.is_connected())
    # /_connect_to_and_update_status()

    def _disconnect_and_update_status(self):
        """Disconnect from device
        """
        assert(self.params.service_ui_core.is_connected())
        self.params.service_ui_core.disconnect()

        self._update_status_in_submenu()

        # As last step, call callback function (if not empty)
        if(self.conn_change_callback):
            self.conn_change_callback(
                self.params.service_ui_core.is_connected())
    # /_disconnect_and_update_status()

    def _update_port_list(self):
        """Update available port list
        """
        # Need to remove current cascade from menu bar
        self.params.menu_bar.delete("Connection")

        self._regenerate_submenu()

        self.params.menu_bar.add_cascade(label="Connection", menu=self.submenu)
    # /_update_port_list()

    def _on_port_change(self):
        """Callback used when user select port option
        """
        selected_port = self.rt.selected_port.get()
        self.lgr.debug(f'Selected new port {selected_port}')

        self._connect_to_and_update_status(port=selected_port)
    # _on_port_change()

    def _update_status_in_submenu(self):
        """Update status label in menu bar
        """
        if(self.params.service_ui_core.is_connected()):
            # This will set "check" icon next to the text status
            self.rt.connected.set(True)

            self.submenu.entryconfigure(
                self.rt.status_checkbutton_idx, label="Connected")

            self.submenu.entryconfigure(
                self.rt.disable_button_idx, state=tkinter.NORMAL)
        else:
            self.rt.connected.set(False)

            self.submenu.entryconfigure(
                self.rt.status_checkbutton_idx, label="Disconnected")

            self.submenu.entryconfigure(
                self.rt.disable_button_idx, state=tkinter.DISABLED)
    # /_update_status_in_submenu()

    def _check_if_uart_port_is_avilable(self):
        """Check if UART port is available

        If not available, inform user and change back to "auto"
        """
        if('auto' == self.params.cfg['basic']['uart_port']):
            # Automatic - not much to do
            return

        # Else port is not set to "auto"
        available_ports = self.params.service_ui_core.get_available_ports()
        selected_port = self.params.cfg['basic']['uart_port']
        if(selected_port in available_ports):
            # Port set by user was found -> OK
            pass
        else:
            # Port set by user was not found -> need to change configuration
            messagebox.showwarning(
                title='Port not found',
                message=f'Port {selected_port} not found!\nChanging to auto')
            self.params.cfg['basic']['uart_port'] = 'auto'
# /ConnectionCls


class _ConnectionRtCls:
    def __init__(self, params: Type[CmnSubmoduleParamsCls]):
        """Initialize some runtime variables for connection class
        """
        # Initialize selected port variable for Tkinter and set default value
        self.selected_port = tkinter.StringVar()
        self.selected_port.set(params.cfg['basic']['uart_port'])

        # Initial state is "not connected"
        self.connected = tkinter.BooleanVar()
        self.connected.set(False)

        self.status_checkbutton_idx = None
        self.disable_button_idx = None
    # /__init__()
# /_ConnectionRtCls
