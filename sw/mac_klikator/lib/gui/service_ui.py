#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
Front-end for service UI core
=============================================

Interface for BFU. It should allow to do most annoying things easily

``author`` Martin Stejskal
"""
from time import sleep
# Because of annotations
from typing import Optional, Type

from lib.common import CmnLgrCls
from lib.load_cfg import LoadCfgCls
from lib.service_ui_core import ServiceUiCore

from .control_w import ControlWCls


class ServiceUiGui:
    def __init__(
            self,
            cfg_handler: Type[LoadCfgCls]  # Initialized structure
    ):
        self.lgr = CmnLgrCls('Service UI - GUI')

        # Store configuration safe and sound
        self.cfg_handler = cfg_handler

        # Initialize service core
        self.core = ServiceUiCore(pwd=self.cfg_handler.get()[
                                  'basic']['ui_passwd'])

        # Initialize GUI itself
        self.gui = ControlWCls(core=self.core, cfg_handler=self.cfg_handler)
    # /__init__()

    def run_once(self):
        """Run "one" round which update GUI widgets
        """
        self.gui.update()
    # /run_once()

    def run_blocking(
            self, sleep_time_ms: Optional[int] = 100):
        """The "infinite" loop which ends as soon as GUI is closed
        """
        while(not self.gui.is_closing()):
            self.run_once()
            sleep(sleep_time_ms/1000)
        # /infinite loop
    # /run_blocking()
# /ServiceUiGui
