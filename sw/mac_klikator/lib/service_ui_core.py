#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
Deals with device service UI
=============================================

Take care about connection, setting and getting parameters

``author`` Martin Stejskal
"""
import serial
import serial.tools.list_ports
from time import perf_counter
from typing import Optional

from lib.common import CmnLgrCls


class ServiceUiCore:
    # Typical reply from device before login
    dev_signature = "\n=== DAZ ====\n== Login =="

    # What appears when successfully logged in
    after_login_signature = "\n=== DAZ ====\n==<| Help |>=="

    ok_signature = "[OK]"
    fail_signature = "[FAIL]"

    # When command is finished, it should show "command prompt". This basically
    # tells, that previous command was finished and device is ready to receive
    # another command
    cmd_finished_signature = "$ "

    # Define timeout for logging in. Note that during login request there can
    # be running scan or some time consuming operation and it can block login
    # process for some time. Set it wisely
    timeout_when_login_sec = 5

    def __init__(self, pwd: Optional[str] = None):
        """Initialize module

        Possible to setup password during initialization
        """
        self.lgr = CmnLgrCls('Service UI core')

        # Other runtime variables under one hood
        self.rt = _ServiceUiCoreRtCls()

        # If password is defined, change default
        if(pwd):
            self.rt.pwd = pwd
    # /__init__()

    def connect(self, port: Optional[str] = "auto"):
        """Try to connect to predefined port in configuration

        Raise ConnectionError if connection fails. Raise PermissionError if
        connection is successful, but login fails.
        """
        if(self.rt.connected):
            msg = "Already connected! If you want to reconnect, you need to "\
                "disconnect first"
            self.lgr.error(msg)
            raise RuntimeError(msg)
        # /if already connected

        if(port == "auto"):
            # Try to search device
            for port_tmp in self.get_available_ports():
                try:
                    self._try_connect_to(port_tmp)
                except ConnectionError as e:
                    self.lgr.info(e)
                    # OK, this port failed -> try another
                    continue

                # It is also possible that someone forget to log out, so
                # eventually device can be found
                if(not self.rt.logged):
                    try:
                        self._login()
                    except PermissionError as e:
                        self.lgr.info(f'Can not login at {port_tmp}\n{e}')
                        # This failed, let's try another port then
                        continue
                # /if not logged yet

                # OK, if login passed, then that is what we need
                msg = f'Successfully connected and logged at {port_tmp}'
                self.lgr.info(msg)
                break
            # /go through all ports
        else:
            # Port is strictly defined
            self._try_connect_to(port)

            # Try to log in if not logged yet (it is possible that session was
            # left open)
            if(not self.rt.logged):
                self._login()
    # /connect()

    def disconnect(self):
        """Disconnect from device gracefully

        It will exit service session and return device back to normal mode
        """
        if(not self.rt.connected):
            # Not connected, can not disconnect
            msg = 'Not connected so I can not disconnect'
            self.lgr.error(msg)
            raise RuntimeError(msg)
        # /if not connected

        # Logout from system
        self._send_cmd(cmd="exit", get_reply=False)

        # Assume that logout went OK
        self.rt.logged = False

        self.rt.uart.close()
        self.rt.connected = False
    # /disconnect()

    def get_available_ports(self) -> list:
        """List all available ports
        """
        available_ports = []

        for port_obj in serial.tools.list_ports.comports():
            port_name = port_obj[0]

            # TODO: This does not work well on Linux. Need to investigate more
            try:
                serial_tmp = serial.Serial(
                    port=port_name, timeout=0.01, baudrate=115200)
                serial_tmp.read()
            except serial.serialutil.SerialException:
                self.lgr.warn(f'Filtering out {port_name} because is used')
            else:
                # Port is free -> add to list
                available_ports.append(port_name)
        # /for all available ports

        self.lgr.debug(f'List of found ports: {available_ports}')
        return available_ports
    # /get_available_ports()

    def is_connected(self) -> bool:
        """Tell whether is core connected to device or not
        """
        if(self.rt.connected and self.rt.logged):
            return True
        else:
            return False
    # /is_connected()

    def get_mac_list(self) -> list:
        """Returns list of MAC address loaded from device

        Raise RuntimeError if something went wrong
        """
        self._check_if_connected()

        mac_list = ""

        reply = self._send_cmd(cmd="get mac list")

        if("<No MAC registered yet>" in reply):
            # Nothing to process -> return empty list
            self.lgr.debug("MAC list is empty")
            return mac_list

        # Else process what was received
        self._check_signatures(reply=reply)

        # Everything before OK signature is meaningless for us now
        reply = reply.split(self.ok_signature)[1]

        # Also do not need tailing "finished signature" here
        reply = reply.split(self.cmd_finished_signature)[0]

        # Get rid of redundant new lines
        reply = reply.replace('\n\n', '\n')

        # Get rid of leading new line if it is there
        if(reply[0] == '\n'):
            reply = reply[1:]

        # Remove also tailing new line if it is there
        if(reply[-1] == '\n'):
            reply = reply[:-1]

        return reply
    # /get_mac_list()

    def set_mac_list(
            self,
            mac_list: str,
            erase_all_stored_mac: Optional[bool] = True):
        """Set given MAC list

        :param mac_list: MAC list as string
        :param erase_all_stored_mac: If set, before writing new list, it will
                                     erase all MAC in device memory
        """
        error_description = self.check_mac_list(mac_list=mac_list)

        # If there is any error
        if(len(error_description)):
            msg = 'Can not set given MAC list, because it is not valid\n'\
                  f'{error_description}'
            self.lgr.warn(msg)
            raise RuntimeError(msg)
        # /if MAC list is not correct

        if(erase_all_stored_mac):
            self.del_mac_list()

        # Set MAC one by one
        for mac_item in mac_list.split('\n'):
            # Skip empty lines
            if(len(mac_item) == 0):
                continue

            reply = self._send_cmd(cmd=f'set mac {mac_item}')

            self._check_signatures(reply=reply)
        # /set MAC one by one

        self.lgr.info('MAC list set')
    # /set_mac_list()

    def del_mac_list(self):
        """Delete all registered MAC in the device

        Raise RuntimeError is something goes wrong
        """
        self._check_if_connected()

        reply = self._send_cmd(cmd="del mac all")

        self._check_signatures(reply=reply)
    # /del_mac_list()

    def check_mac_list(self, mac_list: str) -> str:
        """Check if given MAC list is valid or not

        :param mac_list: MAC list as string in format: <MAC> [Description]
        :returns: Empty string if MAC is valid. If there is some invalid
                  record, it will return that line or describe problem. Can be
                  used for informing user
        """
        # If list is empty. At least few new lines should be there
        if(len(mac_list) == 0):
            return "MAC list is empty"

        # Expected format is: 00:12:34:56:be:ef TestDevice1

        # Check for positions of colons
        for line in mac_list.split('\n'):
            # Skip empty lines
            if(len(line) == 0):
                continue

            # At least MAC should be there -> 2*6 + 5
            if(len(line) < (2*6 + 5)):
                # Too short - not hold MAC for sure
                return f"Too short record. MAC can not fit there\n{line}"

            if(not self._is_char_on_position(text=line, character_list=[':'],
                                             position_list=[2, 5, 8, 11, 14])):
                return f"Invalid MAC format. Missing colon\n{line}"
            # /if columns are not at correct place

            # Check 0-f(F) on other positions
            if(not self._is_char_on_position(
                    text=line, character_list=[
                        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
                        'a', 'b', 'c', 'd', 'e', 'f',
                        'A', 'B', 'C', 'D', 'E', 'F'],
                    position_list=[0, 1, 3, 4, 6, 7, 9, 10, 12, 13, 15, 16])):
                return f"Invalid MAC format. Used non-hex digits\n{line}"
        # /for all lines

        return ""
    # /check_mac_list()
    # ===========================| Internal functions |=======================
    # ====================| Internal functions - high level |=================

    def _try_connect_to(self, port: str):
        """Try connect to given port

        If connection fails, it will raise ConnectionError
        """
        self.lgr.debug(f'Trying port {port}')

        self.rt.uart.port = port

        try:
            self.rt.uart.open()
        except Exception as e:
            msg = f'Can not open port {port}\n{e}'
            self.lgr.debug(msg)
            raise ConnectionError(msg)

        # Port open, try to figure out if correct device is connected to this
        # port
        reply = self._send_cmd(cmd='who are you')

        if(self.after_login_signature in reply):
            # Seems that device is already unlocked.
            self.lgr.info("Already logged in device")

            self.rt.logged = True

        elif(self.dev_signature in reply):
            # Device located. Now it is required to login
            self.lgr.info("Device found. Logging in...")

        else:
            msg = f'Device not found on the port {port}'
            self.lgr.debug(msg)

            # Need to unblock port. The "connected" flag is not set -> just
            # disconnect via UART object
            self.rt.uart.close()
            raise ConnectionError(msg)

        # So far so good, so set status to connected
        self.rt.connected = True
    # /_try_connect_to()

    def _login(self):
        """Try to login into device

        Raise PermissionError if logging is not successful. Expecting that
        UART is connected to correct device already
        """
        assert(self.rt.connected)
        assert(not self.rt.logged)

        # Calculate timeout
        timeout_sec = perf_counter() + self.timeout_when_login_sec

        # Send password. Device should reply promptly
        reply = self._send_cmd(cmd=self.rt.pwd)

        # Check if password is correct
        if(self.ok_signature in reply):
            while(perf_counter() < timeout_sec):
                if(self.after_login_signature in reply):
                    # Login successful
                    self.lgr.info("Login successful")
                    self.rt.logged = True

                    return
                else:
                    # Device still doing something. Try again
                    reply = self._get_reply()
            # /before timeout occurs

            # if we go here, timeout happened -> report as error. However this
            # should not happen normally.
            msg = 'Timeout when waiting for login signature'
            self.lgr.error(msg)

            raise RuntimeError(msg)
        else:
            msg = f'Login failed\n{reply}'
            self.lgr.error(reply)

            # OK, so this port might not be the right one. Close current port
            self.disconnect()

            assert(not self.rt.connected)
            assert(not self.rt.logged)

            raise PermissionError(reply)
    # _login()

    # ===================| Internal functions - middle level |================
    def _check_signatures(
            self,
            reply: str,
            check_for_ok: Optional[bool] = True,
            check_for_fail: Optional[bool] = True):
        """Check if signatures are fine

        :param check_for_ok: If enabled, check if "OK signature" is in reply.
                             If not, raise RuntimeError
        :param check_for_fail: If enabled, check that "fail signature" is NOT
                             in reply. If "fail signature" is there, it will
                             throw RuntimeError
        """
        if(check_for_ok):
            if(not (self.ok_signature in reply)):
                msg = f"OK signature not found in:\n{reply}"
                raise RuntimeError(msg)
        # /if check for OK

        if(check_for_fail):
            if(self.fail_signature in reply):
                msg = f'Fail signature found in:\n{reply}'
                raise RuntimeError(msg)
        # /if check for fail
    # _check_signatures()

    def _is_char_on_position(
            self, text: str, character_list: list, position_list: list
    ) -> bool:
        """Check if given character is on required positions

        :param text: Input text as string
        :param character_list: List of characters. If at least 1 character in
                               this list is found on given position, it is
                               evaluated as pass for that position in the list
        :param position_list: List of positions where given character should be
                              checked
        """
        # Every character should be only 1 character. Not less, not more
        for character in character_list:
            assert(len(character) == 1)

        for position in position_list:
            if(text[position] in character_list):
                # At least one character from list was on given position
                pass
            else:
                # Given character is not found on required position -> return
                return False
        # /for all required positions

        # Else character was found at all required positions
        return True
    # /_is_char_on_position()
    # =====================| Internal functions - low level |=================

    def _check_if_connected(self):
        """Check if device is connected

        If device is not connected, throw RuntimeError
        """
        if(not self.is_connected()):
            msg = 'Device is not connected'
            raise RuntimeError(msg)
    # /_check_if_connected()

    def _send_cmd(
            self,
            cmd=str,
            get_reply: Optional[bool] = True,
            reply_timeout_sec: Optional[int] = None,
            clear_input_buffer: Optional[str] = True) -> str:
        """Send command over UART to device and collect output
        """
        if(clear_input_buffer):
            self.rt.uart.reset_input_buffer()

        # Log command - for debug purpose
        self.lgr.debug(f'TX cmd: {cmd}')

        cmd = f'{cmd}\n'
        self.rt.uart.write(bytes(cmd, encoding='utf-8'))

        reply = ""
        if(get_reply):
            reply = self._get_reply(timeout_sec=reply_timeout_sec)

        # Return some reply always
        return reply
    # /_send_cmd()

    def _get_reply(
            self,
            timeout_sec: Optional[int] = None,
            size: Optional[int] = 999999):
        """Get reply from UART

        If timeout is not defined, default is used
        """
        if(timeout_sec):
            self.rt.uart.timeout = timeout_sec

        try:
            reply = self.rt.uart.read(size=size)
        except serial.serialutil.SerialException as e:
            self.lgr.warning(f'Port {self.rt.uart.port} is already used\n{e}')
            reply = bytes()

        if(len(reply)):
            self.lgr.debug(f'Device reply: {reply}')
        else:
            self.lgr.debug('Device reply: <Empty>')

        # If timeout was changed, set it back to default
        if(timeout_sec):
            self.rt.uart.timeout = self.rt.default_timeout_sec

        try:
            reply = reply.decode('utf-8')
        except UnicodeDecodeError:
            msg = 'Received binary data, but expected string :('
            reply = ""
            self.lgr.warn(msg)

        return reply
    # /_get_reply()
# /ServiceUiCore


class _ServiceUiCoreRtCls:
    def __init__(self):
        # Is device connected?
        self.connected = False

        # Are we logged in?
        self.logged = False

        # Default timeout for UART RX function
        self.default_timeout_sec = 0.3

        # Basic UART settings
        self.uart = serial.Serial(
            baudrate=115200, bytesize=serial.EIGHTBITS,
            parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
            timeout=self.default_timeout_sec)

        # Default password when trying to communicate with device
        self.pwd = "admin"
    # /__init__()
# /_ServiceUiCoreRtCls
