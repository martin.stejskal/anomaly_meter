#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
==================================================
Everything you need to know about current version
==================================================

Extract version from GIT version system

``author`` Martin Stejskal
"""
import subprocess


def get_git_revision_hash():
    try:
        hash = subprocess.check_output(
            ['git', 'rev-parse', 'HEAD']).decode('ascii').strip()
    except Exception:
        hash = 'Can not determine version'
    return hash
# /get_git_revision_hash()


def get_git_revision_short_hash():
    try:
        hash = subprocess.check_output(
            ['git', 'rev-parse', '--short', 'HEAD']).decode('ascii').strip()
    except Exception:
        hash = 'Can not determine version'
    return hash
# /get_git_revision_short_hash()
