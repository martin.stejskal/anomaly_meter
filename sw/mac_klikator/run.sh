#!/bin/bash
# Get project directory, so everything can be done through absolute paths ->
# -> can call this script from anywhere
this_dir=$( dirname $0 )

cd "$this_dir"
pipenv run python mac_klikator.py
