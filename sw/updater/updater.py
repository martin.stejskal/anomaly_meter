#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
==================================================
Application for easy update SDK & project itself
==================================================

GUI application for easy update SDK & project itself

``author`` Martin Stejskal
"""
import logging.config

from lib.common import CmnLgrCls, app_path
from lib.version import get_git_revision_short_hash
from lib.load_cfg import LoadCfgCls
from lib.gui.updater import UpdaterGuiCls

# Location of configuration file for logging and application configuration
log_cfg_file = "{}/cfg/logging.cfg".format(app_path)
default_cfg_file = "{}/cfg/app.yaml".format(app_path)

# Main function
if __name__ == "__main__":
    """Main function.
    """
    # Start with logger
    # Load configure file for logging
    logging.config.fileConfig(log_cfg_file, None, False)

    # Assign logger to this function
    logger = CmnLgrCls("Updater UI application")
    logger.info("\n\n\n\n\n\n\n"
                "Starting application - GIT version: "
                f"{get_git_revision_short_hash()}"
                "\n\n\n")

    # Load configuration
    load_cfg_handler = LoadCfgCls(default_cfg_file)

    # Run GUI
    gui = UpdaterGuiCls(cfg_handler=load_cfg_handler)
    gui.run_blocking()

    logger.info("Exiting...")
# /main function
