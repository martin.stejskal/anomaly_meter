# About
 * This application ease updating project and ESP IDF.
 * This application needs access to internet in order to download updates.

# Installation
 * Application need few Python packages. Just run `install_dependencies.sh`

# Executing application
 * Recommended way of running application is over virtual environment.
   In short call `run.sh`.
 * Still, you're free to run it in your host Python environment as well, but
   you will need to install dependencies manually.

# Configuration
 * By default it is assumed that ESP IDF is at `~/esp/esp-idf` directory. If
   not, take a look into `cfg/app.yaml` ;)


