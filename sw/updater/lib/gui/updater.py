#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
Front-end for updater core
=============================================

Interface for BFU. It should allow to do most annoying things easily

``author`` Martin Stejskal
"""
from time import sleep
from lib.common import CmnLgrCls, app_path
from .control_w import ControlWCls
from lib.updater_core import UpdaterCoreCls
from pathlib import Path

# Because of annotations
from typing import Optional, Type
from lib.load_cfg import LoadCfgCls


class UpdaterGuiCls:
    def __init__(
            self,
            cfg_handler: Type[LoadCfgCls]  # Initialized structure
    ):
        self.lgr = CmnLgrCls('Updater - GUI')

        cfg = cfg_handler.get()
        esp_idf_path = self._get_path_from_str(cfg['basic']['esp_idf'])
        project_path = Path(app_path)

        core = UpdaterCoreCls(esp_idf_path, project_path)
        self.gui = ControlWCls(core=core, cfg_handler=cfg_handler)
    # /__init__()

    def run_once(self):
        """Run "one" round which update GUI widgets
        """
        self.gui.update()

    # /run_once()

    def run_blocking(
            self, sleep_time_ms: Optional[int] = 100):
        """The "infinite" loop which ends as soon as GUI is closed
        """
        while (not self.gui.is_closing()):
            self.run_once()
            sleep(sleep_time_ms / 1000)
        # /infinite loop

    # /run_blocking()

    def _get_path_from_str(self, path: str) -> Type[Path]:
        """From string path return Path object
        """
        # Expand symbol for home, since pathlib does not do this
        if ("~" in path):
            # Need to convert to string, in order to be able to use in replace
            # function
            home_path = str(Path.home())
            path = path.replace("~", home_path)

        return Path(path)
    # /_get_path_from_str()
# /UpdaterGui
