#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
Main control window
=============================================

Handle other frames and functionality.

``author`` Martin Stejskal
"""
import tkinter
from tkinter import messagebox
from pathlib import Path
# Because of exceptions
import subprocess
from typing import Type, Callable

from lib.common import CmnLgrCls, DummyCls
from lib.load_cfg import LoadCfgCls
from lib.updater_core import UpdaterCoreCls


class ControlWCls:
    def __init__(
            self,
            core: Type[UpdaterCoreCls],  # Service UI core
            cfg_handler: Type[LoadCfgCls]  # Initialized structure
    ):
        self.lgr = CmnLgrCls('Control window')

        # Store given parameters into instance
        self.core = core
        self.cfg_handler = cfg_handler  # Whole handler
        self.cfg = cfg_handler.get()  # Only configuration as dictionary

        # Initialize runtime variables
        self.rt = _ControlWRtCls()

        self.control_w = tkinter.Tk()
        self.control_w.title("...:: Loading ::..")

        # Windows should be automatically changed, but it does not work always
        # well
        self.control_w.geometry("300x150")

        # Define "on close" action
        self.control_w.protocol("WM_DELETE_WINDOW", self.close)

        # Add widgets
        self._add_widgets(self.control_w)

        # Update version info
        self._update_idf_version_label()
        self._update_project_version_label()

        self.control_w.title("Updater")
        # Show it!
        self.update()

    # /__init__()

    def is_closing(self) -> bool:
        """Tells upper layer is GUI is closing or not
        """
        return self.rt.is_closing

    # /is_closing()

    def update(self):
        """Update GUI widgets

        Need to be called periodically in order to figure out if user did
        some action
        """
        if (self.is_closing()):
            self.lgr.warn(
                "Received request to update, but GUI is in closing phase")
            return
        # /if closing

        # Resize master windows as needed
        self.control_w.geometry("")

        # And update widgets - this will render changes
        self.control_w.update()

    # /update()

    def close(self):
        """Called when main windows is closing or externally"""
        self.rt.is_closing = True

    # /close()

    # ===========================| Internal functions |=======================
    def _add_widgets(self, parent_frame: Type[tkinter.Tk]):
        """Add frame and widgets to main window
        """
        main_f = tkinter.Frame(parent_frame, borderwidth=2,
                               relief=tkinter.RIDGE)
        main_f.grid(column=0, row=0)

        # Buttons needs to be enabled/disabled when processing -> self
        self.btn_update_project = tkinter.Button(
            main_f, text="Update project",
            command=self._action_update_project)
        self.btn_update_project.grid(column=0, row=0, columnspan=2)

        self.btn_update_idf = tkinter.Button(
            main_f, text="Update IDF to", command=self._action_update_idf)
        self.btn_update_idf.grid(column=0, row=1)

        # Need to readout from entry later -> that is why self
        self.entry_idf_version = tkinter.Entry(main_f, width=20)
        self.entry_idf_version.grid(column=1, row=1)
        # Set some default text there
        self.entry_idf_version.insert(0, "Type version here")

        # Need to update labels later on -> self
        self.lbl_idf_version = tkinter.Label(
            main_f, text="ESP IDF version: ?")
        self.lbl_idf_version.grid(column=0, row=2, columnspan=2)

        self.lbl_project_version = tkinter.Label(
            main_f, text="Project version: ?")
        self.lbl_project_version.grid(column=0, row=3, columnspan=3)

    # /_add_widgets()

    def _get_idf_version(self) -> str:
        """Returns ESP IDF version as string"""
        callback = self.core.get_idf_version
        return self._get_reply_from_core_callback(callback)

    # /_get_idf_version()

    def _get_project_version(self) -> str:
        """Returns ESP IDF version as string"""
        callback = self.core.get_project_version
        return self._get_reply_from_core_callback(callback)

    # /_get_project_version()

    def _get_reply_from_core_callback(self, callback: Callable):
        """Execute core callback, handle it and return loaded value"""

        try:
            output = callback()
        except FileNotFoundError as error:
            output = "???"
            msg = f"{error}\n\n{error.output.decode('ascii')}"
            self.lgr.error(msg)
            messagebox.showerror(
                title="Something went wrong", message=msg)
        except subprocess.CalledProcessError as error:
            output = "???"
            msg = f"{error}\n\n{error.output.decode('ascii')}"
            self.lgr.error(msg)
            messagebox.showerror(
                title="Something went wrong", message=msg)

        return output

    # /_get_reply_from_core_callback()

    def _action_update_project(self):
        """Update current project"""
        # Disable button to let user know that something happening
        self.btn_update_project['state'] = tkinter.DISABLED
        self.update()

        # The sdkconfig file might be changed automatically by idf.py build
        # due to updates in IDF. Therefore it is better to checkout it. This
        # application is for BFU. Developers will use console anyways :)
        # Path is relative to this project
        checkout_list = [Path("../../fw/sdkconfig")]

        try:
            self.core.update_project(checkout_file_list=checkout_list)
        except subprocess.CalledProcessError as error:
            msg = f"{error}\n\n{error.output.decode('ascii')}"
            self.lgr.error(msg)
            messagebox.showerror(
                title="Update failed :(", message=msg)
        else:
            self.lgr.info("Project update successful")
            self._update_project_version_label()
        finally:
            # Re-enable button
            self.btn_update_project['state'] = tkinter.NORMAL
            self.update()

    # /_action_update_project()

    def _action_update_idf(self):
        """Update ESP IDF to version defined in entry field"""
        # Disable button to let user know that something happening
        self.btn_update_idf['state'] = tkinter.DISABLED
        self.update()

        required_version = self.entry_idf_version.get()
        self.lgr.info(f"Trying to upgrade ESP IDF to {required_version}")

        try:
            self.core.update_idf(idf_version=required_version)
        except subprocess.CalledProcessError as error:
            msg = f"{error}\n\n{error.output.decode('ascii')}"
            self.lgr.error(msg)
            messagebox.showerror(
                title="Update failed :(", message=msg)
        else:
            self.lgr.info("ESP IDF update successful")
            self._update_idf_version_label()
        finally:
            # Re-enable button
            self.btn_update_idf['state'] = tkinter.NORMAL
            self.update()

    # /_action_update_idf()

    def _update_idf_version_label(self):
        """Update ESP IDF version label"""
        idf_version = self._get_reply_from_core_callback(
            self.core.get_idf_version)
        self.lbl_idf_version['text'] = f"ESP IDF version: {idf_version}"

    def _update_project_version_label(self):
        """Update project version label"""
        project_version = self._get_reply_from_core_callback(
            self.core.get_project_version)
        self.lbl_project_version[
            'text'] = f"Project version: {project_version}"


# /ControlWCls


class _ControlWRtCls:
    """Most of runtime variables under same hood
    """

    def __init__(self):
        # Changed to True when there is request to close window
        self.is_closing = False

        # Keeps GUI frames
        self.f = DummyCls()

        # Keep GUI modules
        self.m = DummyCls()
    # /__init__()
# /_ControlWRtCls
