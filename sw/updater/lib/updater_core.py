#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
=============================================
Updated core functionalities
=============================================

Core functionalities without GUI part

``author`` Martin Stejskal
"""
import subprocess
from lib.common import CmnLgrCls

from pathlib import Path
from typing import Type, Optional


class UpdaterCoreCls:
    def __init__(self, esp_idf_path: Type[Path], project_path: Type[Path]):
        self.esp_idf_path = esp_idf_path
        self.project_path = project_path

        self.lgr = CmnLgrCls('Updater core')

        self.lgr.debug(f"ESP IDF path: {esp_idf_path}\n"
                       f"Project path: {project_path}")

        # By default, dry run should be disabled, so commands will be actually
        # executed
        self.dry_run = False

        # Check if directories exists
        if not esp_idf_path.is_dir():
            msg = f"The directory {esp_idf_path} does not exists!"
            self.lgr.error(msg)
            raise FileNotFoundError(msg)

        if not project_path.is_dir():
            # This should not happen, because project directory should be
            # always valid
            msg = f"The directory {project_path} does not exists!"
            self.lgr.error(msg)
            raise FileNotFoundError(msg)

    # /__init__()

    def get_idf_version(self) -> str:
        """Return string which describe IDF version
        """
        return self._execute_process(
            ['git', 'describe', '--abbrev=8'], self.esp_idf_path)

    # /get_idf_version()

    def get_project_version(self) -> str:
        """Return string which describe current project version
        """
        return self._execute_process(
            ['git', 'describe', '--abbrev=8'], self.project_path)

    # /get_project_version()

    def update_idf(self, idf_version: Optional[str] = None,
                   branch: Optional[str] = "master",
                   remote: Optional[str] = "origin"):
        """Update IDF to specific version (tag or hash)

        :param idf_version: Define to which version should be IDF updated.
                            Can be tag or hash.
        :param branch: Tell to which branch should project checkout
        :param remote: Which remote repository should be used
        """
        self._update(branch=branch, remote=remote,
                     checkout_version=idf_version, workdir=self.esp_idf_path)

    # /update_idf()

    def update_project(self, branch: Optional[str] = "master",
                       remote: Optional[str] = "origin",
                       checkout_file_list: Optional[list] = []):
        """Update current project to latest version

        :param branch: Tell to which branch should project checkout
        :param remote: Which remote repository should be used
        :param checkout_file_list: Explicitly define list of Pathlib files
                                   which should be checkout before update.
                                   This might be helpful when some files (like
                                   sdkconfig) are sometimes automatically
                                   changed and then update could fail
        """
        self._update(branch=branch, remote=remote,
                     workdir=self.project_path,
                     checkout_file_list=checkout_file_list)

    # /update_project()

    def set_dry_run_flag(self, dry_run: bool):
        """Enable or disable "dry run" flag

        If dry run flag is enabled, no command will be executed, but they will
        be only logged instead.
        """
        self.lgr.info(f"Settings dry run flag to {dry_run}")
        self.dry_run = dry_run

    # /set_dry_run_flag()

    def get_dry_run_flag(self) -> bool:
        """Returns dry run flag
        """
        return self.dry_run

    # /get_dry_run_flag()

    def _update(self, branch: str = "master",
                remote: str = "origin",
                checkout_version: Optional[str] = None,
                workdir: Optional[Path] = None,
                checkout_file_list: Optional[list] = []):
        """Update Git project with submodules

        :param workdir: Working directory. basically path to project
        :param checkout_version: If specified, after update will be checkout
                                 to this version/tag
        :param branch: Branch which should be updated
        :param remote: Remote repository name
        """
        # Jump to updated branch
        self._execute_process(['git', 'checkout', branch], workdir=workdir)

        # Checkout given list of files
        for file in checkout_file_list:
            self._execute_process(['git', 'checkout', str(file)],
                                  workdir=workdir)

        # Download updates
        self._execute_process(['git', 'fetch', remote], workdir=workdir)
        self._execute_process(
            ['git', 'pull', remote, branch], workdir=workdir)

        if checkout_version:
            self._execute_process(
                ['git', 'checkout', checkout_version], workdir=workdir)
        # /if checkout version of branch is defined

        self._execute_process(
            ['git', 'submodule', 'update', '--init', '--recursive'],
            workdir=workdir)

    # /_update()

    def _execute_process(
            self, args: list, workdir: Optional[Path] = None) -> str:
        """Execute process and if everything is fine, returns output

        If process returns non-zero error code, exception is raised
        """
        if self.dry_run:
            self.lgr.info(f"{workdir}$ {args}")
        else:
            # Execute process and also checks if did not failed
            self.lgr.debug(f"{workdir}$ {args}")
            stdout = subprocess.check_output(
                args, cwd=workdir, stderr=subprocess.STDOUT).decode(
                "ascii").strip()

            self.lgr.debug(f"Output:\n{stdout}")
            return stdout
    # /_execute_process()
# /UpdaterCoreCls
