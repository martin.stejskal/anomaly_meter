# Keyboard calibration
 * [Get into service mode](ui_service.md)
 * Type `calibrate kbrd`
 * Command will guide you

# Compass calibration
 * Turn on device, press "left" button, select "settings"
 * In settings menu select "Compass cal." option
 * Warning message appear. Select "OK"
 * Calibration pictogram appears
 * Slowly rotate by device in all axis (x, y, z)
 * When process is finished, device show settings menu

# Calibrate battery ADC
 * This ADC is used for measuring battery voltage. Because of inaccuracy of
   ADC and other components, initial data are quite misleading.
 * You will need external power supply and multimeter for this task.
 * Set output voltage on external power supply to 4.0 V.
 * To output of power supply also connect multimeter
 * Make sure that voltage is approximately 4.0 V. Voltage higher than 5 V can
   permanently damage charging circuit.
 * Connect external power supply into device. Use short cables to avoid voltage
   loss on power cables. Connect it to same connector as is battery connected.
 * [Get into service mode](ui_service.md)
 * Type command `calibrate bat adc`
 * Process will guide you. Just keep in mind that you need to set voltage 
   accurately
 * Note that this settings will not be erased When `default` command is
   executed.

# Additional calibration offset
 * Even though now everything should be fine, there still can be some
   inaccuracy due to different voltage drop on wires.
 * Power on device, it should operate in *detector* mode.
 * Write down voltage on display. For example **4.15** V.
 * Measure voltage directly on connector where battery is connected. Let's say
   that you measure **4.10** V.
 * Required offset is **-50** mV.
 * [Get into service mode](ui_service.md)
 * Type following command where numeric value is required offset:
   `bat set static offset  -50`.
 * Note that this settings will not be erased When `default` command is
   executed.

# What next
 * [Setting MAC](mac_list.md)
