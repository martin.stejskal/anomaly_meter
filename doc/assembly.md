# Assembly HW
 * Just follow
   [ANMLY_MTR-01_pcb_all_layers.pdf](../hw/export/ANMLY_MTR-01_pcb_all_layers.pdf)
   page 19 and 20
 * Debug connector is not required to assemble unless you want develop this
   device
 * If you do not intend to use external sensors (GY-91), you also do not
   mount appropriate connector
 * Take care about LED polarity. Especially LED connected to the charger IC.

# Build firmware
 * You can use "traditional" way or you can use
   [Docker](https://www.docker.com/) image. Advantage of Docker image is that
   it have all required tools inside and you do not need to mess with any
   packages. Obviously, you need Docker installed.

## Traditional way
 * You need to have
   [ESP IDF](https://todo)
   installed on your system
 * Start console (command line), move to this project and then go to `fw`
   folder
 * If you're running on Linux and ESP IDF is stored at
   `/home/$USER/esp/esp-idf`, you can use prepared script, which will add ESP
   IDF tools to your path:

```bash
fw $ source scripts/source_me.sh  # Paste command without "fw $ "
```

 * If you're using Windows, please follow
   [official guide]()
 * Then build firmware

```bash
fw $ idf.py build
```

 * This will take a while

## Docker way
 * Long story short, there is already script prepared for this:

```bash
fw $ ./scripts/build.sh
```

 * This will take a while

# Connect programmer
 * You need USB UART converter with following signals:
   * GND
   * RX (receive data)
   * TX (transmit data)
   * RTS (ready to send)
   * DTR (data terminal ready)
 * On PCB you can find "programming" label which identify programming
   connector. Connect above mentioned lines to converter. Do not forget about
   swapping RX and TX signals. And of course plug USB converter into your PC.
 * Power on device.
 * From same console, as you used for building firmware, type following
   command:

## Traditional way
```bash
fw $ idf.py flash
# Or if you need to specify UART port
fw $ idf.py flash -p /dev/ttyUSBx
```

## Docker way
```bash
fw $ ./scripts/flash_device.sh
# Or if you need to specify UART port
fw $ ./scripts/flash_device.sh /dev/ttyUSBx
```

 * If everything went well, device should be ready. Otherwise try to define
   com port and try again.

# First power on
 * Right after finishing flashing device, device should be restarted. During
   power on, device should test LCD display (show logo), test buzzer (short
   beep), test all colors on RGB LED and test gauge (0 to max and back).
 * By simply powering on device you can easily diagnose if something does not
   work as expected.

# What next
 * [Device calibration](calibration.md)
