# Getting into service mode
 * Use "Service UI" UART as base interface. Locate "Service UI" silk on PCB.
 * Connect USB UART convertor to your PC and this service connector. Mind
   that RX and TX lines represent data direction on given device -> you need
   to use twisted cable (swap RX and TX line).
 * Turn on device
 * Login request should appear. Now you need to input correct password. You
   can find default password in `fw/components/ui_service/ui_service.h`
   as `UI_SERVICE_DEFAULT_PASSWORD`.
 * Once you will "log in", the help will be shown.
 * Now the device is switched to "service mode" and all main tasks are
   postponed.
 * Device is ready to accept commands.
 * You can exit from service mode by typing `exit` command or power off, power
   on device.
