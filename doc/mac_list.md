# Precondition
 * [Get into service mode](ui_service.md)

# Get current MAC list
 * Run command `get mac list`
 * This will print out every single registered MAC

# Add MAC to list
 * Run command `set mac 11:22:33:44:55:66`. In this example is used MAC
   "11:22:33:44:55:66"

# Delete MAC from list
 * Run command `del mac 11:22:33:44:55:66`. in this example MAC
   "11:22:33:44:55:66" will be deleted.
