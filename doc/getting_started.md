# Introduction
 * After power on device start working in detector mode
 * If you want to go to the menu, just press "left" button at any time. In
   general the "left" button means "back".
 * The "right" or "center" button means "enter".
 * Buttons "up" and "down" means up and down ;)

# Detector mode
 * Scanning is signalized by blinking blue LED
 * If LED not blinking, there is some problem and device should be powered off
   and on again.
 * If "anomaly" is detected, device show current signal strength, average
   signal strength and warning level (for example !1).
 * Signal strength is typically from -20 to -90
 * Warning level is from 1 to 4. Value 1 ~ 3 is 1st ~ 3rd warning.
   Value 4 means certain death.

# Compass (optional)
 * Optional feature. Requires GY-91 sensor module.
 * LED features:
   * Green - device is in horizontal position. Compass is accurate
   * Yellow - device is not in horizontal position. Compass is not accurate
   * Additive Blue - strong magnetic field. Compass does not work as compass
     but it can help detect strong magnetic field. Typically magnets or big
     ferromagnetic materials.
 * On the top is azimuth in degrees
 * On the left side is magnetic field bar. Bigger bar, higher magnetic
   intensity. Every bar represents one axe (X, Y and Z).

# Battery
 * Show battery status (voltage and estimated percentage).
 * On next line is displayed time resolution for graph below.
 * By using "up" and "down" button you can zoom in/out and check battery
   characteristics in detail.
 * Device is storing it's own status time to time, but due to limited memory
   it can not monitor battery every minute. So graph below does not have to be
   100% accurate. Nevertheless voltage information is accurate and updated.

# Flashlight
 * By using "up" and "down" button you can increase and decrease intensity.
 * On display you can see LED intensity level.

# Settings
 * User settings. All advanced settings are hidden from user from this menu ;)
